package com.cloo.android;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;

import com.cloo.android.main.model.User;
import com.cloo.android.main.ui.compose.profile.ProfileEditActivity;
import com.cloo.android.main.ui.compose.user.UserActivity;
import com.cloo.android.main.util.AndroidUtils;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

public class ProfileEditActivityTest extends ClooSignedInTest {
    @Rule
    public final ActivityTestRule<UserActivity> userActivityRule = new ActivityTestRule<>(UserActivity.class, true, false);
    @Rule
    public final ActivityTestRule<ProfileEditActivity> profileEditActivityActivityTestRule = new ActivityTestRule<>(ProfileEditActivity.class, true, false);
    @Nullable private User user = null;

    @Before
    @Override
    public void setup() {
        super.setup();

        final CountDownLatch receivedPost = new CountDownLatch(1);
        user = null;
        getApplication().getDatabase().getUserDetailsNode(getApplication().getAuthentication().getSession().getUserId())
                .receive(User.class, (k, o) -> {
                    o.setUserId(k);
                    user = o;
                    receivedPost.countDown();
                })
                .submit();
        try {
            receivedPost.await(10, TimeUnit.SECONDS);
        } catch (@NonNull final InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void changeUsernameTest() {
        ProfileEditActivity activity = profileEditActivityActivityTestRule.launchActivity(new Intent(getApplication(), ProfileEditActivity.class));

        onView(hasDescendant(withText(user.getUsername())));
        onView(hasDescendant(withText(user.getDescription())));

        final String oldUsername = user.getUsername();

        // Test username change
        final String newUsername = "testUsername";
        IdlingResource idlingResource = startTiming(5000);
        onView(withId(R.id.user_profile_username_edit)).perform(ViewActions.clearText());
        onView(withId(R.id.user_profile_username_edit)).perform(ViewActions.typeText(newUsername));
        AndroidUtils.hideKeyboard(activity);
        onView(withId(R.id.user_profile_save)).perform(ViewActions.click());
        stopTiming(idlingResource);

        userActivityRule.launchActivity(UserActivity.newIntent(getApplication(), user.getUserId()));

        onView(hasDescendant(withText(newUsername)));

        activity = profileEditActivityActivityTestRule.launchActivity(new Intent(getApplication(), ProfileEditActivity.class));
        onView(hasDescendant(withText(newUsername)));

        // Restore old username
        idlingResource = startTiming(3000);
        onView(withId(R.id.user_profile_username_edit)).perform(ViewActions.clearText());
        onView(withId(R.id.user_profile_username_edit)).perform(ViewActions.typeText(oldUsername));
        AndroidUtils.hideKeyboard(activity);
        onView(withId(R.id.user_profile_save)).perform(ViewActions.click());
        stopTiming(idlingResource);

        userActivityRule.launchActivity(UserActivity.newIntent(getApplication(), user.getUserId()));

        onView(hasDescendant(withText(oldUsername)));
    }
}
