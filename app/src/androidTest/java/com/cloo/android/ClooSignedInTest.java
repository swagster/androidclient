package com.cloo.android;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingResource;
import android.support.test.rule.ActivityTestRule;
import android.util.Log;

import com.cloo.android.main.application.ClooApplication;
import com.cloo.android.main.application.MainActivity;
import com.cloo.android.main.backend.authentication.Authentication;
import com.cloo.android.main.backend.authentication.FirebaseAuthentication;
import com.cloo.android.main.model.EmailPasswordCredential;

import org.junit.Rule;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import timber.log.Timber;

import static org.junit.Assert.assertTrue;

public class ClooSignedInTest {
    @Rule
    public final ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class, true, false);

    @Nullable private ClooApplication application = null;

    @Nullable
    ClooApplication getApplication() {
        return application;
    }

    @NonNull
    IdlingResource startTiming(final long time) {
        final IdlingResource idlingResource = new ElapsedTimeIdlingResource(time);
        Espresso.registerIdlingResources(idlingResource);
        return idlingResource;
    }

    void stopTiming(final IdlingResource idlingResource) {
        Espresso.unregisterIdlingResources(idlingResource);
    }

    void setup() {
        final CountDownLatch signIn = new CountDownLatch(1);
        final EmailPasswordCredential credentials = new EmailPasswordCredential();
        credentials.email.set("kromm.steffen@gmail.com");
        credentials.password.set("passwort");

        final Authentication authentication = new FirebaseAuthentication();
        authentication.addLoginChangeListener(loggedIn -> {
            if (loggedIn)
                signIn.countDown();
        });
        authentication.signIn(credentials, null, error -> Log.e("Setup", "error on signIn", error));
        try {
            signIn.await(20, TimeUnit.SECONDS);
        } catch (@NonNull final InterruptedException e) {
            Timber.e("Interrupted test", e);
        }

        final Activity activity = activityRule.launchActivity(new Intent(InstrumentationRegistry.getContext(), MainActivity.class));
        application = (ClooApplication) activity.getApplication();
        assertTrue(!application.getAuthentication().getSession().getUserId().isEmpty());
    }
}
