package com.cloo.android;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.cloo.android.main.model.Post;
import com.cloo.android.main.model.PostOption;
import com.cloo.android.main.ui.compose.post.PostActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isClickable;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withTagValue;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class PostActivityTest extends ClooSignedInTest {
    @Rule
    public final ActivityTestRule<PostActivity> postActivityRule = new ActivityTestRule<>(PostActivity.class, true, false);
    @Nullable private Post post = null;

    @Before
    @Override
    public void setup() {
        super.setup();

        final CountDownLatch receivedPost = new CountDownLatch(1);
        post = null;
        getApplication().getDatabase().getAllPostsNode().limitToLast(1).get(Object.class, (k, o) -> getApplication().getDatabase().getPostDetailsNode(k)
                .receive(Post.class, (id, post) -> {
                    post.setKey(id);
                    this.post = post;
                    receivedPost.countDown();
                })
                .submit(), error -> {
        });
        try {
            receivedPost.await(10, TimeUnit.SECONDS);
        } catch (@NonNull final InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testPostVoting() {
        final PostActivity postActivity = postActivityRule.launchActivity(PostActivity.newIntent(InstrumentationRegistry.getContext(), post.getKey()));

        // Vote first option
        IdlingResource idlingResource = startTiming(15000);
        onView(withTagValue(is(post.getOptions().get(0).getUri()))).perform(ViewActions.doubleClick());
        stopTiming(idlingResource);

        assertTrue(postActivity.getPost().selectedOption.get() == 0);

        // Vote seconds option
        idlingResource = startTiming(15000);
        onView(withTagValue(is(post.getOptions().get(1).getUri()))).perform(ViewActions.doubleClick());
        stopTiming(idlingResource);

        assertTrue(postActivity.getPost().selectedOption.get() == 1);

        // Vote first option again to be sure it was no coincidence
        idlingResource = startTiming(15000);
        onView(withTagValue(is(post.getOptions().get(0).getUri()))).perform(ViewActions.doubleClick());
        stopTiming(idlingResource);

        assertTrue(postActivity.getPost().selectedOption.get() == 0);
    }

    @Test
    public void testPostDetails() {
        final PostActivity postActivity = postActivityRule.launchActivity(PostActivity.newIntent(InstrumentationRegistry.getContext(), post.getKey()));
        assertTrue(postActivity.getPostId().equals(post.getKey()));

        // title, description and voteCount must be displayed somewhere
        onView(hasDescendant(withText(post.getTitle())));
        onView(hasDescendant(withText(post.getDescription())));

        // each option image must be visible
        for (final PostOption option : post.getOptions()) {
            onView(hasDescendant(withTagValue(is(option.getUri()))));
        }
    }

    @Test
    public void testTitle() {
        final PostActivity postActivity = postActivityRule.launchActivity(PostActivity.newIntent(InstrumentationRegistry.getContext(), post.getKey()));
        assertTrue(postActivity.getTitle().equals(postActivity.getString(R.string.title_post_details)));
    }

    @Test
    public void testCommentSend() {
        postActivityRule.launchActivity(PostActivity.newIntent(InstrumentationRegistry.getContext(), post.getKey()));

        // Switch to comments tab
        IdlingResource idlingResource = startTiming(2000);
        onView(withText(R.string.title_post_comments)).perform(ViewActions.click());
        stopTiming(idlingResource);

        // Enter comment field should be empty on start and therefore not clickable
        onView(withId(R.id.enter_comment)).check(matches(withText("")));
        onView(withId(R.id.submit_comment)).check(matches(not(isClickable())));
        onView(withId(R.id.comment_sending_indicator)).check(matches(not(isDisplayed())));
        // Enter a random string
        final String testString = "Test@" + System.currentTimeMillis();
        onView(withId(R.id.enter_comment)).perform(ViewActions.typeText(testString));
        onView(withId(R.id.submit_comment)).check(matches(isClickable()));

        // Submit comment
        idlingResource = startTiming(4000);
        onView(withId(R.id.submit_comment)).perform(ViewActions.click());
        stopTiming(idlingResource);

        // Comment was successful submitted
        onView(hasDescendant(withText(testString)));

        // Enter comment field clears after successful comment
        onView(withId(R.id.enter_comment)).check(matches(withText("")));
        onView(withId(R.id.submit_comment)).check(matches(not(isClickable())));
        onView(withId(R.id.comment_sending_indicator)).check(matches(not(isDisplayed())));
    }
}
