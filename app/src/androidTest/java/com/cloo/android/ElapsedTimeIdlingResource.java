package com.cloo.android;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.test.espresso.IdlingResource;

class ElapsedTimeIdlingResource implements IdlingResource {
    private final long startTime;
    private final long waitingTime;
    @Nullable
    private IdlingResource.ResourceCallback resourceCallback = null;

    public ElapsedTimeIdlingResource(final long waitingTime) {
        startTime = System.currentTimeMillis();
        this.waitingTime = waitingTime;
    }

    @NonNull
    @Override
    public String getName() {
        return ElapsedTimeIdlingResource.class.getName() + ":" + waitingTime;
    }

    @Override
    public boolean isIdleNow() {
        final long elapsed = System.currentTimeMillis() - startTime;
        final boolean idle = (elapsed >= waitingTime);
        if (idle) {
            resourceCallback.onTransitionToIdle();
        }
        return idle;
    }

    @Override
    public void registerIdleTransitionCallback(final ResourceCallback resourceCallback) {
        this.resourceCallback = resourceCallback;
    }
}