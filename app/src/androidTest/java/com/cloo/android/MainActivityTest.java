package com.cloo.android;

import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.cloo.android.main.application.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public final IntentsTestRule<MainActivity> mainActvity = new IntentsTestRule<>(MainActivity.class);

    @Test
    public final void testMainActivityStart() {
        mainActvity.getActivity();
        onView(hasDescendant(withId(R.id.toolbar)));
        onView(hasDescendant(withId(R.id.tabs)));
    }

    @Test
    public final void testProfileStart() {
   //     onView(withId(R.id.menu_profil)).perform(ViewActions.click());
   //     intended(hasComponent(new ComponentName(getTargetContext(), Notification.class)));
    }
}
