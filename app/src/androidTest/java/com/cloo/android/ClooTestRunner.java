package com.cloo.android;

import android.app.Application;
import android.content.Context;
import android.support.test.runner.AndroidJUnitRunner;

public class ClooTestRunner extends AndroidJUnitRunner {

    @Override
    public Application newApplication(final ClassLoader cl, final String className, final Context context) throws InstantiationException,
            IllegalAccessException, ClassNotFoundException {
        final String testApplicationClassName = ClooTestApplication.class.getCanonicalName();
        return super.newApplication(cl, testApplicationClassName, context);
    }
}