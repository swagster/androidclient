package com.cloo.android;

import android.content.ComponentName;
import android.support.annotation.NonNull;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.cloo.android.main.backend.authentication.FirebaseAuthentication;
import com.cloo.android.main.ui.compose.login.LoginActivity;
import com.cloo.android.main.ui.compose.register.RegisterActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getTargetContext;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.not;

@RunWith(AndroidJUnit4.class)
public class LoginActivityTest {
    @Rule
    public final IntentsTestRule<LoginActivity> loginActvity = new IntentsTestRule<>(LoginActivity.class);

    @NonNull
    public IdlingResource startTiming(final long time) {
        final IdlingResource idlingResource = new ElapsedTimeIdlingResource(time);
        Espresso.registerIdlingResources(idlingResource);
        return idlingResource;
    }

    public void stopTiming(final IdlingResource idlingResource) {
        Espresso.unregisterIdlingResources(idlingResource);
    }

    @Before
    public final void setup() {
        new FirebaseAuthentication().signOut();
    }

    @Test
    public final void testLoadingIndication() {
        onView(withId(R.id.display)).check(matches(isDisplayed()));
        onView(withId(R.id.loading_spinner)).check(matches(not(isDisplayed())));
        IdlingResource idlingResource = startTiming(3000);
        loginActvity.getActivity().startLoading();
        stopTiming(idlingResource);
        //onView(withId(R.id.display)).check(matches(not(isDisplayed())));
//        onView(withId(R.id.loading_spinner)).check(matches(isDisplayed()));
        idlingResource = startTiming(3000);
        loginActvity.getActivity().endLoading();
        stopTiming(idlingResource);
        //onView(withId(R.id.display)).check(matches(isDisplayed()));
        //onView(withId(R.id.loading_spinner)).check(matches(not(isDisplayed())));
    }

    @Test
    public final void testEmailPasswordInput() {
        loginActvity.getActivity();
        onView(hasDescendant(withId(R.id.login_username)));
        onView(hasDescendant(withId(R.id.login_login)));
        onView(hasDescendant(withId(R.id.register)));
    }

    @Test
    public final void testPasswordResetStart() {
        // onView(withId(R.id.forgot_password)).perform(ViewActions.click());
        // intended(hasComponent(new ComponentName(getTargetContext(), ResetPasswordActivity.class)));
    }

    @Test
    public final void testRegisterStart() {
        onView(withId(R.id.register_wrapper)).perform(ViewActions.click());
        intended(hasComponent(new ComponentName(getTargetContext(), RegisterActivity.class)));
    }
}