package com.cloo.android;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;

import com.cloo.android.main.model.User;
import com.cloo.android.main.ui.compose.user.UserActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withTagValue;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.Matchers.is;

public class UserActivityTest extends ClooSignedInTest {

    @Rule
    public final ActivityTestRule<UserActivity> userActivityRule = new ActivityTestRule<>(UserActivity.class, true, false);
    @Nullable private User user = null;

    @Before
    @Override
    public void setup() {
        super.setup();

        final CountDownLatch receivedPost = new CountDownLatch(1);
        user = null;
        getApplication().getDatabase().getUserDetailsNode(getApplication().getAuthentication().getSession().getUserId())
                .receive(User.class, (k, o) -> {
                    o.setUserId(k);
                    user = o;
                    receivedPost.countDown();
                })
                .recycleAfterUse()
                .submit();
        try {
            receivedPost.await(10, TimeUnit.SECONDS);
        } catch (@NonNull final InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testUserDetails() {
        final UserActivity activity = userActivityRule.launchActivity(UserActivity.newIntent(InstrumentationRegistry.getContext(), user.getUserId()));
        assertTrue(activity.getTitle().equals(activity.getString(R.string.title_user_profil_you)));

        onView(hasDescendant(withText(user.getUsername())));
        onView(hasDescendant(withTagValue(is(user.getProfileImageUrl()))));
    }
}
