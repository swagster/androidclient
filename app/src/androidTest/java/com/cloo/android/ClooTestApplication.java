package com.cloo.android;

import android.util.Log;

import com.cloo.android.main.application.ClooApplication;

public class ClooTestApplication extends ClooApplication {

    @Override
    public final void onCreate() {
        super.onCreate();
        Log.d(ClooTestApplication.class.getSimpleName(), "onCreate:: Test Application Instance.");
    }
}
