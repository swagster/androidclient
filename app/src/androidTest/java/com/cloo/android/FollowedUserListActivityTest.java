package com.cloo.android;

import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;

import com.cloo.android.main.ui.compose.user.FollowerListActivity;
import com.cloo.android.main.ui.compose.user.FollowsListActivity;

import org.junit.Rule;
import org.junit.Test;

public class FollowedUserListActivityTest extends ClooSignedInTest {
    @Rule
    public final ActivityTestRule<FollowerListActivity> followerListActivityActivityTestRule = new ActivityTestRule<>(FollowerListActivity.class, true, false);

    @Test
    public void testActivityStart() {
        followerListActivityActivityTestRule.launchActivity(FollowsListActivity.newIntent(InstrumentationRegistry.getContext(), getApplication().getAuthentication().getSession().getUserId()));
    }
}
