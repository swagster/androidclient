package com.cloo.android.main.ui.compose.user;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentUserDetailsBinding;
import com.cloo.android.main.application.ClooApplication;
import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.backend.database.DatabaseNode;
import com.cloo.android.main.model.User;
import com.cloo.android.main.service.business.follow.FirebaseFunctionFollowService;
import com.cloo.android.main.service.business.follow.FollowService;
import com.cloo.android.main.service.business.follow.FollowsListener;
import com.cloo.android.main.ui.base.BaseActivity;
import com.cloo.android.main.ui.base.BaseFragment;
import com.cloo.android.main.ui.common.FullscreenImageActivity;
import com.cloo.android.main.ui.compose.profile.ProfileEditActivity;
import com.cloo.android.main.util.AndroidUtils;

import timber.log.Timber;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class UserDetailsFragment extends BaseFragment
{
    private static final String USER_ID = "userId";

    @Nullable private DatabaseNode isOnline = null;
    @Nullable private FragmentUserDetailsBinding binding = null;
    @Nullable private FollowService followService = null;
    @Nullable private ClooApplication application = null;
    @Nullable private DatabaseNode userNode = null;

    @NonNull
    public static UserDetailsFragment newInstance(@NonNull final String userId)
    {
        final Bundle bundle = new Bundle();
        bundle.putString(UserDetailsFragment.USER_ID, userId);
        final UserDetailsFragment fragment = new UserDetailsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if (isOnline != null) { isOnline.recycle(); }
        if (userNode != null) {
            userNode.recycle();
        }
    }

    @NonNull
    private String getUserId()
    {
        return getArguments().getString(UserDetailsFragment.USER_ID,
                                        application.getAuthentication().getSession().getUserId());
    }

    @Nullable
    @Override
    public final View onCreateView(@NonNull final LayoutInflater inflater,
                                   @Nullable final ViewGroup container,
                                   @Nullable final Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_details, container,
                                          false);
        application = (ClooApplication) getActivity().getApplication();
        return binding.getRoot();
    }

    private void setStatusMessage(final long timeSinceLastOnline)
    {
        Timber.d("TimeSince Last Online :" + timeSinceLastOnline);
        if (isDetached()) { return; }
        if (timeSinceLastOnline == 0) {
            binding.isOnlineStatusMessage.setText("\uD83D\uDE04");
        }
        else if (timeSinceLastOnline <= 1000 * 60 * 60) {
            binding.isOnlineStatusMessage.setText(Math.random() > 0.5f ? "☺" : "\uD83D\uDE0A");

        }
        else if (timeSinceLastOnline <= 1000 * 60 * 60 * 12) {
            binding.isOnlineStatusMessage.setText("\uD83D\uDE09");

        }
        else if (timeSinceLastOnline <= 1000 * 60 * 60 * 24) {
            binding.isOnlineStatusMessage.setText(
                    Math.random() > 0.5f ? "\uD83D\uDE25" : "\uD83D\uDE35");

        }
        else if (timeSinceLastOnline <= 1000 * 60 * 60 * 24 * 7) {
            binding.isOnlineStatusMessage.setText("\uD83D\uDE34");

        }
        else {
            binding.isOnlineStatusMessage.setText("\uD83D\uDE2A");
        }
    }

    private void onUserOnlineChange(String userId, @Nullable Object isOnline)
    {
        if (isOnline != null) {
            setStatusMessage(0);
        }
        else {
            getParentActivity().getDatabase()
                               .getLastOnline(getUserId())
                               .receive(Long.class, (s, time) -> setStatusMessage(
                                       System.currentTimeMillis() - (time == null ? 0 : time)))
                               .recycleAfterUse()
                               .submit();
        }
    }

    @Override
    public final void onViewCreated(@NonNull final View view,
                                    @Nullable final Bundle savedInstanceState)
    {
        final BaseActivity activity = (BaseActivity) getActivity();
        isOnline = activity.getDatabase().getOnlineNode(getUserId());
        isOnline.onUpdates(Object.class, this::onUserOnlineChange).submit();
        final DatabaseAdapter database = (activity).getDatabase();
        followService = new FirebaseFunctionFollowService(database);
        binding.userDetailsFollower.setOnClickListener(this::showFollowers);
        binding.userDetailsFollows.setOnClickListener(this::showFollows);
        binding.profileImage.profilImage.setOnClickListener(this::onProfileImageClick);
        userNode = application.getDatabase().getUserDetailsNode(getUserId());
        userNode.onUpdates(User.class, this::onUserInformationUpdate).submit();

        AndroidUtils.setImageDrawable(binding.profileImage.profilImage, getContext(),
                                      R.drawable.v_icon_user_picture_placeholder);
        if (!application.getAuthentication().getSession().getUserId().equals(getUserId())) {
            setupFollowButton();
        }
        else {
            AndroidUtils.setImageDrawable(binding.userDetailsPromotedAction, getActivity(),
                                          R.drawable.v_icon_edit_profile);
            binding.userDetailsPromotedAction.setOnClickListener(this::onEditProfileClick);
        }
    }

    private void setupFollowButton()
    {
        final FollowsListener followsListener = new FollowsListener(application.getDatabase(),
                                                                    application.getAuthentication()
                                                                               .getSession()
                                                                               .getUserId(),
                                                                    getUserId());
        followsListener.setFollowChangeCallback(this::onFollowStateChange);
        binding.userDetailsPromotedAction.setVisibility(GONE);
        binding.userDetailsPromotedAction.setOnClickListener(this::onFollowUserClick);
        followService.isFollower(application.getAuthentication().getSession().getUserId(),
                                 getUserId(), isFollower -> {
                    if (canAccessUI() && !isFollower) {
                        binding.userDetailsPromotedAction.setVisibility(View.VISIBLE);
                        AndroidUtils.setImageDrawable(binding.userDetailsPromotedAction,
                                                      getActivity(), R.drawable.v_icon_followed);
                    }
                });
    }

    private void onFollowStateChange(boolean follows)
    {
        if (canAccessUI()) {
            binding.loadingSpinnerFollow.setVisibility(View.INVISIBLE);
            if (follows) {
                binding.userDetailsPromotedAction.hide();
            }
            else {
                AndroidUtils.setImageDrawable(binding.userDetailsPromotedAction, getActivity(),
                                              R.drawable.v_icon_followed);
                binding.userDetailsPromotedAction.show();
            }
        }
    }

    private void onUserInformationUpdate(@NonNull final String k, @NonNull final User o)
    {
        if (isDetached()) { return; }
        o.setUserId(k);
        binding.setUser(o);
        if (o.getDescription() == null || o.getDescription().isEmpty()) {
            binding.descriptionContainer.setVisibility(GONE);
        }
        if (!o.getProfileImageUrl().isEmpty()) {
            binding.setProfileImageUrl(o.getProfileImageUrl());
        }
        binding.notifyChange();
        binding.loadingSpinner.setVisibility(GONE);
        binding.content.setVisibility(VISIBLE);
    }

    private void onFollowUserClick(final View view)
    {
        binding.userDetailsPromotedAction.hide();
        binding.loadingSpinnerFollow.setVisibility(View.VISIBLE);
        followService.follow(application.getAuthentication().getSession().getUserId(), getUserId(),
                             success -> {
                                 if (!success) { binding.userDetailsPromotedAction.show(); }
                             });
    }

    private void onEditProfileClick(final View view)
    {
        final Intent intent = new Intent(getActivity(), ProfileEditActivity.class);
        intent.putExtra("profileImage", R.id.profile_image);
        final ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                getActivity(), binding.profileImage.profilImage, "profileImage");
        getActivity().startActivity(intent, options.toBundle());
    }

    private void onProfileImageClick(final View view)
    {
        if (binding != null && binding.getUser() != null) {
            final Intent intent = FullscreenImageActivity.newInstance(getActivity(),
                                                                      binding.getUser()
                                                                             .getProfileImageUrl());
            intent.putExtra("profileImage", R.id.profile_image);
            final ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    getActivity(), binding.profileImage.profilImage, "profileImage");
            startActivity(intent, options.toBundle());
        }
    }

    private void showFollows(@NonNull final View view)
    {
        final Intent intent = FollowsListActivity.newIntent(getActivity(), getUserId());
        startActivity(intent);
    }

    private void showFollowers(@NonNull final View view)
    {
        final Intent intent = FollowerListActivity.newIntent(getActivity(), getUserId());
        startActivity(intent);
    }
}
