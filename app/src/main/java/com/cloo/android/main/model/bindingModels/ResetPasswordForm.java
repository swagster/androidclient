package com.cloo.android.main.model.bindingModels;

import android.databinding.ObservableField;

public class ResetPasswordForm
{
    public final ObservableField<String> email = new ObservableField<>("");
}
