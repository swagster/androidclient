package com.cloo.android.main.injection.component;

import com.cloo.android.main.service.business.comment.CommentService;
import com.cloo.android.main.injection.module.CommentServiceModule;

import dagger.Component;

@FunctionalInterface
@Component(modules = {CommentServiceModule.class})
public interface CommentListComponent {
    CommentService getCommentService();
}
