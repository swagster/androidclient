package com.cloo.android.main.backend.database;

import android.support.annotation.NonNull;
import android.util.Log;

import com.cloo.android.main.function.DatabaseReceiveCallback;
import com.cloo.android.main.function.ExecutionCallback;
import com.cloo.android.main.function.ReceiveCallback;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class DatabaseQuery implements DatabaseReference
{
    private static final String TAG = DatabaseQuery.class.getSimpleName();
    private final Query query;

    public DatabaseQuery(final Query query)
    {
        this.query = query;
    }

    @Override
    public final void keepSynced(final boolean b)
    {
        query.keepSynced(b);
    }

    @Override
    public void removeListeners()
    {

    }

    @Override
    public final void recycle()
    {

    }

    public final <T> void get(final Class<T> type,
                              @NonNull final DatabaseReceiveCallback<T> listener,
                              @NonNull final ReceiveCallback<Exception> errorListener,
                              @NonNull final ExecutionCallback finishCallback)
    {
        query.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot)
            {
                if (dataSnapshot.hasChildren()) {
                    Log.d(TAG, "Query returned " + dataSnapshot.getChildrenCount() + " elements");
                    for (final DataSnapshot child : dataSnapshot.getChildren()) {
                        try {
                            listener.onReceive(child.getKey(), child.getValue(type));
                        } catch (@NonNull final DatabaseException exception) {
                            Log.e(DatabaseQuery.TAG, "Unexpected database error", exception);
                        }
                    }
                }
                else {
                    listener.onReceive(dataSnapshot.getKey(), dataSnapshot.getValue(type));
                }
                finishCallback.execute();
            }

            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError)
            {
                errorListener.onReceive(databaseError.toException());
                finishCallback.execute();
            }
        });
    }

    public final <T> void get(final Class<T> type,
                              @NonNull final DatabaseReceiveCallback<T> listener,
                              @NonNull final ReceiveCallback<Exception> errorListener)
    {
        get(type, listener, errorListener, () -> {
        });
    }

    @SuppressWarnings("AnonymousInnerClassWithTooManyMethods")
    @Override
    public final <T> void addChildListener(final Class<T> type,
                                           @NonNull final DatabaseReceiveCallback<T> addListener,
                                           @NonNull final DatabaseReceiveCallback<T> changeListener,
                                           @NonNull final DatabaseReceiveCallback<T> movedListener,
                                           @NonNull final DatabaseReceiveCallback<T> removeListener,
                                           @NonNull final ReceiveCallback<Exception> errorListener)
    {
        query.addChildEventListener(new ChildEventListener()
        {
            @Override
            public void onChildAdded(@NonNull final DataSnapshot dataSnapshot,
                                     final String previousChildName)
            {
                try {
                    addListener.onReceive(dataSnapshot.getKey(), dataSnapshot.getValue(type));
                } catch (@NonNull final DatabaseException exception) {
                    Log.e(DatabaseQuery.TAG, "Unexpected database error", exception);
                }
            }

            @Override
            public void onChildChanged(@NonNull final DataSnapshot dataSnapshot,
                                       final String previousChildName)
            {
                try {
                    changeListener.onReceive(dataSnapshot.getKey(), dataSnapshot.getValue(type));
                } catch (@NonNull final DatabaseException exception) {
                    Log.e(DatabaseQuery.TAG, "Unexpected database error", exception);
                }
            }


            @Override
            public void onChildRemoved(@NonNull final DataSnapshot dataSnapshot)
            {
                try {
                    removeListener.onReceive(dataSnapshot.getKey(), dataSnapshot.getValue(type));
                } catch (@NonNull final DatabaseException exception) {
                    Log.e(DatabaseQuery.TAG, "Unexpected database error", exception);
                }
            }

            @Override
            public void onChildMoved(@NonNull final DataSnapshot dataSnapshot,
                                     final String previousChildName)
            {
                try {
                    movedListener.onReceive(dataSnapshot.getKey(), dataSnapshot.getValue(type));
                } catch (@NonNull final DatabaseException exception) {
                    Log.e(DatabaseQuery.TAG, "Unexpected database error", exception);
                }
            }

            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError)
            {
                errorListener.onReceive(databaseError.toException());
            }
        });
    }

    @NonNull
    @Override
    public final DatabaseQuery limitToFirst(final int currentLimit)
    {
        return new DatabaseQuery(query.limitToFirst(currentLimit));
    }

    @Override
    @NonNull
    public final DatabaseQuery limitToLast(final int count)
    {
        return new DatabaseQuery(query.limitToLast(count));
    }

    @NonNull
    @Override
    public final DatabaseQuery orderBy(@NonNull final String field)
    {
        return new DatabaseQuery(query.orderByChild(field));
    }

    @NonNull
    public final DatabaseQuery endAt(final String since)
    {
        return new DatabaseQuery(query.orderByKey().endAt(since));
    }
}
