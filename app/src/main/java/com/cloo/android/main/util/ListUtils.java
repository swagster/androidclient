package com.cloo.android.main.util;

import android.support.annotation.NonNull;

import com.cloo.android.main.function.BooleanFunction;

import java.util.Collection;
import java.util.List;

public final class ListUtils
{
    private ListUtils()
    {
        // Utility class
    }

    public static <T> boolean isValidIndex(final int index, @NonNull final Collection<T> list)
    {
        return index >= 0 && list.size() > index;
    }

    public static <T> boolean removeElement(@NonNull final BooleanFunction<T> equalsCheck,
                                            @NonNull final List<T> list)
    {
        int removedIndex = -1;
        for (int i = 0; i < list.size(); i++) {
            if (equalsCheck.calculate(list.get(i))) {
                removedIndex = i;
                break;
            }
        }
        if (removedIndex != -1) {
            list.remove(removedIndex);
            return true;
        }
        return false;
    }
}
