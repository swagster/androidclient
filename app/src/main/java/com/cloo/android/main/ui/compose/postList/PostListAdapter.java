package com.cloo.android.main.ui.compose.postList;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentListElementPostBinding;
import com.cloo.android.main.application.ClooApplication;
import com.cloo.android.main.backend.database.DatabaseNode;
import com.cloo.android.main.model.Post;
import com.cloo.android.main.model.User;
import com.cloo.android.main.ui.base.BaseActivity;
import com.cloo.android.main.ui.base.DatabaseQueryListAdapter;
import com.cloo.android.main.ui.common.ShareDialog;
import com.cloo.android.main.ui.compose.post.PostActivity;
import com.cloo.android.main.ui.compose.post.PostOptionsAdapter;
import com.cloo.android.main.ui.compose.user.UserActivity;
import com.cloo.android.main.util.AndroidUtils;
import com.cloo.android.main.util.ListUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.cloo.android.main.util.ScreenUtils.getScreenWidth;
import static com.cloo.android.main.util.ScreenUtils.toPixels;

public class PostListAdapter extends DatabaseQueryListAdapter<Post>
{
    protected final List<Post> posts = new ArrayList<>();
    protected final Comparator<Post> POST_COMPARATOR_TIMESTAMP = (postA, postB) -> postA.getTimestamp() > postB
            .getTimestamp() ? -1 : 1;
    final Handler handler = new Handler();
    @NonNull private final LayoutInflater inflater;
    @NonNull private final ClooApplication application;
    @NonNull private final BaseActivity activity;
    @NonNull Timer timer = new Timer();
    private int optionWidth = 0;

    public PostListAdapter(@NonNull final BaseActivity activity)
    {
        this.activity = activity;
        application = (ClooApplication) activity.getApplication();
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public final int getCount()
    {
        return posts.size();
    }

    @Nullable
    @Override
    public final Object getItem(final int i)
    {
        try {
            return posts.get(i);
        } catch (@NonNull final IndexOutOfBoundsException exception) {
            return null;
        }
    }

    @Override
    public final long getItemId(final int i)
    {
        return i;
    }

    @NonNull
    private FragmentListElementPostBinding getBinding(@Nullable final View recycledView,
                                                      @Nullable final ViewGroup viewGroup)
    {
        if (recycledView == null) {
            final FragmentListElementPostBinding binding = DataBindingUtil.inflate(inflater,
                                                                                   R.layout.fragment_list_element_post,
                                                                                   viewGroup,
                                                                                   false);
            if (optionWidth == 0) {
                optionWidth = getScreenWidth() / 2 - toPixels(16, activity.getResources()
                                                                          .getDisplayMetrics());
            }

            return binding;
        }
        else {
            return (FragmentListElementPostBinding) recycledView.getTag();
        }
    }

    @NonNull
    @Override
    public final View getView(final int i, @Nullable final View recycledView,
                              @Nullable final ViewGroup viewGroup)
    {
        final FragmentListElementPostBinding binding = getBinding(recycledView, viewGroup);
        binding.getRoot().setTag(binding);
        final Post post = posts.get(i);
        if (binding.getPost() == null || !binding.getPost().equals(post)) {
            if (binding.getPost() != null && !binding.getPost()
                                                     .getAuthor()
                                                     .equals(post.getAuthor())) {
                application.getImageLoader().clear(binding.profilImageWrapper.profilImage);
                AndroidUtils.setImageDrawable(binding.profilImageWrapper.profilImage, activity,
                                              R.drawable.v_icon_user_picture_placeholder);
            }
            binding.setPost(post);
            setupPostUpdates(binding, post, i);
            setupUser(binding, post, i);
            final PostOptionsAdapter adapter = binding.tag.getTag() == null ? new PostOptionsAdapter(
                    activity) : (PostOptionsAdapter) binding.tag.getTag();
            binding.tag.setTag(adapter);
            adapter.setPost(post);
            adapter.setOnClickListener((adapterView, view, i1, l) -> activity.startActivity(
                    PostActivity.newIntent(activity, post.getKey())));
            binding.postSelection.setAdapter(adapter);
            resizeOptionsGrid(binding.getPost(), binding.postSelection);
            setupShareButton(binding.postListElementShare, binding.getPost());
            binding.postVotesWrapper.postVotecount.setOnClickListener(v -> activity.startActivity(
                    PostActivity.newIntent(activity, post.getKey(),
                                           PostActivity.POST_STATE_VOTES)));
            DatabaseNode voteCountNode = (DatabaseNode) binding.postVotesWrapper.postVotecount.getTag();
            if (voteCountNode != null) {
                voteCountNode.recycle();
            }
            voteCountNode = activity.getDatabase().getVoteCountNode(post.getKey());
            voteCountNode.onUpdates(Long.class, (k, count) -> binding.setVoteCount(
                    count == null ? 0 : count.intValue())).submit();
            binding.getRoot().setOnClickListener(v -> {
                final Intent intent = PostActivity.newIntent(activity, post.getKey());
                activity.startActivity(intent);
            });
        }
        return binding.getRoot();
    }

    private void setupUser(@NonNull final FragmentListElementPostBinding binding,
                           @NonNull final Post post, final int index)
    {
        activity.getDatabase()
                .getUserDetailsNode(post.getAuthor())
                .receive(User.class, (id, user) -> {
                    if (user != null && post.getAuthor()
                                            .equals(binding.getPost()
                                                           .getAuthor()) && !activity.isFinishing()) {
                        user.setId(id);
                        binding.setUser(user);
                        setUser(binding, user);
                    }
                })
                .recycleAfterUse()
                .submit();
    }

    private void setupShareButton(@NonNull final View shareButton, @NonNull final Post post)
    {
        shareButton.setVisibility(post.getAuthor()
                                      .equals(application.getAuthentication()
                                                         .getSession()
                                                         .getUserId()) ? View.INVISIBLE : View.VISIBLE);
        shareButton.setOnClickListener(v -> {
            final FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
            ft.addToBackStack(null);
            ShareDialog.newInstance(post.getKey()).show(ft, post.getKey());
        });
    }

    private void setupPostUpdates(@NonNull final FragmentListElementPostBinding binding,
                                  @NonNull final Post oldPost, final int index)
    {
        DatabaseNode postDetailsReference = (DatabaseNode) binding.postSelection.getTag();
        if (postDetailsReference != null) {
            postDetailsReference.recycle();
        }
        postDetailsReference = activity.getDatabase().getPostDetailsNode(oldPost.getKey());
        postDetailsReference.onUpdates(Post.class,
                                       (k, o) -> listenToPostUpdates(binding, oldPost, k, o, index))
                            .submit();
        binding.postSelection.setTag(postDetailsReference);
    }

    protected void listenToPostUpdates(@NonNull final FragmentListElementPostBinding binding,
                                       @NonNull final Post oldPost, final String k,
                                       @Nullable final Post o, final int index)
    {
        if (!ListUtils.isValidIndex(index, posts)) { return;}
        if (o != null && o.isDeleted()) {
            posts.remove(index);
            return;
        }
        if (o == null || o.equals(oldPost)) {
            return;
        }

        o.setKey(k);
        o.selectedOption.set(oldPost.selectedOption.get());
        if (ListUtils.isValidIndex(index, posts)) {
            binding.setPost(o);
            posts.set(index, o);
            binding.notifyChange();
            ((PostOptionsAdapter) binding.postSelection.getAdapter()).setPost(o);
        }
    }

    private void resizeOptionsGrid(@NonNull final Post post, @NonNull final View optionsGrid)
    {
        if (optionsGrid.getWidth() != optionWidth || optionsGrid.getHeight() != (optionWidth * (1 + ((post
                .getOptions()
                .size() - 1) / 2)))) {
            optionsGrid.setLayoutParams(new LinearLayout.LayoutParams(optionWidth * 2,
                                                                      optionWidth * (1 + ((post.getOptions()
                                                                                               .size() - 1) / 2))));
            optionsGrid.invalidate();
        }
    }

    private void setUser(@NonNull final FragmentListElementPostBinding binding,
                         @NonNull final User user)
    {
        binding.setUser(user);
        binding.profilImageWrapper.profilImage.setOnClickListener(view -> {
            final Intent friendFeedIntent = UserActivity.newIntent(activity, user.getUserId());
            activity.startActivity(friendFeedIntent);
        });
    }

    protected void sort(@NonNull final Comparator<? super Post> comparator)
    {
        synchronized (posts) {
            Collections.sort(posts, comparator);
        }
    }

    @Override
    public final void addDataObject(@NonNull final String postId)
    {
        for (int i = 0; i < posts.size(); i++) {
            final Post oldPost = posts.get(i);
            if (oldPost.getKey().equals(postId)) {
                return;
            }
        }
        if (activity.getAuthentication().isLoggedIn()) {
            activity.getDatabase()
                    .getReportNode(activity.getAuthentication().getSession().getUserId(), postId)
                    .exists(exists -> {
                        if (!exists) {
                            activity.getDatabase()
                                    .getPostDetailsNode(postId)
                                    .receive(Post.class, (k, o) -> {
                                        if (o == null) {
                                            return;
                                        }
                                        o.setKey(k);
                                        addDataObject(o);
                                    })
                                    .recycleAfterUse()
                                    .submit();
                        }
                    })
                    .recycleAfterUse()
                    .submit();
        }
        else {
            activity.getDatabase().getPostDetailsNode(postId).receive(Post.class, (k, o) -> {
                if (o == null) {
                    return;
                }
                o.setKey(k);
                addDataObject(o);
            }).recycleAfterUse().submit();
        }
    }

    protected void addDataObject(@NonNull final Post post)
    {
        if (post.isDeleted()) { return; }
        for (int i = 0; i < posts.size(); i++) {
            final Post oldPost = posts.get(i);
            if (oldPost.getKey().equals(post.getKey())) {
                post.selectedOption.set(oldPost.selectedOption.get());
                posts.set(i, post);
                timer.cancel();
                timer.purge();
                timer = new Timer();
                timer.schedule(new TimerTask()
                {
                    @Override
                    public void run()
                    {
                        if (!activity.isDestroyed()) {
                            handler.post(() -> {
                                sort(POST_COMPARATOR_TIMESTAMP);
                                notifyDataSetChanged();
                            });
                        }
                    }
                }, 200);
                return;
            }
        }
        posts.add(post);
        timer.cancel();
        timer.purge();
        timer = new Timer();
        timer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                if (!activity.isDestroyed()) {
                    handler.post(() -> {
                        sort(POST_COMPARATOR_TIMESTAMP);
                        notifyDataSetChanged();
                    });
                }
            }
        }, 200);
    }

    @Override
    public final void removeDataObject(@NonNull final String postId)
    {
        if (ListUtils.removeElement(post -> postId.equals(post.getKey()), posts)) {
            notifyDataSetChanged();
        }
    }

    @Override
    public final Post getDataObject(final int index)
    {
        return posts.get(index);
    }

    @Override
    public void recycle()
    {

    }

    @NonNull
    @Override
    public String getLastKey()
    {
        return posts.isEmpty() ? "" : posts.get(posts.size() - 1).getKey();
    }
}
