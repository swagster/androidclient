package com.cloo.android.main.function;

@FunctionalInterface
public interface ExecutionCallback {
    void execute();
}
