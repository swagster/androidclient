package com.cloo.android.main.ui.compose.imageSelection;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentGalleryImageBinding;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.cloo.android.main.util.ScreenUtils.getScreenWidth;

class GalleryImageAdapter extends BaseAdapter
{
    @NonNull private final LayoutInflater inflater;
    @NonNull private final Activity activity;
    private final List<String> images = new ArrayList<>();

    public GalleryImageAdapter(@NonNull final Activity activity)
    {
        this.activity = activity;
        inflater = activity.getLayoutInflater();
    }

    public final void addAllImages(@NonNull final Collection<String> uris)
    {
        images.addAll(uris);
    }

    @Override
    public final int getCount()
    {
        return images.size();
    }

    @Override
    public final Object getItem(final int i)
    {
        return images.get(i);
    }

    @Override
    public final long getItemId(final int i)
    {
        return i / 3;
    }

    @NonNull
    private FragmentGalleryImageBinding getBinding(@Nullable final View recycledView,
                                                   @Nullable final ViewGroup viewGroup)
    {
        if (recycledView == null) {
            final FragmentGalleryImageBinding binding = DataBindingUtil.inflate(inflater,
                                                                                R.layout.fragment_gallery_image,
                                                                                viewGroup, false);
            binding.getRoot()
                   .setLayoutParams(
                           new ViewGroup.LayoutParams(getScreenWidth() / 3, getScreenWidth() / 3));
            return binding;
        }
        else {
            return (FragmentGalleryImageBinding) recycledView.getTag();
        }
    }

    @Override
    @NonNull
    public final View getView(final int index, @Nullable final View view,
                              @NonNull final ViewGroup viewGroup)
    {
        final FragmentGalleryImageBinding binding = getBinding(view, viewGroup);
        binding.getRoot().setTag(binding);
        final String imageUrl = images.get(images.size() - (index + 1));
        binding.setImageUrl(imageUrl);
        binding.setOnClick(v -> {
            final GalleryActivity galleryActivity = (GalleryActivity) activity;
            galleryActivity.selectImage(imageUrl);
        });
        return binding.getRoot();
    }
}
