package com.cloo.android.main.injection.component;

import com.cloo.android.main.injection.module.FollowServiceModule;
import com.cloo.android.main.service.business.follow.FollowService;

import dagger.Component;

@FunctionalInterface
@Component(modules = {FollowServiceModule.class})
public interface UserActivityComponent {
    FollowService getFollowService();
}
