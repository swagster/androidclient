package com.cloo.android.main.backend.database;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class DatabaseCodes
{
    /**
     * Signals, that the conditions need to execute an operation were not met.
     */
    public static final int OPERATION_INVALID = 0;
    /**
     * The operation could not be started because a valid internet operation was missing or aborted during the operation.
     */
    public static final int NETWORK_ERROR = 1;
    /**
     * The operation failed due to validation.
     */
    public static final int OPERATION_UNAVAILABLE = 2;
    /**
     * The operation failed due to missing permissions.
     */
    public static final int AUTHENTICATION_FAILED = 4;
    /**
     * The operation failed, because the server could not be reached.
     */
    public static final int SERVER_ERROR = 3;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({DatabaseCodes.AUTHENTICATION_FAILED, DatabaseCodes.OPERATION_INVALID, DatabaseCodes.OPERATION_UNAVAILABLE, DatabaseCodes.NETWORK_ERROR, DatabaseCodes.SERVER_ERROR})
    public @interface DatabaseOperationError
    {}

    @FunctionalInterface
    public interface DatabaseOperationErrorCallback
    {
        void onError(@DatabaseCodes.DatabaseOperationError int errorCode);
    }
}
