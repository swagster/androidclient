package com.cloo.android.main.service.business.camera;

import android.app.Activity;
import android.hardware.Camera;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.view.SurfaceHolder;

import com.cloo.android.main.function.ReceiveCallback;
import com.cloo.android.main.util.MathUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

@SuppressWarnings("deprecation")
public final class LegacyCamera implements ClooCamera
{
    private static final String TAG = LegacyCamera.class.getSimpleName();
    @NonNull private final Camera camera;

    private LegacyCamera(@NonNull final Camera camera)
    {
        this.camera = camera;
        camera.setDisplayOrientation(90);
    }

    @NonNull
    private static android.hardware.Camera.CameraInfo getCameraInfos(final int cameraId)
    {
        final android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        return info;
    }

    private static int getDegrees(final int rotation)
    {
        switch (rotation) {
            case Surface.ROTATION_0:
                return 0;
            case Surface.ROTATION_90:
                return 90;
            case Surface.ROTATION_180:
                return 180;
            case Surface.ROTATION_270:
                return 270;
            default:
                return 0;
        }
    }

    private static int getDisplayRotation(@NonNull final Camera.CameraInfo info, final int degrees)
    {
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            final int mirroredResult = (info.orientation + degrees) % 360;
            return (360 - mirroredResult) % 360;  // compensate the mirror
        }
        else {  // back-facing
            return (info.orientation - degrees + 360) % 360;
        }
    }

    private static void setCameraDisplayOrientation(@NonNull final Activity activity,
                                                    final int cameraId,
                                                    @NonNull final android.hardware.Camera camera)
    {
        final int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        final int degrees = getDegrees(rotation);
        camera.setDisplayOrientation(getDisplayRotation(getCameraInfos(cameraId), degrees));
    }

    /**
     * @return The primary camera or null, if no camera is available
     */
    private static Camera getCamera(@NonNull final Activity activity, final int face)
    {
        final int cameraCount = Camera.getNumberOfCameras();
        if (cameraCount <= 0) {
            return null;
        }
        final Camera.CameraInfo info = new Camera.CameraInfo();
        for (int i = 0; i < cameraCount; i++) {
            Camera.getCameraInfo(i, info);
            if (info.facing == face) {
                try {
                    final Camera c = Camera.open(i);
                    setCameraDisplayOrientation(activity, face, c);
                    final Camera.Parameters parameters = c.getParameters();
                    c.setParameters(parameters);
                    return c;
                } catch (@NonNull final Exception e) {
                    Log.e("Camera", "Couldn't open camera", e);
                    return null;
                }
            }
        }
        return null;
    }

    @Nullable
    public static LegacyCamera getBackCamera(@NonNull final Activity activity)
    {
        Camera c = getCamera(activity, Camera.CameraInfo.CAMERA_FACING_BACK);
        if (c != null) {
            return new LegacyCamera(c);
        }
        else {
            c = getCamera(activity, Camera.CameraInfo.CAMERA_FACING_FRONT);
            if (c != null) {
                return new LegacyCamera(c);
            }
            return null;
        }
    }

    @Nullable
    public static LegacyCamera getFrontCamera(@NonNull final Activity activity)
    {
        Camera c = getCamera(activity, Camera.CameraInfo.CAMERA_FACING_FRONT);
        if (c != null) {
            return new LegacyCamera(c);
        }
        else {
            c = getCamera(activity, Camera.CameraInfo.CAMERA_FACING_BACK);
            if (c != null) {
                return new LegacyCamera(c);
            }
            return null;
        }
    }

    @Nullable
    private Size getOptimalPreviewSize(@Nullable final Iterable<Size> sizes, final int w,
                                       final int h)
    {
        final double ASPECT_TOLERANCE = 0.1;
        final double targetRatio = (double) h / w;
        if (sizes == null) {
            return null;
        }
        Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        for (final Size size : sizes) {
            final double ratio = (double) size.getHeight() / size.getWidth();
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) {
                continue;
            }
            if (Math.abs(size.getHeight() - h) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.getHeight() - h);
            }
        }
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (final Size size : sizes) {
                if (Math.abs(size.getHeight() - h) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.getHeight() - h);
                }
            }
        }
        return optimalSize;
    }

    @Override
    public void setPreviewSize(final int width, final int height)
    {
        Log.d(TAG, "Set preview size:" + width + "," + height);
        final Camera.Parameters parameters = camera.getParameters();
        parameters.setPreviewSize(width, height);
        camera.setParameters(parameters);
        camera.setDisplayOrientation(90);
    }

    @Override
    public void setPreviewDisplay(final SurfaceHolder holder)
    {
        try {
            camera.setPreviewDisplay(holder);
        } catch (@NonNull final IOException e) {
            Log.d(TAG, "Failed to set preview display", e);
        }
    }

    @Override
    public int getFlashMode()
    {
        final Camera.Parameters parameters = camera.getParameters();
        switch (parameters.getFlashMode()) {
            case Camera.Parameters.FLASH_MODE_ON:
                return FLASH_MODE_ON;
            case Camera.Parameters.FLASH_MODE_TORCH:
                return FLASH_MODE_AUTO;
            default:
                return FLASH_MODE_OFF;
        }
    }

    @Override
    public void startPreview()
    {
        try {
            camera.startPreview();
        } catch (@NonNull final Exception e) {
            Timber.e(e);
        }
    }

    @Override
    public void stopPreview()
    {
        try {
            camera.stopPreview();
        } catch (@NonNull final Exception e) {
            Timber.e(e);
        }
    }

    @NonNull
    @Override
    public List<Size> getSupportedSizes()
    {
        final List<Size> sizes = new ArrayList<>();
        for (final Camera.Size size : camera.getParameters().getSupportedPreviewSizes()) {
            sizes.add(new Size(size.width, size.height));
        }
        return sizes;
    }

    @NonNull
    @Override
    public Size getFullscreenPreviewSize(final int width, final int height)
    {
        return getOptimalPreviewSize(getSupportedSizes(), width, height);
    }

    @Override
    public void takePicture(@NonNull final ReceiveCallback<byte[]> receivePicture)
    {
        camera.takePicture(null, null, (data, camera) -> receivePicture.onReceive(data));
    }

    private void setZoom(final int zoom)
    {
        final Camera.Parameters parameters = camera.getParameters();
        final int sanitizedZoom = MathUtils.clamp(zoom, 0, parameters.getMaxZoom());
        parameters.setZoom(sanitizedZoom);
        camera.setParameters(parameters);
    }

    @Override
    public int getMaxZoom()
    {
        final Camera.Parameters parameters = camera.getParameters();
        return parameters.getMaxZoom();
    }

    @Override
    public void zoomIn()
    {
        final Camera.Parameters parameters = camera.getParameters();
        setZoom(MathUtils.clamp(parameters.getZoom() + 1, 0, getMaxZoom()));
    }

    @Override
    public void zoomOut()
    {
        final Camera.Parameters parameters = camera.getParameters();
        setZoom(MathUtils.clamp(parameters.getZoom() - 1, 0, getMaxZoom()));
    }

    @Override
    public void release()
    {
        camera.release();
    }

    @Override
    public void switchFlashMode()
    {
        if (!hasFlash()) { return; }
        final Camera.Parameters parameters = camera.getParameters();
        switch (parameters.getFlashMode()) {
            case Camera.Parameters.FLASH_MODE_ON:
                parameters.setFlashMode(parameters.getSupportedFlashModes()
                                                  .contains(
                                                          Camera.Parameters.FLASH_MODE_TORCH) ? Camera.Parameters.FLASH_MODE_TORCH : Camera.Parameters.FLASH_MODE_OFF);
                break;

            case Camera.Parameters.FLASH_MODE_OFF:
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                break;

            default:
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                break;
        }
        camera.setParameters(parameters);
    }

    @Override
    public boolean hasFlash()
    {
        final Camera.Parameters parameters = camera.getParameters();
        return parameters.getSupportedFlashModes() != null
                // For flash at least on and off mode are necessary. With only off mode it's like no flash is available
                && parameters.getSupportedFlashModes()
                             .contains(
                                     Camera.Parameters.FLASH_MODE_OFF) && parameters.getSupportedFlashModes()
                                                                                    .contains(
                                                                                            Camera.Parameters.FLASH_MODE_ON);
    }
}
