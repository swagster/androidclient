package com.cloo.android.main.injection.module;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.service.business.follow.FirebaseFunctionFollowService;
import com.cloo.android.main.service.business.follow.FollowService;

import dagger.Module;
import dagger.Provides;

@Module(includes = {DatabaseInstanceModule.class})
public class FollowServiceModule
{

    @NonNull
    @Provides
    public FollowService followService(final DatabaseAdapter databaseAdapter)
    {
        return new FirebaseFunctionFollowService(databaseAdapter);
    }
}
