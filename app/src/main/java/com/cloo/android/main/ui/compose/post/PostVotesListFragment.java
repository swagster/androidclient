package com.cloo.android.main.ui.compose.post;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;

import com.cloo.android.databinding.FragmentListBinding;
import com.cloo.android.main.backend.database.DatabaseQuery;
import com.cloo.android.main.model.User;
import com.cloo.android.main.ui.base.DatabaseQueryList;
import com.cloo.android.main.ui.base.DatabaseQueryListAdapter;
import com.cloo.android.main.ui.compose.user.UserListAdapter;

public class PostVotesListFragment extends DatabaseQueryList<User>
{
    private static final String POST_ID = "postId";

    @NonNull
    public static PostVotesListFragment newInstance(final String postId)
    {
        final PostVotesListFragment postVotesListFragment = new PostVotesListFragment();
        final Bundle bundle = new Bundle();
        bundle.putString(PostVotesListFragment.POST_ID, postId);
        postVotesListFragment.setArguments(bundle);
        return postVotesListFragment;
    }

    @Override
    public final void setupView(@NonNull final FragmentListBinding binding)
    {
    }

    @Override
    public void setupPromotedAction(@NonNull final FloatingActionButton fab)
    {
        fab.setVisibility(View.INVISIBLE);
    }

    @Nullable
    @Override
    public final DatabaseQueryListAdapter<User> createAdapter()
    {
        return new UserListAdapter(getAuthenticatedActivity());
    }

    @NonNull
    @Override
    public final DatabaseQuery createQuery(@Nullable final String endKey, final int count)
    {
        final String postId = getParameters().getString(PostVotesListFragment.POST_ID);
        Log.d("PostVotes", "Create Query with id " + postId);
        final DatabaseQuery query = getAuthenticatedActivity().getDatabase()
                                                              .getVotedByUsersNode(postId)
                                                              .limitToLast(count);
        return endKey == null || endKey.isEmpty() ? query : query.endAt(endKey);
    }
}
