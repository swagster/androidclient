package com.cloo.android.main.ui.view;

import android.content.Context;
import android.util.AttributeSet;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;

public class RotatableImageViewTouch extends ImageViewTouch
{
    private float rotation;

    public RotatableImageViewTouch(final Context context, final AttributeSet attrs)
    {
        super(context, attrs);
    }

    public RotatableImageViewTouch(final Context context, final AttributeSet attrs,
                                   final int defStyle)
    {
        super(context, attrs, defStyle);
    }

    @Override
    public float getRotation()
    {
        return rotation;
    }

    @Override
    public void setRotation(final float rotation)
    {
        super.setRotation(rotation);
        this.rotation = rotation;
        invalidate();
    }
}
