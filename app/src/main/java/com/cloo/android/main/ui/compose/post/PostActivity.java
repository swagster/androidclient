package com.cloo.android.main.ui.compose.post;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.cloo.android.R;
import com.cloo.android.databinding.ActivityPostBinding;
import com.cloo.android.main.application.ClooApplication;
import com.cloo.android.main.application.MainActivity;
import com.cloo.android.main.backend.database.DatabaseNode;
import com.cloo.android.main.backend.database.DatabaseReference;
import com.cloo.android.main.function.ReceiveCallback;
import com.cloo.android.main.injection.component.DaggerPostActivityComponent;
import com.cloo.android.main.injection.component.PostActivityComponent;
import com.cloo.android.main.injection.module.DatabaseInstanceModule;
import com.cloo.android.main.model.Post;
import com.cloo.android.main.model.User;
import com.cloo.android.main.service.business.common.ReportService;
import com.cloo.android.main.service.business.notification.NotificationBadgeService;
import com.cloo.android.main.service.business.notification.NotificationBadgeServiceImpl;
import com.cloo.android.main.service.business.post.PostDeletionService;
import com.cloo.android.main.service.business.post.VoteService;
import com.cloo.android.main.ui.base.AuthenticatedActivity;
import com.cloo.android.main.ui.common.ReportDialog;
import com.cloo.android.main.util.MathUtils;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class PostActivity extends AuthenticatedActivity
{
    public static final int POST_STATE_DETAILS = 0;
    public static final int POST_STATE_COMMENTS = 1;
    public static final int POST_STATE_VOTES = 2;
    private static final String TAG = PostActivity.class.getSimpleName();
    private static final String POST_ID = "postId";
    private static final String POST_STATE = "postState";
    @Nullable private ActivityPostBinding binding = null;
    @Nullable private Post post = null;
    @Nullable private PostDeletionService postDeletionService = null;
    @Nullable private VoteService voteService = null;
    @Nullable private DatabaseNode postReference = null;
    @Nullable private Menu menu = null;
    private FirebaseAnalytics mFirebaseAnalytics;

    @NonNull
    public static Intent newIntent(final Context context, final String postId)
    {
        return newIntent(context, postId, POST_STATE_DETAILS);
    }

    @NonNull
    public static Intent newIntent(final Context context, final String postId,
                                   @PostState final int tabIndex)
    {
        final Bundle bundle = new Bundle();
        bundle.putString(PostActivity.POST_ID, postId);
        bundle.putInt(PostActivity.POST_STATE, tabIndex);
        final Intent intent = new Intent(context, PostActivity.class);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected final void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        if (getIntent().hasExtra("notification")) {
            final NotificationBadgeService notificationBadgeService = new NotificationBadgeServiceImpl(
                    ClooApplication.getInstance(this));
            notificationBadgeService.decrementNotificationBadgeCount(1);
        }

        binding = setBindingView(R.layout.activity_post);
        binding.pager.setOffscreenPageLimit(2);
        getPost(post -> {
            setPost(post.getKey(), post);
            setPostNode(getDatabase().getPostDetailsNode(post.getKey()));
            final PostTabsAdapter adapter = new PostTabsAdapter(getSupportFragmentManager(),
                                                                getApplicationContext(),
                                                                post.getKey());
            binding.pager.setAdapter(adapter);
            binding.pager.setCurrentItem(MathUtils.clamp(
                    getIntent().getExtras() != null ? getIntent().getExtras()
                                                                 .getInt(POST_STATE,
                                                                         POST_STATE_DETAILS) : POST_STATE_DETAILS,
                    POST_STATE_DETAILS, POST_STATE_VOTES), false);
            binding.tabs.setupWithViewPager(binding.pager);
        });
        setTitle(getString(R.string.title_post_details));
        setupBackButton(binding.toolbar);
        final PostActivityComponent postActivityComponent = DaggerPostActivityComponent.builder()
                                                                                       .databaseInstanceModule(
                                                                                               new DatabaseInstanceModule(
                                                                                                       getDatabase()))
                                                                                       .build();
        postDeletionService = postActivityComponent.getPostDeletionService();
        voteService = postActivityComponent.getVoteService();

    }

    private void setPostNode(@NonNull final DatabaseNode postNode)
    {
        removePostNode(postReference);
        postNode.onUpdates(Post.class, this::setPost).submit();
        postNode.keepSynced(true);
        postReference = postNode;
    }

    private void getPost(@NonNull final ReceiveCallback<Post> callback)
    {
        final Intent appLinkIntent = getIntent();
        final Uri appLinkData = appLinkIntent.getData();
        final String postId;
        if (appLinkData != null) {
            postId = appLinkData.getLastPathSegment();
        }
        else {
            postId = getIntent().getExtras().getString(PostActivity.POST_ID);
        }
        if (postId == null) {
            Log.e(PostActivity.TAG, "PostDetailActivity started with postId null");
            finish();
        }
        else {
            getDatabase().getPostDetailsNode(postId).receive(Post.class, (key, p) -> {
                if (p != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString(FirebaseAnalytics.Param.ITEM_ID,
                                     getIntent().getExtras().getString(PostActivity.POST_ID));
                    bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, p.getTitle());
                    bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "post");
                    mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM, bundle);
                    p.setKey(key);
                    callback.onReceive(p);
                }
            }).onError(error -> {}).recycleAfterUse().submit();
        }
    }

    private void setPost(@NonNull final String key, @Nullable final Post post)
    {
        if (post != null && canAccessUI()) {
            post.setKey(key);
            if (binding.getPost() != null) {
                post.selectedOption.set(binding.getPost().selectedOption.get());
            }
            this.post = post;
            binding.setPost(post);
            binding.notifyChange();
            voteService.getVotedOption(key, i -> {
                if (i != -1) {
                    post.selectedOption.set(i);
                    binding.notifyChange();
                }
            });
            if (menu != null) {
                menu.findItem(R.id.menu_post_remove).setVisible(isCreatorOfPost(post));
                getDatabase().getReportNode(post.getAuthor(), post.getKey()).exists(exists -> {
                    if (canAccessUI()) { menu.findItem(R.id.menu_post_report).setVisible(!exists); }
                }).onError(error -> {
                    if (canAccessUI()) { menu.findItem(R.id.menu_post_report).setVisible(false); }
                }).recycleAfterUse().submit();
            }
            if (binding.getUser() == null) {
                getDatabase().getUserDetailsNode(post.getAuthor())
                             .receive(User.class, (id, user) -> {
                                 if (user != null && canAccessUI()) {
                                     user.setId(id);
                                     binding.setUser(user);
                                 }
                             })
                             .recycleAfterUse()
                             .submit();
            }
        }
    }

    public final void hideZoomedOption(final View view)
    {
        if (canAccessUI()) { binding.expandedFrame.setVisibility(View.INVISIBLE); }
    }

    private boolean isCreatorOfPost(@Nullable final Post post)
    {
        return post != null && post.getAuthor().equals(getAuthenticatedUserId());
    }

    @Override
    protected final void onDestroy()
    {
        super.onDestroy();
        removePostNode(postReference);
    }

    private void removePostNode(@Nullable final DatabaseReference reference)
    {
        if (reference != null) {
            reference.recycle();
        }
    }

    @Override
    public final void onBackPressed()
    {
        if (binding.expandedFrame.getVisibility() == View.VISIBLE) {
            binding.expandedFrame.setVisibility(View.INVISIBLE);
        }
        else {
            finish();
        }
    }

    private void showRemoveDialog()
    {
        if (canAccessUI()) {
            getFragmentManager().popBackStack();
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Remove your Post");
            alertDialogBuilder.setMessage(
                    "If you remove your post, it will become invisible to other users.")
                              .setCancelable(false)
                              .setPositiveButton("Remove",
                                                 (dialog, id) -> postDeletionService.deletePost(
                                                         getAuthentication().getSession()
                                                                            .getUserId(),
                                                         getPostId(), this::finish, errorCode -> {
                                                         }))
                              .setNegativeButton("Cancel", (dialog, id) -> dialog.cancel());
            alertDialogBuilder.create().show();
        }
    }

    private void showReportDialog()
    {
        if (post == null) { return; }
        if (canAccessUI()) {
            getFragmentManager().popBackStack();
            final ReportDialog fragment = ReportDialog.newInstance(post.getKey(),
                                                                   ReportService.CONTENT_TYPE_POST);
            fragment.show(getSupportFragmentManager(), "");
        }
    }

    private String getUrl(@NonNull final Post post)
    {
        return String.format(getString(R.string.post_details_link_url), post.getKey());
    }

    private void sharePost()
    {
        final Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getUrl(post));
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @Override
    public final boolean onOptionsItemSelected(@NonNull final MenuItem item)
    {
        switch (item.getItemId()) {
            case R.id.menu_post_remove:
                showRemoveDialog();
                break;
            case R.id.menu_post_report:
                showReportDialog();
                break;
            case R.id.menu_post_share:
                sharePost();
                break;
            case R.id.menu_post_back_to_main:
                startActivity(new Intent(this, MainActivity.class));
                break;
            default:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public final boolean onCreateOptionsMenu(@NonNull final Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_post_detail, menu);
        this.menu = menu;
        this.menu.findItem(R.id.menu_post_report).setVisible(!isCreatorOfPost(binding.getPost()));
        this.menu.findItem(R.id.menu_post_remove)
                 .setVisible(binding == null || isCreatorOfPost(binding.getPost()));
        return super.onCreateOptionsMenu(menu);
    }

    public String getPostId()
    {
        return getIntent().getExtras().getString(PostActivity.POST_ID, "");
    }

    @Nullable
    public Post getPost()
    {
        return post;
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({POST_STATE_COMMENTS, POST_STATE_DETAILS, POST_STATE_VOTES})
    @interface PostState
    {}
}