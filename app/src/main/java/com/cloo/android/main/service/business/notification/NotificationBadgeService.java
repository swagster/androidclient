package com.cloo.android.main.service.business.notification;

import android.content.Intent;

public interface NotificationBadgeService
{
    void incrementNotificationBadgeCount(int count);

    void decrementNotificationBadgeCount(int count);

    int getNotificationBadgeCount();

    void markNotificationRead(Intent intent);
}
