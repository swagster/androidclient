package com.cloo.android.main.backend.authentication;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.cloo.android.main.util.Assert;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

public class FirebaseSession implements Session
{
    private final long timeSinceLogin;
    @Nullable private final String userId;
    @Nullable private final String email;

    public FirebaseSession(@Nullable final String userId, @Nullable final String email)
    {
        timeSinceLogin = System.currentTimeMillis();
        this.userId = userId;
        this.email = email;
        FirebaseDatabase.getInstance()
                        .getReference(".info/connected")
                        .addValueEventListener(new ValueEventListener()
                        {
                            @Override
                            public void onDataChange(@NonNull final DataSnapshot dataSnapshot)
                            {
                                if (dataSnapshot.getValue(Boolean.class)) {
                                    if (userId != null && !userId.isEmpty()) {
                                        FirebaseDatabase.getInstance()
                                                        .getReference("users/isOnline/" + userId)
                                                        .onDisconnect()
                                                        .removeValue();
                                        FirebaseDatabase.getInstance()
                                                        .getReference("users/isOnline/" + userId)
                                                        .setValue(true);
                                        FirebaseDatabase.getInstance()
                                                        .getReference("users/lastOnline/" + userId)
                                                        .onDisconnect()
                                                        .setValue(ServerValue.TIMESTAMP);
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(final DatabaseError databaseError)
                            {

                            }
                        });

        FirebaseDatabase.getInstance()
                        .getReference("users")
                        .child("notificationTokens")
                        .child(userId)
                        .child("0")
                        .setValue(FirebaseInstanceId.getInstance().getToken());

        Crashlytics.setUserIdentifier(getUserId());
        Crashlytics.setUserEmail(getEmail());
        Crashlytics.setUserName(getEmail());
    }

    @Nullable
    @Override
    public String getUserId()
    {
        Assert.check(!userId.isEmpty(), "Tried to use empty userId");
        return userId;
    }

    @Override
    public long getTimeSinceLogin()
    {
        return timeSinceLogin;
    }

    @Nullable
    @Override
    public String getEmail()
    {
        return email;
    }
}
