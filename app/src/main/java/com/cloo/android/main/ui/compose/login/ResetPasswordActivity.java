package com.cloo.android.main.ui.compose.login;

import android.content.Context;
import android.content.Intent;
import android.databinding.Observable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.cloo.android.R;
import com.cloo.android.databinding.ActivityResetPasswordBinding;
import com.cloo.android.main.backend.authentication.Authentication;
import com.cloo.android.main.model.bindingModels.ResetPasswordForm;
import com.cloo.android.main.service.util.validation.CredentialValidation;
import com.cloo.android.main.ui.base.BaseActivity;
import com.cloo.android.main.util.AndroidUtils;

import static com.cloo.android.main.util.AndroidUtils.hideKeyboard;
import static com.cloo.android.main.util.AndroidUtils.showKeyboard;
import static com.cloo.android.main.util.AnimationUtils.lerp;

public class ResetPasswordActivity extends BaseActivity
{
    private static final String BUNDLE_EMAIL = "email";
    private final Handler handler = new Handler();
    private final ResetPasswordForm form = new ResetPasswordForm();
    @Nullable private Runnable removeBlinkingCursor = null;
    @NonNull private ActivityResetPasswordBinding binding = null;
    private final Observable.OnPropertyChangedCallback onEmailChangeCallback = new Observable.OnPropertyChangedCallback()
    {
        @Override
        public void onPropertyChanged(final Observable observable, final int i)
        {
            addBlinkingCursor();
            checkEmail(getEmail());
        }
    };
    private boolean pendingCall = false;

    @NonNull
    public static Intent newInstance(final Context context, final String email)
    {
        final Intent intent = new Intent(context, ResetPasswordActivity.class);
        final Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_EMAIL, email);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected final void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        removeBlinkingCursor = this::removeBlinkingCursor;
        binding = setBindingView(R.layout.activity_reset_password);
        setupBackButton(binding.toolbar);
        binding.setModel(form);
        markEmailAsInvalid();
        form.email.addOnPropertyChangedCallback(onEmailChangeCallback);
        if (getIntent().getExtras() != null) {
            form.email.set(getIntent().getExtras().getString(BUNDLE_EMAIL));
        }
    }

    public void onEmailTextViewClick(final View view)
    {
        if (canAccessUI() && binding.email.getCurrentTextColor() == AndroidUtils.getColor(this,
                                                                                          R.color.accent)) {
            binding.email.setCursorVisible(true);
            handler.removeCallbacks(removeBlinkingCursor);
            handler.postDelayed(removeBlinkingCursor, 5000);
        }
    }

    private void addBlinkingCursor()
    {
        if (canAccessUI()) {
            binding.email.setCursorVisible(true);
            handler.removeCallbacks(removeBlinkingCursor);
        }
    }

    private void removeBlinkingCursor()
    {
        if (canAccessUI()) {
            binding.email.setCursorVisible(false);
            hideKeyboard(this);
        }
    }

    private void checkEmail(@NonNull final String email)
    {
        if (CredentialValidation.isEmailValid(email)) {
            getAuthentication().getProviders(email, providers -> {
                if (email.equals(getEmail()) && getAuthentication().hasEmailPasswordProvider(
                        providers)) {
                    markEmailAsExisting();
                }
                else {
                    markEmailAsInvalid();
                }
            });
        }
        else {
            markEmailAsInvalid();
        }
    }

    private void markEmailAsInvalid()
    {
        if (canAccessUI()) {
            binding.setIsValidEmail(false);
            binding.notifyChange();
            binding.email.setSelected(true);
            binding.email.requestFocus();
            showKeyboard(this);
        }
    }

    private void markEmailAsExisting()
    {
        if (canAccessUI()) {
            binding.setIsValidEmail(true);
            binding.notifyChange();
            binding.email.setCursorVisible(false);
            hideKeyboard(this);
        }
    }

    public void trySendPasswordReset(@Nullable final View view)
    {
        if (canSendPasswordReset()) {
            startLoading();
            getAuthentication().getProviders(getEmail(), providers -> {
                if (getAuthentication().hasEmailPasswordProvider(providers)) {
                    if (canAccessUI()) {
                        hideKeyboard(this);
                    }
                    sendPasswordReset();
                }
                else {
                    endLoading();
                    if (canAccessUI()) {
                        binding.email.setError(getString(R.string.error_email_unkown));
                    }
                    markEmailAsInvalid();
                }
            });
        }
    }

    public String getEmail()
    {
        return form.email.get();
    }

    private boolean canSendPasswordReset()
    {
        return !pendingCall && !getEmail().isEmpty();
    }

    private void sendPasswordReset()
    {
        getAuthentication().sendPasswordReset(getEmail(), success -> {
            if (success) {
                onSendPasswordResetSuccess();
            }
        }, this::onSendPasswordResetError);
    }

    private void onSendPasswordResetSuccess()
    {
        if (canAccessUI()) {
            binding.loadingSpinner.setVisibility(View.GONE);
            final CheckEmailDialog fragment = new CheckEmailDialog();
            fragment.show(getSupportFragmentManager(), CheckEmailDialog.class.getSimpleName());
        }
    }

    private void onSendPasswordResetError(final int errorCode)
    {
        switch (errorCode) {
            case Authentication.TOO_MANY_REQUESTS:
                endLoading();
                showToast(getString(R.string.toast_password_reset_too_often));
                break;
        }
    }

    private void endLoading()
    {
        if (canAccessUI()) {
            pendingCall = false;
            lerp(this, binding.loadingSpinner, binding.email);
        }
    }

    private void startLoading()
    {
        if (canAccessUI()) {
            pendingCall = true;
            lerp(this, binding.email, binding.loadingSpinner);
        }
    }
}