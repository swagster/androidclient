package com.cloo.android.main.injection.module;

import android.support.annotation.NonNull;

import com.cloo.android.main.service.util.image.GlideImageLoader;
import com.cloo.android.main.service.util.image.ImageLoader;

import dagger.Module;
import dagger.Provides;

@Module()
public class ImageLoaderModule {
    @NonNull
    @Provides
    public ImageLoader imageLoader() {
        return new GlideImageLoader();
    }
}
