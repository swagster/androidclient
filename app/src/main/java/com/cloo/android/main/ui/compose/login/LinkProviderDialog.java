package com.cloo.android.main.ui.compose.login;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentLinkProviderBinding;
import com.cloo.android.main.backend.authentication.Authentication;
import com.cloo.android.main.model.ParcelableCredential;
import com.cloo.android.main.ui.base.BaseDialog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class LinkProviderDialog extends BaseDialog
{
    public static final String CREDENTIAL_KEY = "credential";
    public static final String EMAIL_KEY = "email";
    @Nullable private ParcelableCredential linkedCredential = null;
    @Nullable private String email = null;
    @Nullable private FragmentLinkProviderBinding binding = null;

    @NonNull
    public static LinkProviderDialog newInstance(@NonNull final String email,
                                                 @NonNull final Parcelable credential)
    {
        final LinkProviderDialog fragment = new LinkProviderDialog();
        final Bundle bundle = new Bundle();
        bundle.putParcelable(LinkProviderDialog.CREDENTIAL_KEY, credential);
        bundle.putString(LinkProviderDialog.EMAIL_KEY, email);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    @NonNull
    public final View onCreateView(@NonNull final LayoutInflater inflater,
                                   @Nullable final ViewGroup container,
                                   @Nullable final Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_link_provider, container,
                                          false);
        parseArguments();
        if (linkedCredential != null && email != null) {
            getParentActivity().getAuthentication()
                               .getProviders(email, this::createProviderLoginViews);
        }
        else {
            dismissAllowingStateLoss();
        }
        return binding.getRoot();
    }

    private void createProviderLoginViews(@NonNull final Collection<String> providers)
    {
        binding.providerList.removeAllViews();
        final String linkedProvider = linkedCredential.getCredential().getProvider();
        if (providers.contains(linkedProvider)) {
            dismissAllowingStateLoss();
        }
        else {
            inflateProviderFragments(binding.providerList.getId(), providers);
        }
    }

    private void parseArguments()
    {
        if (getArguments() != null) {
            linkedCredential = getArguments().getParcelable(LinkProviderDialog.CREDENTIAL_KEY);
            email = getArguments().getString(LinkProviderDialog.EMAIL_KEY);
        }
        else {
            dismissAllowingStateLoss();
        }
    }

    private void inflateProviderFragments(final int container,
                                          @NonNull final Collection<String> providers)
    {
        final List<Fragment> providerFragments = getProviderFragmentList(providers);
        if (providerFragments.isEmpty()) {
            dismissAllowingStateLoss();
        }
        else {
            final FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            for (final Fragment fragment : providerFragments) {
                transaction.add(container, fragment);
            }
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    @NonNull
    private List<Fragment> getProviderFragmentList(@NonNull final Collection<String> providers)
    {
        final List<Fragment> fragmentList = new ArrayList<>(providers.size());
        for (final String provider : providers) {
            final Fragment fragment = getProviderFragment(provider);
            if (fragment != null) {
                fragmentList.add(fragment);
            }
        }
        return fragmentList;
    }

    @Nullable
    private Fragment getProviderFragment(@NonNull final String provider)
    {
        switch (provider) {
            case Authentication.PROVIDER_FACEBOOK:
                return FacebookLoginFragment.newInstance(linkedCredential);
            case Authentication.PROVIDER_EMAIL_PASSWORD:
                return EmailPasswordFragment.newInstance(linkedCredential, email);
            default:
                return null;
        }
    }
}