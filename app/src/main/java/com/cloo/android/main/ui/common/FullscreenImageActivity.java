package com.cloo.android.main.ui.common;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.cloo.android.R;
import com.cloo.android.databinding.ActivityImageZoomedBinding;
import com.cloo.android.main.ui.base.BaseActivity;
import com.cloo.android.main.util.AndroidUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import timber.log.Timber;

public class FullscreenImageActivity extends BaseActivity
{
    private static final String BUNDLE_IMAGE_URL = "imageUrl";
    final Handler handler = new Handler();

    @NonNull
    public static Intent newInstance(final Context context, final String imageUrl)
    {
        final Intent intent = new Intent(context, FullscreenImageActivity.class);
        final Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_IMAGE_URL, imageUrl);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        final ActivityImageZoomedBinding binding = DataBindingUtil.setContentView(this,
                                                                                  R.layout.activity_image_zoomed);
        binding.setImageUrl(getIntent().getExtras().getString(BUNDLE_IMAGE_URL, ""));
        binding.notifyChange();
        Timber.d("Opened Zoomed Image Activity with Image URL %s",
                 getIntent().getExtras().getString(BUNDLE_IMAGE_URL, ""));
        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public final boolean onOptionsItemSelected(@NonNull final MenuItem item)
    {
        switch (item.getItemId()) {
            case R.id.menu_save_image:
                if (!AndroidUtils.isSDCardNotMounted()) {
                    handler.post(
                            () -> Toast.makeText(FullscreenImageActivity.this, "Saving image...",
                                                 Toast.LENGTH_LONG).show());
                    Timber.d("Saved image");
                    final AsyncTask<String, String, String> task = new AsyncTask()
                    {
                        @Override
                        protected Object doInBackground(final Object[] objects)
                        {
                            Timber.d("Started in background saving");
                            final URL imageurl;
                            try {
                                imageurl = new URL(
                                        getIntent().getExtras().getString(BUNDLE_IMAGE_URL, ""));
                                final Bitmap bitmap = BitmapFactory.decodeStream(
                                        imageurl.openConnection().getInputStream());
                                final File mediaStorageDir = new File(
                                        Environment.getExternalStoragePublicDirectory(
                                                Environment.DIRECTORY_PICTURES), "Cloo");
                                if (!mediaStorageDir.exists()) {
                                    if (!mediaStorageDir.mkdirs()) {
                                        Log.d("CameraActivity", "failed to create directory");
                                        return null;
                                    }
                                }
                                final ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                                final File outputFile = new File(
                                        mediaStorageDir.getPath() + File.separator + "cloo-option-" + System
                                                .currentTimeMillis() + ".jpeg");
                                Timber.d("Save image to %s", outputFile.toString());
                                try (final FileOutputStream fos = new FileOutputStream(
                                        outputFile)) {
                                    fos.write(stream.toByteArray());
                                    handler.post(() -> Toast.makeText(FullscreenImageActivity.this,
                                                                      "Saved image",
                                                                      Toast.LENGTH_LONG).show());
                                } catch (@NonNull final FileNotFoundException e) {
                                    Timber.d("File not found");
                                } catch (@NonNull final IOException e) {
                                    Timber.d("IOException");
                                }
                            } catch (@NonNull final MalformedURLException e) {
                                Timber.d("URL is bad");
                            } catch (@NonNull final IOException e) {
                                Timber.d("IOException");
                            }
                            return null;
                        }
                    };
                    task.execute();

                }
                break;
            default:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public final boolean onCreateOptionsMenu(@NonNull final Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_zoomed_image, menu);
        return super.onCreateOptionsMenu(menu);
    }

}
