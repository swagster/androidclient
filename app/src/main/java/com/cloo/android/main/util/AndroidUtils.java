package com.cloo.android.main.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresPermission;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.cloo.android.R;
import com.cloo.android.main.application.ClooApplication;

import java.io.IOException;
import java.util.Locale;

public final class AndroidUtils
{
    private AndroidUtils()
    {

    }

    public static void setLanguage(@NonNull final Activity activity, final Locale locale)
    {
        final Resources res = activity.getResources();
        final DisplayMetrics dm = res.getDisplayMetrics();
        final Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration(conf, dm);
    }

    @RequiresPermission(android.Manifest.permission.ACCESS_NETWORK_STATE)
    public static boolean isNetworkAvailable(@NonNull final Context context)
    {
        final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean isEmpty(@NonNull final TextView textView)
    {
        final CharSequence text = textView.getText();
        return text == null || text.toString().trim().isEmpty();
    }

    @NonNull
    public static String getString(@NonNull final TextView username)
    {
        final CharSequence text = username.getText();
        return text == null ? "" : text.toString().trim();
    }

    @Nullable
    public static Bitmap getBitmap(final ContentResolver resolver, @NonNull final Uri uri)
    {
        try {
            return MediaStore.Images.Media.getBitmap(resolver, uri);
        } catch (@NonNull final IOException e) {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            return BitmapFactory.decodeFile(uri.toString(), options);
        }
    }

    public static boolean isSDCardNotMounted()
    {
        final String state = Environment.getExternalStorageState();
        return !Environment.MEDIA_MOUNTED.equals(state);
    }

    public static boolean needsPermission(@NonNull final Context context,
                                          @NonNull final String permission)
    {
        return ContextCompat.checkSelfPermission(context,
                                                 permission) != PackageManager.PERMISSION_GRANTED;
    }

    @NonNull
    public static String getStringOrEmptyOnNull(@Nullable final String value)
    {
        return value == null ? "" : value;
    }

    public static int getColor(@Nullable final Context context, @ColorRes final int id)
    {
        if (context != null) {
            return ActivityCompat.getColor(context, id);
        }
        else {
            return 0xFFFFFF;
        }
    }

    @NonNull
    public static Intent getStoreIntent(@NonNull ClooApplication application)
    {
        final Uri uri = Uri.parse("market://details?id=" + application.getPackageName());
        final Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to addAllImages following flags to intent.
        goToMarket.addFlags(
                Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        return goToMarket;
    }

    public static void hideKeyboard(@NonNull final Activity context)
    {
        if (context != null) {
            final InputMethodManager inputManager = (InputMethodManager) context.getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            final View focus = context.getCurrentFocus();
            if (focus != null) {
                inputManager.hideSoftInputFromWindow(focus.getWindowToken(),
                                                     InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    public static void showKeyboard(@Nullable final Activity activity)
    {
        if (activity != null) {
            activity.getWindow()
                    .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }
    }

    @NonNull
    public static SharedPreferences getApplicationSharedPreferences(@NonNull final Context context)
    {
        return context.getSharedPreferences(
                String.format(context.getString(R.string.preference_file_key), "application"),
                Context.MODE_PRIVATE);
    }

    @NonNull
    public static SharedPreferences getUserSharedPreferences(@NonNull final Context context)
    {
        final ClooApplication application = (ClooApplication) context.getApplicationContext();
        Assert.check(application.getAuthentication().isLoggedIn(),
                     "You can only getUserSharedPreference if you are logged in");
        return context.getSharedPreferences(
                String.format(context.getString(R.string.preference_file_key),
                              application.getAuthentication().getSession().getUserId()),
                Context.MODE_PRIVATE);
    }

    @SuppressLint("ObsoleteSdkInt")
    public static boolean isApkVersion16()
    {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    public static void setTextColorAccent(@Nullable final TextView view,
                                          @Nullable final Context context)
    {
        if (context != null && view != null) {
            view.setTextColor(ContextCompat.getColor(context, R.color.accent));
        }
    }

    public static void setTextColorNormal(@Nullable final TextView view,
                                          @Nullable final Context context)
    {

        if (context != null && view != null) {
            view.setTextColor(ContextCompat.getColor(context, R.color.text));
        }
    }

    public static void setImageDrawable(@Nullable final ImageView view,
                                        @Nullable final Context context,
                                        @DrawableRes final int resourceId)
    {
        if (context != null && view != null) {
            view.setImageDrawable(ActivityCompat.getDrawable(context, resourceId));
        }
    }
}
