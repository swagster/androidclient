package com.cloo.android.main.hacks;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.cloo.android.main.util.Assert;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import timber.log.Timber;

/**
 * Memeory leak fix for samsung
 * https://github.com/square/leakcanary/issues/177
 */
public class ActivityManagerDetacher implements Application.ActivityLifecycleCallbacks
{
    @Override
    public void onActivityCreated(final Activity activity, final Bundle bundle)
    {

    }

    @Override
    public void onActivityStarted(final Activity activity)
    {

    }

    @Override
    public void onActivityResumed(final Activity activity)
    {

    }

    @Override
    public void onActivityPaused(final Activity activity)
    {

    }

    @Override
    public void onActivityStopped(final Activity activity)
    {

    }

    @Override
    public void onActivitySaveInstanceState(final Activity activity, final Bundle bundle)
    {

    }

    @Override
    public void onActivityDestroyed(@NonNull final Activity activity)
    {
        try {
            detachActivityFromActivityManager(activity);
        } catch (@NonNull final NoSuchFieldException e) {
            Timber.e("", e);
            Assert.check(false,
                         "Samsung activity leak fix has to be removed as ActivityManager field has changed");
        } catch (@NonNull final IllegalAccessException e) {
            Timber.e("", e);
            Assert.check(false,
                         "Samsung activity leak fix did not work, probably activity has leaked");
        }
    }

    /**
     * On Samsung, Activity reference is stored into static mContext field of ActivityManager
     * resulting in activity leak.
     */
    private void detachActivityFromActivityManager(
            @NonNull final Activity activity) throws NoSuchFieldException, IllegalAccessException
    {
        final ActivityManager activityManager = (ActivityManager) activity.
                                                                                  getSystemService(
                                                                                          Context.ACTIVITY_SERVICE);

        final Field contextField = activityManager.getClass().getDeclaredField("mContext");

        final int modifiers = contextField.getModifiers();
        if ((modifiers | Modifier.STATIC) == modifiers) {
            // field is static on Samsung devices only
            contextField.setAccessible(true);

            if (contextField.get(null) == activity) {
                contextField.set(null, null);
            }
        }
    }
}
