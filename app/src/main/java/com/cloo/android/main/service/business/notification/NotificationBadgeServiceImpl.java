package com.cloo.android.main.service.business.notification;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.cloo.android.main.application.ClooApplication;
import com.cloo.android.main.function.FinishCallback;
import com.cloo.android.main.util.AndroidUtils;
import com.cloo.android.main.util.MathUtils;

import me.leolin.shortcutbadger.ShortcutBadger;

public class NotificationBadgeServiceImpl implements NotificationBadgeService, FinishCallback
{
    @NonNull private final ClooApplication application;
    @Nullable private SharedPreferences sharedPreferences = null;

    public NotificationBadgeServiceImpl(@NonNull final ClooApplication application)
    {
        this.application = application;
        if (application.getAuthentication().isLoggedIn()) {
            sharedPreferences = AndroidUtils.getUserSharedPreferences(application);
        }
        else {
            application.getAuthentication().addLoginChangeListener(this);
        }
    }

    @Override
    public void incrementNotificationBadgeCount(final int increment)
    {
        if (sharedPreferences == null) { return; }
        final int newCount = getNotificationBadgeCount() + Math.abs(increment);
        ShortcutBadger.applyCount(application, newCount);
        sharedPreferences.edit().putInt("badgeCount", MathUtils.clampZero(newCount)).apply();
    }

    @Override
    public void decrementNotificationBadgeCount(final int decrement)
    {
        if (sharedPreferences == null) { return; }
        final int newCount = getNotificationBadgeCount() - Math.abs(decrement);
        if (newCount <= 0) {
            ShortcutBadger.removeCount(application);
        }
        else {
            ShortcutBadger.applyCount(application, newCount);
        }
        sharedPreferences.edit().putInt("badgeCount", MathUtils.clampZero(newCount)).apply();
    }

    @Override
    public int getNotificationBadgeCount()
    {

        return sharedPreferences == null ? 0 : sharedPreferences.getInt("badgeCount", 0);
    }

    @Override
    public void markNotificationRead(final Intent intent)
    {

    }

    @Override
    public void onFinish(final boolean success)
    {
        if (success) {
            sharedPreferences = AndroidUtils.getUserSharedPreferences(application);
            application.getAuthentication().removeLoginChangeListener(this);
        }

    }
}
