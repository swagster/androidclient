package com.cloo.android.main.ui.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.cloo.android.main.backend.database.DatabaseNode;
import com.cloo.android.main.function.FinishCallback;
import com.cloo.android.main.ui.compose.login.LoginActivity;

public abstract class AuthenticatedActivity extends BaseActivity implements FinishCallback
{
    @Nullable private DatabaseNode bannedNode = null;

    public String getAuthenticatedUserId()
    {
        return getAuthentication().getSession().getUserId();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        getAuthentication().removeLoginChangeListener(this);
        bannedNode.recycle();
    }

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (!getAuthentication().isLoggedIn() || getAuthentication().getSession()
                                                                    .getUserId()
                                                                    .isEmpty()) {
            supportFinishAfterTransition();
            final Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            Toast.makeText(this, "You need to login first", Toast.LENGTH_LONG).show();
        }
        getAuthentication().addLoginChangeListener(this);
        bannedNode = getDatabase().getBannedUserNode(getAuthenticatedUserId());
        bannedNode.onUpdates(Object.class, (k, o) -> bannedNode.exists(exists -> {
            if (exists) { getAuthentication().signOut(); }
        }).submit()).submit();
    }

    @Override
    public void onFinish(final boolean isLoggedIn)
    {
        if (!isLoggedIn) {
            supportFinishAfterTransition();
            startActivity(new Intent(this, LoginActivity.class));
        }
    }
}
