package com.cloo.android.main.application;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.cloo.android.R;
import com.cloo.android.main.service.business.notification.NotificationBadgeService;
import com.cloo.android.main.service.business.notification.NotificationBadgeServiceImpl;
import com.cloo.android.main.ui.compose.post.PostActivity;
import com.cloo.android.main.ui.compose.user.UserActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import timber.log.Timber;

public class NotificationService extends FirebaseMessagingService
{
    @Nullable private NotificationBadgeService notificationBadgeService = null;

    @Override
    public void onCreate()
    {
        super.onCreate();
        notificationBadgeService = new NotificationBadgeServiceImpl(
                (ClooApplication) getApplicationContext());
    }

    @Override
    public void onMessageReceived(@NonNull final RemoteMessage remoteMessage)
    {
        Timber.d("From: " + remoteMessage.getFrom());
        notificationBadgeService.incrementNotificationBadgeCount(1);
        if (remoteMessage.getData().size() > 0) {
            Timber.d("Message data payload: " + remoteMessage.getData());
            if (remoteMessage.getData().containsKey("type")) {
                final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
                final String type = remoteMessage.getData().get("type");
                switch (type) {
                    case "vote":
                        if (preferences.getBoolean(
                                getString(R.string.preference_local_switch_notification_votes),
                                true)) {
                            sendVoteNotification(remoteMessage.getData().get("postId"),
                                                 remoteMessage.getData().get("title"),
                                                 Long.valueOf(
                                                         remoteMessage.getData().get("voteCount")));
                        }
                        break;
                    case "follower":
                        if (preferences.getBoolean(
                                getString(R.string.preference_local_switch_notification_follower),
                                true)) {
                            sendFollowNotification(remoteMessage.getData().get("followerId"),
                                                   remoteMessage.getData().get("followerName"));
                        }
                        break;
                }
            }
        }
        if (remoteMessage.getNotification() != null) {
            Timber.d("Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }

    private void sendVoteNotification(final String postId, final String postTitle, final long voteCount)
    {
        final Intent intent = PostActivity.newIntent(this, postId);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        final Bundle extra = intent.getExtras();
        extra.putString("notification", "notification");
        intent.putExtras(extra);
        final PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                                                                      PendingIntent.FLAG_ONE_SHOT);

        final Uri defaultSoundUri = RingtoneManager.getDefaultUri(
                RingtoneManager.TYPE_NOTIFICATION);

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
                this).setSmallIcon(R.mipmap.cloo_launcher_logo)
                     .setContentTitle(
                             String.format(getString(R.string.notification_title_vote), postTitle))
                     .setContentText(
                             String.format(getString(R.string.notification_body_votes), voteCount))
                     .setAutoCancel(true)
                     .setSound(defaultSoundUri)
                     .setContentIntent(pendingIntent);

        if (preferences.getBoolean(getString(R.string.preference_local_show_notification_light),
                                   true)) {
            notificationBuilder.setLights(Integer.decode(preferences.getString(
                    getString(R.string.preference_local_list_notification_color), "0x0000FF")), 400,
                                          600);
        }
        final NotificationManager notificationManager = (NotificationManager) getSystemService(
                Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

    private void sendFollowNotification(final String followerId, final String followerName)
    {
        final Intent intent = UserActivity.newIntent(this, followerId);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        final Bundle extra = intent.getExtras();
        extra.putString("notification", "notification");
        intent.putExtras(extra);
        final PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent,
                                                                      PendingIntent.FLAG_ONE_SHOT);
        final Uri defaultSoundUri = RingtoneManager.getDefaultUri(
                RingtoneManager.TYPE_NOTIFICATION);
        final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
                this).setSmallIcon(R.mipmap.cloo_launcher_logo)
                     .setContentTitle(getString(R.string.notification_title_follower))
                     .setContentText(String.format(getString(R.string.notification_body_follower),
                                                   followerName))
                     .setAutoCancel(true)
                     .setSound(defaultSoundUri)
                     .setContentIntent(pendingIntent);

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (preferences.getBoolean(getString(R.string.preference_local_show_notification_light),
                                   true)) {
            notificationBuilder.setLights(Integer.decode(preferences.getString(
                    getString(R.string.preference_local_list_notification_color), "0x0000FF")), 400,
                                          600);
        }
        final NotificationManager notificationManager = (NotificationManager) getSystemService(
                Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1, notificationBuilder.build());
    }
}
