package com.cloo.android.main.service.business.post;

import com.cloo.android.main.backend.database.DatabaseCodes;
import com.cloo.android.main.function.ExecutionCallback;

public interface PostDeletionService
{
    void deletePost(String userId, String postId, ExecutionCallback successCallback,
                    DatabaseCodes.DatabaseOperationErrorCallback errorCallback);

    void restorePost(String userId, String postId, ExecutionCallback successCallback,
                     DatabaseCodes.DatabaseOperationErrorCallback errorCallback);
}
