package com.cloo.android.main.ui.compose.user;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import com.cloo.android.databinding.FragmentListBinding;
import com.cloo.android.main.backend.database.DatabaseQuery;
import com.cloo.android.main.model.Post;
import com.cloo.android.main.model.User;
import com.cloo.android.main.ui.base.DatabaseQueryList;
import com.cloo.android.main.ui.base.DatabaseQueryListAdapter;
import com.cloo.android.main.ui.compose.postList.PostListAdapter;

public class CreatedPostsListFragment extends DatabaseQueryList<Post>
{
    private static final String USER_ID = "userId";

    @NonNull
    public static CreatedPostsListFragment newInstance(final String userId)
    {
        final Bundle bundle = new Bundle();
        bundle.putString(CreatedPostsListFragment.USER_ID, userId);
        final CreatedPostsListFragment fragment = new CreatedPostsListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public final void setupView(@NonNull final FragmentListBinding binding)
    {
        final String userId = getParameters().getString(CreatedPostsListFragment.USER_ID,
                                                        getAuthenticatedActivity().getAuthentication()
                                                                                  .getSession()
                                                                                  .getUserId());
        getAuthenticatedActivity().getDatabase().getCreatedPostsNode(userId).exists(exists -> {
            if (!exists) {
                getAuthenticatedActivity().getDatabase()
                                          .getUserDetailsNode(userId)
                                          .receive(User.class, (k, user) -> {
                                     if (user != null && !isDetached()) {
                                         user.setUserId(k);
                                         binding.listEmpty.setVisibility(View.VISIBLE);
                                         binding.emptyListText.setText(String.format(
                                                 "Too bad :(. Looks like %s hasn't created any posts yet.",
                                                 user.getUsername()));

                                     }
                                 })
                                          .recycleAfterUse()
                                          .submit();
            }
        }).recycleAfterUse().submit();
    }

    @Override
    public void setupPromotedAction(@NonNull final FloatingActionButton fab)
    {
        fab.setVisibility(View.GONE);
    }

    @NonNull
    @Override
    public final DatabaseQueryListAdapter<Post> createAdapter()
    {
        return new PostListAdapter(getAuthenticatedActivity());
    }

    @NonNull
    @Override
    public final DatabaseQuery createQuery(@NonNull final String endKey, final int count)
    {
        final String userId = getParameters().getString(CreatedPostsListFragment.USER_ID,
                                                        getAuthenticatedActivity().getAuthentication()
                                                                                  .getSession()
                                                                                  .getUserId());
        if (endKey.isEmpty()) {
            return getAuthenticatedActivity().getDatabase()
                                             .getCreatedPostsNode(userId)
                                             .limitToLast(count);
        }
        return getAuthenticatedActivity().getDatabase()
                                         .getCreatedPostsNode(userId)
                                         .limitToLast(count)
                                         .endAt(endKey);
    }
}