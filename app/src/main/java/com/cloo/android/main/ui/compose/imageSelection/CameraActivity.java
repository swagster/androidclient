package com.cloo.android.main.ui.compose.imageSelection;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.media.MediaActionSound;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import com.cloo.android.R;
import com.cloo.android.databinding.ActivityImageCaptureBinding;
import com.cloo.android.main.function.ReceiveCallback;
import com.cloo.android.main.injection.component.CameraActivityComponent;
import com.cloo.android.main.injection.component.DaggerCameraActivityComponent;
import com.cloo.android.main.injection.module.CameraServiceModule;
import com.cloo.android.main.service.business.camera.CameraService;
import com.cloo.android.main.service.business.camera.ClooCamera;
import com.cloo.android.main.ui.base.BaseActivity;
import com.cloo.android.main.ui.view.CameraPreview;
import com.cloo.android.main.util.AndroidUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import static android.view.View.GONE;
import static com.cloo.android.main.util.AndroidUtils.isSDCardNotMounted;
import static com.cloo.android.main.util.AndroidUtils.needsPermission;

/**
 * Activity can only be used, if the device has a at least one camera.
 */
public class CameraActivity extends BaseActivity
{
    private static final String TAG = CameraActivity.class.getSimpleName();
    private static final int EXTERNAL_STORAGE = 0;
    private static final int RESULT_CROP = 8;
    @Nullable private CameraService cameraService = null;
    @Nullable private ActivityImageCaptureBinding binding = null;
    @Nullable private CameraPreview preview = null;
    @Nullable private MediaActionSound sound = null;

    private boolean isFrontCamera = false;

    @Nullable
    private static File getImageFile()
    {
        if (isSDCardNotMounted()) {
            return null;
        }
        final File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "Cloo");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("CameraActivity", "failed to create directory");
                return null;
            }
        }
        final String timeStamp = DateFormat.getDateTimeInstance().format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
    }

    @Override
    protected final void onActivityResult(final int requestCode, final int resultCode,
                                          @NonNull final Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CameraActivity.RESULT_CROP && resultCode == Activity.RESULT_OK) {
            Log.w(CameraActivity.TAG,
                  "CameraActivity::onActivityResult received from ImageEditActivity URI:" + data.getData());
            setResult(resultCode, data);
            finish();
        }
    }

    @Override
    public final void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (needsPermissions()) {
            requestPermissions();
        }
        else {
            init();
        }
    }

    private void loadCameraSettings()
    {
        isFrontCamera = AndroidUtils.getUserSharedPreferences(this)
                                    .getBoolean(getString(R.string.preference_camera_active),
                                                false);
    }

    @Override
    protected final void onDestroy()
    {
        super.onDestroy();
        sound.release();
    }

    private void init()
    {
        final CameraActivityComponent cameraActivityComponent = DaggerCameraActivityComponent.builder()
                                                                                             .cameraServiceModule(
                                                                                                     new CameraServiceModule(
                                                                                                             this))
                                                                                             .build();
        cameraService = cameraActivityComponent.getCameraService();
        if (cameraService.hasCamera()) {
            binding = DataBindingUtil.setContentView(this, R.layout.activity_image_capture);
            if (cameraService.getCameraCount() == 1) {
                binding.button2.setVisibility(GONE);
            }

            if (AndroidUtils.isApkVersion16()) {
                final View decorView = getWindow().getDecorView();
                decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
            }
            //else {
            // TODO Hide actionbar for lower versions
            //}

            loadCameraSettings();
            if (AndroidUtils.isApkVersion16()) {
                sound = new MediaActionSound();
                sound.load(MediaActionSound.SHUTTER_CLICK);
            }
            final ReceiveCallback<ClooCamera> receiveCallback = camera -> {
                if (camera != null) {
                    preview = new CameraPreview(this, camera);
                    binding.cameraPreview.addView(preview);
                    binding.buttonCapture.setOnClickListener(v -> {
                        preview.getCamera().takePicture(this::saveCameraImage);
                        if (AndroidUtils.isApkVersion16()) {
                            sound.play(MediaActionSound.SHUTTER_CLICK);
                        }
                    });
                }
            };
            if (isFrontCamera) {
                cameraService.getFrontCamera(receiveCallback);
            }
            else {
                cameraService.getBackCamera(receiveCallback);
            }
        }
    }

    private boolean needsPermissions()
    {
        // If the image shouldn't be stored, it can be stored in temporary storage without a permission
        return (needToStoreImages() && needsPermission(this,
                                                       Manifest.permission.WRITE_EXTERNAL_STORAGE)) || needsPermission(
                this, Manifest.permission.CAMERA);
    }

    private void requestPermissions()
    {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                                                                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            getPermissionRationale().show();
        }
        else {
            ActivityCompat.requestPermissions(this,
                                              new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                                              CameraActivity.EXTERNAL_STORAGE);
        }
    }

    private AlertDialog getPermissionRationale()
    {
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Need Storage permission");
        alertBuilder.setMessage(
                "This lets Cloo store photos you take so you can use them as Post options.");
        alertBuilder.setPositiveButton(android.R.string.yes,
                                       (dialog, which) -> ActivityCompat.requestPermissions(
                                               CameraActivity.this,
                                               new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                                               CameraActivity.EXTERNAL_STORAGE));
        return alertBuilder.create();
    }

    @Override
    public final void onRequestPermissionsResult(final int requestCode,
                                                 @NonNull final String[] permissions,
                                                 @NonNull final int[] grantResults)
    {
        switch (requestCode) {
            case CameraActivity.EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    init();
                }
                else {
                    finish();
                }
                break;
            default:
                finish();
        }
    }

    @Override
    protected final void onStart()
    {
        super.onStart();
        applyCamera();
    }

    @Override
    protected final void onStop()
    {
        super.onStop();
        if (preview != null && preview.getCamera() != null) {
            preview.getCamera().stopPreview();
            preview.getCamera().release();
            preview.setCamera(null);
        }
    }

    private void saveCameraImage(@NonNull final byte[] data)
    {
        final File pictureFile = CameraActivity.getImageFile();
        if (pictureFile == null) {
            Log.d(CameraActivity.TAG, "Error creating media file, check storage permissions");
            getPermissionRationale().show();
            return;
        }

        final ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, pictureFile.getName());
        values.put(MediaStore.Images.Media.DESCRIPTION, "");
        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(MediaStore.Images.ImageColumns.BUCKET_ID,
                   pictureFile.toString().toLowerCase(Locale.US).hashCode());
        values.put(MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                   pictureFile.getName().toLowerCase(Locale.US));
        values.put("_data", pictureFile.getAbsolutePath());

        getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        try (final FileOutputStream fos = new FileOutputStream(pictureFile)) {
            fos.write(data);
        } catch (@NonNull final FileNotFoundException e) {
            Log.d(CameraActivity.TAG, "File not found: " + e.getMessage());
        } catch (@NonNull final IOException e) {
            Log.d(CameraActivity.TAG, "Error accessing file: " + e.getMessage());
        }
        final Intent intent = new Intent(this, ImageEditActivity.class);
        intent.setData(Uri.fromFile(pictureFile));
        startActivityForResult(intent, CameraActivity.RESULT_CROP);
    }

    private boolean needToStoreImages()
    {
        return getSharedPreferences().getBoolean("settings_persists_images", false);
    }

    @Override
    public final boolean dispatchKeyEvent(@NonNull final KeyEvent event)
    {
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                preview.getCamera().zoomIn();
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                preview.getCamera().zoomOut();
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }
    }

    public final void toggleFlashLight(@Nullable final View v)
    {
        if (preview.getCamera() == null) { return; }
        preview.getCamera().switchFlashMode();
        switch (preview.getCamera().getFlashMode()) {
            case ClooCamera.FLASH_MODE_AUTO:
                AndroidUtils.setImageDrawable(binding.cameraToggleFlash, this,
                                              R.drawable.v_icon_camera_flash_auto);
                break;
            case ClooCamera.FLASH_MODE_ON:
                AndroidUtils.setImageDrawable(binding.cameraToggleFlash, this,
                                              R.drawable.v_icon_camera_flash_on);
                break;
            default:
                AndroidUtils.setImageDrawable(binding.cameraToggleFlash, this,
                                              R.drawable.v_icon_camera_flash_off);
                break;
        }
    }

    public final void toggleCamera(final View v)
    {
        isFrontCamera = !isFrontCamera;
        applyCamera();
    }

    private void onReceiveCamera(@Nullable final ClooCamera camera)
    {
        if (camera != null) {
            preview.setCamera(camera);
            binding.setIsFrontCamera(isFrontCamera);
            binding.notifyChange();
        }
        // Couldn't aquire a camera, camera exists though
        else {
            Toast.makeText(this, "Try later :). Another app uses the camera currently.",
                           Toast.LENGTH_LONG).show();
        }
    }

    private void applyCamera()
    {
        if (preview == null) {
            return;
        }
        getSharedPreferences().edit()
                              .putBoolean(getString(R.string.preference_camera_active),
                                          isFrontCamera)
                              .apply();
        if (preview.getCamera() != null) {
            preview.getCamera().stopPreview();
            preview.getCamera().release();
        }
        if (isFrontCamera) {
            cameraService.getFrontCamera(this::onReceiveCamera);
        }
        else {
            cameraService.getBackCamera(this::onReceiveCamera);
        }
    }
}