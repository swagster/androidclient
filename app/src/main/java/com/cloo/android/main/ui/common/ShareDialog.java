package com.cloo.android.main.ui.common;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentShareDialogBinding;
import com.cloo.android.main.ui.base.BaseActivity;
import com.cloo.android.main.service.business.post.ShareService;
import com.cloo.android.main.service.business.post.ShareServiceImpl;
import com.cloo.android.main.ui.base.BaseDialog;
import com.cloo.android.main.util.AndroidUtils;

public class ShareDialog extends BaseDialog
{

    private static final String KEY_POST_ID = "postId";
    @NonNull
    private FragmentShareDialogBinding binding = null;

    @NonNull
    public static ShareDialog newInstance(final String postId) {
        final Bundle bundle = new Bundle();
        bundle.putString(ShareDialog.KEY_POST_ID, postId);
        final ShareDialog shareDialog = new ShareDialog();
        shareDialog.setArguments(bundle);
        return shareDialog;
    }

    @Override
    public final void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        onCreate(savedInstanceState);
        final Bundle bundle = savedInstanceState == null ? getArguments() : savedInstanceState;
        if (bundle == null || bundle.get(ShareDialog.KEY_POST_ID) == null) {
            dismiss();
        } else {
            setupViewWithArguments(bundle);
        }
    }

    private void onSharedFinished(final boolean hasShared) {
        if (hasShared) {
            dismiss();
        }
    }

    private void setupViewWithArguments(@NonNull final Bundle bundle) {
        binding.shareDialogReport.setOnClickListener(v -> {
            final ShareService share = new ShareServiceImpl(((BaseActivity) getActivity()).getDatabase());
            share.share(((BaseActivity) getActivity()).getAuthentication().getSession().getUserId(), bundle.getString(ShareDialog.KEY_POST_ID, ""), AndroidUtils.getString(binding.shareDialogComment), this::onSharedFinished);
        });
    }

    @Nullable
    @Override
    public final View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_share_dialog, container, false);
        binding.shareDialogCancel.setOnClickListener(v -> dismiss());
        return binding.getRoot();
    }
}
