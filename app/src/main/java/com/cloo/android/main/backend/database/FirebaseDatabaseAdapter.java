package com.cloo.android.main.backend.database;

import android.support.annotation.NonNull;

import com.google.firebase.database.FirebaseDatabase;

public class FirebaseDatabaseAdapter extends DatabaseAdapter
{
    public FirebaseDatabaseAdapter()
    {
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }

    @NonNull @Override public DatabaseNode getConnectionReference()
    {
        return FirebaseDatabaseNode.getConnectedReference();
    }

    @Override protected DatabaseNode get(@NonNull final String uri)
    {
        return FirebaseDatabaseNode.create(uri);
    }
}
