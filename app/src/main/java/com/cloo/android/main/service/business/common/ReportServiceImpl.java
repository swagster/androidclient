package com.cloo.android.main.service.business.common;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.authentication.Authentication;
import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.function.FinishCallback;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;

public class ReportServiceImpl implements ReportService
{
    private final Authentication authentication;
    private final DatabaseAdapter database;

    public ReportServiceImpl(final Authentication authentication, final DatabaseAdapter database)
    {
        this.authentication = authentication;
        this.database = database;
    }

    @Override
    public final void sendReport(final String reportedUser, final String contentId,
                                 final long category, @ReportContentType final long contentType,
                                 final String information, @NonNull final FinishCallback callback)
    {
        final Map<String, Object> map = toMap(contentType, category, information);
        database.getReportNode(reportedUser, contentId)
                .setChildren(map)
                .onSuccess(() -> callback.onFinish(true))
                .onError(error -> callback.onFinish(false))
                .recycleAfterUse()
                .submit();
    }

    @NonNull
    private Map<String, Object> toMap(@ReportContentType final long contentType,
                                      final long category, final String information)
    {
        final Map<String, Object> map = new HashMap<>();
        map.put("timestamp", ServerValue.TIMESTAMP);
        map.put("reporter", authentication.getSession().getUserId());
        map.put("information", information);
        map.put("category", category);
        map.put("contentType", contentType);
        return map;
    }
}