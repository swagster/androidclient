package com.cloo.android.main.ui.compose.user;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentListBinding;
import com.cloo.android.main.backend.database.DatabaseQuery;
import com.cloo.android.main.model.User;
import com.cloo.android.main.ui.base.DatabaseQueryList;
import com.cloo.android.main.ui.base.DatabaseQueryListAdapter;
import com.cloo.android.main.ui.common.AddByUsernameDialog;

public class FollowedUserListFragment extends DatabaseQueryList<User>
{
    @Nullable String userId = null;

    @Nullable
    @Override
    public final DatabaseQueryListAdapter<User> createAdapter()
    {
        return new UserListAdapter(getAuthenticatedActivity());
    }

    @Override
    public final void setupView(@NonNull final FragmentListBinding binding)
    {
        if (getParameters() != null) {
            userId = getParameters().getString("userId",
                                               getAuthenticatedActivity().getAuthentication()
                                                                         .getSession()
                                                                         .getUserId());
        }
        else {
            userId = getAuthenticatedActivity().getAuthentication().getSession().getUserId();
        }
    }

    @Override
    public void setupPromotedAction(@NonNull final FloatingActionButton fab)
    {
        if (getParameters() != null) {
            final String userId = getParameters().getString("userId",
                                                            getAuthenticatedActivity().getAuthentication()
                                                                                      .getSession()
                                                                                      .getUserId());
            if (getAuthenticatedActivity().getAuthentication()
                                          .getSession()
                                          .getUserId()
                                          .equals(userId)) {
                fab.hide();
                fab.setImageDrawable(getActivity().getDrawable(R.drawable.v_icon_add_user));
                fab.setOnClickListener(v -> {
                    final FragmentTransaction ft = getActivity().getSupportFragmentManager()
                                                                .beginTransaction();
                    ft.addToBackStack(null);
                    new AddByUsernameDialog().show(ft, "addAllImages-username");
                });
                fab.show();
            }
            else {
                fab.hide();
            }
        }
    }

    @Override
    protected void onAdded(@NonNull final String key, final Object nothing)
    {
        if (!key.equals(userId)) {
            super.onAdded(key, nothing);
        }
    }

    @NonNull
    @Override
    public final DatabaseQuery createQuery(final String endKey, final int count)
    {
        return getAuthenticatedActivity().getDatabase()
                                         .getFollowedUsersNode(userId)
                                         .limitToFirst(count);
    }
}
