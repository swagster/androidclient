package com.cloo.android.main.ui.compose.notification;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentListElementActivityBinding;
import com.cloo.android.main.backend.database.DatabaseNode;
import com.cloo.android.main.model.Notification;
import com.cloo.android.main.model.Post;
import com.cloo.android.main.model.User;
import com.cloo.android.main.ui.base.BaseActivity;
import com.cloo.android.main.ui.base.DatabaseQueryListAdapter;
import com.cloo.android.main.ui.compose.post.PostActivity;
import com.cloo.android.main.ui.compose.user.UserActivity;
import com.cloo.android.main.util.AndroidUtils;
import com.cloo.android.main.util.ListUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserActivitiesListAdapter extends DatabaseQueryListAdapter<Notification>
{
    @NonNull private final static Comparator<Notification> ACTIVITY_COMPARATOR_TIMESTAMP = (a, b) -> a
            .getTimestamp() > b.getTimestamp() ? -1 : 1;
    @NonNull private final LayoutInflater inflater;
    @NonNull private final List<Notification> activities = new ArrayList<>();
    @NonNull private final Map<String, List<String>> activityActors = new HashMap<>();
    @NonNull private final BaseActivity activity;

    public UserActivitiesListAdapter(@NonNull final BaseActivity activity)
    {
        this.activity = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public final int getCount()
    {
        return activities.size();
    }

    @Override
    public final Object getItem(final int i)
    {
        return activities.get(i);
    }

    @Override
    public final long getItemId(final int i)
    {
        return i;
    }

    private void addActivity(@NonNull final Notification notification)
    {
        for (int i = 0; i < activities.size(); i++) {
            if (activities.get(i).getId().equals(notification.getId())) {
                activities.set(i, notification);
                notifyDataSetChanged();
                return;
            }
        }
        activities.add(notification);
        Collections.sort(activities, ACTIVITY_COMPARATOR_TIMESTAMP);
        final List<String> actors = new ArrayList<>();
        final DatabaseNode activityActorsNode = activity.getDatabase()
                                                        .getActivityActorsNode(
                                                                activity.getAuthentication()
                                                                        .getSession()
                                                                        .getUserId(),
                                                                notification.getId());
        activityActorsNode.limitToLast(5)
                          .get(Object.class, (userId, o) -> actors.add(userId), error -> {
                          }, () -> {
                              activityActors.put(notification.getId(), actors);
                              notifyDataSetChanged();
                          });
    }

    @NonNull
    private FragmentListElementActivityBinding getBinding(@Nullable final View recycledView,
                                                          @Nullable final ViewGroup viewGroup)
    {
        if (recycledView == null) {
            return DataBindingUtil.inflate(inflater, R.layout.fragment_list_element_activity,
                                           viewGroup, false);
        }
        else {
            return (FragmentListElementActivityBinding) recycledView.getTag();
        }
    }

    @Override
    public final View getView(final int i, @Nullable final View view,
                              @Nullable final ViewGroup viewGroup)
    {
        final FragmentListElementActivityBinding binding = getBinding(view, viewGroup);
        binding.getRoot().setTag(binding);
        AndroidUtils.setImageDrawable(binding.profilImageWrapper.profilImage, activity,
                                      R.drawable.v_icon_user_picture_placeholder);
        final Notification notification = activities.get(i);
        final List<String> actors = activityActors.get(notification.getId());
        if (actors != null) {
            activity.getDatabase()
                    .getUserDetailsNode(actors.get(0))
                    .receive(User.class, (userId, user) -> {
                        if (user != null && ListUtils.isValidIndex(i, actors) && actors.get(i)
                                                                                       .equals(userId)) {
                            user.setId(userId);
                            if (!user.getProfileImageUrl().isEmpty()) {
                                binding.setImageUrl(user.getProfileImageUrl());
                            }
                            String actorMsg = user.getUsername() + " ";
                            if (activityActors.get(notification.getId()).size() > 1) {
                                actorMsg += "and " + (activityActors.get(notification.getId())
                                                                    .size() - 1) + " more people ";
                            }
                            if (notification.getContentType() == 0) {
                                actorMsg += "voted";
                            }
                            else if (notification.getContentType() == 1) {
                                actorMsg += "commented";
                            }
                            else {
                                actorMsg += "follows you";
                            }
                            binding.setActors(actorMsg);
                            binding.notifyChange();
                        }
                    })
                    .recycleAfterUse()
                    .submit();
            switch ((int) notification.getContentType()) {
                case 0:
                    binding.wrapper.setOnClickListener(v -> {
                        final Intent postActivityIntent = PostActivity.newIntent(activity,
                                                                                 notification.getActualContentId());
                        activity.startActivity(postActivityIntent);
                    });
                    activity.getDatabase()
                            .getPostDetailsNode(notification.getActualContentId())
                            .receive(Post.class, (postId, post) -> {
                                if (post != null) {
                                    binding.setMessage(post.getTitle());
                                }
                            })
                            .recycleAfterUse()
                            .submit();
                    break;
                default:
                    binding.wrapper.setOnClickListener(v -> {
                        final Intent postActivityIntent = UserActivity.newIntent(activity,
                                                                                 notification.getActualContentId());
                        activity.startActivity(postActivityIntent);
                    });
                    activity.getDatabase()
                            .getUserDetailsNode(notification.getActualContentId())
                            .receive(User.class, (postId, user) -> {
                                if (user != null) {
                                    binding.setMessage(user.getUsername());
                                }
                            })
                            .recycleAfterUse()
                            .submit();
            }
        }
        return binding.getRoot();
    }

    @Override
    public void addDataObject(@NonNull final String key)
    {
        activity.getDatabase()
                .getActivityDetailsNode(activity.getAuthentication().getSession().getUserId(), key)
                .receive(Notification.class, (k, a) -> {
                    if (a != null) {
                        a.setId(k);
                        addActivity(a);
                    }
                })
                .recycleAfterUse()
                .submit();
    }

    @Override
    public void removeDataObject(@NonNull final String key)
    {
        if (ListUtils.removeElement(param -> param.getId().equals(key), activities)) {
            notifyDataSetChanged();
        }
    }

    @Override
    @NonNull
    public Notification getDataObject(final int index)
    {
        return activities.get(index);
    }

    @Override
    public void recycle()
    {

    }

    @NonNull
    @Override
    public String getLastKey()
    {
        return activities.isEmpty() ? "" : activities.get(activities.size() - 1).getId();
    }
}
