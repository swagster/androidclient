package com.cloo.android.main.ui.compose.user;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.cloo.android.R;

public class UserTabsAdapter extends FragmentStatePagerAdapter {
    private static final int NUM_TABS = 3;
    @NonNull
    private final UserDetailsFragment userDetailsFragment;
    @NonNull
    private final CreatedPostsListFragment createdPostsListFragment;
    @NonNull
    private final VotedPostsListFragment votedPostsListFragment;
    @NonNull
    private final Context context;

    public UserTabsAdapter(@NonNull final FragmentManager fragmentManager, @NonNull final Context context, @NonNull final String userId) {
        super(fragmentManager);
        this.context = context;
        createdPostsListFragment = CreatedPostsListFragment.newInstance(userId);
        userDetailsFragment = UserDetailsFragment.newInstance(userId);
        votedPostsListFragment = VotedPostsListFragment.newInstance(userId);
    }

    @NonNull
    @Override
    public final CharSequence getPageTitle(final int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.title_user_details);
            case 1:
                return context.getString(R.string.title_user_voted_posts);
            case 2:
                return context.getString(R.string.title_user_created_posts);
            default:
                return context.getString(R.string.app_name);
        }
    }

    @NonNull
    @Override
    public final Fragment getItem(final int position) {
        switch (position) {
            case 0:
                return userDetailsFragment;
            case 1:
                return votedPostsListFragment;
            case 2:
                return createdPostsListFragment;
            default:
                return userDetailsFragment;
        }
    }

    @Override
    public final int getCount() {
        return UserTabsAdapter.NUM_TABS;
    }
}
