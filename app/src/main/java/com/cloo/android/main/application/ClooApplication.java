package com.cloo.android.main.application;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.cloo.android.BuildConfig;
import com.cloo.android.main.backend.authentication.Authentication;
import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.hacks.ActivityManagerDetacher;
import com.cloo.android.main.injection.component.ClooApplicationComponent;
import com.cloo.android.main.injection.component.DaggerClooApplicationComponent;
import com.cloo.android.main.service.util.image.ImageLoader;
import com.cloo.android.main.service.util.location.LocationTrackerService;
import com.cloo.android.main.service.util.location.LocationTrackerServiceImpl;
import com.cloo.android.main.util.OnCreateInitialized;
import com.crashlytics.android.Crashlytics;
import com.squareup.leakcanary.LeakCanary;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

public class ClooApplication extends Application
{
    @Nullable @OnCreateInitialized private static ClooApplication s_instance = null;
    @Nullable private LocationTrackerService locationTrackerService = null;
    @Nullable @OnCreateInitialized private ImageLoader imageLoader = null;
    @Nullable @OnCreateInitialized private DatabaseAdapter databaseAdapter = null;
    @Nullable @OnCreateInitialized private Authentication authentication = null;

    @NonNull
    public static ClooApplication getInstance(@NonNull final Context context)
    {
        return (ClooApplication) context.getApplicationContext();
    }

    @NonNull
    public static ClooApplication getS_instance()
    {
        return s_instance;
    }

    private static void setS_instance(final ClooApplication application)
    {
        s_instance = application;
    }

    @NonNull
    public final Authentication getAuthentication()
    {
        return authentication;
    }

    @NonNull
    public final DatabaseAdapter getDatabase()
    {
        return databaseAdapter;
    }

    @NonNull
    public final ImageLoader getImageLoader()
    {
        return imageLoader;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        Fabric.with(this, new Crashlytics());
        if (BuildConfig.DEBUG) {
            if (LeakCanary.isInAnalyzerProcess(this)) {
                return;
            }
            LeakCanary.install(this);
            StrictMode.setThreadPolicy(
                    new StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectLeakedSqlLiteObjects()
                                                                    .detectLeakedClosableObjects()
                                                                    .detectActivityLeaks()
                                                                    .detectLeakedRegistrationObjects()
                                                                    .penaltyLog()
                                                                    .build());
            Timber.plant(new Timber.DebugTree());
        }

        setS_instance(this);
        final ClooApplicationComponent applicationComponent = DaggerClooApplicationComponent.builder()
                                                                                            .build();

        imageLoader = applicationComponent.getImageLoader();
        databaseAdapter = applicationComponent.getDatabase();
        authentication = applicationComponent.getAuthentication();

        // Apply platform fixes and hacks
        registerActivityLifecycleCallbacks(new ActivityManagerDetacher());

        Timber.d("Finished onCreate");
    }

    public void setupLocationTracker()
    {
        if (locationTrackerService != null) {
            locationTrackerService.stop();
        }
        locationTrackerService = new LocationTrackerServiceImpl(this);
    }
}
