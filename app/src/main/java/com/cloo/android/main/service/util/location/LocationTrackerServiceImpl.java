package com.cloo.android.main.service.util.location;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import com.cloo.android.R;
import com.cloo.android.main.application.ClooApplication;
import com.cloo.android.main.function.ReceiveCallback;
import com.cloo.android.main.util.AndroidUtils;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

import static android.content.Context.LOCATION_SERVICE;

public class LocationTrackerServiceImpl implements LocationTrackerService, LocationListener, SharedPreferences.OnSharedPreferenceChangeListener
{
    @NonNull private final LocationManager mLocationManager;
    @NonNull private final ClooApplication application;
    @Nullable private Location location = null;

    public LocationTrackerServiceImpl(@NonNull final ClooApplication context)
    {
        application = context;
        mLocationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        start();
    }

    private void start()
    {
        PreferenceManager.getDefaultSharedPreferences(application)
                         .registerOnSharedPreferenceChangeListener(this);
        if (!AndroidUtils.getUserSharedPreferences(application)
                         .getBoolean(application.getString(
                                 R.string.preference_local_switch_track_position),
                                     true) || ActivityCompat.checkSelfPermission(application,
                                                                                 Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat
                .checkSelfPermission(application,
                                     Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 300000, 0, this);
    }

    @Override
    public void getLatestLocation(@NonNull final ReceiveCallback<Location> location)
    {
        if (this.location != null) { location.onReceive(this.location); }
    }

    @Override
    public void stop()
    {
        if (mLocationManager != null) { mLocationManager.removeUpdates(this); }
        PreferenceManager.getDefaultSharedPreferences(application)
                         .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onLocationChanged(@NonNull final Location location)
    {
        this.location = location;
        final Map<String, Object> positionMap = new HashMap<>();
        positionMap.put("lon", location.getLongitude());
        positionMap.put("lat", location.getLatitude());
        positionMap.put("timestamp", ServerValue.TIMESTAMP);
        application.getDatabase()
                   .getUserPositionNode(application.getAuthentication().getSession().getUserId())
                   .setChildren(positionMap)
                   .recycleAfterUse()
                   .submit();
    }

    @Override
    public void onStatusChanged(final String s, final int i, final Bundle bundle)
    {

    }

    @Override
    public void onProviderEnabled(final String s)
    {

    }

    @Override
    public void onProviderDisabled(final String s)
    {

    }

    @Override
    public void onSharedPreferenceChanged(@NonNull final SharedPreferences sharedPreferences,
                                          @NonNull final String s)
    {
        Timber.d("SharedPref changed " + s);
        if (s.equals(application.getString(R.string.preference_local_switch_track_position))) {
            stop();
            if (sharedPreferences.getBoolean(s, true)) {
                start();
            }
        }
    }
}
