package com.cloo.android.main.ui.compose.user;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;

import com.cloo.android.databinding.FragmentListBinding;
import com.cloo.android.main.backend.database.DatabaseQuery;
import com.cloo.android.main.model.Post;
import com.cloo.android.main.ui.base.DatabaseQueryList;
import com.cloo.android.main.ui.base.DatabaseQueryListAdapter;
import com.cloo.android.main.ui.compose.postList.PostListAdapter;

import static android.view.View.GONE;

public class VotedPostsListFragment extends DatabaseQueryList<Post> {
    private static final String USER_ID = "userID";

    @NonNull
    public static VotedPostsListFragment newInstance(final String userId) {
        final Bundle bundle = new Bundle();
        bundle.putString(VotedPostsListFragment.USER_ID, userId);
        final VotedPostsListFragment fragment = new VotedPostsListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @NonNull
    @Override
    public final DatabaseQueryListAdapter<Post> createAdapter() {
        return new PostListAdapter(getAuthenticatedActivity());
    }

    @Override
    public final void setupView(@NonNull final FragmentListBinding binding) {
    }

    @Override
    public void setupPromotedAction(@NonNull final FloatingActionButton fab) {
        fab.setVisibility(GONE);
    }

    @NonNull
    @Override
    public final DatabaseQuery createQuery(@Nullable final String endKey, final int count)
    {
        final String userId = getParameters() == null ? getAuthenticatedActivity().getAuthentication()
                                                                                  .getSession()
                                                                                  .getUserId() : getParameters()
                .getString(VotedPostsListFragment.USER_ID,
                           getAuthenticatedActivity().getAuthentication().getSession().getUserId());
        return endKey.isEmpty() ? getAuthenticatedActivity().getDatabase()
                                                            .getVotedOnPostsNode(userId)
                                                            .limitToLast(
                                                                    count) : getAuthenticatedActivity()
                .getDatabase()
                .getVotedOnPostsNode(userId)
                .limitToLast(count)
                .endAt(endKey);
    }
}