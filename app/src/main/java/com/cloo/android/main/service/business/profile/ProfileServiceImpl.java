package com.cloo.android.main.service.business.profile;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.SparseArray;

import com.cloo.android.main.application.ClooApplication;
import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.backend.database.DatabaseCodes;
import com.cloo.android.main.backend.storage.Storage;
import com.cloo.android.main.function.ExecutionCallback;

import timber.log.Timber;

public class ProfileServiceImpl implements ProfileService
{
    @NonNull private final ClooApplication context;
    private final Storage storage;
    private final UsernameService usernameService;
    private final DatabaseAdapter databaseAdapter;

    public ProfileServiceImpl(@NonNull final Context context, final Storage storage,
                              final DatabaseAdapter databaseAdapter,
                              final UsernameService usernameService)
    {
        this.context = (ClooApplication) context.getApplicationContext();
        this.storage = storage;
        this.databaseAdapter = databaseAdapter;
        this.usernameService = usernameService;
    }

    public void changeDescription(final String userId, @NonNull final String description,
                                  @NonNull final ExecutionCallback successCallback,
                                  final DatabaseCodes.DatabaseOperationErrorCallback errorCallback)
    {
        databaseAdapter.getUserDetailDescriptionNode(userId)
                       .receive(String.class, (s, oldDescription) -> {
                           if (oldDescription == null || !description.equals(oldDescription)) {
                               databaseAdapter.getUserDetailDescriptionNode(userId)
                                              .set(description)
                                              .onSuccess(successCallback)
                                              .onError(errorCallback)
                                              .recycleAfterUse()
                                              .submit();
                           }
                           else {
                               Timber.d("User description wasn't changed, success");
                               successCallback.execute();
                           }
                       })
                       .onError(errorCallback)
                       .recycleAfterUse()
                       .submit();
    }

    public void changeProfileImage(final String userId, final String profileUrl,
                                   final ExecutionCallback successCallback,
                                   @NonNull final DatabaseCodes.DatabaseOperationErrorCallback errorCallback)
    {
        storage.uploadUserProfileImage(context, Uri.parse(profileUrl),
                                       (storageKey, downloadUrl) -> {
                                           if (downloadUrl != null) {
                                               Timber.d("Uploaded profileImage download URL %s",
                                                        downloadUrl.toString());
                                               databaseAdapter.getUserDetailImageNode(userId)
                                                              .set(downloadUrl.toString())
                                                              .onSuccess(successCallback)
                                                              .onError(
                                                                      error -> errorCallback.onError(
                                                                              DatabaseCodes.OPERATION_UNAVAILABLE))
                                                              .recycleAfterUse()
                                                              .submit();
                                           }
                                           else {
                                               Log.d("ProfileService", "Failed to upload image");
                                               errorCallback.onError(
                                                       DatabaseCodes.OPERATION_UNAVAILABLE);
                                           }
                                       });
    }

    private void changeUsername(final String username, final ExecutionCallback successCallback,
                                final DatabaseCodes.DatabaseOperationErrorCallback errorCallback)
    {
        usernameService.changeUsername(context.getAuthentication().getSession().getUserId(),
                                       username, successCallback, errorCallback);
    }

    @Override
    public void updateProfileInformations(@NonNull final ExecutionCallback successCallback,
                                          @NonNull final DatabaseCodes.DatabaseOperationErrorCallback errorCallback,
                                          @NonNull final SparseArray<String> informations)
    {
        if (informations.size() == 0) {
            successCallback.execute();
            return;
        }

        changeUsername(informations.get(ProfileService.PROFILE_USERNAME), () -> {
            if (!Uri.parse(informations.get(ProfileService.PROFILE_PICTURE)).equals(Uri.EMPTY)) {
                storage.uploadUserProfileImage(context, Uri.parse(
                        informations.get(ProfileService.PROFILE_PICTURE)),
                                               (storageKey, downloadUrl) -> {
                                                   if (downloadUrl != null) {
                                                       if (!downloadUrl.equals(Uri.EMPTY)) {
                                                           databaseAdapter.getUserDetailImageNode(
                                                                   context.getAuthentication()
                                                                          .getSession()
                                                                          .getUserId())
                                                                          .set(downloadUrl.toString())
                                                                          .onSuccess(
                                                                                  successCallback)
                                                                          .onError(errorCallback)
                                                                          .recycleAfterUse()
                                                                          .submit();
                                                       }
                                                       else {
                                                           successCallback.execute();
                                                       }
                                                   }
                                                   else {
                                                       errorCallback.onError(
                                                               DatabaseCodes.NETWORK_ERROR);
                                                   }
                                               });
                successCallback.execute();
            }
            else {
                successCallback.execute();
            }
        }, errorCallback);
    }
}
