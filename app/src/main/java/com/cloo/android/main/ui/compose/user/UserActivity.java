package com.cloo.android.main.ui.compose.user;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;

import com.cloo.android.R;
import com.cloo.android.databinding.ActivityUserBinding;
import com.cloo.android.main.application.ClooApplication;
import com.cloo.android.main.backend.database.DatabaseNode;
import com.cloo.android.main.injection.component.DaggerUserActivityComponent;
import com.cloo.android.main.injection.component.UserActivityComponent;
import com.cloo.android.main.injection.module.DatabaseInstanceModule;
import com.cloo.android.main.model.User;
import com.cloo.android.main.service.business.follow.FollowService;
import com.cloo.android.main.service.business.follow.FollowsListener;
import com.cloo.android.main.service.business.notification.NotificationBadgeService;
import com.cloo.android.main.service.business.notification.NotificationBadgeServiceImpl;
import com.cloo.android.main.ui.base.AuthenticatedActivity;
import com.cloo.android.main.ui.compose.profile.DeletedPostsActivity;
import com.cloo.android.main.util.Assert;
import com.cloo.android.main.util.OnCreateInitialized;

public class UserActivity extends AuthenticatedActivity
{
    private static final String USER_ID = "userId";

    private String userId;

    @Nullable @OnCreateInitialized private ActivityUserBinding binding = null;
    @Nullable @OnCreateInitialized private DatabaseNode userRemote = null;
    @Nullable @OnCreateInitialized private FollowService followService = null;
    @Nullable @OnCreateInitialized private FollowsListener followsListener = null;

    @NonNull
    public static Intent newIntent(final Context context, final String userId)
    {
        final Intent intent = new Intent(context, UserActivity.class);
        final Bundle bundle = new Bundle();
        bundle.putString(UserActivity.USER_ID, userId);
        intent.putExtras(bundle);
        return intent;
    }

    private String getUserId()
    {
        if (userId == null) {
            final Intent appLinkIntent = getIntent();
            final String appLinkAction = appLinkIntent.getAction();
            if (appLinkAction != null) {
                final Uri appLinkData = appLinkIntent.getData();
                userId = appLinkData.getLastPathSegment();
            }
            else if (getIntent().getExtras() == null) {
                userId = getAuthentication().getSession().getUserId();
            }
            else {
                userId = getIntent().getExtras()
                                    .getString(UserActivity.USER_ID,
                                               getAuthentication().getSession().getUserId());
            }
        }
        return userId;
    }

    @Override
    protected final void onDestroy()
    {
        super.onDestroy();
        userRemote.recycle();
    }

    @Override
    protected final void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = setBindingView(R.layout.activity_user);
        binding.pager.setOffscreenPageLimit(2);
        if (getIntent().hasExtra("notification")) {
            final NotificationBadgeService notificationBadgeService = new NotificationBadgeServiceImpl(
                    ClooApplication.getInstance(this));
            notificationBadgeService.decrementNotificationBadgeCount(1);
        }
        final UserActivityComponent userActivityComponent = DaggerUserActivityComponent.builder()
                                                                                       .databaseInstanceModule(
                                                                                               new DatabaseInstanceModule(
                                                                                                       getDatabase()))
                                                                                       .build();
        followService = userActivityComponent.getFollowService();
        final UserTabsAdapter adapter = new UserTabsAdapter(getSupportFragmentManager(),
                                                            getApplicationContext(), getUserId());
        binding.pager.setAdapter(adapter);
        binding.tabs.setupWithViewPager(binding.pager);
        if (getUserId().equals(getAuthentication().getSession().getUserId())) {
            setTitle(getString(R.string.title_user_profil_you));
        }
        followsListener = new FollowsListener(getDatabase(),
                                              getAuthentication().getSession().getUserId(),
                                              getUserId());
        userRemote = getDatabase().getUserDetailsNode(getUserId());
        userRemote.onUpdates(User.class, this::onUserUpdate).submit();
        setupAbortButton(binding.toolbar);
    }

    private void onUserUpdate(@NonNull final String id, @Nullable final User user)
    {
        if (user != null) {
            user.setUserId(id);
            receiveUser(user);
        }
    }

    private void receiveUser(@NonNull final User user)
    {
        Assert.check(getUserId().equals(user.getUserId()), "Received wrong user in Notification");
        binding.setUser(user);
        if (!user.getUsername().isEmpty() && !user.getUserId()
                                                  .equals(getAuthentication().getSession()
                                                                             .getUserId())) {
            setTitle(String.format(getString(R.string.title_user_profile_other),
                                   user.getUsername()));
        }
    }

    @Override
    public final boolean onOptionsItemSelected(@NonNull final MenuItem item)
    {
        switch (item.getItemId()) {
            case R.id.menu_user_details_unfollow:
                followService.follow(getAuthentication().getSession().getUserId(), getUserId(),
                                     success -> {
                                     });
                return true;
            case R.id.menu_user_details_deleted:
                startActivity(new Intent(this, DeletedPostsActivity.class));
                return true;
            case R.id.menu_user_details_share:
                startActivity(getClooUserActivityLinkIntent());
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @NonNull
    private Intent getClooUserActivityLinkIntent()
    {
        final Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "http://www.theclooapp.com/user/" + getUserId());
        sendIntent.setType("text/plain");
        return sendIntent;
    }

    @Override
    public final boolean onCreateOptionsMenu(@NonNull final Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_user_details, menu);
        menu.getItem(0).setVisible(false);
        menu.getItem(2).setVisible(false);
        if (getUserId().equals(getAuthenticatedUserId())) {
            getDatabase().getDeletedPostsNode(getUserId())
                         .exists(menu.getItem(0)::setVisible).recycleAfterUse()
                         .submit();
        }
        followsListener.setFollowChangeCallback(follows -> menu.getItem(2).setVisible(follows));
        followService.isFollower(getAuthentication().getSession().getUserId(), getUserId(),
                                 isFollower -> {
                                     if (isFollower) { menu.getItem(2).setVisible(true);}
                                 });
        return super.onCreateOptionsMenu(menu);
    }
}
