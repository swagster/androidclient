package com.cloo.android.main.injection.module;

import com.cloo.android.main.backend.database.DatabaseAdapter;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseInstanceModule
{
    private final DatabaseAdapter databaseAdapter;

    public DatabaseInstanceModule(final DatabaseAdapter databaseAdapter)
    {
        this.databaseAdapter = databaseAdapter;
    }

    @Provides
    public DatabaseAdapter databaseAdapter()
    {
        return databaseAdapter;
    }
}
