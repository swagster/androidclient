package com.cloo.android.main.ui.compose.profile;

import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;

import com.cloo.android.databinding.FragmentListBinding;
import com.cloo.android.main.backend.database.DatabaseQuery;
import com.cloo.android.main.model.Post;
import com.cloo.android.main.ui.base.DatabaseQueryList;
import com.cloo.android.main.ui.base.DatabaseQueryListAdapter;

public class DeletedPostsFragment extends DatabaseQueryList<Post>
{
    @Override
    protected void setupView(@NonNull final FragmentListBinding binding)
    {

    }

    @Override
    public void setupPromotedAction(@NonNull final FloatingActionButton fab)
    {

    }

    @NonNull
    @Override
    protected DatabaseQueryListAdapter<Post> createAdapter()
    {
        return new DeletedPostsAdapter(getAuthenticatedActivity());
    }

    @NonNull
    @Override
    protected DatabaseQuery createQuery(final String endKey, final int count)
    {
        return getAuthenticatedActivity().getDatabase()
                                         .getDeletedPostsNode(
                                                 getAuthenticatedActivity().getAuthenticatedUserId())
                                         .limitToLast(count);
    }
}
