package com.cloo.android.main.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.facebook.AccessToken;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;

public class FacebookCredential implements ParcelableCredential
{
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator()
    {
        @Override
        @NonNull
        public FacebookCredential createFromParcel(@NonNull final Parcel in)
        {
            return new FacebookCredential(in);
        }

        @Override
        @NonNull
        public FacebookCredential[] newArray(final int size)
        {
            return new FacebookCredential[size];
        }
    };
    @NonNull
    private final AccessToken token;

    private FacebookCredential(@NonNull final Parcel parcel)
    {
        token = parcel.readParcelable(ClassLoader.getSystemClassLoader());
    }

    public FacebookCredential(@NonNull final AccessToken token)
    {
        this.token = token;
    }

    @NonNull
    @Override
    public final AuthCredential getCredential()
    {
        return FacebookAuthProvider.getCredential(token.getToken());
    }

    @Override
    public final int describeContents()
    {
        return 0;
    }

    @Override
    public final void writeToParcel(@NonNull final Parcel dest, final int flags)
    {
        dest.writeParcelable(token, flags);
    }
}
