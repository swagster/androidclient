package com.cloo.android.main.service.business.post;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.backend.database.DatabaseCodes;
import com.cloo.android.main.function.ExecutionCallback;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;

public class PostDeletionServiceImpl implements PostDeletionService
{
    private final DatabaseAdapter databaseAdapter;

    public PostDeletionServiceImpl(final DatabaseAdapter databaseAdapter)
    {
        this.databaseAdapter = databaseAdapter;
    }

    @Override
    public void deletePost(final String userId, @NonNull final String postId,
                           final ExecutionCallback successCallback,
                           final DatabaseCodes.DatabaseOperationErrorCallback errorCallback)
    {
        final Map<String, Object> deletedPost = new HashMap<>(1);
        deletedPost.put(postId, ServerValue.TIMESTAMP);
        databaseAdapter.getDeletedPostsNode(userId)
                       .setChildren(deletedPost)
                       .onSuccess(() -> databaseAdapter.getPostDetailsNode(postId)
                                                       .child("deleted")
                                                       .set(true)
                                                       .onSuccess(
                                                               () -> databaseAdapter.getAllPostsNode(
                                                                       postId)
                                                                                    .delete()
                                                                                    .onError(
                                                                                            errorCallback)
                                                                                    .onSuccess(
                                                                                            successCallback)
                                                                                    .recycleAfterUse()
                                                                                    .submit())
                                                       .onError(errorCallback)
                                                       .recycleAfterUse()
                                                       .submit())
                       .onError(errorCallback)
                       .recycleAfterUse()
                       .submit();
    }

    @Override
    public void restorePost(final String userId, @NonNull final String postId,
                            final ExecutionCallback successCallback,
                            final DatabaseCodes.DatabaseOperationErrorCallback errorCallback)
    {

        databaseAdapter.getDeletedPostsNode(userId)
                       .child(postId)
                       .delete()
                       .onSuccess(() -> databaseAdapter.getPostDetailsNode(postId)
                                                       .child("deleted")
                                                       .set(false)
                                                       .onSuccess(successCallback)
                                                       .onError(errorCallback)
                                                       .recycleAfterUse()
                                                       .submit())
                       .onError(errorCallback)
                       .recycleAfterUse()
                       .submit();
    }
}
