package com.cloo.android.main.service.business.common;

import android.support.annotation.IntDef;
import android.support.annotation.NonNull;

import com.cloo.android.main.function.FinishCallback;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@FunctionalInterface
public interface ReportService {
    int CONTENT_TYPE_POST = 0;
    int CONTENT_TYPE_COMMENT = 1;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({
            ReportService.CONTENT_TYPE_POST,
            ReportService.CONTENT_TYPE_COMMENT
    })
    @interface ReportContentType {
    }

    void sendReport(final String reportedUser, final String contentId,
                    final long category, @ReportServiceImpl.ReportContentType final long contentType,
                    final String information, @NonNull final FinishCallback callback);
}
