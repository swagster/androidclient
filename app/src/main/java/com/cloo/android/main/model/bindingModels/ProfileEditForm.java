package com.cloo.android.main.model.bindingModels;

import android.databinding.ObservableField;

public class ProfileEditForm
{
    public final ObservableField<String> username = new ObservableField<>("");
    public final ObservableField<String> description = new ObservableField<>("");
}
