package com.cloo.android.main.backend.authentication;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;

import com.cloo.android.main.function.FinishCallback;
import com.cloo.android.main.function.ReceiveCallback;
import com.cloo.android.main.model.ParcelableCredential;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Collection;

public interface Authentication
{
    String PROVIDER_EMAIL_PASSWORD = "password";
    String PROVIDER_GOOGLE = "google.com";
    String PROVIDER_FACEBOOK = "facebook.com";
    int TOO_MANY_REQUESTS = 0;
    int CONNECTION_FAILED = 1;

    void registerEmailPassword(@NonNull String email, @NonNull String password,
                               @NonNull FinishCallback listener);

    void removeLoginChangeListener(@NonNull FinishCallback listener);

    void addLoginChangeListener(@NonNull FinishCallback listener);

    boolean isLoggedIn();

    void signOut();

    void getProviders(@NonNull String email, @NonNull ReceiveCallback<Collection<String>> listener);

    void signIn(@NonNull ParcelableCredential credential,
                @Nullable ParcelableCredential providerCredentials,
                @NonNull ReceiveCallback<Exception> listener);

    void linkCredentials(@NonNull ParcelableCredential credential);

    boolean hasEmailPasswordProvider(@NonNull Collection<String> providers);

    boolean hasFacebookProvider(@NonNull Collection<String> providers);

    void sendPasswordReset(String email, FinishCallback onSuccess,
                           final ReceiveCallback<Integer> onError);

    void removeProvider(@AuthProviderConstant String provider,
                        ReceiveCallback<Boolean> successCallback);

    @NonNull
    Session getSession();

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({PROVIDER_EMAIL_PASSWORD, PROVIDER_FACEBOOK, PROVIDER_GOOGLE})
    @interface AuthProviderConstant
    {}
}
