package com.cloo.android.main.ui.base;

import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.ViewGroup;

public abstract class BaseDialog extends DialogFragment
{
    @Nullable private BaseActivity activity = null;

    @Nullable
    public BaseActivity getParentActivity()
    {
        if (getActivity() == null) {
            return null;
        }
        if (activity == null) {
            activity = (BaseActivity) getActivity();
        }
        return activity;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        getDialog().getWindow()
                   .setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                              ViewGroup.LayoutParams.WRAP_CONTENT);
    }
}
