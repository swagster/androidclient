package com.cloo.android.main.ui.common;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentAddByUsernameBinding;
import com.cloo.android.main.ui.base.BaseActivity;
import com.cloo.android.main.model.User;
import com.cloo.android.main.ui.base.BaseDialog;
import com.cloo.android.main.ui.compose.user.UserActivity;
import com.cloo.android.main.util.AndroidUtils;

public class AddByUsernameDialog extends BaseDialog
{
    @Nullable private FragmentAddByUsernameBinding binding = null;


    @Nullable
    @Override
    public final View onCreateView(@NonNull final LayoutInflater inflater,
                                   @Nullable final ViewGroup container,
                                   @Nullable final Bundle savedInstanceState)
    {
        final BaseActivity activity = (BaseActivity) getActivity();
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_by_username, container,
                                          false);
        binding.addByUsernameCancel.setOnClickListener(v -> dismiss());
        binding.addByUsernameAction.setOnClickListener(v -> {
            final String username = AndroidUtils.getString(binding.addByUsernameText);
            if (username.isEmpty()) {
                binding.addByUsernameText.setError(
                        "Hey, what username has the person you're looking for?");
            }
            else {
                activity.getDatabase().getUsernameNode(username).exists(exists -> {
                    if (!exists) {
                        Toast.makeText(getContext(), "Sorry, there's no user with that username :(",
                                       Toast.LENGTH_LONG).show();
                    }
                    else {
                        activity.getDatabase()
                                .getUsernameNode(username)
                                .receive(String.class, (k, userId) -> activity.getDatabase()
                                                                              .getUserDetailsNode(
                                                                                      userId)
                                                                              .receive(User.class,
                                                                                       (id, user) -> {
                                                                                           if (user != null && user
                                                                                                   .getUsername()
                                                                                                   .equals(username)) {
                                                                                               final Intent intent = new Intent(
                                                                                                       getActivity(),
                                                                                                       UserActivity.class);
                                                                                               intent.putExtra(
                                                                                                       "userId",
                                                                                                       userId);
                                                                                               startActivity(
                                                                                                       intent);
                                                                                               dismiss();
                                                                                           }
                                                                                       })
                                                                              .recycleAfterUse()
                                                                              .submit())
                                .recycleAfterUse()
                                .submit();
                    }
                }).submit();
            }
        });
        return binding.getRoot();
    }
}
