package com.cloo.android.main.ui.binding;

import android.databinding.BindingAdapter;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.AbsListView;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.cloo.android.main.application.ClooApplication;
import com.cloo.android.main.service.util.image.ImageLoader;

public final class CustomBindings
{
    private CustomBindings()
    {
        // Utility class
    }

    @BindingAdapter("imageUrl")
    public static void bitmapSource(@NonNull final ImageView view, @Nullable final String uri)
    {
        if (uri != null && !uri.equals(view.getTag())) {
            final ImageLoader loader = ClooApplication.getInstance(view.getContext()).getImageLoader();
            loader.clear(view);
            if (!uri.isEmpty()) {
                loader.load(uri, view);
            }
            view.setTag(uri);
        }
    }

    @BindingAdapter("adapter")
    public static void adapterObject(@NonNull final AdapterView view, final Adapter adapter)
    {
        view.setAdapter(adapter);
    }

    @BindingAdapter("scrollListener")
    public static void scrollListener(@NonNull final AbsListView view, final AbsListView.OnScrollListener listener)
    {
        view.setOnScrollListener(listener);
    }
}
