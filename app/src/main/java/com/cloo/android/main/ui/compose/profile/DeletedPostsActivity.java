package com.cloo.android.main.ui.compose.profile;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.cloo.android.R;
import com.cloo.android.databinding.ActivityDeletedPostsBinding;
import com.cloo.android.main.ui.base.AuthenticatedActivity;

public class DeletedPostsActivity extends AuthenticatedActivity
{

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        final ActivityDeletedPostsBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_deleted_posts);
        final DeletedPostsFragment fragment = new DeletedPostsFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.list_container, fragment).commit();

        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }
}
