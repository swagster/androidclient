package com.cloo.android.main.service.business.post;

import android.support.annotation.NonNull;

import com.cloo.android.main.function.ReceiveCallback;
import com.cloo.android.main.model.Post;

public interface VoteService {
    void voteOnPost(@NonNull final Post post, final int option);

    void getVotedOption(final String postId, ReceiveCallback<Integer> receivedOption);
}
