package com.cloo.android.main.service.business.post;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.database.DatabaseCodes;
import com.cloo.android.main.function.ReceiveCallback;
import com.cloo.android.main.model.PostOption;

import java.util.List;

public interface PostCreationService {
    /**
     * Creates a post object on the backend with the given title, category, description and options.
     * <p>
     * On Success, the given callback is executed with the postId of the newly created post as an argument.
     * <p>
     * On Failure, the given errorCallback is executed with an error code.
     */
    void createPost(String userId, final int category,
                    @NonNull final String title, @NonNull final String description,
                    @NonNull final List<PostOption> options,
                    @NonNull final ReceiveCallback<String> successCallback,
                    @NonNull final DatabaseCodes.DatabaseOperationErrorCallback errorCallback);

    /**
     * @return True, if the given String can be used as a post title
     */
    boolean isValidTitle(@NonNull final String title);

    boolean isValidDescription(@NonNull final String description);

    boolean areValidOptions(@NonNull final List<PostOption> options);
}
