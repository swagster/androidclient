package com.cloo.android.main.injection.module;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.authentication.Authentication;
import com.cloo.android.main.backend.authentication.FirebaseAuthentication;

import dagger.Module;
import dagger.Provides;

@Module
public class AuthenticationModule
{
    @NonNull
    @Provides
    public Authentication authentication()
    {
        return new FirebaseAuthentication();
    }
}
