package com.cloo.android.main.service.business.follow;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.backend.database.DatabaseNode;
import com.cloo.android.main.function.FinishCallback;
import com.cloo.android.main.function.ReceiveCallback;

public class FirebaseFunctionFollowService implements FollowService
{
    private final DatabaseAdapter database;

    public FirebaseFunctionFollowService(final DatabaseAdapter database)
    {
        this.database = database;
    }

    @Override
    public void follow(@NonNull final String follower, @NonNull final String followedUser,
                       @NonNull final FinishCallback onFinish)
    {
        final DatabaseNode reference = database.getFollowedUserNode(follower, followedUser);
        reference.exists(exists -> {
            if (!exists) {
                followUser(follower, followedUser, onFinish);
            }
            else {
                unfollowUser(follower, followedUser, onFinish);
            }
        }).onError(error -> onFinish.onFinish(false)).submit();
    }

    private void followUser(@NonNull final String follower, @NonNull final String userId,
                            @NonNull final FinishCallback onFinish)
    {
        database.getFollowerNode(userId, follower)
                .set(true)
                .onError(error -> onFinish.onFinish(false))
                .onSuccess(() -> onFinish.onFinish(true))
                .recycleAfterUse()
                .submit();
    }

    private void unfollowUser(@NonNull final String follower, @NonNull final String userId,
                              @NonNull final FinishCallback onFinish)
    {
        database.getFollowerNode(userId, follower)
                .delete()
                .onSuccess(() -> onFinish.onFinish(true))
                .onError(error -> onFinish.onFinish(false))
                .recycleAfterUse()
                .submit();
    }

    @Override
    public void isFollower(@NonNull final String follower, @NonNull final String followedUser,
                           @NonNull final ReceiveCallback<Boolean> isFollower)
    {

        final DatabaseNode reference = database.getFollowedUserNode(follower, followedUser);
        reference.exists(isFollower).onError(error -> isFollower.onReceive(false)).submit();
    }
}
