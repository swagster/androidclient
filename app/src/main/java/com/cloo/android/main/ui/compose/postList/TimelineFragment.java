package com.cloo.android.main.ui.compose.postList;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentListBinding;
import com.cloo.android.main.backend.database.DatabaseNode;
import com.cloo.android.main.backend.database.DatabaseQuery;
import com.cloo.android.main.model.Post;
import com.cloo.android.main.ui.base.DatabaseQueryList;
import com.cloo.android.main.ui.base.DatabaseQueryListAdapter;
import com.cloo.android.main.ui.compose.postCreation.PostCreationActivity;
import com.cloo.android.main.util.AndroidUtils;

public class TimelineFragment extends DatabaseQueryList<Post>
{

    @Override
    public void setupView(@NonNull final FragmentListBinding binding)
    {
        getAuthenticatedActivity().getDatabase()
                                  .getUserMailboxNode(getAuthenticatedActivity().getAuthentication()
                                                                                .getSession()
                                                                                .getUserId())
                                  .exists(exists -> {
                             if (!exists) {
                                 binding.listEmpty.setVisibility(View.VISIBLE);
                                 AndroidUtils.setImageDrawable(binding.emptyPicture, getActivity(),
                                                               R.drawable.empty_list_discover);
                                 binding.emptyListText.setText(
                                         "Your timeline is empty.\n The timeline shows posts of people you follow. \nIf you like posts of an user start following him to fill your timeline and always see his newest posts");
                             }
                         })
                                  .recycleAfterUse()
                                  .submit();
    }

    @Override
    public void setupPromotedAction(@NonNull final FloatingActionButton fab)
    {
        if (getParentActivity() != null) {
            fab.setImageDrawable(getParentActivity().getDrawable(R.drawable.v_icon_create_post));
        }
        fab.show();
        fab.setOnClickListener(
                v -> startActivity(new Intent(getActivity(), PostCreationActivity.class)));
    }

    @Nullable
    @Override
    public DatabaseQueryListAdapter<Post> createAdapter()
    {
        return new PostListAdapter(getAuthenticatedActivity());
    }

    @NonNull
    @Override
    public DatabaseQuery createQuery(final String endKey, final int count)
    {
        final DatabaseNode mailboxReference = getAuthenticatedActivity().getDatabase()
                                                                        .getUserMailboxNode(
                                                                                getAuthenticatedActivity()
                                                                                        .getAuthenticatedUserId());
        return mailboxReference.limitToLast(count);
    }
}
