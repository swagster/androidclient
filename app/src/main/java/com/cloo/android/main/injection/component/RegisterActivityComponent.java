package com.cloo.android.main.injection.component;

import com.cloo.android.main.injection.module.RegisterServiceModule;
import com.cloo.android.main.injection.module.ServerConnectionListenerModule;
import com.cloo.android.main.service.business.register.RegisterService;
import com.cloo.android.main.service.util.network.ServerConnectionListener;

import dagger.Component;

@Component(modules = {RegisterServiceModule.class, ServerConnectionListenerModule.class})
public interface RegisterActivityComponent {
    RegisterService getRegisterService();

    ServerConnectionListener getServerConnectionListener();
}
