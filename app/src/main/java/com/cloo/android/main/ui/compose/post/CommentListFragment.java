package com.cloo.android.main.ui.compose.post;

import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.databinding.Observable.OnPropertyChangedCallback;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentEnterCommentBinding;
import com.cloo.android.databinding.FragmentListBinding;
import com.cloo.android.main.application.ClooApplication;
import com.cloo.android.main.backend.database.DatabaseNode;
import com.cloo.android.main.backend.database.DatabaseQuery;
import com.cloo.android.main.injection.component.CommentListComponent;
import com.cloo.android.main.injection.component.DaggerCommentListComponent;
import com.cloo.android.main.injection.module.DatabaseInstanceModule;
import com.cloo.android.main.model.Comment;
import com.cloo.android.main.model.User;
import com.cloo.android.main.service.business.comment.CommentService;
import com.cloo.android.main.ui.base.DatabaseQueryList;
import com.cloo.android.main.ui.base.DatabaseQueryListAdapter;
import com.cloo.android.main.util.AndroidUtils;
import com.cloo.android.main.util.Assert;

public class CommentListFragment extends DatabaseQueryList<Comment> {
    private static final String POST_ID = "postId";
    @NonNull
    public final ObservableField<String> commentText = new ObservableField<>("");
    @Nullable private FragmentEnterCommentBinding binding = null;
    @NonNull
    private final OnPropertyChangedCallback commentTextChangeCallback = new OnPropertyChangedCallback() {
        @Override
        public void onPropertyChanged(@NonNull final Observable observable, final int i) {
            final boolean canSend = canSendComment();
            binding.submitComment.setClickable(canSend);
            AndroidUtils.setImageDrawable(binding.submitComment, binding.submitComment.getContext(), canSend ? R.drawable.v_icon_send_comment : R.drawable.v_icon_send_comment_disabled);
        }
    };
    @Nullable private CommentService commentService = null;
    @Nullable private ClooApplication application = null;

    @NonNull
    public static CommentListFragment newInstance(@NonNull final String postId) {
        final Bundle commentBundle = new Bundle();
        commentBundle.putString(CommentListFragment.POST_ID, postId);
        final CommentListFragment fragment = new CommentListFragment();
        fragment.setArguments(commentBundle);
        return fragment;
    }

    @NonNull
    @Override
    public final DatabaseQueryListAdapter<Comment> createAdapter() {
        return new CommentListAdapter(getAuthenticatedActivity());
    }

    @Override
    public final void setupView(final @NonNull FragmentListBinding binding) {
        final CommentListComponent commentListComponent = DaggerCommentListComponent.builder()
                                                                                    .databaseInstanceModule(
                                                                                            new DatabaseInstanceModule(
                                                                                                    getAuthenticatedActivity()
                                                                                                            .getDatabase()))
                                                                                    .build();
        commentService = commentListComponent.getCommentService();

        this.binding = DataBindingUtil.inflate(getActivity().getLayoutInflater(), R.layout.fragment_enter_comment, binding.list, false);
        binding.list.addHeaderView(this.binding.getRoot(), null, false);
        this.binding.setOnSend(this::sendComment);
        this.binding.setModel(this);
        application = (ClooApplication) getActivity().getApplication();
        commentText.addOnPropertyChangedCallback(commentTextChangeCallback);

        getAuthenticatedActivity().getDatabase()
                                  .getUserDetailsNode(
                                          application.getAuthentication().getSession().getUserId())
                                  .receive(User.class, (id, user) -> {
                    if (user != null) {
                        this.binding.setProfileImageUrl(user.getProfileImageUrl());
                    }
                })
                                  .recycleAfterUse()
                                  .submit();
    }

    @Override
    public void setupPromotedAction(@NonNull final FloatingActionButton fab) {
        fab.setVisibility(View.INVISIBLE);
    }

    @Override
    @NonNull
    public final DatabaseQuery createQuery(final String endKey, final int count) {
        Assert.check(application != null, "Application has to be initialized before sendComment");

        final DatabaseNode databaseNode = application.getDatabase().getCommentsOfPostNode(getPostId());
        final DatabaseQuery query = databaseNode.limitToLast(count);
        databaseNode.recycle();
        return query;
    }

    @NonNull
    private String getPostId() {
        if (getParameters() != null) {
            return getParameters().getString(CommentListFragment.POST_ID, "");
        } else {
            return "";
        }
    }

    private boolean canSendComment() {
        return !binding.getPendingSend() && !commentText.get().isEmpty();
    }

    private void sendComment(@NonNull final View view) {
        Assert.check(application != null, "Application has to be initialized before sendComment");
        Assert.check(commentService != null, "CommentService has to be initialized before sendComment");

        if (canSendComment()) {
            binding.setPendingSend(true);

            commentService.createComment(commentText.get(), getPostId(), application.getAuthentication().getSession().getUserId(),
                    () -> {
                        commentText.set("");
                        commentText.notifyChange();
                        binding.setPendingSend(false);
                    },
                    error -> binding.setPendingSend(false));
        }
    }
}