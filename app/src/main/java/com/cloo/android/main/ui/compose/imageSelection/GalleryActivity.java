package com.cloo.android.main.ui.compose.imageSelection;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.cloo.android.R;
import com.cloo.android.databinding.ActivityGalleryBinding;
import com.cloo.android.main.ui.base.BaseActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

import static com.cloo.android.main.util.AndroidUtils.isSDCardNotMounted;
import static com.cloo.android.main.util.AndroidUtils.needsPermission;

public class GalleryActivity extends BaseActivity
{
    private static final int RESULT_CROP = 8;
    private static final int EXTERNAL_STORAGE = 0;
    @Nullable private ActivityGalleryBinding binding = null;

    @NonNull
    private static List<String> getImagesPath(@NonNull final Context context)
    {
        final List<String> imagePaths = new ArrayList<>();
        try (Cursor cursor = context.getContentResolver()
                                    .query(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                           new String[]{MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME},
                                           null, null, null)) {
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    imagePaths.add(cursor.getString(
                            cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)));
                }
            }
        }
        return imagePaths;
    }

    @Override
    protected final void onActivityResult(final int requestCode, final int resultCode,
                                          @Nullable final Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GalleryActivity.RESULT_CROP && resultCode == Activity.RESULT_OK && data != null) {
            Timber.d("GalleryActivity receive URI:" + data.getData());
            setResult(resultCode, data);
            finish();
        }
    }

    @Override
    public final void onRequestPermissionsResult(final int requestCode,
                                                 @NonNull final String[] permissions,
                                                 @NonNull final int[] grantResults)
    {
        switch (requestCode) {
            case EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    init();
                }
                else {
                    finish();
                }
                break;
        }
    }

    private void init()
    {
        if (isSDCardNotMounted()) {
            Toast.makeText(this,
                           "Hey, seems like your SD-Card is not plugged in. Please connect it to choose pictures from it :)",
                           Toast.LENGTH_LONG).show();
        }
        else {
            final GalleryImageAdapter adapter = new GalleryImageAdapter(this);
            binding.gallyerGridview.setAdapter(adapter);
            adapter.addAllImages(GalleryActivity.getImagesPath(this));
            adapter.notifyDataSetChanged();
        }
    }

    private void requestPermissions()
    {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                                                                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            getPermissionRationale().show();
        }
        else {
            ActivityCompat.requestPermissions(this,
                                              new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                              EXTERNAL_STORAGE);
        }
    }

    private AlertDialog getPermissionRationale()
    {
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Need Storage permission");
        alertBuilder.setMessage("This lets Cloo look up your device for images.");
        alertBuilder.setPositiveButton(android.R.string.yes,
                                       (dialog, which) -> ActivityCompat.requestPermissions(
                                               GalleryActivity.this,
                                               new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                                               EXTERNAL_STORAGE));
        return alertBuilder.create();
    }

    @Override
    public final void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_gallery);
        setupBackButton(binding.toolbar);
        if (needsPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            requestPermissions();
        }
        else {
            init();
        }
    }

    public final void selectImage(@NonNull final String imagePath)
    {
        final Intent intent = new Intent(this, ImageEditActivity.class);
        // Calculates the correct Uri format
        intent.setData(Uri.fromFile(new File(imagePath)));
        startActivityForResult(intent, GalleryActivity.RESULT_CROP);
    }
}