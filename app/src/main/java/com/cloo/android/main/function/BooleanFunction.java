package com.cloo.android.main.function;

@FunctionalInterface
public interface BooleanFunction<T>
{
    boolean calculate(T param);
}
