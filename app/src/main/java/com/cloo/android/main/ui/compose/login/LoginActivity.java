package com.cloo.android.main.ui.compose.login;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;

import com.cloo.android.BuildConfig;
import com.cloo.android.R;
import com.cloo.android.databinding.ActivityLoginBinding;
import com.cloo.android.main.application.ClooApplication;
import com.cloo.android.main.application.MainActivity;
import com.cloo.android.main.application.UpdateAppActivity;
import com.cloo.android.main.backend.authentication.Authentication;
import com.cloo.android.main.function.FinishCallback;
import com.cloo.android.main.ui.base.BaseActivity;
import com.cloo.android.main.ui.compose.register.RegisterActivity;
import com.cloo.android.main.util.AndroidUtils;
import com.cloo.android.main.util.AnimationUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.Locale;

import timber.log.Timber;

public class LoginActivity extends BaseActivity implements FinishCallback
{
    @Nullable private final GoogleApiClient googleApiClient = null;
    @Nullable private ActivityLoginBinding binding = null;
    @Nullable private Authentication authentication = null;

    public final void startLoading()
    {
        if (canAccessUI()) {
            runOnUiThread(() -> AnimationUtils.lerp(getApplicationContext(), binding.display,
                                                    binding.loadingSpinner));
        }
    }

    public final void endLoading()
    {
        if (canAccessUI()) {
            runOnUiThread(() -> AnimationUtils.lerp(getApplicationContext(), binding.loadingSpinner,
                                                    binding.display));
        }
    }

    @Override
    public final void onStart()
    {
        super.onStart();
        if (authentication == null) {
            final GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
            final int result = apiAvailability.isGooglePlayServicesAvailable(this);
            if (result != ConnectionResult.SUCCESS) {
                final Dialog dialog = apiAvailability.getErrorDialog(this, result, 42);
                dialog.show();
            }
            else {
                init();
                authentication.addLoginChangeListener(this);
            }
        }
        else {
            authentication.addLoginChangeListener(this);
        }
    }

    @Override
    public final void onStop()
    {
        super.onStop();
        if (authentication != null) { authentication.removeLoginChangeListener(this); }
        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
    }


    @Override
    protected final void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = setBindingView(R.layout.activity_login);
        setSupportActionBar(binding.toolbar);
        setTitle(getString(R.string.title_login));
    }

    @Override
    public final void onActivityResult(final int requestCode, final int resultCode,
                                       @Nullable final Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 42) {
            init();
        }
        else {
            for (final Fragment fragment : getSupportFragmentManager().getFragments()) {
                Timber.d("Call onActivityResult for fragment " + fragment);
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    private void init()
    {
        authentication = ((ClooApplication) getApplication()).getAuthentication();
        binding.registerWrapper.register.setOnClickListener(v -> {
            final Intent intent = new Intent(this, RegisterActivity.class);
            final Bundle bundle = new Bundle();
            intent.putExtras(bundle);
            startActivity(intent);
        });
    }

    private void saveLastUserEmail()
    {
        final String email = getAuthentication().getSession().getEmail();
        AndroidUtils.getApplicationSharedPreferences(this)
                    .edit()
                    .putString(getString(R.string.preference_local_switch_last_login_email), email)
                    .apply();
    }

    @Override
    public final void onFinish(final boolean isSignedIn)
    {
        Timber.d(String.format("LoginActivity:: AuthState changed to %s", isSignedIn));

        getDatabase().getVersionNode().receive(Long.class, (k, versionCode) -> {
            Timber.d(String.format(Locale.getDefault(), "Received current Cloo DB Version %s %d", k,
                                   versionCode));
            long version = versionCode == null ? 0 : versionCode;
            PackageInfo pInfo = null;
            try {
                pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                if (!BuildConfig.DEBUG && pInfo.versionCode < version) {
                    finish();
                    startActivity(new Intent(this, UpdateAppActivity.class));
                }
                else {
                    if (!BuildConfig.DEBUG && pInfo.versionCode > version) {
                        getDatabase().getVersionNode().set(pInfo.versionCode).submit();
                    }
                    if (isSignedIn) {
                        Timber.d("Logged in");
                        saveLastUserEmail();
                        continueWithMainActivity();
                    }
                    else {
                        if (canAccessUI()) {
                            AnimationUtils.lerp(this, binding.bigLogo, binding.content);
                        }
                        // Check that everything is setup correctly on activity restart
                        getAuthentication().signOut();
                    }
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }).submit();

    }

    private void continueWithMainActivity()
    {
        finish();
        final Intent mainActivity = new Intent(this, MainActivity.class);
        startActivity(mainActivity);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    @Override
    public final boolean onCreateOptionsMenu(@NonNull final Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public final boolean onOptionsItemSelected(@NonNull final MenuItem item)
    {
        switch (item.getItemId()) {
            case R.id.menu_reset_password:
                sendPasswordReset();
                break;
            default:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }


    private void sendPasswordReset()
    {
        final EmailPasswordFragment fragment = (EmailPasswordFragment) getSupportFragmentManager().findFragmentById(
                R.id.email_password_fragment);
        if (fragment != null) {
            startActivity(ResetPasswordActivity.newInstance(this, fragment.getEmail()));
        }
    }
}
