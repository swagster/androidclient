package com.cloo.android.main.service.business.post;

import android.support.annotation.NonNull;

import com.cloo.android.main.function.FinishCallback;

@FunctionalInterface
public interface ShareService {
    void share(String userId, String postId, String comment, @NonNull final FinishCallback callback);
}
