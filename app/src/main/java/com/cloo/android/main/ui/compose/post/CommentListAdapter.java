package com.cloo.android.main.ui.compose.post;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentListElementCommentBinding;
import com.cloo.android.main.model.Comment;
import com.cloo.android.main.model.User;
import com.cloo.android.main.ui.base.BaseActivity;
import com.cloo.android.main.ui.base.DatabaseQueryListAdapter;
import com.cloo.android.main.util.TimeFormatUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_DAYS;
import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_HOURS;
import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_MINUTES;
import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_MONTHS;
import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_SECONDS;
import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_WEEKS;
import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_YEARS;

public class CommentListAdapter extends DatabaseQueryListAdapter<Comment>
{
    @NonNull private final Comparator<Comment> POST_COMPARATOR_TIMESTAMP = (postA, postB) -> postA.timestamp > postB.timestamp ? -1 : 1;
    @NonNull private final LayoutInflater inflater;
    @NonNull private final BaseActivity activity;
    @NonNull private final List<Comment> comments = new ArrayList<>();
    @NonNull private final Calendar cal = Calendar.getInstance();

    CommentListAdapter(@NonNull final BaseActivity activity)
    {
        this.activity = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private static View getRoot(@NonNull final ViewDataBinding binding)
    {
        return binding.getRoot();
    }

    @Override
    public final int getCount()
    {
        return comments.size();
    }

    @Override
    public final Object getItem(final int i)
    {
        return comments.get(i);
    }

    @Override
    public final long getItemId(final int i)
    {
        return i;
    }

    private void addComment(@NonNull final Comment comment)
    {
        // https://developer.android.com/training/articles/perf-tips.html#Loops
        for (int i = 0; i < comments.size(); i++) {
            if (comment.key.equals(comments.get(i).key)) {
                return;
            }
        }
        comments.add(comment);
        notifyDataSetChanged();
        Collections.sort(comments, POST_COMPARATOR_TIMESTAMP);
    }

    @NonNull
    private FragmentListElementCommentBinding getBinding(@Nullable final View recycledView,
                                                         @Nullable final ViewGroup viewGroup)
    {
        if (recycledView == null) {
            return DataBindingUtil.inflate(inflater, R.layout.fragment_list_element_comment,
                                           viewGroup, false);
        }
        else {
            return (FragmentListElementCommentBinding) recycledView.getTag();
        }
    }

    @NonNull
    private String getDisplayText(@NonNull final String commentText)
    {
        final String[] lines = commentText.split("\n");
        int index = 0;
        String output = "";
        while (index < lines.length) {
            if (!lines[index].isEmpty()) {
                output += lines[index];
                if (index > 3) { break; }
                if (index != lines.length - 1) {
                    output += "\n";
                }
            }
            index++;
        }
        return output.length() > 65 ? output.subSequence(0,
                                                         44) + ".." : output + (index != lines.length - 1 ? ".." : "");
    }


    @Override
    @NonNull
    public final View getView(final int i, @Nullable final View view,
                              @Nullable final ViewGroup viewGroup)
    {
        final FragmentListElementCommentBinding binding = getBinding(view, viewGroup);
        final Comment comment = getDataObject(i);
        binding.setComment(comment);

        binding.setDisplayedText(getDisplayText(comment.getText()));
        binding.commentTimePassed.setText(getTimePassed(comment.timestamp));

        binding.blocked.setVisibility(View.GONE);
        binding.content.setVisibility(View.GONE);
        activity.getDatabase()
                .getReportNode(activity.getAuthentication().getSession().getUserId(),
                               comment.getKey())
                .onUpdates(Object.class, (k, o) -> activity.getDatabase()
                                                           .getReportNode(
                                                                   activity.getAuthentication()
                                                                           .getSession()
                                                                           .getUserId(),
                                                                   comment.getKey())
                                                           .exists(exists -> {
                                                               if (exists) {
                                                                   binding.blocked.setVisibility(
                                                                           View.VISIBLE);
                                                                   binding.content.setVisibility(
                                                                           View.GONE);
                                                               }
                                                               else {
                                                                   binding.blocked.setVisibility(
                                                                           View.GONE);
                                                                   binding.content.setVisibility(
                                                                           View.VISIBLE);

                                                               }
                                                           })
                                                           .recycleAfterUse()
                                                           .submit())
                .submit();
        binding.setUser(null);
        final View rowView = CommentListAdapter.getRoot(binding);
        rowView.setTag(binding);

        activity.getDatabase()
                .getUserDetailsNode(comment.author)
                .receive(User.class, (id, user) -> {
                    user.setId(id);
                    binding.setUser(user);
                    rowView.setOnClickListener(v -> {
                        CommentDetailsDialog fragment = CommentDetailsDialog.newInstance(user,
                                                                                         comment);
                        activity.getSupportFragmentManager().popBackStack();
                        fragment.show(activity.getSupportFragmentManager(), "comment");
                    });
                })
                .recycleAfterUse()
                .submit();
        return rowView;
    }

    @NonNull
    private String getTimePassed(final long timestamp)
    {
        final int unit = TimeFormatUtils.getTimeUnit(timestamp);
        final long passedUnits = TimeFormatUtils.getPassedUnits(unit, timestamp);
        switch (unit) {
            case TIME_UNIT_SECONDS:
                return activity.getResources()
                               .getQuantityString(R.plurals.time_ago_seconds, (int) passedUnits,
                                                  passedUnits);
            case TIME_UNIT_MINUTES:
                return activity.getResources()
                               .getQuantityString(R.plurals.time_ago_minutes, (int) passedUnits,
                                                  passedUnits);
            case TIME_UNIT_HOURS:
                return activity.getResources()
                               .getQuantityString(R.plurals.time_ago_hours, (int) passedUnits,
                                                  passedUnits);
            case TIME_UNIT_DAYS:
                return activity.getResources()
                               .getQuantityString(R.plurals.time_ago_days, (int) passedUnits,
                                                  passedUnits);
            case TIME_UNIT_WEEKS:
                return activity.getResources()
                               .getQuantityString(R.plurals.time_ago_weeks, (int) passedUnits,
                                                  passedUnits);
            case TIME_UNIT_MONTHS:
                return activity.getResources()
                               .getQuantityString(R.plurals.time_ago_months, (int) passedUnits,
                                                  passedUnits);
            case TIME_UNIT_YEARS:
                cal.setTimeInMillis(timestamp);
                return String.format(activity.getString(R.string.time_ago_years),
                                     cal.get(Calendar.YEAR));
            default:
                return "";
        }
    }

    @Override
    public final void addDataObject(@NonNull final String commentId)
    {
        activity.getDatabase()
                .getCommentDetailsNode(commentId)
                .receive(Comment.class, (id, comment) -> {
                    if (comment != null) {
                        comment.key = id;
                        comment.setId(id);
                        addComment(comment);
                    }
                })
                .recycleAfterUse()
                .submit();
    }

    @Override
    public void removeDataObject(@NonNull final String key)
    {
        Comment removedComment = null;
        for (final Comment c : comments) {
            if (c.key.equals(key)) {
                removedComment = c;
                break;
            }
        }
        if (removedComment != null) {
            comments.remove(removedComment);
            notifyDataSetChanged();
        }
    }

    @Override
    @NonNull
    public final Comment getDataObject(final int index)
    {
        return comments.get(index);
    }

    @Override
    public void recycle()
    {

    }

    @NonNull
    @Override
    public String getLastKey()
    {
        return comments.isEmpty() ? "" : comments.get(comments.size() - 1).key;
    }
}