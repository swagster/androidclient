package com.cloo.android.main.ui.compose.login;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.cloo.android.R;
import com.cloo.android.main.ui.base.BaseDialog;

public class CheckEmailDialog extends BaseDialog
{
    @NonNull
    @Override
    public final Dialog onCreateDialog(final Bundle savedInstanceState)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.paragraph_reset_password_success).
                setPositiveButton(R.string.dialog_ok, (d, i) -> dismiss());
        return builder.create();
    }

    @Override
    public final void onDismiss(final DialogInterface dialog)
    {
        getActivity().finish();
        super.onDismiss(dialog);
    }
}
