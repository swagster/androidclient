package com.cloo.android.main.injection.module;

import android.app.Activity;
import android.support.annotation.NonNull;

import com.cloo.android.main.service.business.camera.CameraService;
import com.cloo.android.main.service.business.camera.CameraServiceImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class CameraServiceModule {
    private final Activity activity;

    public CameraServiceModule(final Activity activity) {
        this.activity = activity;
    }

    @NonNull
    @Provides
    public CameraService cameraService() {
        return new CameraServiceImpl(activity);
    }
}
