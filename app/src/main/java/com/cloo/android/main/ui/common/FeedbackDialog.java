package com.cloo.android.main.ui.common;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentSendFeedbackBinding;
import com.cloo.android.main.service.business.common.FeedbackService;
import com.cloo.android.main.service.business.common.FeedbackServiceImpl;
import com.cloo.android.main.ui.base.BaseActivity;
import com.cloo.android.main.ui.base.BaseDialog;
import com.cloo.android.main.util.AndroidUtils;

public class FeedbackDialog extends BaseDialog
{
    @Nullable
    @Override
    public final View onCreateView(@NonNull final LayoutInflater inflater,
                                   @Nullable final ViewGroup container,
                                   @Nullable final Bundle savedInstanceState)
    {
        final FragmentSendFeedbackBinding binding = DataBindingUtil.inflate(inflater,
                                                                            R.layout.fragment_send_feedback,
                                                                            container, false);

        final BaseActivity activity = (BaseActivity) getActivity();
        final FeedbackService feedbackService = new FeedbackServiceImpl(activity.getDatabase());
        binding.shareDialogCancel.setOnClickListener(v -> dismiss());
        binding.shareDialogReport.setOnClickListener(v -> feedbackService.sendFeedback(activity.getAuthentication().getSession().getUserId(),
                                                                                   AndroidUtils.getString(binding.shareDialogComment), 0,
                                     success -> {
                                         if (success) {
                                             dismiss();
                                             Toast.makeText(getActivity(),
                                                            "Thank you for taking the time and giving us feedback <3. We're happy to incorporate your improvements",
                                                            Toast.LENGTH_LONG).show();
                                         }
                                         else {
                                             Toast.makeText(getActivity(),
                                                            "Ups, something went wrong. Please try it later again",
                                                            Toast.LENGTH_LONG).show();
                                         }
                                     }));
        return binding.getRoot();
    }
}
