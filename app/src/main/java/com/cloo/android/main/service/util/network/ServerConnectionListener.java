package com.cloo.android.main.service.util.network;

import android.accounts.NetworkErrorException;
import android.support.annotation.NonNull;

import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.backend.database.DatabaseNode;

import java.util.ArrayList;
import java.util.Collection;

public class ServerConnectionListener {
    @NonNull private final DatabaseNode connectedRef;
    private final Collection<ConnectionChangeCallback> callbacks = new ArrayList<>();
    private boolean connected = false;

    public ServerConnectionListener(@NonNull final DatabaseAdapter databaseAdapter) {
        connectedRef = databaseAdapter.getConnectionReference();
        connectedRef.onUpdates(Boolean.class, this::onReceiveConnectionState)
                .onError(error -> onCancel(null))
                .submit();
    }

    public final boolean isConnected() {
        return connected;
    }

    public final void notifyCallbacks() {
        connectedRef
                .receive(Boolean.class, this::onReceiveConnectionState)
                .onError(error -> onCancel(new NetworkErrorException()))
                .submit();
    }

    private void onReceiveConnectionState(final String connectionKey, final boolean connected) {
        this.connected = connected;
        for (final ConnectionChangeCallback cb : callbacks) {
            cb.onConnectionChange(connected);
        }
    }

    private void onCancel(final Exception exception) {
        connected = false;
        for (final ConnectionChangeCallback cb : callbacks) {
            cb.onConnectionChange(false);
        }
    }

    /**
     * Adds the given callback to the listener. The consecutive executions of the callback may execute
     * the callback with the same arguments.
     * The callback is guarenteed to be executed, if the connection state of the client to the server changes.
     */
    public final void addCallback(final ConnectionChangeCallback callback) {
        callbacks.add(callback);
    }

    public final boolean removeCallback(final ConnectionChangeCallback callback) {
        return callbacks.remove(callback);
    }

    public final void recycle() {
        connectedRef.recycle();
    }

    @FunctionalInterface
    public interface ConnectionChangeCallback {
        void onConnectionChange(boolean connected);
    }
}
