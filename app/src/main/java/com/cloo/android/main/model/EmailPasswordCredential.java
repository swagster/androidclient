package com.cloo.android.main.model;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;

public class EmailPasswordCredential implements ParcelableCredential
{
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator()
    {
        @Override
        @NonNull
        public EmailPasswordCredential createFromParcel(@NonNull final Parcel in)
        {
            return new EmailPasswordCredential(in);
        }

        @Override
        @NonNull
        public EmailPasswordCredential[] newArray(final int size)
        {
            return new EmailPasswordCredential[size];
        }
    };
    public final ObservableField<String> email = new ObservableField<>("");
    public final ObservableField<String> password = new ObservableField<>("");
    public final ObservableBoolean registered = new ObservableBoolean(false);

    public EmailPasswordCredential()
    {
        // Default constructor for creation without parcel.
    }

    private EmailPasswordCredential(@NonNull final Parcel parcel)
    {
        email.set(parcel.readString());
        password.set(parcel.readString());
        registered.set(Boolean.parseBoolean(parcel.readString()));
    }

    @Override
    public final int describeContents()
    {
        return 0;
    }

    @Override
    public final void writeToParcel(@NonNull final Parcel dest, final int flags)
    {
        dest.writeString(email.get());
        dest.writeString(password.get());
        dest.writeString(String.valueOf(registered.get()));
    }

    @NonNull
    @Override
    public final AuthCredential getCredential()
    {
        return EmailAuthProvider.getCredential(email.get(), password.get());
    }
}
