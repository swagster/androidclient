package com.cloo.android.main.injection.module;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.storage.FirebaseStorage;
import com.cloo.android.main.backend.storage.Storage;

import dagger.Module;
import dagger.Provides;

@Module
public class StorageModule {

    @NonNull
    @Provides
    public Storage storage() {
        return new FirebaseStorage();
    }
}
