package com.cloo.android.main.ui.compose.postCreation;

import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentPostCreationOptionSelectionBinding;
import com.cloo.android.main.function.FinishCallback;
import com.cloo.android.main.injection.component.DaggerPostCreationActivityComponent;
import com.cloo.android.main.injection.component.PostCreationActivityComponent;
import com.cloo.android.main.injection.module.CameraServiceModule;
import com.cloo.android.main.injection.module.ContextModule;
import com.cloo.android.main.injection.module.DatabaseInstanceModule;
import com.cloo.android.main.ui.base.BaseActivity;

import java.util.List;

public class PostCreationOptionSelectionFragment extends Fragment
{
    @Nullable FragmentPostCreationOptionSelectionBinding binding = null;
    @Nullable PostCreationOptionsAdapter adapter = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater,
                             @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater,
                                          R.layout.fragment_post_creation_option_selection,
                                          container, false);
        final PostCreationActivityComponent dependencies = DaggerPostCreationActivityComponent.builder()
                                                                                              .contextModule(
                                                                                                      new ContextModule(
                                                                                                              getActivity()))
                                                                                              .databaseInstanceModule(
                                                                                                      new DatabaseInstanceModule(
                                                                                                              ((BaseActivity) getActivity())
                                                                                                                      .getDatabase()))
                                                                                              .cameraServiceModule(
                                                                                                      new CameraServiceModule(
                                                                                                              getActivity()))
                                                                                              .build();
        adapter = new PostCreationOptionsAdapter((BaseActivity) getActivity(),
                                                 dependencies.getCameraService(),
                                                 dependencies.getStorage(), 0);
        binding.setOptionsAdapter(adapter);
        return binding.getRoot();
    }

    public void set(final int requestCode, final Uri data)
    {
        adapter.set(requestCode, data);
    }

    @NonNull
    public List<Uri> getUrls()
    {
        return adapter.getUrls();
    }

    public void uploadOptions(@NonNull final FinishCallback onSuccess)
    {
        adapter.uploadOptions(onSuccess);
    }
}
