package com.cloo.android.main.injection.module;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.service.business.comment.CommentService;
import com.cloo.android.main.service.business.comment.CommentServiceImpl;

import dagger.Module;
import dagger.Provides;

@Module(includes = {DatabaseModule.class, DatabaseInstanceModule.class})
public class CommentServiceModule {
    @NonNull
    @Provides
    CommentService commentService(final DatabaseAdapter database) {
        return new CommentServiceImpl(database);
    }
}
