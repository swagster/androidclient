package com.cloo.android.main.function;

@FunctionalInterface
public interface FinishCallback
{
    void onFinish(boolean success);
}
