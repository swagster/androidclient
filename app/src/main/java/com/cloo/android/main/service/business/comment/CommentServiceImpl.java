package com.cloo.android.main.service.business.comment;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.backend.database.DatabaseCodes;
import com.cloo.android.main.backend.database.DatabaseNode;
import com.cloo.android.main.function.ExecutionCallback;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;

public class CommentServiceImpl implements CommentService
{
    private final DatabaseAdapter database;

    public CommentServiceImpl(final DatabaseAdapter database)
    {
        this.database = database;
    }

    @NonNull
    private Map<String, Object> getCommentMap(final String text, final String postId,
                                              final String author)
    {
        final Map<String, Object> comment = new HashMap<>();
        comment.put("author", author);
        comment.put("post", postId);
        comment.put("timestamp", ServerValue.TIMESTAMP);
        comment.put("lastEdited", ServerValue.TIMESTAMP);
        comment.put("text", text);
        return comment;
    }

    @Override
    public void createComment(@NonNull final String text, @NonNull final String postId,
                              @NonNull final String author,
                              @NonNull final ExecutionCallback successCallback,
                              @NonNull final DatabaseCodes.DatabaseOperationErrorCallback errorCallback)
    {
        final DatabaseNode reference = database.getCommentDetailsNode().generateChild();
        reference.setChildren(getCommentMap(text, postId, author))
                 .onSuccess(successCallback)
                 .onError(errorCallback)
                 .recycleAfterUse()
                 .submit();
    }

    @Override
    public void removeComment(@NonNull final String commentId,
                              @NonNull final ExecutionCallback successCallback,
                              @NonNull final DatabaseCodes.DatabaseOperationErrorCallback errorCallback)
    {
        database.getCommentDetailsNode(commentId).delete()
                .onError(errorCallback).onSuccess(successCallback)
                .recycleAfterUse()
                .submit();
    }

    @Override
    public void editComment(@NonNull final String commentId, @NonNull final String newText,
                            @NonNull final ExecutionCallback successCallback,
                            @NonNull final DatabaseCodes.DatabaseOperationErrorCallback errorCallback)
    {
        successCallback.execute();
    }
}
