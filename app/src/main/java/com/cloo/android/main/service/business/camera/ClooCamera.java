package com.cloo.android.main.service.business.camera;

import android.support.annotation.NonNull;
import android.util.Size;
import android.view.SurfaceHolder;

import com.cloo.android.main.function.ReceiveCallback;

import java.util.List;

public interface ClooCamera
{
    int FLASH_MODE_ON = 0;
    int FLASH_MODE_AUTO = 1;
    int FLASH_MODE_OFF = 2;

    void setPreviewSize(int width, int height);

    void setPreviewDisplay(SurfaceHolder holder);

    int getFlashMode();

    /**
     * Starts the live preview in the holder. The holder of the camera must be set before this operation.
     */
    void startPreview();

    /**
     * Stops the live preview in the holder. The holder of the camera must be set before this operation.
     */
    void stopPreview();

    @NonNull
    List<Size> getSupportedSizes();

    /**
     * @param width  The width of the camera preview holder.
     * @param height The height of the camera preview holder.
     * @return The valid size of the camera preview to fill the holder.
     */
    @NonNull
    Size getFullscreenPreviewSize(int width, int height);

    /**
     * Takes a picture with the camera and executes the receivePicture callback with the image data as an argument.
     *
     * @param receivePicture The bytes of the image with jpeg encoding
     */
    void takePicture(@NonNull final ReceiveCallback<byte[]> receivePicture);

    /**
     * @return The maximal valid zoom value of the camera.
     */
    int getMaxZoom();

    /**
     * Increments the zoom of the camera, if the current zoom isn't at the maximum of the camera.
     */
    void zoomIn();

    /**
     * Decrements the zoom of the camera, if the current zoom isn't at 0.
     */
    void zoomOut();

    /**
     * Releases the camera and all it's resource, so it can be acquired by others.
     * The camera must not be used after this operation.
     */
    void release();

    /**
     * Cycles through the available flash modes
     */
    void switchFlashMode();

    boolean hasFlash();
}
