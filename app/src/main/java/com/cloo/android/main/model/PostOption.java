package com.cloo.android.main.model;

import android.support.annotation.Keep;
import android.support.annotation.Nullable;

@Keep
public class PostOption
{
    @Nullable private String uri = null;

    public PostOption()
    {
        // Builder
    }

    public PostOption(@Nullable final String uri)
    {
        this.uri = uri;
    }

    @Nullable
    public final String getUri()
    {
        return uri;
    }

    public final void setUri(@Nullable final String uri)
    {
        this.uri = uri;
    }
}
