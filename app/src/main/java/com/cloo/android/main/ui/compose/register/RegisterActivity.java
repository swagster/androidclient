package com.cloo.android.main.ui.compose.register;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.cloo.android.R;
import com.cloo.android.databinding.ActivityRegisterBinding;
import com.cloo.android.main.application.ClooApplication;
import com.cloo.android.main.injection.component.DaggerRegisterActivityComponent;
import com.cloo.android.main.injection.component.RegisterActivityComponent;
import com.cloo.android.main.injection.module.DatabaseInstanceModule;
import com.cloo.android.main.model.EmailPasswordCredential;
import com.cloo.android.main.model.ParcelableCredential;
import com.cloo.android.main.service.business.register.RegisterService;
import com.cloo.android.main.ui.base.BaseActivity;
import com.cloo.android.main.util.AnimationUtils;

import timber.log.Timber;

public class RegisterActivity extends BaseActivity
{
    private static final String BUNDLE_EMAIL = "email";
    private static final String BUNDLE_PASSWORD = "password";
    @Nullable private ActivityRegisterBinding binding = null;
    @Nullable private String email = null;
    @Nullable private String password = null;

    @Nullable private RegisterService registerService = null;
    private int index = 0;
    @Nullable private RegisterEmailFragment registerEmailFragment = null;
    @Nullable private RegisterPasswordFragment registerPasswordFragment = null;

    @Override
    protected void onSaveInstanceState(@NonNull final Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putString(BUNDLE_EMAIL, email);
        outState.putString(BUNDLE_PASSWORD, password);
    }

    @NonNull
    public String getEmail()
    {
        if (email == null) {
            final Bundle bundle = getIntent().getExtras();
            email = bundle.getString(BUNDLE_EMAIL, "");
        }
        return email;
    }

    @NonNull
    public String getPassword()
    {
        if (password == null) {
            final Bundle bundle = getIntent().getExtras();
            password = bundle.getString(BUNDLE_PASSWORD, "");
        }
        return password;
    }

    @NonNull
    private ParcelableCredential getCredentials()
    {
        final EmailPasswordCredential emailPassword = new EmailPasswordCredential();
        emailPassword.email.set(email);
        emailPassword.password.set(password);
        return emailPassword;
    }

    @Override
    protected final void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        final RegisterActivityComponent registerActivityComponent = DaggerRegisterActivityComponent.builder()
                                                                                                   .databaseInstanceModule(
                                                                                                           new DatabaseInstanceModule(
                                                                                                                   getDatabase()))
                                                                                                   .build();
        registerService = registerActivityComponent.getRegisterService();
        setupBackButton(binding.toolbar);
        registerEmailFragment = (RegisterEmailFragment) getSupportFragmentManager().findFragmentById(
                R.id.email_fragment);
        registerEmailFragment.setup(binding.registerOfflineLabel, binding.registerForward,
                                    registerActivityComponent.getServerConnectionListener(),
                                    getAuthentication());
        registerPasswordFragment = (RegisterPasswordFragment) getSupportFragmentManager().findFragmentById(
                R.id.password_fragment);
        registerPasswordFragment.setup(binding.registerOfflineLabel, binding.registerForward);
    }

    private void previousDialogPage(final View view)
    {
        if (index > 0) {
            index--;
            binding.registerSubmit.hide();
            binding.registerForward.show();
            binding.registerContent.showPrevious();
        }
    }

    public final void nextDialogPage(final View view)
    {
        if (index < 1) {
            registerEmailFragment.onSubmit(success -> {
                if (success) {
                    email = registerEmailFragment.getEmail();
                    index++;
                    binding.registerSubmit.show();
                    binding.registerForward.hide();
                    binding.registerContent.showNext();
                }
            });
        }
    }

    public final void submitRegister(final View view)
    {
        if (registerPasswordFragment.onSubmit()) {
            password = registerPasswordFragment.getPassword();
            AnimationUtils.lerp(this, binding.pending, binding.loadingSpinner);
            registerService.register(email, password, success -> {
                Timber.d(
                        String.format("registerService::register finsihed %b %s %s", success, email,
                                      password));
                if (success) {
                    finish();
                    ((ClooApplication) getApplication()).getAuthentication()
                                                        .signIn(getCredentials(), null, error -> {
                                                        });
                }
                else {
                    AnimationUtils.lerp(this, binding.loadingSpinner, binding.pending);
                    Toast.makeText(this, "Registration failed. Please try again.",
                                   Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    public final void onBackPressed()
    {
        if (index == 0) {
            finish();
        }
        else {
            previousDialogPage(null);
        }
    }
}
