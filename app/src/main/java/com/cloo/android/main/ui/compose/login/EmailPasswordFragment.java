package com.cloo.android.main.ui.compose.login;

import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentEmailPasswordLoginBinding;
import com.cloo.android.main.model.EmailPasswordCredential;
import com.cloo.android.main.model.ParcelableCredential;
import com.cloo.android.main.service.util.validation.CredentialValidation;
import com.cloo.android.main.service.util.view.SimpleTextWatcher;
import com.cloo.android.main.ui.base.BaseFragment;
import com.cloo.android.main.util.AndroidUtils;

import timber.log.Timber;

import static com.cloo.android.main.util.AndroidUtils.isNetworkAvailable;

public class EmailPasswordFragment extends BaseFragment
{
    private final EmailPasswordCredential credentials = new EmailPasswordCredential();
    @Nullable private FragmentEmailPasswordLoginBinding binding = null;
    @Nullable private ParcelableCredential credential = null;
    @Nullable private LoginActivity activity = null;

    @NonNull
    public static EmailPasswordFragment newInstance(@NonNull final Parcelable credential,
                                                    @NonNull final String email)
    {
        final EmailPasswordFragment fragment = new EmailPasswordFragment();
        final Bundle bundle = new Bundle();
        bundle.putParcelable(LinkProviderDialog.CREDENTIAL_KEY, credential);
        bundle.putString(LinkProviderDialog.EMAIL_KEY, email);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public final void onSaveInstanceState(@NonNull final Bundle outState)
    {
        super.onSaveInstanceState(outState);
        if (credential != null) {
            outState.putParcelable(LinkProviderDialog.CREDENTIAL_KEY, credential);
        }
    }

    @Override
    @NonNull
    public final View onCreateView(@NonNull final LayoutInflater inflater,
                                   @Nullable final ViewGroup container,
                                   @Nullable final Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_email_password_login,
                                          container, false);
        binding.setCredentials(credentials);

        activity = (LoginActivity) getActivity();
        if (getArguments() != null) {
            credentials.email.set(getArguments().getString("email"));
            credential = getArguments().getParcelable(LinkProviderDialog.CREDENTIAL_KEY);
        }
        credentials.email.set(getLastSignInEmail());
        setupView();

        return binding.getRoot();
    }

    private void setupView()
    {
        binding.loginPassword.addTextChangedListener(new SimpleTextWatcher(credentials.password));
        binding.loginUsernameField.addTextChangedListener(
                new SimpleTextWatcher(credentials.email, email -> {
                    if (CredentialValidation.isEmailValid(email) && isNetworkAvailable(
                            getActivity().getApplicationContext())) {
                        checkEmail(email);
                    }
                    else {
                        AndroidUtils.setTextColorNormal(binding.loginUsernameField, getActivity());
                        binding.loginUsernameField.requestFocus();
                    }
                }));
        binding.loginLogin.setOnClickListener(this::tryLogin);
    }

    @NonNull
    private String getLastSignInEmail()
    {
        final SharedPreferences sharedPref = AndroidUtils.getApplicationSharedPreferences(
                getActivity().getApplicationContext());
        return sharedPref.getString(getString(R.string.preference_local_switch_last_login_email),
                                    "");
    }

    private void checkEmail(@NonNull final String email)
    {
        activity.getAuthentication().getProviders(email, providers -> {
            if (!isDetached() && binding != null) {
                credentials.registered.set(
                        activity.getAuthentication().hasEmailPasswordProvider(providers));
                AndroidUtils.setTextColorAccent(binding.loginUsernameField, getActivity());
                binding.loginPassword.requestFocus();
            }
        });
    }

    private void tryLogin(@NonNull final View view)
    {
        if (canLogin()) {
            activity.startLoading();
            AndroidUtils.hideKeyboard(getActivity());
            activity.getAuthentication().getProviders(getEmail(), providers -> {
                if (activity.getAuthentication().hasEmailPasswordProvider(providers)) {
                    signIn(view);
                }
                else if (!providers.isEmpty()) {
                    activity.endLoading();
                    showLinkDialog();
                }
            });
        }
    }

    private void showLinkDialog()
    {
        final FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.addToBackStack(null);
        final LinkProviderDialog linkFacebookFragment = LinkProviderDialog.newInstance(
                credentials.email.get(), credentials);
        linkFacebookFragment.show(ft, null);
    }

    private boolean canLogin()
    {
        return checkEmail() && checkPassword() && isNetworkAvailable(
                getActivity().getApplicationContext());
    }

    private boolean checkEmail()
    {
        if (!CredentialValidation.isEmailValid(getEmail())) {
            binding.loginUsername.setError(getString(R.string.error_login_email_format));
            return false;
        }
        return true;
    }

    private boolean checkPassword()
    {
        if (!CredentialValidation.isPasswordValid(getPassword())) {
            binding.loginPassword.setError(getString(R.string.error_login_password_too_short));
            return false;
        }
        return true;
    }

    private void signIn(@NonNull final View view)
    {
        Timber.d("signIn with EmailPAssword");
        activity.getAuthentication().signIn(credentials, credential, success -> {
            if (!activity.isDestroyed()) {
                Timber.d("signIn with credentials callback");
                activity.endLoading();
                if (success != null) {
                    Timber.d("tryLogin:success");
                    binding.loginPassword.setError("Wrong password");
                }
            }
        });
    }

    @NonNull
    String getEmail()
    {
        return credentials.email.get();
    }

    @NonNull
    private String getPassword()
    {
        return credentials.password.get();
    }
}
