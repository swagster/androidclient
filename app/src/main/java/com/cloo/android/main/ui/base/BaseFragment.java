package com.cloo.android.main.ui.base;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

public abstract class BaseFragment extends Fragment
{
    @Nullable private BaseActivity activity = null;

    @Nullable
    public BaseActivity getParentActivity()
    {
        if (getActivity() == null) {
            return null;
        }
        if (activity == null) {
            activity = (BaseActivity) getActivity();
        }
        return activity;
    }

    public boolean canAccessUI()
    {
        return getParentActivity() != null && getParentActivity().canAccessUI() && !isDetached() && isAdded();
    }
}
