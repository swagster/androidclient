package com.cloo.android.main.model.bindingModels;

import android.databinding.ObservableField;

public class ReportDialogForm
{
    public final ObservableField<String> information = new ObservableField<>("");
}
