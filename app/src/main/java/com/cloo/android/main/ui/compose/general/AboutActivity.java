package com.cloo.android.main.ui.compose.general;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.cloo.android.BuildConfig;
import com.cloo.android.R;
import com.cloo.android.databinding.ActivityAboutBinding;
import com.cloo.android.main.ui.base.BaseActivity;
import com.cloo.android.main.ui.common.FeedbackDialog;
import com.cloo.android.main.util.AndroidUtils;

import java.util.Locale;

public class AboutActivity extends BaseActivity
{
    @Nullable GestureDetector detector = null;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        final ActivityAboutBinding binding = setBindingView(R.layout.activity_about);
        setupBackButton(binding.toolbar);
        binding.setVersion(BuildConfig.VERSION_NAME);
        detector = createGestureDetector();
        binding.feedback.setOnClickListener(this::showFeedbackDialog);
        binding.aboutRateApp.setOnClickListener(this::onRateClick);
        binding.webLink.setOnClickListener(this::onWeblinkClick);
        binding.textView.setOnTouchListener((view, motionEvent) -> {
            detector.onTouchEvent(motionEvent);
            return false;
        });
    }

    private void showFeedbackDialog(final View view)
    {
        getFragmentManager().popBackStack();
        final FeedbackDialog fragment = new FeedbackDialog();
        fragment.show(getSupportFragmentManager(), "");
    }

    @NonNull
    private GestureDetector createGestureDetector()
    {
        return new GestureDetector(this, new GestureDetector.SimpleOnGestureListener()
        {
            @Override
            public boolean onDoubleTap(final MotionEvent e)
            {
                final long sessionStart = AndroidUtils.getUserSharedPreferences(AboutActivity.this)
                                                      .getLong(getString(
                                                              R.string.storage_session_start), 0);
                AndroidUtils.getUserSharedPreferences(AboutActivity.this)
                            .edit()
                            .putLong(getString(R.string.storage_usage_time),
                                     AndroidUtils.getUserSharedPreferences(AboutActivity.this)
                                                 .getLong(getString(R.string.storage_usage_time),
                                                          0) + (System.currentTimeMillis() - sessionStart))
                            .putLong(getString(R.string.storage_session_start),
                                     System.currentTimeMillis())
                            .apply();
                final long timeUsed = AndroidUtils.getUserSharedPreferences(AboutActivity.this)
                                                  .getLong(getString(R.string.storage_usage_time),
                                                           0);
                showToast(String.format(Locale.getDefault(), "You used Cloo for %d minutes",
                                        (timeUsed / 1000 / 60)));
                return true;
            }
        });
    }

    private void onWeblinkClick(final View view)
    {
        final String url = "http://www.theclooapp.com";
        final Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void onRateClick(final View view)
    {
        try {
            startActivity(AndroidUtils.getStoreIntent(getClooApplication()));
        } catch (@NonNull final ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(
                    "http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
        }
    }


}
