package com.cloo.android.main.service.business.profile;

import com.cloo.android.main.function.ReceiveCallback;
import com.cloo.android.main.backend.authentication.Session;

@FunctionalInterface
public interface UserCreationService {
    void create(Session session, ReceiveCallback<Boolean> onSuccess);
}
