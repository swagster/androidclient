package com.cloo.android.main.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
public @interface OnCreateInitialized {
}
