package com.cloo.android.main.ui.compose.post;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.cloo.android.R;
import com.cloo.android.databinding.DPostListElementOptionBinding;
import com.cloo.android.main.model.Post;
import com.cloo.android.main.model.PostOption;
import com.cloo.android.main.service.business.post.VoteService;
import com.cloo.android.main.service.business.post.VoteServiceImpl;
import com.cloo.android.main.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import static com.cloo.android.main.util.ScreenUtils.toPixels;

public class PostOptionsAdapter extends BaseAdapter
{
    @NonNull private static final String TAG = PostOptionsAdapter.class.getSimpleName();
    @NonNull private final Activity activity;
    @NonNull private final LayoutInflater inflater;
    private final long voteCount = 0;
    private final int optionLength;
    @NonNull private final List<PostOption> options = new ArrayList<>();
    @NonNull private final VoteService voteService;
    @Nullable private Post post = null;
    @NonNull private long[] optionVotes = new long[0];
    @Nullable private AdapterView.OnItemClickListener onItemClickListener = null;


    public PostOptionsAdapter(@NonNull final BaseActivity activity)
    {
        this(activity, PostOptionsAdapter.getScreenWidth() / 2 - toPixels(16,
                                                                          activity.getResources()
                                                                                  .getDisplayMetrics()));
    }

    PostOptionsAdapter(@NonNull final BaseActivity activity, final int width)
    {
        optionLength = width;
        this.activity = activity;
        voteService = new VoteServiceImpl(activity.getDatabase(), activity.getAuthentication());
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Log.d(PostOptionsAdapter.TAG, "Created PostOptionsAdapter");
    }

    private static int getScreenWidth()
    {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    private void setOptionVote(int index, long votes)
    {
        if (optionVotes.length > index) { optionVotes[index] = votes; }
    }

    @Override
    public final int getCount()
    {
        return options.size();
    }

    @Override
    public final Object getItem(final int i)
    {
        return options.get(i);
    }

    @Override
    public final long getItemId(final int i)
    {
        return i;
    }

    @NonNull
    private DPostListElementOptionBinding getBinding(@Nullable final View recycledView,
                                                     @Nullable final ViewGroup viewGroup)
    {
        if (recycledView == null) {
            final DPostListElementOptionBinding binding = DataBindingUtil.inflate(inflater,
                                                                                  R.layout.d_post_list_element_option,
                                                                                  viewGroup, false);
            if (binding.optionImage.getWidth() != optionLength) {
                binding.optionImage.setLayoutParams(
                        new RelativeLayout.LayoutParams(optionLength, optionLength));
                binding.optionPercentage.setLayoutParams(
                        new RelativeLayout.LayoutParams(optionLength, optionLength));
            }
            return binding;
        }
        else {
            return (DPostListElementOptionBinding) recycledView.getTag();
        }
    }

    @NonNull
    public String getPercent(final int index)
    {
        if (optionVotes[index] == 0) {
            return "0";
        }
        final String percentString;
        long votes = 0;
        int lastNotZeroIndex = 0;
        for (int i = 0; i < optionVotes.length; i++) {
            votes += optionVotes[i];
            if (optionVotes[i] != 0) { lastNotZeroIndex = i; }
        }
        if (votes == 0) { return ""; }
        if (lastNotZeroIndex == index) {
            percentString = String.valueOf(
                    100 - (int) (100 * ((votes - optionVotes[index]) / (float) votes)));
        }
        else {
            percentString = String.valueOf((int) (100 * (optionVotes[index] / (float) votes)));
        }
        return percentString;
    }

    @Override
    public final View getView(final int i, @Nullable final View recycledView,
                              final ViewGroup viewGroup)
    {
        final DPostListElementOptionBinding binding = getBinding(recycledView, viewGroup);
        final View rowView = binding.getRoot();
        rowView.setTag(binding);
        binding.setPost(post);
        if (options.get(i) != null) {
            binding.setOption(options.get(i));
            binding.setOptionNumber(i);
            binding.setPercent(getPercent(i));
        }
        final GestureDetector detector = new GestureDetector(activity,
                                                             new GestureDetector.SimpleOnGestureListener()
                                                             {
                                                                 @Override
                                                                 public boolean onDoubleTap(
                                                                         final MotionEvent e)
                                                                 {
                                                                     voteService.voteOnPost(post,
                                                                                            i);
                                                                     post.selectedOption.set(i);
                                                                     binding.setPercent(
                                                                             getPercent(i));
                                                                     return true;
                                                                 }

                                                                 @Override
                                                                 public boolean onSingleTapConfirmed(
                                                                         final MotionEvent e)
                                                                 {
                                                                     if (onItemClickListener != null) {
                                                                         onItemClickListener.onItemClick(
                                                                                 null, null, i, 0);
                                                                     }
                                                                     return super.onSingleTapConfirmed(
                                                                             e);
                                                                 }
                                                             });
        binding.optionWrapper.setOnTouchListener((view, motionEvent) -> {
            detector.onTouchEvent(motionEvent);
            return true;
        });
        return rowView;
    }

    @Nullable
    public final Post getPost()
    {
        return post;
    }

    public final void setPost(@NonNull final Post post)
    {
        this.post = post;
        options.clear();
        if (!post.getOptions().isEmpty()) {
            options.addAll(post.getOptions());
        }
        if (post.getAuthor()
                .equals(((BaseActivity) activity).getAuthentication().getSession().getUserId())) {
            post.selectedOption.set(Integer.MAX_VALUE);
            notifyDataSetChanged();
        }
        else {
            voteService.getVotedOption(post.getKey(), (value) -> {
                post.selectedOption.set(value);
                notifyDataSetChanged();
            });
        }

        optionVotes = new long[post.getOptions().size()];
        for (int i = 0; i < post.getOptions().size(); i++) {
            final int index = i;
            ((BaseActivity) activity).getDatabase()
                                     .getOptionVoteCount(post.getKey(), i)
                                     .onUpdates(Long.class, (k, votes) -> {
                                         setOptionVote(index, votes == null ? 0 : votes);
                                         notifyDataSetChanged();
                                     })
                                     .submit();
        }
        notifyDataSetChanged();
    }

    public void setOnClickListener(final AdapterView.OnItemClickListener onClickListener)
    {
        onItemClickListener = onClickListener;
    }
}
