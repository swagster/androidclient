package com.cloo.android.main.injection.module;

import android.content.Context;
import android.support.annotation.NonNull;

import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.backend.storage.Storage;
import com.cloo.android.main.service.business.profile.ProfileService;
import com.cloo.android.main.service.business.profile.ProfileServiceImpl;
import com.cloo.android.main.service.business.profile.UsernameService;

import dagger.Module;
import dagger.Provides;

@Module(includes = {StorageModule.class, ContextModule.class, UsernameServiceModule.class, DatabaseInstanceModule.class})
public class ProfileServiceModule
{
    @NonNull
    @Provides
    public ProfileService profileService(@NonNull final Context context, final Storage storage,
                                         final UsernameService usernameService,
                                         final DatabaseAdapter databaseAdapter)
    {
        return new ProfileServiceImpl(context, storage, databaseAdapter, usernameService);
    }
}
