package com.cloo.android.main.service.business.comment;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.database.DatabaseCodes;
import com.cloo.android.main.function.ExecutionCallback;

public interface CommentService
{
    /**
     * Creates a new comment with the given text, created by the given author on the post with the id postId.
     *
     * @param text            The text of the comment.
     * @param postId          The id of the post which is commented with the comment
     * @param author          The id of the user that creates the comment
     * @param successCallback Executed, if the comment was successfully created.
     * @param errorCallback   Executed, if the operation failed.
     */
    void createComment(@NonNull final String text, @NonNull final String postId,
                       @NonNull final String author,
                       @NonNull final ExecutionCallback successCallback,
                       @NonNull final DatabaseCodes.DatabaseOperationErrorCallback errorCallback);

    void removeComment(@NonNull final String commentId,
                       @NonNull final ExecutionCallback successCallback,
                       @NonNull final DatabaseCodes.DatabaseOperationErrorCallback errorCallback);

    void editComment(@NonNull final String commentId, @NonNull final String newText,
                     @NonNull final ExecutionCallback successCallback,
                     @NonNull final DatabaseCodes.DatabaseOperationErrorCallback errorCallback);
}
