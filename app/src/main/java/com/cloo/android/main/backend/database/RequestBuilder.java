package com.cloo.android.main.backend.database;

import android.support.annotation.NonNull;

import com.cloo.android.main.function.ExecutionCallback;

public interface RequestBuilder
{
    @NonNull
    RequestBuilder onError(DatabaseCodes.DatabaseOperationErrorCallback errorCallback);

    @NonNull
    RequestBuilder onSuccess(ExecutionCallback successCallback);

    @NonNull
    RequestBuilder onFinish(ExecutionCallback finishCallback);

    @NonNull
    RequestBuilder recycleAfterUse();

    void submit();
}