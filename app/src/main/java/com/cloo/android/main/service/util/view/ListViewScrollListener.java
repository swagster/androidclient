package com.cloo.android.main.service.util.view;

import android.support.annotation.NonNull;
import android.widget.AbsListView;

public class ListViewScrollListener implements AbsListView.OnScrollListener {
    @NonNull
    private final OnBottomReachedListener onBottomReachedListener;
    private int lastItemCount = -1;

    public ListViewScrollListener(@NonNull final OnBottomReachedListener listener) {
        onBottomReachedListener = listener;
    }

    private static boolean isScrollbarAtBottom(@NonNull final AbsListView listView) {
        return listView.getChildAt(Math.max(0, listView.getChildCount() - 1)).getBottom() <= listView.getHeight();
    }

    private static boolean isLastVisibleItem(@NonNull final AbsListView listView, final int index) {
        return listView.getLastVisiblePosition() == index;
    }

    @Override
    public final void onScrollStateChanged(@NonNull final AbsListView listView, final int i) {
        if (isScrollbarNearBottom(listView)) {
            lastItemCount = listView.getAdapter().getCount();
            onBottomReachedListener.onBottomReached(listView);
        }
    }

    @Override
    public void onScroll(final AbsListView absListView, final int i, final int i1, final int i2) {
    }

    private boolean isScrollbarNearBottom(@NonNull final AbsListView listView) {
        return ListViewScrollListener.isLastVisibleItem(listView, listView.getAdapter().getCount() - 1) && isListSizeChanged(
                listView) && ListViewScrollListener.isScrollbarAtBottom(listView);
    }

    private boolean isListSizeChanged(@NonNull final AbsListView listView) {
        return lastItemCount != listView.getAdapter().getCount();
    }

    @FunctionalInterface
    public interface OnBottomReachedListener {
        void onBottomReached(AbsListView listView);
    }
}
