package com.cloo.android.main.backend.storage;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.cloo.android.main.function.ReceiveCallback;

public interface Storage {
    void uploadUserProfileImage(@NonNull final Context context, @NonNull final Uri data, @NonNull final
    OnUploadFinishedListener listener);

    void uploadPostOptionImage(Bitmap bitmap, @NonNull final String id, @NonNull final
    OnUploadFinishedListener listener, @NonNull final ReceiveCallback<Integer> progress);
}
