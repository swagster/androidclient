package com.cloo.android.main.injection.module;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.service.business.post.PostDeletionService;
import com.cloo.android.main.service.business.post.PostDeletionServiceImpl;

import dagger.Module;
import dagger.Provides;

@Module(includes = DatabaseInstanceModule.class)
public class PostDeletionServiceModule {
    @NonNull
    @Provides
    public PostDeletionService postDeletionService(final DatabaseAdapter databaseAdapter) {
        return new PostDeletionServiceImpl(databaseAdapter);
    }
}
