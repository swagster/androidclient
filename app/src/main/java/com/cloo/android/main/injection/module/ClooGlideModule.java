package com.cloo.android.main.injection.module;

import android.content.Context;
import android.support.annotation.Keep;
import android.support.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.module.GlideModule;

@Keep
public class ClooGlideModule implements GlideModule
{
    @Override
    public final void applyOptions(final Context context, @NonNull final GlideBuilder builder)
    {
        builder.setDiskCache(new InternalCacheDiskCacheFactory(context, 20000000));
        builder.setMemoryCache(new LruResourceCache(150000));
        builder.setBitmapPool(new LruBitmapPool(150000));
    }

    @Override
    public void registerComponents(final Context context, final Glide glide)
    {

    }
}
