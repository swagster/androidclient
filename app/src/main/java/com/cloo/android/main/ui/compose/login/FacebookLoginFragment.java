package com.cloo.android.main.ui.compose.login;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentFacebookLoginBinding;
import com.cloo.android.main.model.FacebookCredential;
import com.cloo.android.main.model.ParcelableCredential;
import com.cloo.android.main.ui.base.BaseFragment;
import com.cloo.android.main.util.FacebookUtils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;

import java.util.Collection;

import static com.cloo.android.main.util.FacebookUtils.requestFacebookEmail;

public class FacebookLoginFragment extends BaseFragment implements FacebookCallback<LoginResult>
{
    private static final String TAG = FacebookLoginFragment.class.getSimpleName();
    private final CallbackManager callbackManager = CallbackManager.Factory.create();
    @Nullable private ParcelableCredential providerCredentials = null;

    @NonNull
    public static FacebookLoginFragment newInstance(@NonNull final Parcelable credential)
    {
        final Bundle bundle = new Bundle();
        bundle.putParcelable(LinkProviderDialog.CREDENTIAL_KEY, credential);
        final FacebookLoginFragment fragment = new FacebookLoginFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private boolean mustLink(@NonNull final Collection<String> providers)
    {
        return !providers.isEmpty() && !getParentActivity().getAuthentication()
                                                           .hasFacebookProvider(providers);
    }

    @Override
    public final void onSuccess(@NonNull final LoginResult loginResult)
    {
        Log.d(FacebookLoginFragment.TAG, "facebook:onSuccess:" + loginResult);
        final LoginActivity activity = (LoginActivity) getActivity();
        activity.startLoading();
        processLoginResult(loginResult.getAccessToken());
    }

    @Override
    public final void onCancel()
    {
        Log.d(FacebookLoginFragment.TAG, "facebook:onCancel");
    }

    @Override
    public final void onError(@NonNull final FacebookException error)
    {
        Log.d(FacebookLoginFragment.TAG, "facebook:onError", error);
    }

    @Override
    public final void onActivityCreated(final Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        readArgumentBundle(savedInstanceState);
    }

    @Override
    public final void onSaveInstanceState(@NonNull final Bundle outState)
    {
        Log.d(FacebookLoginFragment.TAG, "Save instance FbFragment");
        if (providerCredentials != null) {
            outState.putParcelable(LinkProviderDialog.CREDENTIAL_KEY, providerCredentials);
        }
    }

    @Override
    public final void onActivityResult(final int requestCode, final int resultCode,
                                       @Nullable final Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    @NonNull
    public final View onCreateView(@NonNull final LayoutInflater inflater,
                                   @Nullable final ViewGroup container,
                                   @Nullable final Bundle savedInstanceState)
    {
        final FragmentFacebookLoginBinding binding = DataBindingUtil.inflate(inflater,
                                                                             R.layout.fragment_facebook_login,
                                                                             container, false);
        binding.buttonFacebookLogin.setReadPermissions("email", "public_profile", "user_birthday");
        binding.buttonFacebookLogin.registerCallback(callbackManager, this);
        readArgumentBundle(getArguments());
        readArgumentBundle(savedInstanceState);
        return binding.getRoot();
    }

    private void readArgumentBundle(@Nullable final Bundle bundle)
    {
        if (bundle != null) {
            Log.d(FacebookLoginFragment.TAG, "Read ParcebalCredential in FbLogin");
            providerCredentials = bundle.getParcelable(LinkProviderDialog.CREDENTIAL_KEY);
        }
    }

    private void processLoginResult(@NonNull final AccessToken token)
    {
        requestFacebookEmail(token, email -> getParentActivity().getAuthentication()
                                                                .getProviders(email, providers -> {
                                                                    if (mustLink(providers)) {
                                                                        FacebookUtils.logout();
                                                                        openCredentialLinkDialog(
                                                                                token, email);
                                                                    }
                                                                    else {
                                                                        signIn(new FacebookCredential(
                                                                                token));
                                                                    }
                                                                }));
    }

    private void openCredentialLinkDialog(@NonNull final AccessToken token,
                                          @NonNull final String email)
    {
        final LinkProviderDialog linkFacebookFragment = LinkProviderDialog.newInstance(email,
                                                                                       new FacebookCredential(
                                                                                               token));
        getChildFragmentManager().beginTransaction()
                                 .addToBackStack(null)
                                 .add(linkFacebookFragment, null)
                                 .commit();
    }

    private void signIn(@NonNull final ParcelableCredential credentials)
    {
        final LoginActivity activity = (LoginActivity) getActivity();
        Log.d(FacebookLoginFragment.TAG, "Link With Facebook-Account " + providerCredentials);
        getParentActivity().getAuthentication()
                           .signIn(credentials, providerCredentials, success -> {
                               if (success != null) {
                                   FacebookUtils.logout();
                               }
                               else {
                                   providerCredentials = null;
                               }
                               activity.endLoading();
                           });
    }
}