package com.cloo.android.main.backend.database;

import android.support.annotation.NonNull;

import com.cloo.android.main.function.DatabaseReceiveCallback;
import com.cloo.android.main.function.ReceiveCallback;

public interface DatabaseReference
{

    <T> void addChildListener(Class<T> type, DatabaseReceiveCallback<T> addListener,
                              DatabaseReceiveCallback<T> changeListener,
                              DatabaseReceiveCallback<T> movedListener,
                              DatabaseReceiveCallback<T> removeListener,
                              ReceiveCallback<Exception> errorListener);

    @NonNull DatabaseQuery limitToLast(int count);

    @NonNull DatabaseQuery orderBy(String field);

    void keepSynced(boolean b);

    @NonNull DatabaseQuery limitToFirst(int currentLimit);

    void removeListeners();

    void recycle();
}
