package com.cloo.android.main.injection.component;

import com.cloo.android.main.injection.module.PostDeletionServiceModule;
import com.cloo.android.main.injection.module.VoteServiceModule;
import com.cloo.android.main.service.business.post.PostDeletionService;
import com.cloo.android.main.service.business.post.VoteService;

import dagger.Component;

@Component(modules = {VoteServiceModule.class, PostDeletionServiceModule.class})
public interface PostActivityComponent {
    VoteService getVoteService();

    PostDeletionService getPostDeletionService();
}
