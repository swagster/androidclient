package com.cloo.android.main.ui.compose.user;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.cloo.android.R;
import com.cloo.android.databinding.ActivityFollowsListBinding;
import com.cloo.android.main.model.User;
import com.cloo.android.main.ui.base.AuthenticatedActivity;
import com.cloo.android.main.ui.common.AddByUsernameDialog;

public class FollowsListActivity extends AuthenticatedActivity
{
    private static final String BUNDLE_USER_ID = "userId";

    @NonNull
    public static Intent newIntent(final Context context, final String userId)
    {
        final Intent intent = new Intent(context, FollowsListActivity.class);
        final Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_USER_ID, userId);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected final void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        final ActivityFollowsListBinding binding = DataBindingUtil.setContentView(this,
                                                                                  R.layout.activity_follows_list);

        final Intent intent = getIntent();
        final FollowedUserListFragment fragment = new FollowedUserListFragment();
        fragment.setArguments(intent.getExtras());
        getSupportFragmentManager().beginTransaction().add(R.id.inject_fragment, fragment).commit();
        setupBackButton(binding.toolbar);
        final String userId = intent.getExtras()
                                    .getString(BUNDLE_USER_ID,
                                               getAuthentication().getSession().getUserId());
        getDatabase().getUserDetailsNode(userId)
                     .receive(User.class, (k, obj) -> setTitle(getAuthentication().getSession()
                                                                                  .getUserId()
                                                                                  .equals(userId) ? getString(
                             R.string.title_follows_list_you) : String.format(
                             getString(R.string.title_follows_list), obj.getUsername())))
                     .recycleAfterUse()
                     .submit();

        if (userId.equals(getAuthentication().getSession().getUserId())) {
            binding.addFriendAction.setOnClickListener(v -> {
                final AddByUsernameDialog dialog = new AddByUsernameDialog();
                getSupportFragmentManager().popBackStack();
                dialog.show(getSupportFragmentManager(), "add_user");
            });
        }
        else {
            binding.addFriendAction.setVisibility(View.GONE);
        }
    }
}
