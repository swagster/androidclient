package com.cloo.android.main.injection.module;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.backend.database.FirebaseDatabaseAdapter;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule
{
    @NonNull
    @Provides
    @Named("databaseAdapter")
    public DatabaseAdapter databaseAdapter()
    {
        return new FirebaseDatabaseAdapter();
    }
}
