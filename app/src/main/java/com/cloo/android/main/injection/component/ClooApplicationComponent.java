package com.cloo.android.main.injection.component;

import com.cloo.android.main.backend.authentication.Authentication;
import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.injection.module.AuthenticationModule;
import com.cloo.android.main.injection.module.DatabaseModule;
import com.cloo.android.main.injection.module.ImageLoaderModule;
import com.cloo.android.main.service.util.image.ImageLoader;

import javax.inject.Named;

import dagger.Component;

@Component(modules = {ImageLoaderModule.class, DatabaseModule.class, AuthenticationModule.class})
public interface ClooApplicationComponent {
    ImageLoader getImageLoader();

    @Named("databaseAdapter")
    DatabaseAdapter getDatabase();

    Authentication getAuthentication();
}
