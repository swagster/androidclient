package com.cloo.android.main.service.business.profile;

import android.support.annotation.NonNull;
import android.util.Log;

import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.function.ReceiveCallback;
import com.cloo.android.main.backend.authentication.Session;

import java.util.HashMap;
import java.util.Map;

public class UserCreationServiceImpl implements UserCreationService
{
    private static final String TAG = UserCreationServiceImpl.class.getSimpleName();
    private final UsernameService usernameService;
    private final DatabaseAdapter database;

    public UserCreationServiceImpl(final UsernameService usernameService,
                                   final DatabaseAdapter database)
    {
        this.usernameService = usernameService;
        this.database = database;
    }

    @NonNull
    private Map<String, Object> getUserMap(final String userId, final String email,
                                           final String username)
    {
        final Map<String, Object> userMap = new HashMap<>();
        userMap.put("email", email);
        userMap.put("username", username);
        userMap.put("description", "");
        userMap.put("voteCount", 0);
        userMap.put("followerCount", 0);
        userMap.put("followedUserCount", 0);
        userMap.put("createdPostCount", 0);
        userMap.put("clooScore", 0);
        return userMap;
    }

    @Override
    public void create(@NonNull final Session session,
                       @NonNull final ReceiveCallback<Boolean> onSuccess)
    {
        database.getUserDetailsNode(session.getUserId()).exists(exists -> {
            if (!exists) {
                usernameService.generateUsername(username -> {
                    Log.d(TAG, "Received generated username:" + username);
                    database.getUsernameNode(username).set(session.getUserId()).onSuccess(() -> {
                        Log.d(TAG, "Created username mapping");
                        final Map<String, Object> userMap = getUserMap(session.getUserId(),
                                                                       session.getEmail(),
                                                                       username);
                        database.getUserDetailsNode(session.getUserId())
                                .setChildren(userMap)
                                .onSuccess(() -> {
                                    Log.d(TAG, "Update user details");
                                    onSuccess.onReceive(true);
                                })
                                .onError(error -> {
                                    Log.d(TAG, "Failed to update user details");
                                    onSuccess.onReceive(false);
                                })
                                .recycleAfterUse()
                                .submit();
                    }).onError(error -> {
                        Log.d(TAG, "Failed to generate username mapping");
                        onSuccess.onReceive(false);
                    }).recycleAfterUse().submit();
                }, error -> onSuccess.onReceive(false));
            }
            else {
                onSuccess.onReceive(true);
            }
        }).recycleAfterUse().submit();
    }
}
