package com.cloo.android.main.backend.database;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pools;
import android.util.Log;

import com.cloo.android.main.function.DatabaseReceiveCallback;
import com.cloo.android.main.function.ExecutionCallback;
import com.cloo.android.main.function.FinishCallback;
import com.cloo.android.main.function.ReceiveCallback;
import com.cloo.android.main.util.Assert;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import timber.log.Timber;

final class FirebaseDatabaseNode implements DatabaseNode
{
    private static final AtomicReference<DatabaseReference> s_databaseReference = new AtomicReference<>();
    private static final Pools.Pool<FirebaseDatabaseNode> nodeReferencePool = new Pools.SimplePool<>(
            96);
    private static final FinishCallback v = s -> {
    };
    private final List<String> lockedListeners = new ArrayList<>();
    private final Map<Object, ValueEventListener> listeners = new HashMap<>();
    private boolean synced = false;
    private volatile boolean recycled = false;
    @Nullable private DatabaseReference node = null;

    private FirebaseDatabaseNode()
    {

    }

    private FirebaseDatabaseNode(@NonNull final String uri)
    {
        node = getDatabaseReference().child(uri);
    }

    private FirebaseDatabaseNode(@NonNull final DatabaseReference reference)
    {
        node = reference;
    }

    @NonNull
    private static DatabaseReference getDatabaseReference()
    {
        if (s_databaseReference.get() == null) {
            s_databaseReference.compareAndSet(null, FirebaseDatabase.getInstance().getReference());
            Assert.check(s_databaseReference != null, "DatabaseReference is null in ClooDatabase");
        }
        return s_databaseReference.get();
    }

    static DatabaseNode create(@NonNull final String uri)
    {
        FirebaseDatabaseNode ref = FirebaseDatabaseNode.nodeReferencePool.acquire();
        if (ref == null) {
            ref = new FirebaseDatabaseNode(uri);
        }
        else {
            ref.node = getDatabaseReference().child(uri);
            ref.reuse();
        }
        return ref;
    }

    private static DatabaseNode create(@NonNull final DatabaseReference reference)
    {
        FirebaseDatabaseNode ref = FirebaseDatabaseNode.nodeReferencePool.acquire();
        if (ref == null) {
            ref = new FirebaseDatabaseNode(reference);
        }
        else {
            ref.node = reference;
            ref.reuse();
        }
        return ref;
    }

    @NonNull
    public static DatabaseNode getConnectedReference()
    {
        final FirebaseDatabaseNode node = new FirebaseDatabaseNode();
        node.node = FirebaseDatabase.getInstance().getReference(".info/connected");
        return node;
    }

    private void reuse()
    {
        synchronized (lockedListeners) {
            recycled = false;
        }
    }

    @Override
    public void recycle()
    {
        synchronized (lockedListeners) {
            Assert.check(!recycled, "Tried to recycled twice");
            recycled = true;
            removeListeners();
            lockedListeners.clear();
            if (synced) { node.keepSynced(false); }
            synced = false;

        }

        FirebaseDatabaseNode.nodeReferencePool.release(this);
    }

    private <T> void addValueEventListener(final Class<T> type, final boolean isArray,
                                           @NonNull final DatabaseReceiveCallback<T> listener,
                                           @NonNull final ReceiveCallback<Exception> errorListener)
    {
        Assert.check(!recycled, "Tried to use DatabaseNode");
        final ValueEventListener valueEventListener = new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot)
            {
                if (recycled) {
                    return;
                }
                if (isArray) {
                    for (final DataSnapshot child : dataSnapshot.getChildren()) {
                        listener.onReceive(child.getKey(), child.getValue(type));
                    }
                }
                else {
                    try {
                        listener.onReceive(dataSnapshot.getKey(),
                                           dataSnapshot.exists() ? dataSnapshot.getValue(
                                                   type) : null);
                    } catch (@NonNull final DatabaseException exception) {
                        Timber.e(
                                "Failed in valueListener onChange from data:" + dataSnapshot + " into type:" + type);
                        errorListener.onReceive(exception);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError)
            {
                if (recycled) {
                    return;
                }
                errorListener.onReceive(databaseError.toException());
            }
        };
        node.addValueEventListener(valueEventListener);
        listeners.put(listener, valueEventListener);
    }

    @SuppressWarnings("AnonymousInnerClassWithTooManyMethods")
    @Override
    public <T> void addChildListener(final Class<T> type,
                                     @NonNull final DatabaseReceiveCallback<T> addListener,
                                     @NonNull final DatabaseReceiveCallback<T> changeListener,
                                     @NonNull final DatabaseReceiveCallback<T> movedListener,
                                     @NonNull final DatabaseReceiveCallback<T> removeListener,
                                     @NonNull final ReceiveCallback<Exception> errorListener)
    {
        Assert.check(!recycled, "Tried to use DatabaseNode");
        node.addChildEventListener(new ChildEventListener()
        {
            @Override
            public void onChildAdded(@NonNull final DataSnapshot dataSnapshot,
                                     @NonNull final String s)
            {
                if (recycled) {
                    return;
                }
                try {
                    addListener.onReceive(s, dataSnapshot.getValue(type));
                } catch (@NonNull final DatabaseException exception) {
                    Timber.e(
                            "Failed in childListener childAdded from data:" + dataSnapshot + " into type:" + type);
                    errorListener.onReceive(exception);
                }
            }

            @Override
            public void onChildChanged(@NonNull final DataSnapshot dataSnapshot,
                                       @NonNull final String s)
            {
                if (recycled) {
                    return;
                }
                try {
                    changeListener.onReceive(s, dataSnapshot.getValue(type));
                } catch (@NonNull final DatabaseException exception) {
                    Timber.e(
                            "Failed in childListener childChanged from data:" + dataSnapshot + " into type:" + type);
                    errorListener.onReceive(exception);
                }
            }

            @Override
            public void onChildRemoved(@NonNull final DataSnapshot dataSnapshot)
            {
                if (recycled) {
                    return;
                }
                try {
                    removeListener.onReceive(dataSnapshot.getKey(), dataSnapshot.getValue(type));
                } catch (@NonNull final DatabaseException exception) {
                    Timber.e(
                            "Failed in childListener childRemoved from data:" + dataSnapshot + " into type:" + type);
                    errorListener.onReceive(exception);
                }
            }

            @Override
            public void onChildMoved(@NonNull final DataSnapshot dataSnapshot,
                                     @NonNull final String s)
            {
                if (recycled) {
                    return;
                }
                try {
                    movedListener.onReceive(s, dataSnapshot.getValue(type));
                } catch (@NonNull final DatabaseException exception) {
                    Timber.e(
                            "Failed in childListener childMoved from data:" + dataSnapshot + " into type:" + type);
                    errorListener.onReceive(exception);
                }
            }

            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError)
            {
                if (recycled) {
                    return;
                }
                errorListener.onReceive(databaseError.toException());
            }
        });
    }

    @Override
    public void keepSynced(final boolean b)
    {
        Assert.check(!recycled, "Tried to use DatabaseNode");
        synced = b;
        node.keepSynced(b);
    }

    @Override
    @NonNull
    public DatabaseQuery limitToFirst(final int currentLimit)
    {
        Assert.check(!recycled, "Tried to use DatabaseNode");
        return new DatabaseQuery(node.limitToFirst(currentLimit));
    }

    @Override
    public void removeListeners()
    {
        for (final ValueEventListener listener : listeners.values()) {
            node.removeEventListener(listener);
        }
        listeners.clear();
    }

    @Override
    @NonNull
    public DatabaseNode child(@NonNull final String key)
    {
        Assert.check(!recycled, "Tried to use DatabaseNode");
        return create(node.child(key));
    }

    @Override
    @NonNull
    public DatabaseNode generateChild()
    {
        Assert.check(!recycled, "Tried to use DatabaseNode");
        return create(node.push());
    }

    @Override
    public <T> void getChildren(final Class<T> type,
                                @NonNull final DatabaseReceiveCallback<T> successCallback,
                                @NonNull final ReceiveCallback<Exception> errorCallback,
                                @Nullable final FinishCallback finishCallback)
    {
        Assert.check(!recycled, "Tried to use DatabaseNode");
        node.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot)
            {
                if (recycled) {
                    return;
                }
                for (final DataSnapshot child : dataSnapshot.getChildren()) {
                    successCallback.onReceive(child.getKey(),
                                              child.exists() ? child.getValue(type) : null);
                }
                if (finishCallback != null) {
                    finishCallback.onFinish(true);
                }
            }

            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError)
            {
                if (recycled) {
                    return;
                }
                errorCallback.onReceive(databaseError.toException());
            }
        });
    }

    private void handleDatabaseErrors(@NonNull final DatabaseException exception)
    {
        Timber.e("Unexpected database error on node " + node, exception);
        //if (exception.getMessage().contains("denied")) {
        //}
    }

    private <T> void get(final Class<T> type, @NonNull final DatabaseReceiveCallback<T> listener,
                         final ReceiveCallback<Exception> errorListener)
    {
        get(type, listener, errorListener, FirebaseDatabaseNode.v);
    }

    private <T> void get(final Class<T> type, @NonNull final DatabaseReceiveCallback<T> listener,
                         @Nullable final ReceiveCallback<Exception> errorListener,
                         @NonNull final FinishCallback callback)
    {
        Assert.check(!recycled, "Tried to use DatabaseNode");

        node.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot)
            {
                if (recycled) {
                    return;
                }
                try {
                    listener.onReceive(dataSnapshot.getKey(),
                                       dataSnapshot.exists() ? dataSnapshot.getValue(type) : null);
                } catch (@NonNull final DatabaseException exception) {
                    handleDatabaseErrors(exception);
                    errorListener.onReceive(exception);
                }
                callback.onFinish(true);
            }

            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError)
            {
                if (recycled) {
                    return;
                }
                if (errorListener != null) {
                    errorListener.onReceive(databaseError.toException());
                }
                else {
                    Log.e("DatabaseNode", "Failed to retrieve data", databaseError.toException());
                }
                callback.onFinish(false);
            }
        });
    }

    private void delete(@NonNull final ExecutionCallback executionCallback,
                        @NonNull final ReceiveCallback<Exception> errorCallback)
    {
        Assert.check(!recycled, "Tried to use DatabaseNode");
        final String timestamp = String.valueOf(System.nanoTime());
        lockedListeners.add(timestamp);
        final ValueEventListener valueEventListener = new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot)
            {
                if (recycled) {
                    return;
                }
                synchronized (lockedListeners) {
                    // An valueEventListener update can occur between the actual update.
                    if (lockedListeners.contains(timestamp)
                            // check that the change is the deletion
                            && !dataSnapshot.exists()) {
                        lockedListeners.remove(timestamp);
                        if (executionCallback != null) { executionCallback.execute(); }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError)
            {
                if (recycled) {
                    return;
                }
                synchronized (lockedListeners) {
                    if (lockedListeners.contains(timestamp)) {
                        lockedListeners.remove(timestamp);
                        errorCallback.onReceive(databaseError.toException());
                    }
                }
            }
        };
        node.addValueEventListener(valueEventListener);
        node.removeValue((databaseError, databaseReference) -> {
            if (recycled) {
                return;
            }
            node.removeEventListener(valueEventListener);
        });
    }

    @Override
    public String getKey()
    {
        Assert.check(!recycled, "Tried to getLocalKey after recycling");
        return node.getKey();
    }


    @Override
    @NonNull
    public DatabaseQuery limitToLast(final int count)
    {
        Assert.check(!recycled, "Tried to call limitToLast on recycled DatabaseNode");
        return new DatabaseQuery(node.limitToLast(count));
    }

    @Override
    @NonNull
    public DatabaseQuery orderBy(@NonNull final String field)
    {
        Assert.check(!recycled, "Tried to call orderBy on recycled DatabaseNode");
        return new DatabaseQuery(node.orderByChild(field));
    }

    @Override
    public String toString()
    {
        return node.getRef().toString();
    }


    @Override
    public <T> RequestBuilder receive(final Class<T> type,
                                      @NonNull final DatabaseReceiveCallback<T> receiveCallback)
    {
        return FirebaseRequestBuilder.get(this, (e, f, s) -> get(type, receiveCallback, object -> {
            if (e != null) { e.onError(DatabaseCodes.OPERATION_UNAVAILABLE); }
        }, success -> {
            if (f != null) { f.execute(); }
        }));
    }

    @Override
    public <T> RequestBuilder onListUpdates(final Class<T> type,
                                            @NonNull final DatabaseReceiveCallback<T> receiveCallback)
    {
        return FirebaseRequestBuilder.get(this, (e, f, s) -> addValueEventListener(type, true,
                                                                                   receiveCallback,
                                                                                   object -> {
                                                                                       if (e != null) {
                                                                                           e.onError(
                                                                                                   DatabaseCodes.OPERATION_UNAVAILABLE);
                                                                                       }
                                                                                   }));
    }

    @Override
    public <T> RequestBuilder onUpdates(final Class<T> type,
                                        @NonNull final DatabaseReceiveCallback<T> receiveCallback)
    {
        return FirebaseRequestBuilder.get(this, (e, f, s) -> addValueEventListener(type, false,
                                                                                   receiveCallback,
                                                                                   object -> {
                                                                                       if (e != null) {
                                                                                           e.onError(
                                                                                                   DatabaseCodes.OPERATION_UNAVAILABLE);
                                                                                       }
                                                                                   }));
    }

    @Override
    public RequestBuilder exists(@NonNull final ReceiveCallback<Boolean> receiveCallback)
    {
        return FirebaseRequestBuilder.get(this, (e, f, s) -> exists(receiveCallback, () -> {
        }, error -> {
            if (e != null) { e.onError(DatabaseCodes.OPERATION_UNAVAILABLE); }
        }, () -> {
            if (f != null) { f.execute(); }
        }));
    }

    @Override
    public RequestBuilder set(@NonNull final Object value)
    {
        return FirebaseRequestBuilder.get(this,
                                          (errorCallback, finishCallback, successCallback) -> setValue(
                                                  value, (k, o) -> {
                                                      if (successCallback != null) {
                                                          successCallback.execute();
                                                      }
                                                  }, error -> {
                                                      if (error != null) {
                                                          errorCallback.onError(
                                                                  DatabaseCodes.OPERATION_UNAVAILABLE);
                                                      }
                                                  }, finishCallback));
    }

    private void setValue(@NonNull final Object value,
                          @NonNull final DatabaseReceiveCallback<Object> callback,
                          @NonNull final ReceiveCallback<Exception> errorCallback,
                          @NonNull final ExecutionCallback executionCallback)
    {
        Assert.check(!recycled, "Tried to setValue after recycling");
        // To receive local updates even in offline mode a ValueEventListener is needed.
        // ValueEventListeners can be called multiple times on one update, so we need to ensure it's
        // exectued only once.
        final String timestamp = String.valueOf(System.nanoTime());
        synchronized (lockedListeners) {
            lockedListeners.add(timestamp);
        }

        final ValueEventListener valueEventListener = new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot)
            {
                if (recycled) {
                    return;
                }
                synchronized (lockedListeners) {
                    try {
                        // Check if the ValueEventListener was triggered already
                        if (lockedListeners.contains(timestamp) && dataSnapshot.exists()
                                // Check if the new Value is the given value. An valueEventListener update
                                // could be triggered with another value before the update of setValue
                                && dataSnapshot.getValue(value.getClass()) == value) {
                            lockedListeners.remove(timestamp);
                            callback.onReceive(dataSnapshot.getKey(), value);
                            executionCallback.execute();
                            Timber.e(
                                    "LockedListeners on Node after update " + lockedListeners.toString() + " Exectued " + executionCallback);
                        }
                    }
                    // Catch errors raised due to database layout corruption (Value type doesn't match expected type)
                    catch (@NonNull final DatabaseException exception) {
                        handleDatabaseErrors(exception);
                        // Check if the ValueEventListener was triggered already
                        if (lockedListeners.contains(timestamp)) {
                            lockedListeners.remove(timestamp);
                            errorCallback.onReceive(exception);
                            executionCallback.execute();
                            Timber.e(
                                    "LockedListeners on Node after update " + lockedListeners.toString());
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError)
            {
                if (recycled) {
                    return;
                }
                synchronized (lockedListeners) {
                    // Check if the ValueEventListener was triggered already
                    if (lockedListeners.contains(timestamp)) {
                        lockedListeners.remove(timestamp);
                        errorCallback.onReceive(databaseError.toException());
                        executionCallback.execute();
                    }
                }
            }
        };

        node.addValueEventListener(valueEventListener);
        node.setValue(value, (databaseError, databaseReference) -> {
            if (recycled) {
                return;
            }
            node.removeEventListener(valueEventListener);
        });
    }

    @Override
    public RequestBuilder delete()
    {
        return FirebaseRequestBuilder.get(this,
                                          (errorCallback, finishCallback, successCallback) -> delete(
                                                  successCallback,
                                                  errorCode -> errorCallback.onError(
                                                          DatabaseCodes.OPERATION_UNAVAILABLE)));
    }

    @Override
    public RequestBuilder setChildren(@NonNull final Map<String, Object> childValues)
    {
        return FirebaseRequestBuilder.get(this,
                                          (errorCallback, finishCallback, successCallback) -> updateChildren(
                                                  childValues, success -> successCallback.execute(),
                                                  error -> errorCallback.onError(
                                                          DatabaseCodes.OPERATION_UNAVAILABLE)));
    }

    /**
     * Updates the child data with the given map object. On finish the supplied FinishCallback is called (also on error). On Error additionally the errorListener is called and supplies a Exception.
     */
    private void updateChildren(@NonNull final Map<String, Object> post,
                                @NonNull final FinishCallback listener,
                                @NonNull final ReceiveCallback<Exception> errorListener)
    {
        Assert.check(!recycled, "Tried to call update Children after recycled");
        node.updateChildren(post, (databaseError, databaseReference) -> {
            if (recycled) {
                return;
            }
            if (databaseError != null) {
                errorListener.onReceive(databaseError.toException());
            }
            listener.onFinish(databaseError == null);
        });
    }

    private void exists(@NonNull final ReceiveCallback<Boolean> existCallback,
                        @NonNull final ExecutionCallback successCallback,
                        @NonNull final ReceiveCallback<Exception> errorCallback,
                        @NonNull final ExecutionCallback finishCallback)
    {
        Assert.check(!recycled, "Tried to use DatabaseNode");
        node.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot)
            {
                if (recycled) {
                    return;
                }
                existCallback.onReceive(dataSnapshot.exists());
                successCallback.execute();
                finishCallback.execute();
            }

            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError)
            {
                if (recycled) {
                    return;
                }
                errorCallback.onReceive(databaseError.toException());
                finishCallback.execute();
            }
        });
    }

    @FunctionalInterface
    interface BuildFinishCallback
    {
        void onBuild(DatabaseCodes.DatabaseOperationErrorCallback errorCallback,
                     ExecutionCallback finishCallback, ExecutionCallback successCallback);
    }

    private static final class FirebaseRequestBuilder implements RequestBuilder
    {
        private static final Pools.Pool<FirebaseRequestBuilder> s_pool = new Pools.SimplePool<>(15);
        @Nullable public DatabaseCodes.DatabaseOperationErrorCallback errorCallback = null;
        boolean recycled = false;
        @Nullable private BuildFinishCallback buildFinishCallback;
        @Nullable private ExecutionCallback successCallback = null;
        @Nullable private ExecutionCallback finishCallback = null;
        @Nullable private FirebaseDatabaseNode node;
        @Nullable private final ExecutionCallback cleanupCallback = () -> {
            successCallback = null;
            errorCallback = null;
            buildFinishCallback = null;
            finishCallback = null;
            recycled = false;
            node = null;
        };


        private FirebaseRequestBuilder(@NonNull final FirebaseDatabaseNode node,
                                       @NonNull final BuildFinishCallback buildFinishCallback)
        {
            this.node = node;
            this.buildFinishCallback = buildFinishCallback;
        }

        public static RequestBuilder get(@NonNull final FirebaseDatabaseNode firebaseDatabaseNode,
                                         @NonNull final BuildFinishCallback buildFinishCallback)
        {
            final FirebaseRequestBuilder builder = s_pool.acquire();
            if (builder != null) {
                builder.node = firebaseDatabaseNode;
                builder.buildFinishCallback = buildFinishCallback;
                builder.successCallback = null;
                builder.errorCallback = null;
                return builder;
            }
            else {
                return new FirebaseRequestBuilder(firebaseDatabaseNode, buildFinishCallback);
            }
        }

        @NonNull
        @Override
        public RequestBuilder onError(
                @NonNull final DatabaseCodes.DatabaseOperationErrorCallback errorCallback)
        {
            Assert.check(this.errorCallback == null, "Don't set onError twice");
            this.errorCallback = errorCallback;
            return this;
        }

        @NonNull
        @Override
        public RequestBuilder onSuccess(@NonNull final ExecutionCallback successCallback)
        {
            Assert.check(this.successCallback == null, "Don't set successCallback twice");
            this.successCallback = successCallback;
            return this;
        }

        @NonNull
        @Override
        public RequestBuilder onFinish(@NonNull final ExecutionCallback finishCallback)
        {
            Assert.check(this.finishCallback == null, "Don't set finishCallback twice");
            this.finishCallback = finishCallback;
            return this;
        }

        @Override
        public void submit()
        {
            if (successCallback == null) { successCallback = () -> {}; }
            if (errorCallback == null) {
                errorCallback = errorCode -> {

                };
            }
            if (recycled) {
                buildFinishCallback.onBuild(errorCallback, () -> {
                    if (finishCallback != null) {
                        finishCallback.execute();
                    }
                    node.recycle();
                    cleanupCallback.execute();
                    s_pool.release(this);
                }, successCallback);
                return;
            }
            buildFinishCallback.onBuild(errorCallback, () -> {
                if (finishCallback != null) {
                    finishCallback.execute();
                }
                cleanupCallback.execute();
                s_pool.release(this);
            }, successCallback);
        }

        @NonNull
        @Override
        public RequestBuilder recycleAfterUse()
        {
            recycled = true;
            return this;
        }
    }
}
