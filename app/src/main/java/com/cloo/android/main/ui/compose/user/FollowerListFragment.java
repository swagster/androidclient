package com.cloo.android.main.ui.compose.user;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;

import com.cloo.android.databinding.FragmentListBinding;
import com.cloo.android.main.backend.database.DatabaseQuery;
import com.cloo.android.main.model.User;
import com.cloo.android.main.ui.base.DatabaseQueryList;
import com.cloo.android.main.ui.base.DatabaseQueryListAdapter;

import static android.view.View.GONE;

public class FollowerListFragment extends DatabaseQueryList<User>
{
    String userId = "";

    @Nullable
    @Override
    public final DatabaseQueryListAdapter<User> createAdapter()
    {
        return new UserListAdapter(getAuthenticatedActivity());
    }

    @Override
    public final void setupView(@NonNull final FragmentListBinding binding)
    {
        if (getParameters() != null) {
            userId = getParameters().getString("userId",
                                               getAuthenticatedActivity().getAuthentication()
                                                                         .getSession()
                                                                         .getUserId());
        }
        else {
            userId = getAuthenticatedActivity().getAuthentication().getSession().getUserId();
        }
    }

    @Override
    public void setupPromotedAction(@NonNull final FloatingActionButton fab)
    {
        fab.setVisibility(GONE);
    }

    @Override
    protected void onAdded(@NonNull final String key, final Object nothing)
    {
        if (!key.equals(userId)) {
            super.onAdded(key, nothing);
        }
    }

    @NonNull
    @Override
    public final DatabaseQuery createQuery(final String endKey, final int count)
    {
        return getAuthenticatedActivity().getDatabase()
                                         .getFollowersNode(userId)
                                         .limitToFirst(count);
    }
}
