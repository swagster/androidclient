package com.cloo.android.main.injection.component;

import com.cloo.android.main.backend.storage.Storage;
import com.cloo.android.main.injection.module.CameraServiceModule;
import com.cloo.android.main.injection.module.PostCreationServiceModule;
import com.cloo.android.main.injection.module.ServerConnectionListenerModule;
import com.cloo.android.main.injection.module.StorageModule;
import com.cloo.android.main.service.business.camera.CameraService;
import com.cloo.android.main.service.business.post.PostCreationService;
import com.cloo.android.main.service.util.network.ServerConnectionListener;

import dagger.Component;

@Component(modules = {ServerConnectionListenerModule.class, PostCreationServiceModule.class, CameraServiceModule.class, StorageModule.class})
public interface PostCreationActivityComponent {
    PostCreationService getPostCreationService();

    ServerConnectionListener getServerConnectionListener();

    CameraService getCameraService();

    Storage getStorage();
}
