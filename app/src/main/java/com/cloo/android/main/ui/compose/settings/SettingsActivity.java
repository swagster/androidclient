package com.cloo.android.main.ui.compose.settings;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.cloo.android.R;
import com.cloo.android.databinding.ActivitySettingsBinding;
import com.cloo.android.main.ui.base.AuthenticatedActivity;

import java.util.ArrayList;
import java.util.List;

import static android.widget.Toast.LENGTH_LONG;

public class SettingsActivity extends AuthenticatedActivity
{

    @Override
    protected final void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        final ActivitySettingsBinding binding = DataBindingUtil.setContentView(this,
                                                                               R.layout.activity_settings);
        setupBackButton(binding.toolbar);
        binding.settingsLogout.setOnClickListener(v -> getAuthentication().signOut());

        getAuthentication().getProviders(getAuthentication().getSession().getEmail(), providers -> {
            final List<String> list = new ArrayList<>();
            list.addAll(providers);
            final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                                                                    android.R.layout.simple_list_item_1,
                                                                    list);
            binding.authProviders.setAdapter(adapter);

            binding.authProviders.setOnItemClickListener((adapterView, view, i, l) -> {
                if (adapter.getCount() > 1) {
                    final String provid = list.get(i);
                    getAuthentication().removeProvider(provid, success -> {
                        if (!success) { list.add(provid); }
                    });
                    list.remove(i);
                    adapter.notifyDataSetChanged();
                }
                else {
                    Toast.makeText(SettingsActivity.this,
                                   "You need atleast one authentication method", LENGTH_LONG)
                         .show();
                }
            });
        });
    }
}