package com.cloo.android.main.injection.module;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.service.business.profile.UserCreationService;
import com.cloo.android.main.service.business.profile.UserCreationServiceImpl;
import com.cloo.android.main.service.business.profile.UsernameService;

import dagger.Module;
import dagger.Provides;

@Module(includes = {UsernameServiceModule.class, DatabaseInstanceModule.class})
public class UserCreationModule {
    @NonNull
    @Provides
    public UserCreationService userCreationService(final UsernameService usernameService, final DatabaseAdapter databaseAdapter) {
        return new UserCreationServiceImpl(usernameService, databaseAdapter);
    }
}
