package com.cloo.android.main.service.util.location;

import android.location.Location;

import com.cloo.android.main.function.ReceiveCallback;

public interface LocationTrackerService
{
    void getLatestLocation(ReceiveCallback<Location> location);

    void stop();
}
