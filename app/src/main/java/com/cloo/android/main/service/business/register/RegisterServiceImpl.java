package com.cloo.android.main.service.business.register;

import android.support.annotation.NonNull;
import android.util.Log;

import com.cloo.android.main.backend.authentication.Authentication;
import com.cloo.android.main.function.ReceiveCallback;
import com.cloo.android.main.model.EmailPasswordCredential;
import com.cloo.android.main.service.business.profile.UserCreationService;

public class RegisterServiceImpl implements RegisterService
{
    private static final String TAG = RegisterServiceImpl.class.getSimpleName();
    private final Authentication authentication;
    private final UserCreationService userCreationService;

    public RegisterServiceImpl(final Authentication authentication,
                               final UserCreationService userCreationService)
    {
        this.userCreationService = userCreationService;
        this.authentication = authentication;
    }

    @Override
    public void register(@NonNull final String email, @NonNull final String password,
                         @NonNull final ReceiveCallback<Boolean> onSuccess)
    {
        authentication.registerEmailPassword(email, password, success -> {
            Log.d(TAG, "register::registerEmailPassword successes:" + success);
            final EmailPasswordCredential emailPassword = new EmailPasswordCredential();
            emailPassword.email.set(email);
            emailPassword.password.set(password);
            onSuccess.onReceive(success);
            /*
            authentication.signIn(emailPassword, null, error -> {
                Log.d(TAG, "SignIn::error", error);
                onSuccess.onReceive(error == null);
            });*/
        });
    }
}
