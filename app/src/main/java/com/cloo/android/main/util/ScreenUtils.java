package com.cloo.android.main.util;

import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;

public final class ScreenUtils {
    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int toPixels(final int dp, final DisplayMetrics metrics) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
    }

    public static float getAspectRatio(final int width, final int height) {
        if (height >= width) {
            return (float) height / (float) width;
        } else {
            return (float) width / (float) height;
        }
    }
}
