package com.cloo.android.main.ui.compose.register;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentRegisterEmailBinding;
import com.cloo.android.main.backend.authentication.Authentication;
import com.cloo.android.main.function.ReceiveCallback;
import com.cloo.android.main.service.util.network.ServerConnectionListener;
import com.cloo.android.main.service.util.validation.CredentialValidation;
import com.cloo.android.main.ui.base.BaseFragment;
import com.cloo.android.main.util.AndroidUtils;

import java.util.Collection;

import static com.cloo.android.main.util.AndroidUtils.setImageDrawable;
import static com.cloo.android.main.util.AndroidUtils.setTextColorAccent;
import static com.cloo.android.main.util.AndroidUtils.setTextColorNormal;

public class RegisterEmailFragment extends BaseFragment implements ServerConnectionListener.ConnectionChangeCallback, TextWatcher
{
    @Nullable private Authentication authentication = null;
    @Nullable private FragmentRegisterEmailBinding binding = null;
    @Nullable private FloatingActionButton actionButton = null;
    @Nullable private TextView statusMessage = null;
    private boolean isConnected = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater,
                             @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register_email, container,
                                          false);
        binding.email.addTextChangedListener(this);
        return binding.getRoot();
    }

    public void setup(final TextView statusMessage, final FloatingActionButton actionButton,
                      @NonNull final ServerConnectionListener serverConnectionListener,
                      final Authentication authentication)
    {
        this.authentication = authentication;
        serverConnectionListener.addCallback(this);
        this.actionButton = actionButton;
        this.statusMessage = statusMessage;
    }

    public void onSubmit(@NonNull final ReceiveCallback<Boolean> success)
    {
        if (getEmail().isEmpty()) {
            success.onReceive(false);
        }
        else {
            authentication.getProviders(getEmail(),
                                        providers -> success.onReceive(providers.isEmpty()));
        }
    }

    private void checkEmail(@NonNull final String email)
    {
        if (CredentialValidation.isEmailValid(email)) {
            if (isConnected) { authentication.getProviders(email, this::onReceiveProviders); }
        }
        else {
            markEmailAsValid();
        }
    }

    private void onReceiveProviders(@NonNull final Collection<String> providers)
    {
        if (binding != null && !isDetached()) {
            if (providers.isEmpty()) {
                markEmailAsValid();
            }
            else {
                markEmailAsUsed();
            }
        }
    }

    private void markEmailAsUsed()
    {
        if (canAccessUI()) {
            binding.email.setError(getString(R.string.error_register_email_used));
            setTextColorNormal(binding.email, getActivity());
            setImageDrawable(actionButton, getActivity(), R.drawable.v_icon_forward_disabled);
        }
    }

    private void markEmailAsValid()
    {
        setTextColorAccent(binding.email, getActivity());
        setImageDrawable(actionButton, getActivity(), R.drawable.v_icon_forward);
    }

    @Override
    public final void onConnectionChange(final boolean connected)
    {
        if (isDetached()) { return; }
        isConnected = connected;
        if (isConnected) {
            checkEmail(getEmail());
        }
        else {
            setImageDrawable(actionButton, getActivity(), R.drawable.v_icon_forward_disabled);
        }
        statusMessage.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }

    @NonNull
    public String getEmail()
    {
        return AndroidUtils.getString(binding.email);
    }

    @Override
    public void beforeTextChanged(final CharSequence charSequence, final int i, final int i1,
                                  final int i2)
    {

    }

    @Override
    public void onTextChanged(final CharSequence charSequence, final int i, final int i1,
                              final int i2)
    {

    }

    @Override
    public void afterTextChanged(final Editable editable)
    {
        checkEmail(getEmail());
    }
}
