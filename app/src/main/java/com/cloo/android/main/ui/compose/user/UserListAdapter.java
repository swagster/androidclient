package com.cloo.android.main.ui.compose.user;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentListElementFriendBinding;
import com.cloo.android.main.model.Post;
import com.cloo.android.main.model.User;
import com.cloo.android.main.ui.base.BaseActivity;
import com.cloo.android.main.ui.base.DatabaseQueryListAdapter;
import com.cloo.android.main.util.AndroidUtils;
import com.cloo.android.main.util.ListUtils;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class UserListAdapter extends DatabaseQueryListAdapter<User>
{
    @NonNull private final BaseActivity activity;
    @NonNull private final LayoutInflater inflater;
    private final List<User> users = new CopyOnWriteArrayList<>();

    public UserListAdapter(@NonNull final BaseActivity activity)
    {
        this.activity = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private static View getRoot(@NonNull final ViewDataBinding binding)
    {
        return binding.getRoot();
    }

    @Override
    public final int getCount()
    {
        return users.size();
    }

    @Override
    public final Object getItem(final int i)
    {
        return users.get(i);
    }

    @Override
    public final long getItemId(final int i)
    {
        return i;
    }

    private void addUser(@NonNull final User user)
    {
        for (final User user1 : users) {
            if (user1.getUserId().equals(user.getUserId())) {
                users.set(users.indexOf(user1), user);
                return;
            }
        }
        users.add(user);
        notifyDataSetChanged();
    }

    private FragmentListElementFriendBinding getBinding(@Nullable final View recycledView,
                                                        @Nullable final ViewGroup viewGroup)
    {
        if (recycledView == null) {
            return DataBindingUtil.inflate(inflater, R.layout.fragment_list_element_friend,
                                           viewGroup, false);
        }
        else {
            return (FragmentListElementFriendBinding) recycledView.getTag();
        }
    }

    @Override
    public final View getView(final int i, @NonNull final View view,
                              @NonNull final ViewGroup viewGroup)
    {
        final FragmentListElementFriendBinding binding = getBinding(view, viewGroup);
        final User user = users.get(i);

        binding.setLatestPostTitle("");
        if (!user.getProfileImageUrl().isEmpty()) {
            binding.setProfileImageUrl(user.getProfileImageUrl());
        }
        else {
            AndroidUtils.setImageDrawable(binding.profileImage.profilImage, activity,
                                          R.drawable.v_icon_user_picture_placeholder);
        }
        binding.setUsername(user.getUsername());

        final View rowView = UserListAdapter.getRoot(binding);
        rowView.setTag(binding);
        rowView.setOnClickListener(v -> {
            final Intent friendFeedIntent = UserActivity.newIntent(activity.getApplicationContext(),
                                                                   user.getUserId());
            activity.startActivity(friendFeedIntent);
        });
        activity.getDatabase()
                .getCreatedPostsNode(user.getUserId())
                .limitToLast(1)
                .get(Boolean.class, (key, obj) -> activity.getDatabase()
                                                          .getPostDetailsNode(key)
                                                          .receive(Post.class,
                                                                   (k, post) -> binding.setLatestPostTitle(
                                                                           post == null ? "" : post.getTitle()))
                                                          .recycleAfterUse()
                                                          .submit(), error -> {
                });
        return rowView;
    }

    @Override
    public final void addDataObject(@NonNull final String key)
    {
        activity.getDatabase().getUserDetailsNode(key).receive(User.class, (id, user) -> {
            if (user != null) {
                user.setId(id);
                addUser(user);
            }
        }).recycleAfterUse().submit();
    }

    @Override
    public final void removeDataObject(@NonNull final String key)
    {
        if (ListUtils.removeElement(param -> param.getUserId().equals(key), users)) {
            notifyDataSetChanged();
        }
    }

    @Override
    public final User getDataObject(final int index)
    {
        return users.get(index);
    }

    @Override
    public void recycle()
    {

    }

    @Nullable
    @Override
    public String getLastKey()
    {
        return users.isEmpty() ? null : users.get(users.size() - 1).getUserId();
    }
}