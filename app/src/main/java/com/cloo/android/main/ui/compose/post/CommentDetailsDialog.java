package com.cloo.android.main.ui.compose.post;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentCommentDetailsBinding;
import com.cloo.android.main.application.ClooApplication;
import com.cloo.android.main.model.Comment;
import com.cloo.android.main.model.User;
import com.cloo.android.main.service.business.comment.CommentService;
import com.cloo.android.main.service.business.comment.CommentServiceImpl;
import com.cloo.android.main.service.business.common.ReportService;
import com.cloo.android.main.ui.base.BaseActivity;
import com.cloo.android.main.ui.common.ReportDialog;
import com.cloo.android.main.util.TimeFormatUtils;

import java.util.Calendar;

import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_DAYS;
import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_HOURS;
import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_MINUTES;
import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_MONTHS;
import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_SECONDS;
import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_WEEKS;
import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_YEARS;

public class CommentDetailsDialog extends DialogFragment
{
    private static final String BUNDLE_COMMENT = "BUNDLE_COMMENT";
    private static final String BUNDLE_USER = "BUNDLE_USER";
    @NonNull private final Calendar cal = Calendar.getInstance();
    @Nullable private FragmentCommentDetailsBinding binding = null;

    @NonNull
    public static CommentDetailsDialog newInstance(final User user, final Comment comment)
    {
        final Bundle bundle = new Bundle();
        bundle.putParcelable(BUNDLE_USER, user);
        bundle.putParcelable(BUNDLE_COMMENT, comment);
        final CommentDetailsDialog fragment = new CommentDetailsDialog();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        getDialog().getWindow()
                   .setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                              ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @NonNull
    @Override
    public final View onCreateView(@NonNull final LayoutInflater inflater,
                                   @Nullable final ViewGroup container,
                                   @Nullable final Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_comment_details, container,
                                          false);
        binding.setUser(getArguments().getParcelable(BUNDLE_USER));
        binding.setComment(getArguments().getParcelable(BUNDLE_COMMENT));
        binding.commentDetailsMenu.setOnClickListener(v -> {
            if (((ClooApplication) getActivity().getApplicationContext()).getAuthentication()
                                                                         .getSession()
                                                                         .getUserId()
                                                                         .equals(binding.getUser()
                                                                                        .getId())) {
                showRemoveDialog();
            }
            else {
                getFragmentManager().popBackStack();
                final ReportDialog dialog = ReportDialog.newInstance(binding.getComment().getKey(),
                                                                     ReportService.CONTENT_TYPE_COMMENT);
                dialog.show(getActivity().getSupportFragmentManager(), "add_user");
            }
        });
        binding.commentTimePassed.setText(getTimePassed(binding.getComment().timestamp));
        return binding.getRoot();
    }

    @NonNull
    private String getTimePassed(final long timestamp)
    {
        final int unit = TimeFormatUtils.getTimeUnit(timestamp);
        final int passedUnits = (int) TimeFormatUtils.getPassedUnits(unit, timestamp);
        switch (unit) {
            case TIME_UNIT_SECONDS:
                return getActivity().getResources()
                                    .getQuantityString(R.plurals.time_ago_seconds, passedUnits,
                                                       passedUnits);
            case TIME_UNIT_MINUTES:
                return getActivity().getResources()
                                    .getQuantityString(R.plurals.time_ago_minutes, passedUnits,
                                                       passedUnits);
            case TIME_UNIT_HOURS:
                return getActivity().getResources()
                                    .getQuantityString(R.plurals.time_ago_hours, passedUnits,
                                                       passedUnits);
            case TIME_UNIT_DAYS:
                return getActivity().getResources()
                                    .getQuantityString(R.plurals.time_ago_days, passedUnits,
                                                       passedUnits);
            case TIME_UNIT_WEEKS:
                return getActivity().getResources()
                                    .getQuantityString(R.plurals.time_ago_weeks, passedUnits,
                                                       passedUnits);
            case TIME_UNIT_MONTHS:
                return getActivity().getResources()
                                    .getQuantityString(R.plurals.time_ago_months, passedUnits,
                                                       passedUnits);
            case TIME_UNIT_YEARS:
                cal.setTimeInMillis(timestamp);
                return String.format(getActivity().getString(R.string.time_ago_years),
                                     cal.get(Calendar.YEAR));
            default:
                return "";
        }
    }

    private void showRemoveDialog()
    {
        final CommentService commentService = new CommentServiceImpl(
                ((BaseActivity) getActivity()).getDatabase());
        getFragmentManager().popBackStack();
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle("Remove your comment");
        alertDialogBuilder.setMessage("Your comment cannot be restored after you removed it!")
                          .setCancelable(false)
                          .setPositiveButton("Remove", (dialog, id) -> commentService.removeComment(
                                  ((Comment) getArguments().getParcelable(BUNDLE_COMMENT)).getKey(),
                                  this::dismiss, error -> Toast.makeText(getActivity(),
                                                                         "Failed to remove the comment",
                                                                         Toast.LENGTH_LONG).show()))
                          .setNegativeButton("Cancel", (dialog, id) -> dialog.cancel());
        final AlertDialog dialog = alertDialogBuilder.create();
        dialog.show();
        // dialog.getButton(DialogInterface.BUTTON_NEGATIVE)
        //       .setTextColor(getActivity().getColor(R.color.accent));
        // dialog.getButton(DialogInterface.BUTTON_POSITIVE)
        //       .setTextColor(getActivity().getColor(R.color.accent));
    }
}