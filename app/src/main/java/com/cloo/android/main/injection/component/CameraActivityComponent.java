package com.cloo.android.main.injection.component;

import com.cloo.android.main.injection.module.CameraServiceModule;
import com.cloo.android.main.service.business.camera.CameraService;

import dagger.Component;

@FunctionalInterface
@Component(modules = {CameraServiceModule.class})
public interface CameraActivityComponent
{
    CameraService getCameraService();
}
