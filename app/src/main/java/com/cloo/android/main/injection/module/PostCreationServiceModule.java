package com.cloo.android.main.injection.module;

import android.content.Context;
import android.support.annotation.NonNull;

import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.service.business.post.PostCreationService;
import com.cloo.android.main.service.business.post.PostCreationServiceImpl;

import dagger.Module;
import dagger.Provides;

@Module(includes = {DatabaseInstanceModule.class, ContextModule.class})
public class PostCreationServiceModule
{
    @NonNull
    @Provides
    public PostCreationService postCreationService(@NonNull final Context context,
                                                   final DatabaseAdapter databaseAdapter)
    {
        return new PostCreationServiceImpl(context, databaseAdapter);
    }
}
