package com.cloo.android.main.service.business.post;

import android.content.Context;
import android.support.annotation.NonNull;

import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.backend.database.DatabaseCodes;
import com.cloo.android.main.backend.database.DatabaseNode;
import com.cloo.android.main.function.FinishCallback;
import com.cloo.android.main.function.ReceiveCallback;
import com.cloo.android.main.model.PostOption;
import com.cloo.android.main.service.util.validation.PostCreationValidator;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

public class PostCreationServiceImpl implements PostCreationService
{
    private final DatabaseAdapter database;
    @NonNull private final PostCreationValidator validator;

    public PostCreationServiceImpl(@NonNull final Context context, final DatabaseAdapter database)
    {
        this.database = database;
        validator = new PostCreationValidator(context);
    }

    @NonNull
    private Map<String, Object> toMap(final String userId, final int category, final String title,
                                      final String description, final List<PostOption> options)
    {
        final Map<String, Object> result = new HashMap<>(16);
        result.put("description", description);
        result.put("category", category);
        result.put("title", title);
        result.put("options", options);
        result.put("author", userId);
        result.put("timestamp", ServerValue.TIMESTAMP);
        return result;
    }

    @Override
    public final boolean isValidTitle(@NonNull final String title)
    {
        return title.length() <= 32 && !title.trim().isEmpty();
    }

    @Override
    public final boolean isValidDescription(@NonNull final String description)
    {
        return description.length() <= 144;
    }

    @Override
    public final boolean areValidOptions(@NonNull final List<PostOption> options)
    {
        return options.size() >= 2 && options.size() <= 4;
    }

    private void generateMailboxNodes(@NonNull final String userId, final String postId,
                                      @NonNull final FinishCallback callback)
    {
        database.getFollowersNode(userId).getChildren(Object.class, (key, obj) -> {
            final DatabaseNode mailBoxRef = database.getUserMailBoxPostNode(key, postId);
            mailBoxRef.set(true).submit();
        }, error -> callback.onFinish(false), success -> callback.onFinish(true));
    }

    private void generateHashTagNodes(final String... hashtags)
    {

    }

    private void setUserCreated(final String userId, @NonNull final String postId,
                                @NonNull final FinishCallback callback)
    {
        database.getCreatedPostNode(userId, postId)
                .set(true)
                .onSuccess(() -> database.getPostVotesNode(postId)
                                         .set(0)
                                         .onSuccess(() -> callback.onFinish(true))
                                         .onError(error -> callback.onFinish(false))
                                         .recycleAfterUse()
                                         .submit())
                .onError(error -> callback.onFinish(false))
                .recycleAfterUse()
                .submit();
    }

    @Override
    public final void createPost(@NonNull final String userId, final int category,
                                 @NonNull final String title, @NonNull final String description,
                                 @NonNull final List<PostOption> options,
                                 @NonNull final ReceiveCallback<String> listener,
                                 @NonNull final DatabaseCodes.DatabaseOperationErrorCallback errorCallback)
    {
        if (!isValidTitle(title) || !isValidDescription(description) || !areValidOptions(options)) {
            errorCallback.onError(DatabaseCodes.OPERATION_INVALID);
        }

        final DatabaseNode reference = database.getPostDetailsNode().generateChild();
        final String postId = reference.getKey();

        reference.setChildren(toMap(userId, category, title.trim(), description.trim(), options))
                 .onSuccess(() -> {
                     Timber.d("Created post details");
                     generateMailboxNodes(userId, postId, couldCreateNodes -> {
                         if (couldCreateNodes) {
                             Timber.d("Created mailbox nodes");
                             setUserCreated(userId, postId, couldSetUser -> {
                                 Timber.d("Created user mapping");
                                 database.getAllPostsNode(postId).set(true).onSuccess(() -> {
                                     Timber.d("Created all post node");
                                     database.getPostByCategoryNode(category, postId)
                                             .set(true)
                                             .onSuccess(() -> {

                                                 Timber.d("Created category node");
                                                 listener.onReceive(postId);
                                             })
                                             .onError(errorCallback)
                                             .recycleAfterUse()
                                             .submit();
                                 }).onError(errorCallback).recycleAfterUse().submit();
                             });
                         }
                         else {
                             errorCallback.onError(DatabaseCodes.OPERATION_UNAVAILABLE);
                         }
                     });
                 })
                 .onError(e -> errorCallback.onError(DatabaseCodes.OPERATION_UNAVAILABLE))
                 .recycleAfterUse()
                 .submit();
    }
}
