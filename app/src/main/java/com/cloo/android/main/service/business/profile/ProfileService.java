package com.cloo.android.main.service.business.profile;

import android.util.SparseArray;

import com.cloo.android.main.backend.database.DatabaseCodes;
import com.cloo.android.main.function.ExecutionCallback;

public interface ProfileService
{
    int PROFILE_DESCRIPTION = 0;
    int PROFILE_USERNAME = 1;
    int PROFILE_BIRTHDAY = 2;
    int PROFILE_GENDER = 3;
    int PROFILE_PICTURE = 4;

    void updateProfileInformations(ExecutionCallback successCallback,
                                   DatabaseCodes.DatabaseOperationErrorCallback errorCallback,
                                   SparseArray<String> informations);
}
