package com.cloo.android.main.service.util.view;

import android.support.annotation.NonNull;

public class NumberAbbreviationFormatter
{
    private final int maxDigits;

    public NumberAbbreviationFormatter(int maxDigits)
    {
        this.maxDigits = maxDigits;
    }

    @NonNull
    public String format(long number)
    {
        return String.valueOf(number);
    }

    @NonNull
    public String format(int number)
    {
        return String.valueOf(number);
    }

    @NonNull
    public String format(double number)
    {
        return String.valueOf(number);
    }
}
