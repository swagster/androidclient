package com.cloo.android.main.service.business.post;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.authentication.Authentication;
import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.function.ReceiveCallback;
import com.cloo.android.main.model.Post;

public class VoteServiceImpl implements VoteService
{
    private final DatabaseAdapter database;
    private final Authentication authentication;

    public VoteServiceImpl(final DatabaseAdapter database, final Authentication authentication)
    {
        this.database = database;
        this.authentication = authentication;
    }

    @Override
    public void voteOnPost(@NonNull final Post post, final int option)
    {
        if (post.selectedOption.get() == option) {
            return;
        }
        post.selectedOption.set(option);
        getVotedOption(post.getKey(), (currentlyVotedOption) -> {
            if (option != currentlyVotedOption) {
                database.getVotedByUsersNode(post.getKey(), authentication.getSession().getUserId())
                        .set(option)
                        .recycleAfterUse()
                        .submit();
            }
        });
    }

    @Override
    public void getVotedOption(@NonNull final String postId,
                               @NonNull final ReceiveCallback<Integer> receivedOption)
    {
        database.getVotedByUsersNode(postId, authentication.getSession().getUserId())
                .receive(Integer.class,
                         (key, obj) -> receivedOption.onReceive(obj != null ? obj : -1))
                .recycleAfterUse()
                .submit();
    }
}