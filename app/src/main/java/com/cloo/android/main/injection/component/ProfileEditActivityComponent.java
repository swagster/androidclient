package com.cloo.android.main.injection.component;

import com.cloo.android.main.injection.module.ProfileServiceModule;
import com.cloo.android.main.service.business.profile.ProfileService;

import dagger.Component;

@Component(modules = {ProfileServiceModule.class})
public interface ProfileEditActivityComponent
{
    ProfileService getProfileService();
}
