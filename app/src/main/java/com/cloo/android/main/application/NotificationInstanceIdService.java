package com.cloo.android.main.application;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.cloo.android.main.application.ClooApplication;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class NotificationInstanceIdService extends FirebaseInstanceIdService
{
    @Override
    public void onTokenRefresh()
    {
        final ClooApplication application = (ClooApplication) getApplication();
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.edit().putString("notificationToken", refreshedToken).apply();
        if (application.getAuthentication().isLoggedIn()) {
            saveToken(refreshedToken, application.getAuthentication().getSession().getUserId());
        }
    }

    private void saveToken(final String token, final String userId)
    {
        final ClooApplication application = (ClooApplication) getApplication();
        application.getDatabase()
                   .getNotificationTokensNode(userId, token)
                   .set(true)
                   .recycleAfterUse()
                   .submit();
    }
}