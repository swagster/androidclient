package com.cloo.android.main.service.util.validation;

import android.support.annotation.NonNull;

import java.util.regex.Pattern;


public final class CredentialValidation
{
    private static final int MINIMAL_PASSWORD_LENGTH = 6;
    private static final Pattern EMAIL_PATTERN = Pattern.compile(
            "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$", Pattern.CASE_INSENSITIVE);

    private CredentialValidation()
    {
        // Utility class
    }

    public static boolean isEmailValid(@NonNull final CharSequence email)
    {
        return EMAIL_PATTERN.matcher(email).matches();
    }

    public static boolean isPasswordValid(@NonNull final CharSequence password)
    {
        return password.length() >= MINIMAL_PASSWORD_LENGTH;
    }
}
