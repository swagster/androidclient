package com.cloo.android.main.function;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

@FunctionalInterface
public interface DatabaseReceiveCallback<T> {
    void onReceive(@NonNull String key, @Nullable T obj);
}
