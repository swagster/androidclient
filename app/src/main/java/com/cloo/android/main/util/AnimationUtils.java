package com.cloo.android.main.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;

public final class AnimationUtils
{
    private static int s_shortDuration = 0;

    private AnimationUtils()
    {
        // Utility class
    }

    public static void lerp(@NonNull final Context context, @NonNull final View fadeOut,
                            @NonNull final View fadeIn)
    {
        if (AnimationUtils.s_shortDuration == 0) {
            AnimationUtils.s_shortDuration = context.getResources()
                                                    .getInteger(
                                                            android.R.integer.config_shortAnimTime);
        }
        fadeIn.setAlpha(0f);
        fadeIn.setVisibility(View.VISIBLE);
        fadeIn.animate().alpha(1f).setDuration(AnimationUtils.s_shortDuration).setListener(null);
        fadeOut.animate()
               .alpha(0f)
               .setDuration(AnimationUtils.s_shortDuration)
               .setListener(new AnimatorListenerAdapter()
               {
                   @Override
                   public void onAnimationEnd(final Animator animation)
                   {
                       fadeOut.setVisibility(View.GONE);
                   }
               });
    }
}
