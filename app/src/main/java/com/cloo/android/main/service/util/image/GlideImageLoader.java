package com.cloo.android.main.service.util.image;

import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.ViewTarget;
import com.cloo.android.R;

public class GlideImageLoader implements ImageLoader {

    public GlideImageLoader() {
        // Fix glide and data-binding compatibility (use of setTag exception)
        ViewTarget.setTagId(R.id.glide_tag);
    }

    @Override
    public void load(final String uri, @NonNull final ImageView image) {
        Glide.with(image.getContext().getApplicationContext()).load(uri).into(image);
    }

    @Override
    public void clear(@NonNull final ImageView image) {
        Glide.clear(image);
    }
}
