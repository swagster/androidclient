package com.cloo.android.main.injection.module;


import android.content.Context;
import android.support.annotation.NonNull;

import com.cloo.android.main.application.ClooApplication;
import com.cloo.android.main.service.business.notification.NotificationBadgeService;
import com.cloo.android.main.service.business.notification.NotificationBadgeServiceImpl;

import dagger.Module;

@Module(includes = ContextModule.class)
public class NotificationBadgeServiceModule
{
    @NonNull
    public NotificationBadgeService notificationBadgeService(@NonNull final Context context)
    {
        return new NotificationBadgeServiceImpl(
                ClooApplication.getInstance(context.getApplicationContext()));
    }
}
