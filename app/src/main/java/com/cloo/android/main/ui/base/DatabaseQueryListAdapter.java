package com.cloo.android.main.ui.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.BaseAdapter;

public abstract class DatabaseQueryListAdapter<T> extends BaseAdapter {

    public abstract void addDataObject(@NonNull String key);

    public abstract void removeDataObject(@NonNull String key);

    public abstract T getDataObject(int index);

    public abstract void recycle();

    @Nullable
    public abstract String getLastKey();
}
