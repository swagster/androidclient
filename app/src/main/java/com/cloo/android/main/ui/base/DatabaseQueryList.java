package com.cloo.android.main.ui.base;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentListBinding;
import com.cloo.android.databinding.FragmentListLoadingIndicatorBinding;
import com.cloo.android.main.backend.database.DatabaseQuery;
import com.cloo.android.main.service.util.view.ListViewScrollListener;
import com.cloo.android.main.util.Assert;

import timber.log.Timber;

public abstract class DatabaseQueryList<T> extends BaseFragment implements ListViewScrollListener.OnBottomReachedListener
{
    private static final int QUERY_INCREMENT = 8;
    @Nullable protected DatabaseQueryListAdapter<T> adapter = null;
    @Nullable private AuthenticatedActivity activity = null;
    @Nullable private FragmentListBinding binding = null;
    @Nullable private FragmentListLoadingIndicatorBinding loadingIndicatorBinding = null;
    @Nullable private Bundle parameters = null;

    @Nullable private DatabaseQuery dataUpdateQuery = null;
    @Nullable private DatabaseQuery removeQuery = null;

    private int unloadedObjects = -1;
    private int currentLimit;

    @NonNull
    protected final AuthenticatedActivity getAuthenticatedActivity()
    {
        return activity;
    }

    @NonNull
    public final DatabaseQueryListAdapter<T> getAdapter()
    {
        return adapter;
    }

    private void init()
    {
        if (getActivity() != null && parameters != null) {
            adapter = createAdapter();
            binding.setAdapter(adapter);
            binding.setScrollListener(new ListViewScrollListener(this));
            onBottomReached(binding.list);
        }
    }

    public boolean loadNewItemsAuto()
    {
        return true;
    }

    protected void loadNewItems()
    {
        if (unloadedObjects > 0) {
            createQuery("", unloadedObjects).get(Object.class, this::onAdded, error -> {

            }, () -> {
                unloadedObjects = 0;
                binding.swipeList.setRefreshing(false);
                binding.swipeList.setEnabled(false);
            });
        }
        else {
            binding.swipeList.setRefreshing(false);
            binding.swipeList.setEnabled(false);
        }
    }

    @Override
    public final void onViewCreated(@NonNull final View view,
                                    @Nullable final Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        loadingIndicatorBinding = DataBindingUtil.inflate(getActivity().getLayoutInflater(),
                                                          R.layout.fragment_list_loading_indicator,
                                                          binding.list, false);
        binding.list.addFooterView(loadingIndicatorBinding.getRoot(), null, false);

        parameters = getArguments();
        if (parameters == null) {
            parameters = new Bundle();
        }
        setupView(binding);

        dataUpdateQuery = createQuery("", 1);
        dataUpdateQuery.addChildListener(Object.class, (key, obj) -> {
            binding.listEmpty.setVisibility(View.INVISIBLE);
            currentLimit++;
            unloadedObjects++;
            binding.swipeList.setEnabled(true);
            if (loadNewItemsAuto()) {
                loadNewItems();
            }
            else {
                unloadedObjects = Math.min(unloadedObjects, 16);
            }
            if (removeQuery != null) {
                removeQuery.recycle();
            }
            removeQuery = createQuery("", currentLimit);
            removeQuery.addChildListener(Object.class, this::onAdded, this::onAdded, this::onAdded,
                                         this::onRemoved, error -> {});
        }, (key, obj) -> {

        }, (key, obj) -> {

        }, (key, obj) -> {

        }, error -> {});

        binding.swipeList.setOnRefreshListener(this::loadNewItems);
        init();
    }

    @NonNull
    @Override
    public final View onCreateView(@NonNull final LayoutInflater inflater,
                                   @Nullable final ViewGroup container,
                                   @Nullable final Bundle savedInstanceState)
    {
        Assert.check(getActivity() instanceof AuthenticatedActivity,
                     "DatabaseQueryLists need an AuthenticatedActivity parent");
        activity = (AuthenticatedActivity) getActivity();
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list, container, false);
        return binding.getRoot();
    }

    @Override
    public final void onDestroy()
    {
        super.onDestroy();
        adapter.recycle();
    }

    @Override
    public final void onBottomReached(@NonNull final AbsListView listView)
    {
        if (adapter != null) {
            loadingIndicatorBinding.listLoadingIndicator.setVisibility(View.VISIBLE);
            currentLimit += DatabaseQueryList.QUERY_INCREMENT;
            createQuery(adapter.getLastKey(), currentLimit).get(Object.class, this::onAdded,
                                                                error -> {

                                                                },
                                                                () -> loadingIndicatorBinding.listLoadingIndicator
                                                                        .setVisibility(
                                                                                View.INVISIBLE));
            if (removeQuery != null) {
                removeQuery.recycle();
            }
            removeQuery = createQuery("", currentLimit);
            removeQuery.addChildListener(Object.class, this::onAdded, this::onAdded, this::onAdded,
                                         this::onRemoved, error -> {});
        }
    }

    protected void onAdded(@NonNull final String key, final Object nothing)
    {
        adapter.addDataObject(key);
    }

    public void onRemoved(@NonNull final String key, final Object nothing)
    {
        Timber.d("Removed data object %s", key);
        adapter.removeDataObject(key);
    }

    @Nullable
    protected final Bundle getParameters()
    {
        return parameters;
    }

    protected abstract void setupView(@NonNull final FragmentListBinding binding);

    public abstract void setupPromotedAction(@NonNull final FloatingActionButton fab);

    @Nullable
    protected abstract DatabaseQueryListAdapter<T> createAdapter();

    @NonNull
    protected abstract DatabaseQuery createQuery(final @Nullable String endKey, final int count);
}
