package com.cloo.android.main.util;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.cloo.android.main.function.ReceiveCallback;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;

import org.json.JSONException;

public final class FacebookUtils
{
    private static final String TAG = FacebookUtils.class.getSimpleName();

    private FacebookUtils()
    {
        // Utility class
    }

    public static void requestFacebookEmail(@NonNull final AccessToken token,
                                            @NonNull final ReceiveCallback<String> listener)
    {
        final GraphRequest request = GraphRequest.newMeRequest(token, (object, response) -> {
            try {
                listener.onReceive(object.getString("email"));
            } catch (@NonNull final JSONException e) {
                listener.onReceive("");
                Log.e(FacebookUtils.TAG, "receiveField:error", e);
            }
        });
        final Bundle parameters = new Bundle();
        parameters.putString("fields", "email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    public static void logout()
    {
        LoginManager.getInstance().logOut();
    }
}
