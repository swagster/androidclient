package com.cloo.android.main.ui.compose.imageSelection;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.cloo.android.R;
import com.cloo.android.databinding.ActivityEditImageBinding;
import com.cloo.android.main.ui.base.BaseActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.cloo.android.main.util.ScreenUtils.getScreenWidth;

public class ImageEditActivity extends BaseActivity
{
    private static final int EDIT_IMAGE_FAILURE = 1;

    @Nullable private ActivityEditImageBinding binding = null;
    private float rotation;

    @Override
    protected final void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_image);

        binding.image.setLayoutParams(
                new LinearLayout.LayoutParams(getScreenWidth(), getScreenWidth()));
        if (getIntent() != null && getIntent().getData() != null) {
            binding.setSource(getIntent().getData().toString());
        }
        setupAbortButton(binding.toolbar);
    }

    @Override
    public final boolean onOptionsItemSelected(@NonNull final MenuItem item)
    {
        switch (item.getItemId()) {
            case R.id.menu_crop_rotate:
                rotate();
                break;
            case R.id.menu_crop_submit:
                onApplyEdits();
                break;
            default:
                setResult(ImageEditActivity.EDIT_IMAGE_FAILURE);
        }
        return true;
    }

    private void rotate()
    {
        binding.image.setRotation(binding.image.getRotation() + 90);
        rotation += 90;
        binding.image.invalidate();
    }

    private void onApplyEdits()
    {
        final Intent intent = new Intent();
        binding.image.setDrawingCacheEnabled(true);
        binding.image.buildDrawingCache();
        final Bitmap bitmap = binding.image.getDrawingCache();
        final Matrix matrix = new Matrix();
        matrix.postRotate(rotation);
        final Bitmap changedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                                                         bitmap.getHeight(), matrix, false);

        final File outputDir = getCacheDir(); // context being the Activity pointer
        try {
            final File outputFile = File.createTempFile(
                    "edit_image" + getIntent().getIntExtra("requestCode", 0), ".png", outputDir);
            final FileOutputStream outStream = new FileOutputStream(outputFile);
            changedBitmap.compress(Bitmap.CompressFormat.JPEG, 85, outStream);
            outStream.close();
            intent.setData(Uri.parse(outputFile.getAbsolutePath()));
        } catch (@NonNull final IOException e) {
            e.printStackTrace();
        }
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public final void onBackPressed()
    {
        super.onBackPressed();
        setResult(Activity.RESULT_CANCELED);
    }


    @Override
    public final boolean onCreateOptionsMenu(@NonNull final Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_crop, menu);
        return super.onCreateOptionsMenu(menu);
    }
}