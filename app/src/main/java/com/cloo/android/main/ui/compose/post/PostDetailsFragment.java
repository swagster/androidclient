package com.cloo.android.main.ui.compose.post;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentPostDetailsBinding;
import com.cloo.android.main.backend.database.DatabaseNode;
import com.cloo.android.main.model.Post;
import com.cloo.android.main.model.User;
import com.cloo.android.main.ui.base.BaseFragment;
import com.cloo.android.main.ui.common.FullscreenImageActivity;
import com.cloo.android.main.ui.compose.user.UserActivity;
import com.cloo.android.main.util.AndroidUtils;
import com.cloo.android.main.util.TimeFormatUtils;

import java.util.Calendar;

import static com.cloo.android.main.util.ScreenUtils.getScreenWidth;
import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_DAYS;
import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_HOURS;
import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_MINUTES;
import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_MONTHS;
import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_SECONDS;
import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_WEEKS;
import static com.cloo.android.main.util.TimeFormatUtils.TIME_UNIT_YEARS;

public class PostDetailsFragment extends BaseFragment
{
    private static final String POST_ID = "postId";
    @NonNull private final Calendar cal = Calendar.getInstance();
    @Nullable private FragmentPostDetailsBinding binding = null;
    @Nullable private PostOptionsAdapter adapter = null;
    @Nullable private DatabaseNode reference = null;
    @Nullable private DatabaseNode voteCount = null;

    @NonNull
    public static PostDetailsFragment newInstance(final String postId)
    {
        final Bundle bundle = new Bundle();
        bundle.putString(PostDetailsFragment.POST_ID, postId);
        final PostDetailsFragment fragment = new PostDetailsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public final void onDestroy()
    {
        super.onDestroy();
        reference.recycle();
        reference = null;
        voteCount.recycle();
        voteCount = null;
    }

    @Override
    public final void onStop()
    {
        super.onStop();
        reference.removeListeners();
        voteCount.removeListeners();
    }

    @Override
    public final void onStart()
    {
        super.onStart();
        reference.onUpdates(Post.class, (k, post) -> {
            if (binding != null && post != null && canAccessUI()) {

                binding.setPost(post);
                binding.setTimestamp(getTimePassed(post.getTimestamp()));
                adapter.setPost(post);
                binding.notifyChange();
                binding.profilImageWrapper.profilImage.setOnClickListener(v -> startActivity(
                        UserActivity.newIntent(getActivity(), post.getAuthor())));
            }
        }).submit();
        voteCount.onUpdates(Long.class, (k, count) -> {
            binding.setVoteCount(count == null ? 0 : count);
        }).submit();
    }

    @Override
    public final void onViewCreated(final View view, @Nullable final Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        reference = getParentActivity().getDatabase()
                                       .getPostDetailsNode(
                                               getArguments().getString(PostDetailsFragment.POST_ID,
                                                                        ""));
        AndroidUtils.setImageDrawable(binding.profilImageWrapper.profilImage, getActivity(),
                                      R.drawable.v_icon_user_picture_placeholder);
        reference.receive(Post.class, (k, post) -> {
            if (binding != null && post != null && canAccessUI()) {
                post.setKey(k);
                binding.setPost(post);
                adapter.setPost(post);
                binding.setTimestamp(getTimePassed(post.getTimestamp()));
                binding.notifyChange();
                getParentActivity().getDatabase()
                                   .getUserDetailsNode(post.getAuthor())
                                   .receive(User.class, (id, user) -> {
                                       if (user != null && canAccessUI()) {
                                           user.setId(id);
                                           binding.setUser(user);
                                           if (!user.getProfileImageUrl().isEmpty()) {
                                               binding.setProfileImage(user.getProfileImageUrl());
                                               binding.notifyChange();
                                           }
                                       }
                                   })
                                   .recycleAfterUse()
                                   .submit();
            }
        }).submit();
        voteCount = getParentActivity().getDatabase()
                                       .getVoteCountNode(
                                               getArguments().getString(PostDetailsFragment.POST_ID,
                                                                        ""));
    }


    @NonNull
    private String getTimePassed(final long timestamp)
    {
        if (canAccessUI()) {
            final int unit = TimeFormatUtils.getTimeUnit(timestamp);
            final long passedUnits = TimeFormatUtils.getPassedUnits(unit, timestamp);
            switch (unit) {
                case TIME_UNIT_SECONDS:
                    return getResources().getQuantityString(R.plurals.time_ago_seconds,
                                                            (int) passedUnits, passedUnits);
                case TIME_UNIT_MINUTES:
                    return getResources().getQuantityString(R.plurals.time_ago_minutes,
                                                            (int) passedUnits, passedUnits);
                case TIME_UNIT_HOURS:
                    return getResources().getQuantityString(R.plurals.time_ago_hours,
                                                            (int) passedUnits, passedUnits);
                case TIME_UNIT_DAYS:
                    return getResources().getQuantityString(R.plurals.time_ago_days,
                                                            (int) passedUnits, passedUnits);
                case TIME_UNIT_WEEKS:
                    return getResources().getQuantityString(R.plurals.time_ago_weeks,
                                                            (int) passedUnits, passedUnits);
                case TIME_UNIT_MONTHS:
                    return getResources().getQuantityString(R.plurals.time_ago_months,
                                                            (int) passedUnits, passedUnits);
                case TIME_UNIT_YEARS:
                    cal.setTimeInMillis(timestamp);
                    return String.format(getString(R.string.time_ago_years),
                                         cal.get(Calendar.YEAR));
                default:
                    return "";
            }
        }
        else {
            return "";
        }
    }

    @Nullable
    @Override
    public final View onCreateView(@NonNull final LayoutInflater inflater,
                                   @Nullable final ViewGroup container,
                                   @Nullable final Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_post_details, container,
                                          false);
        adapter = new PostOptionsAdapter(getParentActivity(), getScreenWidth() / 2);
        binding.postSelection.setAdapter(adapter);
        adapter.setOnClickListener((adapterView, view, i, l) -> startActivity(
                FullscreenImageActivity.newInstance(getActivity(), adapter.getPost()
                                                                          .getOptions()
                                                                          .get(i)
                                                                          .getUri())));
        return binding.getRoot();
    }
}
