package com.cloo.android.main.model.bindingModels;

import android.databinding.ObservableField;

public class PostCreationForm
{
    public final ObservableField<String> title = new ObservableField<>("");
    public final ObservableField<String> description = new ObservableField<>("");
}
