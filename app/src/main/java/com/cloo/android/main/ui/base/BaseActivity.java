package com.cloo.android.main.ui.base;

import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.cloo.android.R;
import com.cloo.android.main.application.ClooApplication;
import com.cloo.android.main.backend.authentication.Authentication;
import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.util.AndroidUtils;
import com.cloo.android.main.util.Assert;

import static com.cloo.android.main.util.AndroidUtils.hideKeyboard;

public abstract class BaseActivity extends AppCompatActivity
{
    private boolean destroyed = false;
    @Nullable private ViewDataBinding binding = null;
    private boolean setBinding;

    @Nullable
    protected final SharedPreferences getSharedPreferences()
    {
        return AndroidUtils.getUserSharedPreferences(this);
    }

    @NonNull
    public DatabaseAdapter getDatabase()
    {
        return ClooApplication.getS_instance().getDatabase();
    }

    @NonNull
    public Authentication getAuthentication()
    {
        return ((ClooApplication) getApplication()).getAuthentication();
    }

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    public boolean canAccessUI()
    {
        Assert.check(setBinding,
                     "You need to set the activity dataview through setBindingView in order to use canAccessUI");
        return binding != null && !isDestroyed();
    }

    protected <T extends ViewDataBinding> T setBindingView(final int layoutId)
    {
        setBinding = true;
        final T view = DataBindingUtil.setContentView(this, layoutId);
        binding = view;
        return view;
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        destroyed = true;
    }

    @Override
    public final boolean isDestroyed()
    {
        return destroyed;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull final MenuItem item)
    {
        switch (item.getItemId()) {
            case android.R.id.home: {
                hideKeyboard(this);
                supportFinishAfterTransition();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        hideKeyboard(this);
        supportFinishAfterTransition();
    }

    public void setupBackButton(final Toolbar toolbar)
    {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    public void setupAbortButton(final Toolbar toolbar)
    {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null && getApplicationContext() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(
                    AppCompatResources.getDrawable(getApplicationContext(),
                                                   R.drawable.v_icon_back_button_cross));
        }
    }

    public void showToast(@NonNull final String message)
    {
        if (getApplicationContext() != null) {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    @NonNull
    public ClooApplication getClooApplication()
    {
        return (ClooApplication) getApplication();
    }
}
