package com.cloo.android.main.service.business.common;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.backend.database.DatabaseNode;
import com.cloo.android.main.function.ReceiveCallback;

import java.util.HashMap;
import java.util.Map;

public class FeedbackServiceImpl implements FeedbackService
{

    private final DatabaseAdapter database;

    public FeedbackServiceImpl(final DatabaseAdapter database)
    {
        this.database = database;
    }

    @Override
    public void sendFeedback(final String userId, @NonNull final String message, final int category,
                             @NonNull final ReceiveCallback<Boolean> finishCallback)
    {
        if (message.isEmpty()) {
            finishCallback.onReceive(true);
        }
        else {
            final DatabaseNode node = database.getFeedbackNode().generateChild();
            final Map<String, Object> values = new HashMap<>();
            values.put("userId", userId);
            values.put("message", message);
            values.put("category", category);
            node.setChildren(values)
                .onSuccess(() -> finishCallback.onReceive(true))
                .onError(error -> finishCallback.onReceive(false))
                .recycleAfterUse()
                .submit();
        }
    }
}
