package com.cloo.android.main.service.business.post;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.function.FinishCallback;

public class ShareServiceImpl implements ShareService {
    private final DatabaseAdapter database;

    public ShareServiceImpl(final DatabaseAdapter database) {
        this.database = database;
    }

    @Override
    public final void share(@NonNull final String userId, @NonNull final String postId, final String comment, @NonNull final FinishCallback callback) {
        database.getUserSharedPostsNode(userId, postId)
                .set(true)
                .onError(error -> callback.onFinish(false)).onSuccess(() -> callback.onFinish(true))
                .recycleAfterUse()
                .submit();
    }
}
