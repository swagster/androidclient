package com.cloo.android.main.ui.compose.postList;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentListBinding;
import com.cloo.android.main.backend.database.DatabaseQuery;
import com.cloo.android.main.model.Post;
import com.cloo.android.main.ui.base.DatabaseQueryList;
import com.cloo.android.main.ui.base.DatabaseQueryListAdapter;
import com.cloo.android.main.ui.compose.postCreation.PostCreationActivity;

import static android.view.View.GONE;

public class DiscoverListFragment extends DatabaseQueryList<Post>
{

    @Override
    public void setupView(@NonNull final FragmentListBinding binding)
    {
        getAuthenticatedActivity().getDatabase().getAllPostsNode().exists(exists -> {
            if (!exists) {
                binding.emptyPicture.setImageDrawable(
                        getActivity().getDrawable(R.drawable.empty_list_discover));
                binding.list.setVisibility(GONE);
                binding.listEmpty.setVisibility(View.VISIBLE);
            }
        }).recycleAfterUse().submit();
    }

    @Override
    public void setupPromotedAction(@NonNull final FloatingActionButton fab)
    {
        if (getParentActivity() != null) {
            fab.setImageDrawable(getParentActivity().getDrawable(R.drawable.v_icon_create_post));
        }
        fab.show();
        fab.setOnClickListener(v -> {
            if (getContext() != null) {
                startActivity(new Intent(getContext(), PostCreationActivity.class));
            }
        });
    }

    @Nullable
    @Override
    public DatabaseQueryListAdapter<Post> createAdapter()
    {
        return new PostListAdapter(getAuthenticatedActivity());
    }

    @Override
    public boolean loadNewItemsAuto()
    {
        return false;
    }

    @NonNull
    @Override
    public DatabaseQuery createQuery(@NonNull final String endKey, final int count)
    {
        return endKey.isEmpty() ? getAuthenticatedActivity().getDatabase()
                                                            .getAllPostsNode()
                                                            .limitToLast(
                                                                    count) : getAuthenticatedActivity()
                .getDatabase()
                .getAllPostsNode()
                .limitToLast(
                                                                                             count)
                .endAt(endKey);
    }
}