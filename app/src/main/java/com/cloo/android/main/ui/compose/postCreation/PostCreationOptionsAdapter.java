package com.cloo.android.main.ui.compose.postCreation;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.cloo.android.R;
import com.cloo.android.databinding.FragmentPostCreationAddOptionBinding;
import com.cloo.android.databinding.FragmentPostCreationOptionBinding;
import com.cloo.android.main.backend.database.DatabaseNode;
import com.cloo.android.main.backend.storage.Storage;
import com.cloo.android.main.function.FinishCallback;
import com.cloo.android.main.service.business.camera.CameraService;
import com.cloo.android.main.ui.base.BaseActivity;
import com.cloo.android.main.ui.compose.imageSelection.CameraActivity;
import com.cloo.android.main.ui.compose.imageSelection.GalleryActivity;
import com.cloo.android.main.util.AndroidUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

import static android.view.View.GONE;
import static com.cloo.android.main.util.ScreenUtils.getScreenWidth;
import static com.cloo.android.main.util.ScreenUtils.toPixels;

class PostCreationOptionsAdapter extends BaseAdapter
{
    final Handler handler = new Handler();
    @NonNull private final LayoutInflater inflater;
    @NonNull private final Storage storage;
    @NonNull private final BaseActivity activity;
    private final List<Uri> optionUrls = new ArrayList<>();
    private final List<Integer> uploaded = new ArrayList<>();
    private final List<Bitmap> bitmaps = new ArrayList<>();
    private final int optionLength;
    @NonNull private final CameraService cameraService;

    public PostCreationOptionsAdapter(@NonNull final BaseActivity activity,
                                      @NonNull final CameraService cameraService,
                                      @NonNull final Storage storage, final int category)
    {
        this.activity = activity;
        this.storage = storage;
        this.cameraService = cameraService;
        optionUrls.add(Uri.EMPTY);
        uploaded.add(0);
        optionUrls.add(Uri.EMPTY);
        uploaded.add(0);
        bitmaps.add(null);
        bitmaps.add(null);
        optionLength = getScreenWidth() / 2 - toPixels(24,
                                                       activity.getResources().getDisplayMetrics());
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public final int getCount()
    {
        return Math.min(4, optionUrls.size() + 1);
    }

    @Override
    public final Object getItem(final int i)
    {
        return optionUrls.get(i);
    }

    @Override
    public final long getItemId(final int i)
    {
        return i;
    }

    @NonNull
    public final List<Uri> getUrls()
    {
        return new ArrayList<>(optionUrls);
    }

    public final Uri getUri(final int i)
    {
        return optionUrls.get(i);
    }

    private FragmentPostCreationOptionBinding getBinding(@Nullable final View recycledView,
                                                         @Nullable final ViewGroup viewGroup)
    {
        if (recycledView == null || recycledView.getTag() == null) {
            final FragmentPostCreationOptionBinding binding = DataBindingUtil.inflate(inflater,
                                                                                      R.layout.fragment_post_creation_option,
                                                                                      viewGroup,
                                                                                      false);
            if (binding.image.getWidth() != optionLength) {
                binding.image.setLayoutParams(
                        new LinearLayout.LayoutParams(optionLength, optionLength));
                binding.postCreationImageOverlay.setLayoutParams(
                        new FrameLayout.LayoutParams(optionLength, optionLength));
                binding.postRemove.setLayoutParams(
                        new FrameLayout.LayoutParams(optionLength, optionLength));
                binding.postCreationPickCameraWrapper.setLayoutParams(
                        new FrameLayout.LayoutParams(optionLength, optionLength));
                binding.uploading.setLayoutParams(
                        new FrameLayout.LayoutParams(optionLength, optionLength));
                binding.uploadFinished.setLayoutParams(
                        new FrameLayout.LayoutParams(optionLength, optionLength));
            }
            return binding;
        }
        else {
            return (FragmentPostCreationOptionBinding) recycledView.getTag();
        }
    }

    @Override
    public final View getView(final int i, final View recycledView, final ViewGroup viewGroup)
    {
        if (optionUrls.size() != 4 && i == optionUrls.size()) {
            final FragmentPostCreationAddOptionBinding binding = DataBindingUtil.inflate(inflater,
                                                                                         R.layout.fragment_post_creation_add_option,
                                                                                         viewGroup,
                                                                                         false);
            binding.postCreationAddOption.setLayoutParams(
                    new FrameLayout.LayoutParams(optionLength, optionLength));
            binding.setOnAddListener(view -> {
                optionUrls.add(Uri.EMPTY);
                uploaded.add(0);
                bitmaps.add(null);
                notifyDataSetChanged();
            });
            return binding.getRoot();
        }
        else {
            final FragmentPostCreationOptionBinding binding = getBinding(recycledView, viewGroup);
            final View rowView = binding.getRoot();
            rowView.setTag(binding);
            if (i > 1) {
                binding.postCreationRemoveOption.setVisibility(View.VISIBLE);
                binding.postCreationRemoveOption.setOnClickListener(view -> {
                    optionUrls.remove(i);
                    bitmaps.remove(i);
                    notifyDataSetChanged();
                });
            }
            else {
                if (uploaded.get(i) == 0) {
                    binding.postCreationRemoveOption.setVisibility(View.INVISIBLE);
                }
            }
            //  if (optionUrls.get(i).isEmpty()) {
            binding.postCreationPickCamera.setVisibility(View.VISIBLE);
            if (uploaded.get(i) == 0) {
                binding.postCreationImageOverlay.setText(
                        i == 3 ? "D" : i == 2 ? "C" : i == 1 ? "B" : "A");
                binding.image.setOnClickListener(v -> activity.startActivityForResult(
                        new Intent(activity, GalleryActivity.class), i));
            }
          /*  }
            else {
                binding.postCreationPickCamera.setVisibility(View.INVISIBLE);
                final Intent intent = new Intent(activity, ImageEditActivity.class);
                intent.setData(Uri.parse(optionUrls.get(i)));
                binding.image.setOnClickListener(v -> activity.startActivityForResult(intent, i));
            }*/
            if (uploaded.get(i) != 0) {
                binding.postCreationImageOverlay.setText("");
                binding.image.setOnClickListener(v -> {
                });
            }
            else {
                binding.setUrl(optionUrls.get(i).toString());
            }
            if (uploaded.get(i) != 1) {
                binding.uploading.setVisibility(GONE);
                if (uploaded.get(i) >= 2) {
                    binding.uploadFinished.setVisibility(View.VISIBLE);
                    if (uploaded.get(i) == 2) {
                        AndroidUtils.setImageDrawable(binding.uploadIndicator, activity,
                                                      R.drawable.v_icon_submit);
                    }
                    else {
                        AndroidUtils.setImageDrawable(binding.uploadIndicator, activity,
                                                      R.drawable.v_icon_clear);
                    }
                }
                else {
                    binding.uploadFinished.setVisibility(GONE);
                }
            }
            else {
                binding.uploadFinished.setVisibility(GONE);
                binding.uploading.setVisibility(View.VISIBLE);
            }
            binding.postCreationPickCamera.setOnClickListener(
                    v -> activity.startActivityForResult(new Intent(activity, CameraActivity.class),
                                                         i));
            if (!cameraService.hasCamera()) {
                binding.postCreationPickCamera.setVisibility(GONE);
            }
            return rowView;
        }
    }

    public final void set(final int requestCode, final Uri s)
    {
        Log.d("TEST", "Set:" + requestCode + " " + s);
        optionUrls.set(requestCode, s);
        uploaded.set(requestCode, 0);
        Timber.d("" + new File(String.valueOf(s)).exists());
        Glide.with(activity)
             .load(new File(String.valueOf(s)))
             .asBitmap()
             .into(new SimpleTarget<Bitmap>()
             {
                 @Override
                 public void onResourceReady(final Bitmap resource,
                                             final GlideAnimation<? super Bitmap> glideAnimation)
                 {
                     Timber.d("TESTTTTT");
                     bitmaps.set(requestCode, resource);
                 }
             });
        notifyDataSetInvalidated();
    }

    public final boolean isValid()
    {
        return !optionUrls.get(0).equals(Uri.EMPTY) && !optionUrls.get(1).equals(Uri.EMPTY);
    }

    private void uploadOption(final String postId, final int index,
                              @NonNull final FinishCallback callback)
    {
        Log.d("PostCreationAdapter", "Upload option:" + index);
        if (index == getUrls().size()) {
            callback.onFinish(true);
            return;
        }
        uploaded.set(index, 1);
        notifyDataSetChanged();
        storage.uploadPostOptionImage(bitmaps.get(index), postId + "+" + index,
                                      (key, uri) -> handler.post(() -> {
                                          Timber.d("Upload post option image finsihed " + postId);
                                          if (uri == null) {
                                              uploaded.set(index, 3);
                                              callback.onFinish(false);
                                          }
                                          else {
                                              optionUrls.set(index, uri);
                                              uploaded.set(index, 2);
                                              uploadOption(postId, index + 1, callback);
                                          }
                                          notifyDataSetChanged();
                                      }), progress -> {
        });
    }

    public final void uploadOptions(@NonNull final FinishCallback callback)
    {
        for (int i = optionUrls.size() - 1; i >= 0; i--) {
            if (optionUrls.get(i).equals(Uri.EMPTY)) {
                optionUrls.remove(i);
                notifyDataSetChanged();
            }
        }
        final DatabaseNode reference = activity.getDatabase().getPostDetailsNode().generateChild();
        final String postId = reference.getKey();
        uploadOption(postId, 0, callback);
    }
}
