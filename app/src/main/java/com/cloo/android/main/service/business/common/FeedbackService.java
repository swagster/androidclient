package com.cloo.android.main.service.business.common;

import com.cloo.android.main.function.ReceiveCallback;

@FunctionalInterface
public interface FeedbackService
{
    void sendFeedback(String userId, String message, int category, ReceiveCallback<Boolean> finishCallback);
}
