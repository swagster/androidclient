package com.cloo.android.main.ui.common;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentReportDialogBinding;
import com.cloo.android.main.application.ClooApplication;
import com.cloo.android.main.model.Comment;
import com.cloo.android.main.model.Post;
import com.cloo.android.main.model.bindingModels.ReportDialogForm;
import com.cloo.android.main.service.business.common.ReportService;
import com.cloo.android.main.service.business.common.ReportServiceImpl;
import com.cloo.android.main.ui.base.BaseActivity;
import com.cloo.android.main.ui.base.BaseDialog;

import timber.log.Timber;

import static com.cloo.android.main.util.MathUtils.clamp;
import static com.cloo.android.main.util.MathUtils.clampZero;

public class ReportDialog extends BaseDialog
{
    private static final String BUNDLE_CONTENT = "post";
    private static final String BUNDLE_CONTENT_TYPE = "contentType";
    private static final String BUNDLE_INFORMATION = "information";
    private static final String BUNDLE_SELECTED_CATEGORY = "selectedCategory";
    private final ReportDialogForm form = new ReportDialogForm();
    @Nullable private ReportService reportService = null;
    @Nullable private FragmentReportDialogBinding binding = null;
    @Nullable private String content = null;
    private int contentType = 0;

    @NonNull
    public static ReportDialog newInstance(@NonNull final String contentId, final int contentType)
    {
        final ReportDialog fragment = new ReportDialog();
        final Bundle bundle = new Bundle();
        bundle.putString(ReportDialog.BUNDLE_CONTENT, contentId);
        bundle.putInt(ReportDialog.BUNDLE_CONTENT_TYPE, contentType);
        fragment.setArguments(bundle);
        return fragment;
    }

    private int getContentType()
    {
        return contentType;
    }

    @Nullable
    private String getContent()
    {
        return content;
    }

    @NonNull
    private String getInformation()
    {
        return form.information.get();
    }

    private int getSelectedCategory()
    {
        return binding.spinner.getSelectedItemPosition();
    }

    @Override
    public final void onSaveInstanceState(@NonNull final Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putString(ReportDialog.BUNDLE_INFORMATION, getInformation());
        outState.putInt(ReportDialog.BUNDLE_SELECTED_CATEGORY, getSelectedCategory());
        outState.putString(ReportDialog.BUNDLE_CONTENT, getContent());
        outState.putInt(ReportDialog.BUNDLE_CONTENT_TYPE, getContentType());
    }

    @Override
    public final void onViewCreated(@NonNull final View view,
                                    @Nullable final Bundle savedInstanceState)
    {

        final Bundle bundle = savedInstanceState == null ? getArguments() : savedInstanceState;
        if (bundle == null) {
            Timber.e("Added ReportDialog with an empty bundle");
            dismiss();
        }
        else {
            setupViewWithArguments(bundle);
        }
    }

    @Override
    public final View onCreateView(@NonNull final LayoutInflater inflater,
                                   @Nullable final ViewGroup container,
                                   @Nullable final Bundle savedInstanceState)
    {
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                                                                                   R.array.report_issues,
                                                                                   android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        reportService = new ReportServiceImpl(((BaseActivity) getActivity()).getAuthentication(),
                                              ((BaseActivity) getActivity()).getDatabase());
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_report_dialog, container,
                                          false);
        binding.spinner.setAdapter(adapter);
        binding.spinner.setSelection(clampZero(adapter.getCount() - 2));
        binding.setModel(form);
        binding.setOnCancel(this::onCancel);
        restoreSavedInstanceState(savedInstanceState);
        return binding.getRoot();
    }

    private void restoreSavedInstanceState(@Nullable final Bundle savedInstanceState)
    {
        if (savedInstanceState != null) {
            form.information.set(savedInstanceState.getString(ReportDialog.BUNDLE_INFORMATION));
            binding.spinner.setSelection(
                    clamp(savedInstanceState.getInt(ReportDialog.BUNDLE_SELECTED_CATEGORY), 0,
                          binding.spinner.getAdapter().getCount()));
        }
    }

    private void setupViewWithArguments(@NonNull final Bundle bundle)
    {
        content = bundle.getString(ReportDialog.BUNDLE_CONTENT);
        contentType = bundle.getInt(ReportDialog.BUNDLE_CONTENT_TYPE, -1);
        switch (getContentType()) {
            case ReportServiceImpl.CONTENT_TYPE_COMMENT:
                getParentActivity().getClooApplication()
                                   .getDatabase()
                                   .getCommentDetailsNode(getContent())
                                   .receive(Comment.class, (key, comment) -> {
                                       if (comment != null && !isDetached()) {
                                           comment.setId(key);
                                           setupCommentReport(comment);
                                       }
                                   })
                                   .recycleAfterUse()
                                   .submit();
                break;
            case ReportServiceImpl.CONTENT_TYPE_POST:
                getParentActivity().getClooApplication()
                                   .getDatabase()
                                   .getPostDetailsNode(getContent())
                                   .receive(Post.class, (key, post) -> {
                                       if (post != null && !isDetached()) {
                                           post.setKey(key);
                                           setupPostReport(post);
                                       }
                                   })
                                   .recycleAfterUse()
                                   .submit();
                break;
            default:
                Timber.d("Added ReportDialog with an invalid contentType");
                break;
        }
    }

    private void setupCommentReport(@NonNull final Comment comment)
    {
        binding.setTitle(getString(R.string.dialog_report_title_comment));
        binding.setOnSubmit(v -> reportService.sendReport(getParentActivity().getClooApplication()
                                                                             .getAuthentication()
                                                                             .getSession()
                                                                             .getUserId(),
                                                          comment.getId(), getSelectedCategory(),
                                                          ReportServiceImpl.CONTENT_TYPE_COMMENT,
                                                          getInformation(),
                                                          this::onReportSendFinished));
    }

    private void setupPostReport(@NonNull final Post post)
    {
        final ClooApplication application = (ClooApplication) getActivity().getApplication();
        binding.setTitle(getString(R.string.dialog_report_title_post));
        binding.setOnSubmit(v -> reportService.sendReport(
                application.getAuthentication().getSession().getUserId(), post.getKey(),
                getSelectedCategory(), ReportServiceImpl.CONTENT_TYPE_POST, getInformation(),
                this::onReportSendFinished));
    }

    private void onCancel(@NonNull final View v)
    {
        dismiss();
    }

    private void onReportSendFinished(final boolean success)
    {
        if (!isDetached()) {
            if (success) {
                dismiss();
            }
            else {
                Toast.makeText(getActivity(), getString(R.string.toast_report_dialog_send_failed),
                               Toast.LENGTH_SHORT).show();
            }
        }
    }
}