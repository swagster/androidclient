package com.cloo.android.main.util;

public final class MathUtils {
    public static long clampZero(final long value) {
        return value >= 0 ? value : 0;
    }

    public static int clampZero(final int value) {
        return value >= 0 ? value : 0;
    }

    public static float clamp(final float value, final float min, final float max) {
        return value >= min ? value <= max ? value : max : min;
    }

    public static int clamp(final int value, final int min, final int max) {
        return value >= min ? value <= max ? value : max : min;
    }
}
