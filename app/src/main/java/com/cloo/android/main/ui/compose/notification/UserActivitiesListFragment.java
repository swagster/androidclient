package com.cloo.android.main.ui.compose.notification;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentListBinding;
import com.cloo.android.main.backend.database.DatabaseQuery;
import com.cloo.android.main.model.Notification;
import com.cloo.android.main.ui.base.DatabaseQueryList;
import com.cloo.android.main.ui.base.DatabaseQueryListAdapter;
import com.cloo.android.main.ui.compose.postCreation.PostCreationActivity;

public class UserActivitiesListFragment extends DatabaseQueryList<Notification>
{
    @Nullable private FloatingActionButton fab = null;
    private boolean exists = true;

    @Override
    public void setupView(@NonNull final FragmentListBinding binding)
    {
        binding.emptyListText.setText(R.string.paragraph_notifications_empty);
        getAuthenticatedActivity().getDatabase()
                                  .getActivitiesDetailsNode(
                                          getAuthenticatedActivity().getAuthentication()
                                                                    .getSession()
                                                                    .getUserId())
                                  .exists(exists -> {
                             this.exists = exists;
                             if (!exists) {
                                 binding.listEmpty.setVisibility(View.VISIBLE);
                                 if (fab != null) { fab.show(); }
                             }
                         })
                                  .recycleAfterUse()
                                  .submit();
    }

    @Override
    public void setupPromotedAction(@NonNull final FloatingActionButton fab)
    {
        this.fab = fab;
        if (!exists) {
            fab.setImageDrawable(getActivity().getDrawable(R.drawable.v_icon_create_post));
            fab.setOnClickListener(
                    (v) -> startActivity(new Intent(getContext(), PostCreationActivity.class)));
            fab.show();
        }
        else {
            fab.hide();
        }
    }

    @NonNull
    @Override
    public DatabaseQueryListAdapter<Notification> createAdapter()
    {
        return new UserActivitiesListAdapter(getAuthenticatedActivity());
    }

    @NonNull
    @Override
    public DatabaseQuery createQuery(final String endKey, final int count)
    {
        return getAuthenticatedActivity().getDatabase()
                                         .getActivitiesDetailsNode(
                                                 getAuthenticatedActivity().getAuthentication()
                                                                           .getSession()
                                                                           .getUserId())
                                         .limitToFirst(count);
    }
}
