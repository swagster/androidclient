package com.cloo.android.main.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Keep;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.firebase.database.Exclude;

@Keep
public class Comment implements Parcelable
{
    @Exclude public static final Parcelable.Creator CREATOR = new Parcelable.Creator()
    {
        @Override
        @NonNull
        public Comment createFromParcel(@NonNull final Parcel in)
        {
            return new Comment(in);
        }

        @Override
        @NonNull
        public Comment[] newArray(final int size)
        {
            return new Comment[size];
        }
    };

    @Nullable public String author = null;
    @Nullable public String post = null;
    @Nullable public String key = null;
    public long timestamp = 0L;
    @Nullable private String text = null;
    private long lastEdited = 0L;

    public Comment()
    {
    }

    public Comment(@NonNull final Parcel parcel)
    {
        author = parcel.readString();
        post = parcel.readString();
        text = parcel.readString();
        key = parcel.readString();
        lastEdited = parcel.readLong();
        timestamp = parcel.readLong();
    }

    public Comment(final String author, final String post, final String text, final long lastEdited,
                   final long timestamp)
    {
        this.author = author;
        this.post = post;
        this.text = text;
        this.lastEdited = lastEdited;
        this.timestamp = timestamp;
    }

    @Nullable
    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(final String author)
    {
        this.author = author;
    }

    @Nullable
    public String getPost()
    {
        return post;
    }

    public void setPost(final String post)
    {
        this.post = post;
    }

    @Nullable
    public String getText()
    {
        return text;
    }

    public void setText(final String text)
    {
        this.text = text;
    }

    @Nullable
    public String getKey()
    {
        return key;
    }

    public void setKey(final String key)
    {
        this.key = key;
    }

    public long getLastEdited()
    {
        return lastEdited;
    }

    public void setLastEdited(final long lastEdited)
    {
        this.lastEdited = lastEdited;
    }

    public long getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(final long timestamp)
    {
        this.timestamp = timestamp;
    }

    @Nullable
    public String getId()
    {
        return key;
    }

    public void setId(final String id)
    {
        key = id;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull final Parcel parcel, final int i)
    {
        parcel.writeString(author);
        parcel.writeString(post);
        parcel.writeString(text);
        parcel.writeString(key);
        parcel.writeLong(lastEdited);
        parcel.writeLong(timestamp);
    }
}
