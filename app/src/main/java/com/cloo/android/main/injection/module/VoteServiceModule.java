package com.cloo.android.main.injection.module;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.authentication.Authentication;
import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.service.business.post.VoteService;
import com.cloo.android.main.service.business.post.VoteServiceImpl;

import dagger.Module;
import dagger.Provides;

@Module(includes = {DatabaseInstanceModule.class, AuthenticationModule.class})
public class VoteServiceModule
{
    @NonNull
    @Provides
    public VoteService voteService(final DatabaseAdapter database,
                                   final Authentication authentication)
    {
        return new VoteServiceImpl(database, authentication);
    }
}
