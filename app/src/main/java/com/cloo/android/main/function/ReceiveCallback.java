package com.cloo.android.main.function;

@FunctionalInterface
public interface ReceiveCallback<T>
{
    void onReceive(T object);
}
