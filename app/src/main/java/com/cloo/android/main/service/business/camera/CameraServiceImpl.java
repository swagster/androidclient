package com.cloo.android.main.service.business.camera;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.support.annotation.NonNull;

import com.cloo.android.main.function.ReceiveCallback;

public class CameraServiceImpl implements CameraService
{
    private final Activity activity;

    public CameraServiceImpl(final Activity activity)
    {
        this.activity = activity;
    }

    @Override
    public boolean hasCamera()
    {
        return activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    @Override
    public int getCameraCount()
    {
        //noinspection deprecation
        return Camera.getNumberOfCameras();
    }

    @Override
    public void getFrontCamera(@NonNull final ReceiveCallback<ClooCamera> receiveCallback)
    {
        receiveCallback.onReceive(LegacyCamera.getFrontCamera(activity));
    }

    @Override
    public void getBackCamera(@NonNull final ReceiveCallback<ClooCamera> receiveCallback)
    {
        receiveCallback.onReceive(LegacyCamera.getBackCamera(activity));
    }
}
