package com.cloo.android.main.backend.database;

import android.support.annotation.NonNull;

@SuppressWarnings("ClassWithTooManyMethods")
public abstract class DatabaseAdapter
{
    private static final String NODE_SEPARATOR = "/";

    private static final String USERS_BRANCH = "users" + NODE_SEPARATOR;
    private static final String USER_FOLLOWED_USERS = USERS_BRANCH + "followedUsers" + NODE_SEPARATOR;
    private static final String USER_FOLLOWERS = USERS_BRANCH + "followers" + NODE_SEPARATOR;

    protected abstract DatabaseNode get(@NonNull final String uri);

    @NonNull
    public abstract DatabaseNode getConnectionReference();

    public DatabaseNode getFollowedUsersNode(@NonNull final String userId)
    {
        return get(USER_FOLLOWED_USERS + userId);
    }

    public DatabaseNode getFollowedUserNode(@NonNull final String userId,
                                            @NonNull final String postId)
    {
        return get(USER_FOLLOWED_USERS + userId + NODE_SEPARATOR + postId);
    }

    public DatabaseNode getFollowersNode(@NonNull final String userId)
    {
        return get(USER_FOLLOWERS + userId);
    }

    public DatabaseNode getFollowerNode(@NonNull final String userId,
                                        @NonNull final String followerUserId)
    {
        return get(USER_FOLLOWERS + userId + NODE_SEPARATOR + followerUserId);
    }

    public DatabaseNode getCreatedPostsNode(final String userId)
    {
        return get("users/createdPosts/" + userId);
    }

    public DatabaseNode getCreatedPostNode(final String userId, final String postId)
    {
        return get("users/createdPosts/" + userId + "/" + postId);
    }

    public DatabaseNode getUserDetailsNode(final String userId)
    {
        return get("users/details/" + userId);
    }

    public DatabaseNode getUserDetailImageNode(final String userId)
    {
        return get("users/details/" + userId + "/profileImageUrl");
    }

    public DatabaseNode getUserDetailsUsernameNode(final String userId)
    {
        return get("users/details/" + userId + "/username");
    }

    public DatabaseNode getUserDetailDescriptionNode(final String userId)
    {
        return get("users/details/" + userId + "/description");
    }

    public DatabaseNode getVotedOnPostsNode(final String userId)
    {
        return get("users/votedOnPosts/" + userId);
    }

    public DatabaseNode getReportNode(final String userId, final String postId)
    {
        return get("users/reports/" + postId + "/" + userId);
    }

    public DatabaseNode getUserSharedPostsNode(final String userId, final String postId)
    {
        return get("users/sharedPosts/" + userId + "/" + postId);
    }

    public DatabaseNode getUsernameNode(final String username)
    {
        return get("users/username/" + username);
    }

    public DatabaseNode getCommentDetailsNode()
    {
        return get("comments/details");
    }

    public DatabaseNode getCommentDetailsNode(final String commentId)
    {
        return get("comments/details/" + commentId);
    }

    public DatabaseNode getCommentsOfPostNode(final String postId)
    {
        return get("comments/posts/" + postId);
    }

    public DatabaseNode getCommentOfPostNode(final String postId, final String commentId)
    {
        return get("comments/posts/" + postId + "/" + commentId);
    }

    public DatabaseNode getVoteCountNode(final String postId)
    {
        return get("posts/totalVoteCount/" + postId);
    }

    public DatabaseNode getCommentCountNode(final String postId)
    {
        return get("posts/commentCount/" + postId);
    }

    public DatabaseNode getPostDetailsNode()
    {
        return get("posts/details");
    }

    public DatabaseNode getPostDetailsNode(@NonNull final String postId)
    {
        return get("posts/details/" + postId);
    }

    public DatabaseNode getPostVotesNode(@NonNull final String postId)
    {
        return get("posts/votes/" + postId);
    }

    public DatabaseNode getPostByCategoryNode(final int category, @NonNull final String postId)
    {
        return get("posts/category/" + category + "/" + postId);
    }

    public DatabaseNode getVotedByUsersNode(@NonNull final String postId,
                                            @NonNull final String userId)
    {
        return get("posts/votedByUsers/" + postId + "/" + userId);
    }

    public DatabaseNode getUserMailboxNode(final String userId)
    {
        return get("posts/timeline/mailbox/" + userId);
    }

    public DatabaseNode getUserMailBoxPostNode(final String userId, final String postId)
    {
        return get("posts/timeline/mailbox/" + userId + "/" + postId);
    }

    public DatabaseNode getAllPostsNode()
    {
        return get("posts/all");
    }

    public DatabaseNode getAllPostsNode(final String postId)
    {
        return get("posts/all/" + postId);
    }

    public DatabaseNode getVotedByUsersNode(final String postId)
    {
        return get("posts/votedByUsers/" + postId);
    }

    public DatabaseNode getBannedUserNode(final String userId)
    {
        return get("banned/" + userId);
    }

    public DatabaseNode getActivitiesDetailsNode(final String userId)
    {
        return get("activities/details/" + userId);
    }

    public DatabaseNode getActivityDetailsNode(final String userId, final String activityId)
    {
        return get("activities/details/" + userId + "/" + activityId);
    }

    public DatabaseNode getActivityActorsNode(final String userId, final String activityId)
    {
        return get("activities/details/" + userId + "/" + activityId + "/actors");
    }

    public DatabaseNode getDeletedPostsNode(final String userId)
    {
        return get("posts/deleted/" + userId);
    }

    public DatabaseNode getFeedbackNode()
    {
        return get("feedback");
    }

    public DatabaseNode getUserPositionNode(final String userId)
    {
        return get("users/position/" + userId);
    }

    public DatabaseNode getNotificationTokensNode(final String userId, final String token)
    {
        return get("users/notificationTokens/" + userId + "/" + token);
    }

    public DatabaseNode getOnlineNode(final String userId)
    {
        return get("users/isOnline/" + userId);
    }

    public DatabaseNode getLastOnline(final String userId)
    {
        return get("users/lastOnline/" + userId);
    }

    public DatabaseNode getPrefNode(final String userId)
    {
        return get("users/prefs/" + userId);
    }

    public DatabaseNode getPrefNode(final String userId, final String key)
    {
        return get("users/prefs/" + userId + "/" + key);
    }

    public DatabaseNode getVersionNode()
    {
        return get("version/android");
    }

    public DatabaseNode getCommentCount(String postId)
    {
        return get("posts/commentCount/" + postId);
    }

    public DatabaseNode getOptionVoteCount(String postId, int index)
    {
        return get("posts/optionVoteCount/" + postId + "/" + index);
    }
}
