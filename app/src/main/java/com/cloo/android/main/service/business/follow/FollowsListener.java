package com.cloo.android.main.service.business.follow;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.function.ReceiveCallback;

public class FollowsListener {
    @Nullable
    private ReceiveCallback<Boolean> isFollowed = null;

    public FollowsListener(@NonNull final DatabaseAdapter database, @NonNull final String follower, @NonNull final String followedUser) {
        database.getFollowedUserNode(follower, followedUser)
                .onUpdates(Boolean.class, (k, o) -> {
                    if (isFollowed != null) {
                        isFollowed.onReceive(o != null && o);
                    }
                })
                .submit();
    }

    public void setFollowChangeCallback(final ReceiveCallback<Boolean> isFollowed) {
        this.isFollowed = isFollowed;
    }
}
