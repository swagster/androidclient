package com.cloo.android.main.ui.compose.profile;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.SparseArray;
import android.view.View;
import android.widget.Toast;

import com.cloo.android.R;
import com.cloo.android.databinding.ActivityUserProfileEditBinding;
import com.cloo.android.main.backend.database.DatabaseCodes;
import com.cloo.android.main.backend.database.DatabaseNode;
import com.cloo.android.main.function.DatabaseReceiveCallback;
import com.cloo.android.main.injection.component.DaggerProfileEditActivityComponent;
import com.cloo.android.main.injection.component.ProfileEditActivityComponent;
import com.cloo.android.main.injection.module.ContextModule;
import com.cloo.android.main.injection.module.DatabaseInstanceModule;
import com.cloo.android.main.model.User;
import com.cloo.android.main.model.bindingModels.ProfileEditForm;
import com.cloo.android.main.service.business.profile.ProfileService;
import com.cloo.android.main.ui.base.AuthenticatedActivity;
import com.cloo.android.main.ui.compose.imageSelection.CameraActivity;
import com.cloo.android.main.ui.compose.imageSelection.GalleryActivity;
import com.cloo.android.main.util.AndroidUtils;

import timber.log.Timber;

public class ProfileEditActivity extends AuthenticatedActivity implements DatabaseReceiveCallback<User>
{
    private static final String BUNDLE_USERNAME = "username";
    private static final String BUNDLE_DESCRIPTION = "description";
    private static final String BUNDLE_IMAGE_URL = "imageUrl";
    private final ProfileEditForm form = new ProfileEditForm();
    @Nullable private ActivityUserProfileEditBinding binding = null;
    @Nullable private ProfileService profileService = null;
    @Nullable private DatabaseNode profileInformationNode = null;
    @Nullable private User user = null;

    @Override
    public final void onActivityResult(final int requestCode, final int resultCode,
                                       @Nullable final Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
            final String newImage = data.getData().toString();
            if (!newImage.isEmpty()) {
                binding.setProfileImage(newImage);
            }
        }
    }

    private String getUsername()
    {
        return form.username.get();
    }

    private String getDescription()
    {
        return form.description.get();
    }

    @NonNull
    private String getProfileImageUrl()
    {
        return binding.getProfileImage() == null ? "" : binding.getProfileImage();
    }

    @Override
    protected void onSaveInstanceState(@NonNull final Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putString(BUNDLE_USERNAME, getUsername());
        outState.putString(BUNDLE_DESCRIPTION, getDescription());
        outState.putString(BUNDLE_IMAGE_URL, getProfileImageUrl());
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        profileInformationNode = getDatabase().getUserDetailsNode(getAuthenticatedUserId());
        profileInformationNode.receive(User.class, this)
                              .onError(this::onProfileLoadingError)
                              .submit();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        profileInformationNode.recycle();
        profileInformationNode = null;
    }

    @Override
    protected final void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_profile_edit);
        setupAbortButton(binding.toolbar);
        binding.setModel(form);
        final ProfileEditActivityComponent dependencies = DaggerProfileEditActivityComponent.builder()
                                                                                            .contextModule(
                                                                                                    new ContextModule(
                                                                                                            this))
                                                                                            .databaseInstanceModule(
                                                                                                    new DatabaseInstanceModule(
                                                                                                            getDatabase()))
                                                                                            .build();
        profileService = dependencies.getProfileService();
        binding.userProfileSave.hide();
        if (savedInstanceState != null) {
            binding.setProfileImage(savedInstanceState.getString(BUNDLE_IMAGE_URL));
            form.username.set(savedInstanceState.getString(BUNDLE_USERNAME));
            form.description.set(savedInstanceState.getString(BUNDLE_DESCRIPTION));
        }
    }

    public void selectImageFromCamera(final View view)
    {
        startActivityForResult(new Intent(this, CameraActivity.class), 0);
    }

    public void selectImageFromGallery(final View view)
    {
        startActivityForResult(new Intent(this, GalleryActivity.class), 0);
    }

    public void saveProfileChanges(final View view)
    {
        AndroidUtils.hideKeyboard(this);
        binding.userProfileSave.hide();
        binding.setPending(true);
        profileService.updateProfileInformations(this::supportFinishAfterTransition,
                                                 this::onProfileSavingError, getInformations());
    }

    private void onProfileLoadingError(@DatabaseCodes.DatabaseOperationError final int error)
    {
        Toast.makeText(getApplicationContext(),
                       R.string.toast_profile_edit_failed_to_load_informations, Toast.LENGTH_LONG)
             .show();
    }

    @SuppressWarnings("OverlyLongMethod")
    private void onProfileSavingError(@DatabaseCodes.DatabaseOperationError final int error)
    {
        if (!isDestroyed()) {
            binding.userProfileSave.show();
            binding.setPending(false);
        }
        switch (error) {
            case DatabaseCodes.AUTHENTICATION_FAILED:
                Toast.makeText(getApplicationContext(), R.string.toast_ops_authentication_failed,
                               Toast.LENGTH_LONG).show();
                break;
            case DatabaseCodes.OPERATION_UNAVAILABLE:
                Timber.d("DatabaseRules Validation error");
                Toast.makeText(getApplicationContext(), R.string.toast_operation_failed,
                               Toast.LENGTH_LONG).show();
                break;
            // The new profile informations can only be invalid if the username is already in use.
            // Other informations that have a wrong format or wrong values are ignored and or sanitized.
            case DatabaseCodes.OPERATION_INVALID:
                if (!isDestroyed()) {
                    binding.userProfileUsernameEdit.requestFocus();
                    binding.userProfileUsernameEdit.setError(
                            getString(R.string.error_profile_edit_username_inuse));
                }
                break;
            case DatabaseCodes.NETWORK_ERROR:
            case DatabaseCodes.SERVER_ERROR:
            default:
                Toast.makeText(getApplicationContext(),
                               R.string.toast_profile_edit_failed_save_changes, Toast.LENGTH_LONG)
                     .show();
        }
    }

    @NonNull
    private SparseArray<String> getInformations()
    {
        final SparseArray<String> informations = new SparseArray<>();
        informations.put(ProfileService.PROFILE_DESCRIPTION, getDescription());
        informations.put(ProfileService.PROFILE_USERNAME, getUsername());
        informations.put(ProfileService.PROFILE_PICTURE, getProfileImageUrl());
        return informations;
    }

    @Override
    public void onReceive(@NonNull final String key, @Nullable final User o)
    {
        if (o != null && !isDestroyed()) {
            o.setUserId(key);
            user = o;
            if (getProfileImageUrl().isEmpty()) {
                if (!o.getProfileImageUrl().isEmpty()) {
                    binding.setProfileImage(o.getProfileImageUrl());
                }
                else {
                    AndroidUtils.setImageDrawable(binding.userProfileImage, this,
                                                  R.drawable.v_icon_user_picture_placeholder);
                }
            }
            if (form.username.get().isEmpty()) { form.username.set(o.getUsername()); }
            if (form.description.get().isEmpty()) { form.description.set(o.getDescription()); }
            binding.userProfileSave.show();
        }
    }
}