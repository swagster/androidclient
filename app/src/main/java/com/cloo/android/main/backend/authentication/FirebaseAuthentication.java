package com.cloo.android.main.backend.authentication;

import android.accounts.NetworkErrorException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.cloo.android.main.application.ClooApplication;
import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.function.FinishCallback;
import com.cloo.android.main.function.ReceiveCallback;
import com.cloo.android.main.model.ParcelableCredential;
import com.cloo.android.main.service.business.profile.UserCreationService;
import com.cloo.android.main.service.business.profile.UserCreationServiceImpl;
import com.cloo.android.main.service.business.profile.UsernameServiceImpl;
import com.cloo.android.main.util.Assert;
import com.cloo.android.main.util.FacebookUtils;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import timber.log.Timber;

public class FirebaseAuthentication implements Authentication, FirebaseAuth.AuthStateListener
{
    private static final String TAG = Authentication.class.getSimpleName();
    private final FirebaseAuth firebaseAuth;
    private final Collection<FinishCallback> listeners = new HashSet<>(2);

    public FirebaseAuthentication()
    {
        firebaseAuth = FirebaseAuth.getInstance();
    }

    private static DatabaseAdapter getDatabase()
    {
        return ClooApplication.getInstance(
                FirebaseDatabase.getInstance().getApp().getApplicationContext()).getDatabase();
    }

    @NonNull
    private FirebaseAuth getAuth()
    {
        return firebaseAuth;
    }

    @Override
    public final void registerEmailPassword(@NonNull final String email,
                                            @NonNull final String password,
                                            @NonNull final FinishCallback listener)
    {
        Timber.d("Register email password provider");
        getAuth().createUserWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
            Timber.d("OnComplete::registerEmailPassword " + task.toString());
            listener.onFinish(task.isSuccessful());
        });
    }

    @Override
    public final void removeLoginChangeListener(@NonNull final FinishCallback listener)
    {
        listeners.remove(listener);
        if (listeners.isEmpty()) {
            firebaseAuth.removeAuthStateListener(this);
        }
    }

    @Override
    public final void addLoginChangeListener(@NonNull final FinishCallback listener)
    {
        listeners.add(listener);
        if (!listeners.isEmpty()) {
            firebaseAuth.addAuthStateListener(this);
        }
    }

    @Override
    public final boolean isLoggedIn()
    {
        return getAuth().getCurrentUser() != null;
    }

    @Override
    public final void signOut()
    {
        FacebookUtils.logout();
        if (isLoggedIn()) {
            getAuth().signOut();
        }
    }

    @Override
    public final void getProviders(@NonNull final String email,
                                   @NonNull final ReceiveCallback<Collection<String>> listener)
    {
        if (email.isEmpty()) {
            listener.onReceive(new ArrayList<>(0));
        }
        else {
            getAuth().fetchProvidersForEmail(email).addOnSuccessListener(providers -> {
                final List<String> providerList = providers.getProviders() == null ? new ArrayList<>(
                        0) : providers.getProviders();
                listener.onReceive(providerList);
            }).addOnFailureListener(e -> {
                Log.d(FirebaseAuthentication.TAG, "fetchProvidersForEmail:error", e);
                listener.onReceive(new ArrayList<>(0));
            });
        }
    }

    @Override
    public final void signIn(@NonNull final ParcelableCredential credential,
                             @Nullable final ParcelableCredential providerCredentials,
                             @NonNull final ReceiveCallback<Exception> listener)
    {
        signOut();
        getAuth().signInWithCredential(credential.getCredential()).addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                Log.d(FirebaseAuthentication.TAG, "signIn:error", task.getException());
                listener.onReceive(task.getException());
            }
            else {
                Log.d(FirebaseAuthentication.TAG, "signIn:success", task.getException());
                final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user == null) {
                    Log.d(FirebaseAuthentication.TAG, "signIn:error user is null",
                          task.getException());
                    listener.onReceive(task.getException());
                }
                else {
                    getDatabase().getBannedUserNode(user.getUid()).exists(exists -> {
                        Log.d(FirebaseAuthentication.TAG, "signIn:check if banned",
                              task.getException());
                        if (!exists) {
                            if (providerCredentials != null) {
                                Log.d(FirebaseAuthentication.TAG, "Link credential");
                                linkCredentials(providerCredentials);
                            }
                            listener.onReceive(null);
                        }
                        else {
                            listener.onReceive(new BannedException());
                        }
                    }).onError(errorCode -> listener.onReceive(new NetworkErrorException()))

                                 .recycleAfterUse().submit();
                }
            }
        });
    }

    @Override
    public final void linkCredentials(@NonNull final ParcelableCredential credential)
    {
        final FirebaseUser firebaseUser = getAuth().getCurrentUser();
        Assert.check(firebaseUser != null, "Called linkCredentials with currentUser null");
        getAuth().getCurrentUser()
                 .linkWithCredential(credential.getCredential())
                 .addOnCompleteListener(task -> {
                     if (task.isSuccessful()) {
                         Log.d(FirebaseAuthentication.TAG,
                               "linkWithCredential:success Provider:" + credential);
                     }
                     else {
                         Log.e(FirebaseAuthentication.TAG,
                               "linkWithCredentail:error Provider:" + credential,
                               task.getException());
                     }
                 });
    }

    @Override
    public final boolean hasEmailPasswordProvider(@NonNull final Collection<String> providers)
    {
        return providers.contains(Authentication.PROVIDER_EMAIL_PASSWORD);
    }

    @Override
    public final boolean hasFacebookProvider(@NonNull final Collection<String> providers)
    {
        return providers.contains(Authentication.PROVIDER_FACEBOOK);
    }

    @Override
    public final void sendPasswordReset(@NonNull final String email,
                                        @NonNull final FinishCallback onSuccess,
                                        @NonNull final ReceiveCallback<Integer> onError)
    {
        getAuth().sendPasswordResetEmail(email).addOnCompleteListener(task -> {
            final Exception exception = task.getException();
            Timber.e("Error on sendPasswordReset", exception);
            if (!task.isSuccessful()) {
                if (exception instanceof FirebaseTooManyRequestsException) {
                    onError.onReceive(TOO_MANY_REQUESTS);
                }
                else if (exception instanceof FirebaseNetworkException) {
                    onError.onReceive(CONNECTION_FAILED);
                }
            }
            onSuccess.onFinish(task.isSuccessful());
        });
    }

    @Override
    public void removeProvider(@NonNull final String provider,
                               @NonNull final ReceiveCallback<Boolean> successCallback)
    {
        getAuth().getCurrentUser()
                 .unlink(provider)
                 .addOnCompleteListener(task -> successCallback.onReceive(task.isSuccessful()));
    }

    @NonNull
    @Override
    public Session getSession()
    {
        return new FirebaseSession(!isLoggedIn() ? "" : getAuth().getCurrentUser().getUid(),
                                   !isLoggedIn() ? "" : getAuth().getCurrentUser().getEmail());
    }

    @Override
    public final void onAuthStateChanged(@NonNull final FirebaseAuth firebaseAuth)
    {
        Log.d(FirebaseAuthentication.TAG, "onAuthStateChanged::" + isLoggedIn());
        if (isLoggedIn()) {
            final UserCreationService userCreationService = new UserCreationServiceImpl(
                    new UsernameServiceImpl(getDatabase()), getDatabase());
            userCreationService.create(getSession(), couldCreateUser -> {
                Log.d(TAG, "register::create user success:" + couldCreateUser);
                if (isLoggedIn()) {
                    getDatabase().getUserDetailsNode(
                            FirebaseAuth.getInstance().getCurrentUser().getUid()).exists(exists -> {
                        for (final FinishCallback listener : listeners) {
                            listener.onFinish(isLoggedIn() && couldCreateUser);
                        }
                    }).recycleAfterUse().submit();
                }
                else {
                    for (final FinishCallback listener : listeners) {
                        listener.onFinish(isLoggedIn());
                    }
                }
            });
        }
        else {
            for (final FinishCallback listener : listeners) {
                listener.onFinish(isLoggedIn());
            }
        }
    }
}
