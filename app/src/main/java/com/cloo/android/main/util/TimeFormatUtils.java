package com.cloo.android.main.util;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class TimeFormatUtils {
    private static final long MILLI_PER_SECOND = 1000;
    private static final long MILLI_PER_MINUTE = TimeFormatUtils.MILLI_PER_SECOND * 60;
    private static final long MILLI_PER_HOUR = TimeFormatUtils.MILLI_PER_MINUTE * 60;
    private static final long MILLI_PER_DAY = TimeFormatUtils.MILLI_PER_HOUR * 24;
    private static final long MILLI_PER_WEEK = TimeFormatUtils.MILLI_PER_DAY * 7;
    private static final long MILLI_PER_MONTH = TimeFormatUtils.MILLI_PER_DAY * 30;
    private static final long MILLI_PER_YEAR = TimeFormatUtils.MILLI_PER_MONTH * 12;

    public static final int TIME_UNIT_SECONDS = 0;
    public static final int TIME_UNIT_MINUTES = 1;
    public static final int TIME_UNIT_HOURS = 2;
    public static final int TIME_UNIT_DAYS = 3;
    public static final int TIME_UNIT_WEEKS = 6;
    public static final int TIME_UNIT_MONTHS = 4;
    public static final int TIME_UNIT_YEARS = 5;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({
            TimeFormatUtils.TIME_UNIT_SECONDS, TimeFormatUtils.TIME_UNIT_DAYS, TimeFormatUtils.TIME_UNIT_MINUTES, TimeFormatUtils.TIME_UNIT_HOURS, TimeFormatUtils.TIME_UNIT_MONTHS, TimeFormatUtils.TIME_UNIT_YEARS, TimeFormatUtils.TIME_UNIT_WEEKS})
    @interface TimeUnit {
    }

    public static long getPassedUnits(@TimeUnit final int unit, final long timestamp) {
        final long deltaMillis = System.currentTimeMillis() - timestamp;
        switch (unit) {
            case TimeFormatUtils.TIME_UNIT_SECONDS:
                return deltaMillis / TimeFormatUtils.MILLI_PER_SECOND;
            case TimeFormatUtils.TIME_UNIT_MINUTES:
                return deltaMillis / TimeFormatUtils.MILLI_PER_MINUTE;
            case TimeFormatUtils.TIME_UNIT_HOURS:
                return deltaMillis / TimeFormatUtils.MILLI_PER_HOUR;
            case TimeFormatUtils.TIME_UNIT_DAYS:
                return deltaMillis / TimeFormatUtils.MILLI_PER_DAY;
            case TimeFormatUtils.TIME_UNIT_WEEKS:
                return deltaMillis / TimeFormatUtils.MILLI_PER_WEEK;
            case TimeFormatUtils.TIME_UNIT_MONTHS:
                return deltaMillis / TimeFormatUtils.MILLI_PER_MONTH;
            case TimeFormatUtils.TIME_UNIT_YEARS:
                return deltaMillis / TimeFormatUtils.MILLI_PER_YEAR;
        }
        return 0;
    }

    @TimeUnit
    public static int getTimeUnit(final long timestamp) {
        final long deltaMillis = System.currentTimeMillis() - timestamp;
        if (deltaMillis < TimeFormatUtils.MILLI_PER_MINUTE) {
            return TimeFormatUtils.TIME_UNIT_SECONDS;
        } else if (deltaMillis < TimeFormatUtils.MILLI_PER_HOUR) {
            return TimeFormatUtils.TIME_UNIT_MINUTES;
        } else if (deltaMillis < TimeFormatUtils.MILLI_PER_DAY) {
            return TimeFormatUtils.TIME_UNIT_HOURS;
        } else if (deltaMillis < TimeFormatUtils.MILLI_PER_WEEK) {
            return TimeFormatUtils.TIME_UNIT_DAYS;
        } else if (deltaMillis < TimeFormatUtils.MILLI_PER_MONTH) {
            return TimeFormatUtils.TIME_UNIT_WEEKS;
        } else if (deltaMillis < TimeFormatUtils.MILLI_PER_YEAR) {
            return TimeFormatUtils.TIME_UNIT_MONTHS;
        } else {
            return TimeFormatUtils.TIME_UNIT_YEARS;
        }
    }
}
