package com.cloo.android.main.backend.authentication;

import android.support.annotation.Nullable;

public interface Session
{
    @Nullable
    String getUserId();

    long getTimeSinceLogin();

    @Nullable
    String getEmail();
}
