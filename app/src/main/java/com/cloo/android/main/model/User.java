package com.cloo.android.main.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Keep;
import android.support.annotation.NonNull;

import com.google.firebase.database.Exclude;

@SuppressWarnings("ClassWithTooManyFields")
@Keep
public class User implements Parcelable
{
    @Exclude public static final Parcelable.Creator CREATOR = new Parcelable.Creator()
    {
        @Override
        @NonNull
        public User createFromParcel(@NonNull final Parcel in)
        {
            return new User(in);
        }

        @Override
        @NonNull
        public User[] newArray(final int size)
        {
            return new User[size];
        }
    };
    @NonNull @Exclude private String userId = "";
    @NonNull private String email = "";
    @NonNull private String profileImageUrl = "";
    @NonNull private String username = "";
    @NonNull private String birthdate = "";
    @NonNull private String description = "";
    @NonNull private String gender = "";
    private long clooScore = 0L;
    private long voteCount = 0L;
    private long createdPostCount = 0L;
    private long followedUserCount = 0L;
    private long followerCount = 0L;

    public User()
    {

    }

    public User(@NonNull final Parcel parcel)
    {
        userId = parcel.readString();
        username = parcel.readString();
        profileImageUrl = parcel.readString();
        clooScore = parcel.readLong();
        voteCount = parcel.readLong();
        createdPostCount = parcel.readLong();
        followedUserCount = parcel.readLong();
        followerCount = parcel.readLong();
    }

    @NonNull
    public String getDescription()
    {
        return description;
    }

    public void setDescription(@NonNull final String description)
    {
        this.description = description;
    }

    @NonNull
    @Exclude
    public final String getUserId()
    {
        return userId;
    }

    @Exclude
    public final void setUserId(@NonNull final String userId)
    {
        this.userId = userId;
    }

    @NonNull
    public final String getBirthdate()
    {
        return birthdate;
    }

    public final void setBirthdate(@NonNull final String birthdate)
    {
        this.birthdate = birthdate;
    }

    @NonNull
    public final String getGender()
    {
        return gender;
    }

    public final void setGender(@NonNull final String gender)
    {
        this.gender = gender;
    }

    public final long getClooScore()
    {
        return clooScore;
    }

    public final void setClooScore(final long clooScore)
    {
        this.clooScore = clooScore;
    }

    @NonNull
    public final String getProfileImageUrl()
    {
        return profileImageUrl;
    }

    public final void setProfileImageUrl(@NonNull final String profileImageUrl)
    {
        this.profileImageUrl = profileImageUrl;
    }

    public final long getVoteCount()
    {
        return voteCount;
    }

    public final void setVoteCount(final long voteCount)
    {
        this.voteCount = voteCount;
    }

    public final long getCreatedPostCount()
    {
        return createdPostCount;
    }

    public final void setCreatedPostCount(final long createdPostCount)
    {
        this.createdPostCount = createdPostCount;
    }

    public final long getFollowedUserCount()
    {
        return followedUserCount;
    }

    public final void setFollowedUserCount(final long followedUserCount)
    {
        this.followedUserCount = followedUserCount;
    }

    public final long getFollowerCount()
    {
        return followerCount;
    }

    public final void setFollowerCount(final long followerCount)
    {
        this.followerCount = followerCount;
    }

    @NonNull
    public final String getEmail()
    {
        return email;
    }

    public final void setEmail(@NonNull final String email)
    {
        this.email = email;
    }

    @NonNull
    public final String getUsername()
    {
        return username;
    }

    public final void setUsername(@NonNull final String username)
    {
        this.username = username;
    }

    @Exclude
    @Override
    public final int describeContents()
    {
        return 0;
    }

    @Exclude
    @Override
    public final void writeToParcel(@NonNull final Parcel dest, final int flags)
    {
        dest.writeString(userId);
        dest.writeString(username);
        dest.writeString(profileImageUrl);
        dest.writeLong(clooScore);
        dest.writeLong(voteCount);
        dest.writeLong(createdPostCount);
        dest.writeLong(followedUserCount);
        dest.writeLong(followerCount);
    }

    @NonNull
    public String getId()
    {
        return userId;
    }

    public void setId(@NonNull final String id)
    {
        userId = id;
    }
}
