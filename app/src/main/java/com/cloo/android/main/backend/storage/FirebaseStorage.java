package com.cloo.android.main.backend.storage;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.cloo.android.main.application.ClooApplication;
import com.cloo.android.main.function.ReceiveCallback;
import com.cloo.android.main.util.AndroidUtils;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import static com.cloo.android.main.util.ArrayUtils.isEmpty;

public class FirebaseStorage implements Storage
{
    private static final String STORAGE_URL = "gs://decideapp-33df0.appspot.com";
    private static final byte[] EMPTY_DATA = new byte[0];
    private static final String TAG = Storage.class.getSimpleName();


    @NonNull
    private static StorageReference getUserImageReference()
    {
        return com.google.firebase.storage.FirebaseStorage.getInstance()
                                                          .getReferenceFromUrl(STORAGE_URL)
                                                          .child("profile_images");
    }

    @NonNull
    private static StorageReference getUserImageReference(@NonNull final String userId)
    {
        return getUserImageReference().child(userId);
    }

    @NonNull
    private static StorageReference getPostImageReference()
    {
        return com.google.firebase.storage.FirebaseStorage.getInstance()
                                                          .getReferenceFromUrl(STORAGE_URL)
                                                          .child("post_images");
    }

    @NonNull
    private static StorageReference getPostImageReference(@NonNull final String key)
    {
        return getPostImageReference().child(key);
    }

    @NonNull
    private static byte[] reduce(@NonNull final Bitmap bitmap)
    {
        final int size = Math.min(bitmap.getHeight(), bitmap.getWidth());
        final Bitmap resized = Bitmap.createScaledBitmap(
                Bitmap.createBitmap(bitmap, 0, 0, size, size), 256, 256, true);

        final ByteArrayOutputStream dataOutputStream = new ByteArrayOutputStream();
        resized.compress(Bitmap.CompressFormat.JPEG, 100, dataOutputStream);
        return dataOutputStream.toByteArray();
    }

    @NonNull
    public static byte[] getData(@NonNull final ContentResolver resolver, @NonNull final Uri uri)
    {
        final Bitmap bitmap = AndroidUtils.getBitmap(resolver, uri);
        if (bitmap == null) {
            return EMPTY_DATA;
        }
        else {
            final byte[] result;
            result = reduce(bitmap);
            bitmap.recycle();
            return result;
        }
    }

    private void uploadImage(@NonNull final ContentResolver resolver,
                             @NonNull final StorageReference reference, @NonNull final Uri uri,
                             @NonNull final OnUploadFinishedListener successListener,
                             @NonNull final ReceiveCallback<Integer> progressListener)
    {
        Log.d(TAG, "Image src:" + uri);
        final byte[] data = getData(resolver, uri);
        if (!isEmpty(data)) {
            final ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
            final UploadTask task = reference.putStream(inputStream);
            task.addOnProgressListener(taskSnapshot -> progressListener.onReceive(
                    (int) taskSnapshot.getBytesTransferred()))
                .addOnSuccessListener(taskSnapshot -> successListener.onUploadFinished(
                        taskSnapshot.getStorage().getName(), taskSnapshot.getDownloadUrl()))
                .addOnFailureListener(e -> Log.e(TAG, "Fail image upload " + reference, e));
        }
        else {
            successListener.onUploadFinished("", Uri.EMPTY);
            Log.d(TAG, "Empty data");
        }
    }

    private void uploadImage(@NonNull final Bitmap bitmap,
                             @NonNull final StorageReference reference,
                             @NonNull final OnUploadFinishedListener successListener,
                             @NonNull final ReceiveCallback<Integer> progressListener)
    {
        final byte[] result;
        result = reduce(bitmap);
        bitmap.recycle();
        if (!isEmpty(result)) {
            final ByteArrayInputStream inputStream = new ByteArrayInputStream(result);
            final UploadTask task = reference.putStream(inputStream);
            task.addOnProgressListener(taskSnapshot -> progressListener.onReceive(
                    (int) taskSnapshot.getBytesTransferred()))
                .addOnSuccessListener(taskSnapshot -> successListener.onUploadFinished(
                        taskSnapshot.getStorage().getName(), taskSnapshot.getDownloadUrl()))
                .addOnFailureListener(e -> Log.e(TAG, "Fail image upload " + reference, e));
        }
        else {
            successListener.onUploadFinished("", Uri.EMPTY);
            Log.d(TAG, "Empty data");
        }
    }

    @Override
    public void uploadUserProfileImage(@NonNull final Context context, @NonNull final Uri data,
                                       @NonNull final OnUploadFinishedListener listener)
    {
        if (data.equals(Uri.EMPTY)) { listener.onUploadFinished("", Uri.EMPTY); }
        final ClooApplication application = (ClooApplication) context.getApplicationContext();
        if (application.getAuthentication().isLoggedIn()) {
            uploadImage(context.getContentResolver(), getUserImageReference(
                    application.getAuthentication().getSession().getUserId()), data, listener,
                        i -> {
                        });
        }
        else {
            listener.onUploadFinished("", Uri.EMPTY);
        }
    }

    @Override
    public void uploadPostOptionImage(@NonNull final Bitmap bitmap, @NonNull final String id,
                                      @NonNull final OnUploadFinishedListener listener,
                                      @NonNull final ReceiveCallback<Integer> progress)
    {
        uploadImage(bitmap, getPostImageReference(id), listener, progress);
    }
}
