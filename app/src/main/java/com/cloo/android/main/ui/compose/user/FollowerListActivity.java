package com.cloo.android.main.ui.compose.user;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.cloo.android.R;
import com.cloo.android.databinding.ActivityFollowerListBinding;
import com.cloo.android.main.model.User;
import com.cloo.android.main.ui.base.AuthenticatedActivity;

public class FollowerListActivity extends AuthenticatedActivity
{
    private static final String BUNDLE_USER_ID = "userId";

    @NonNull
    public static Intent newIntent(final Context context, final String userId)
    {
        final Intent intent = new Intent(context, FollowerListActivity.class);
        final Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_USER_ID, userId);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected final void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        final ActivityFollowerListBinding binding = DataBindingUtil.setContentView(this,
                                                                                   R.layout.activity_follower_list);
        setupBackButton(binding.toolbar);
        final Intent intent = getIntent();
        final FollowerListFragment fragment = new FollowerListFragment();
        fragment.setArguments(intent.getExtras());
        getSupportFragmentManager().beginTransaction().add(R.id.inject_fragment, fragment).commit();

        final String userId = intent.getExtras()
                                    .getString(BUNDLE_USER_ID,
                                               getAuthentication().getSession().getUserId());
        if (userId.equals(getAuthentication().getSession().getUserId())) {
            setTitle(getString(R.string.title_follower_list_you));
        }
        else {
            getDatabase().getUserDetailsNode(userId)
                         .receive(User.class, (id, user) -> setTitle(
                                 String.format(getString(R.string.title_follower_list),
                                               user.getUsername())))
                         .recycleAfterUse()
                         .submit();
        }
    }
}
