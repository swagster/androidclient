package com.cloo.android.main.application;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.cloo.android.R;
import com.cloo.android.databinding.ActivityUpdateAppBinding;
import com.cloo.android.main.ui.base.BaseActivity;

public class UpdateAppActivity extends BaseActivity
{


    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        final ActivityUpdateAppBinding binding = DataBindingUtil.setContentView(this,
                                                                                R.layout.activity_update_app);
        binding.update.setOnClickListener(v ->

                                          {
                                              final Uri uri = Uri.parse(
                                                      "market://details?id=" + getApplicationContext()
                                                              .getPackageName());
                                              final Intent goToMarket = new Intent(
                                                      Intent.ACTION_VIEW, uri);
                                              // To count with Play market backstack, After pressing back button,
                                              // to taken back to our application, we need to addAllImages following flags to intent.
                                              goToMarket.addFlags(
                                                      Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                              try {
                                                  startActivity(goToMarket);
                                              } catch (@NonNull final ActivityNotFoundException e) {
                                                  startActivity(new Intent(Intent.ACTION_VIEW,
                                                                           Uri.parse(
                                                                                   "http://play.google.com/store/apps/details?id=" + getApplicationContext()
                                                                                           .getPackageName())));
                                              }
                                          });
    }
}
