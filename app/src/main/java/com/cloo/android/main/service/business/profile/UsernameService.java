package com.cloo.android.main.service.business.profile;

import com.cloo.android.main.backend.database.DatabaseCodes;
import com.cloo.android.main.function.ExecutionCallback;
import com.cloo.android.main.function.ReceiveCallback;

public interface UsernameService {
    void changeUsername(String userId, String username, ExecutionCallback successCallback, DatabaseCodes.DatabaseOperationErrorCallback errorCallback);

    void generateUsername(ReceiveCallback<String> receiveCallback, ReceiveCallback<Exception> errorCallback);
}
