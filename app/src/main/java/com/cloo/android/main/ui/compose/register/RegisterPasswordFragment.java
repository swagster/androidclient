package com.cloo.android.main.ui.compose.register;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.content.res.AppCompatResources;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentRegisterPasswordBinding;
import com.cloo.android.main.service.util.validation.CredentialValidation;
import com.cloo.android.main.util.AndroidUtils;
import com.cloo.android.main.util.Assert;

public class RegisterPasswordFragment extends Fragment implements TextWatcher {
    @Nullable private FragmentRegisterPasswordBinding binding = null;
    @Nullable private FloatingActionButton actionButton = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register_password, container,
                false);
        binding.registerPassword.addTextChangedListener(this);
        binding.registerPasswordRepeat.addTextChangedListener(this);
        return binding.getRoot();
    }

    public void setup(final TextView statusMessage, @NonNull final FloatingActionButton actionButton) {
        this.actionButton = actionButton;
        actionButton.setImageDrawable(isValidPassword() ? AppCompatResources.getDrawable(getActivity(), R.drawable.v_icon_submit) : AppCompatResources.getDrawable(getActivity(), R.drawable.v_icon_submit_disabled));
    }

    private boolean isValidPassword() {
        return CredentialValidation.isPasswordValid(AndroidUtils.getString(binding.registerPassword)) &&
                AndroidUtils.getString(binding.registerPassword).equals(AndroidUtils.getString(binding.registerPasswordRepeat));
    }

    public boolean onSubmit() {
        return isValidPassword();
    }

    @NonNull
    public String getPassword() {
        return AndroidUtils.getString(binding.registerPassword);
    }

    @Override
    public void beforeTextChanged(final CharSequence charSequence, final int i, final int i1, final int i2) {

    }

    @Override
    public void onTextChanged(final CharSequence charSequence, final int i, final int i1, final int i2) {

    }

    @Override
    public void afterTextChanged(final Editable editable) {
        Assert.check(actionButton != null, "Action button was not initialized");
        actionButton.setImageDrawable(isValidPassword() ? AppCompatResources.getDrawable(getActivity(), R.drawable.v_icon_submit) : AppCompatResources.getDrawable(getActivity(), R.drawable.v_icon_submit_disabled));
    }
}
