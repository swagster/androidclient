package com.cloo.android.main.injection.module;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {
    private final Context context;

    public ContextModule(final Context context) {
        this.context = context;
    }

    @Provides
    public Context context() {
        return context;
    }
}
