package com.cloo.android.main.backend.database;

import android.support.annotation.NonNull;

import com.cloo.android.main.function.DatabaseReceiveCallback;
import com.cloo.android.main.function.FinishCallback;
import com.cloo.android.main.function.ReceiveCallback;

import java.util.Map;

public interface DatabaseNode extends DatabaseReference
{
    @NonNull
    DatabaseNode child(String key);

    @NonNull
    DatabaseNode generateChild();

    <T> void getChildren(final Class<T> type, @NonNull final DatabaseReceiveCallback<T> listener,
                         @NonNull final ReceiveCallback<Exception> errorListener,
                         @NonNull final FinishCallback callback);

    String getKey();

    /**
     * Sets the remote value of the node to the given value. On success callback is called with the new Server value. If an error occurs, errorCallback is called.
     * ExecutionCallback is called once after completion or abortion of the operation.
     * Each callback is never called more than once
     * This operation is safe to use in offline mode.
     * The local database model is updated on success.
     */
    <T> RequestBuilder receive(Class<T> type, DatabaseReceiveCallback<T> receiveCallback);

    <T> RequestBuilder onListUpdates(Class<T> type, DatabaseReceiveCallback<T> receiveCallback);

    <T> RequestBuilder onUpdates(Class<T> type, DatabaseReceiveCallback<T> receiveCallback);

    RequestBuilder exists(ReceiveCallback<Boolean> receiveCallback);

    RequestBuilder set(Object value);

    RequestBuilder delete();

    RequestBuilder setChildren(Map<String, Object> childValues);
}
