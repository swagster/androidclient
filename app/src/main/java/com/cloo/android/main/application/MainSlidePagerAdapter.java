package com.cloo.android.main.application;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;

import com.cloo.android.R;
import com.cloo.android.main.service.business.notification.NotificationBadgeService;
import com.cloo.android.main.service.business.notification.NotificationBadgeServiceImpl;
import com.cloo.android.main.ui.compose.postList.DiscoverListFragment;
import com.cloo.android.main.ui.compose.postList.TimelineFragment;
import com.cloo.android.main.ui.compose.user.FollowedUserListFragment;
import com.cloo.android.main.util.AndroidUtils;

class MainSlidePagerAdapter extends FragmentPagerAdapter implements SharedPreferences.OnSharedPreferenceChangeListener
{
    private static final int NUM_TABS = 3;
    @NonNull private final Context context;
    private final TimelineFragment timelineFragment = new TimelineFragment();
    private final DiscoverListFragment trendListFragment = new DiscoverListFragment();
    private final FollowedUserListFragment followerListFragment = new FollowedUserListFragment();
    @NonNull private final NotificationBadgeService notificationBadgeService;
    private final FloatingActionButton fab;
    private boolean init;

    public MainSlidePagerAdapter(@NonNull final FragmentManager fragmentManager,
                                 @NonNull final Context context, final FloatingActionButton fab)
    {
        super(fragmentManager);
        notificationBadgeService = new NotificationBadgeServiceImpl(
                ClooApplication.getInstance(context));
        this.context = context;
        this.fab = fab;
    }

    @NonNull
    @Override
    public final CharSequence getPageTitle(final int position)
    {
        String title;
        Drawable myDrawable;
        if (!init) {
            init = true;
            AndroidUtils.getUserSharedPreferences(context)
                        .registerOnSharedPreferenceChangeListener(this);
        }
        switch (position) {
            case 1:
                myDrawable = context.getResources().getDrawable(R.drawable.v_icon_discover);
                title = "";//context.getString(R.string.title_discover_posts);
                break;
            case 0:
                myDrawable = context.getResources().getDrawable(R.drawable.v_icon_timeline);
                title = "";//context.getString(R.string.title_timeline);
                break;
            case 2:
            default:
                myDrawable = context.getResources().getDrawable(R.drawable.v_icon_follower);
                title = "";//context.getString(R.string.title_timeline);
        }
        SpannableStringBuilder sb = new SpannableStringBuilder(
                "  " + title); // space added before text for convenience
        myDrawable.setBounds(0, 0, myDrawable.getIntrinsicWidth(), myDrawable.getIntrinsicHeight());
        ImageSpan span = new ImageSpan(myDrawable, ImageSpan.ALIGN_BASELINE);
        sb.setSpan(span, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        return sb;
    }

    public void setupFab(final int currentPosition)
    {
        switch (currentPosition) {
            case 1:
                trendListFragment.setupPromotedAction(fab);
                break;
            case 0:
                timelineFragment.setupPromotedAction(fab);
                break;
            case 2:
                followerListFragment.setupPromotedAction(fab);
            default:

        }
    }

    @NonNull
    @Override
    public final Fragment getItem(final int position)
    {
        switch (position) {
            case 1:
                return trendListFragment;
            case 0:
                return timelineFragment;
            default:
                return followerListFragment;
        }
    }


    @Override
    public void onSharedPreferenceChanged(final SharedPreferences sharedPreferences,
                                          @NonNull final String s)
    {
        if (s.equals("badgeCount")) {
            notifyDataSetChanged();
        }
    }

    @Override
    public final int getCount()
    {
        return MainSlidePagerAdapter.NUM_TABS;
    }
}
