package com.cloo.android.main.service.util.validation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.cloo.android.R;

public class PostCreationValidator
{
    private final int MAX_CHARACTERS_POST_TITLE;
    private final int MAX_CHARACTERS_POST_DESCRIPTION;

    public PostCreationValidator(@NonNull final Context context)
    {
        MAX_CHARACTERS_POST_DESCRIPTION = context.getResources()
                                                 .getInteger(
                                                         R.integer.character_count_post_description);
        MAX_CHARACTERS_POST_TITLE = context.getResources()
                                           .getInteger(R.integer.character_count_post_title);
    }

    public boolean isValidPostTitle(@Nullable final String title)
    {
        return title != null && !title.isEmpty() && title.length() < MAX_CHARACTERS_POST_TITLE;
    }

    public boolean isValidPostDescription(@Nullable final String description)
    {
        return description != null && description.length() < MAX_CHARACTERS_POST_DESCRIPTION;
    }
}
