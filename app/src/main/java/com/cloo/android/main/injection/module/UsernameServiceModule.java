package com.cloo.android.main.injection.module;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.service.business.profile.UsernameService;
import com.cloo.android.main.service.business.profile.UsernameServiceImpl;

import dagger.Module;
import dagger.Provides;

@Module(includes = {DatabaseInstanceModule.class})
public class UsernameServiceModule {

    @NonNull
    @Provides
    public UsernameService usernameService(final DatabaseAdapter databaseAdapter) {
        return new UsernameServiceImpl(databaseAdapter);
    }
}
