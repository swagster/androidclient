package com.cloo.android.main.ui.compose.profile;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.cloo.android.databinding.FragmentListElementPostBinding;
import com.cloo.android.main.model.Post;
import com.cloo.android.main.ui.base.BaseActivity;
import com.cloo.android.main.ui.compose.post.PostOptionsAdapter;
import com.cloo.android.main.ui.compose.postList.PostListAdapter;

public class DeletedPostsAdapter extends PostListAdapter
{
    public DeletedPostsAdapter(@NonNull final BaseActivity activity)
    {
        super(activity);
    }


    @Override
    protected void listenToPostUpdates(@NonNull final FragmentListElementPostBinding binding,
                                       @NonNull final Post oldPost, final String k,
                                       @Nullable final Post o, final int index)
    {
        if (o == null || o.equals(oldPost)) {
            return;
        }

        o.setKey(k);
        o.selectedOption.set(oldPost.selectedOption.get());
        binding.setPost(o);
        posts.set(index, o);
        binding.notifyChange();
        ((PostOptionsAdapter) binding.postSelection.getAdapter()).setPost(o);

    }

    @Override
    protected void addDataObject(@NonNull final Post post)
    {
        for (int i = 0; i < posts.size(); i++) {
            final Post oldPost = posts.get(i);
            if (oldPost.getKey().equals(post.getKey())) {
                post.selectedOption.set(oldPost.selectedOption.get());
                posts.set(i, post);
                notifyDataSetInvalidated();
                return;
            }
        }
        posts.add(post);
        sort(POST_COMPARATOR_TIMESTAMP);
        notifyDataSetChanged();
    }
}
