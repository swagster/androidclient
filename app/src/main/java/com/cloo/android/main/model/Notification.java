package com.cloo.android.main.model;

import android.support.annotation.Keep;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.firebase.database.ServerValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

@Keep
public class Notification
{
    private long timestamp = 0L;
    @Nullable private String content = null;
    @Nullable private String actualContentId = null;
    private long contentType = 0L;
    private List<String> extras = new ArrayList<>();
    private long type = 0L;
    @Nullable private String id = null;

    public Notification()
    {
    }

    public Notification(final long timestamp, @NonNull final String content, final long contentType,
                        final List<String> extras, final long type)
    {
        this.timestamp = timestamp;
        this.content = content;
        actualContentId = content.substring(0, content.indexOf('@'));
        this.contentType = contentType;
        this.extras = extras;
        this.type = type;
    }

    public long getContentType()
    {
        return contentType;
    }

    public void setContentType(final long contentType)
    {
        this.contentType = contentType;
    }

    public long getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(final long timestamp)
    {
        this.timestamp = timestamp;
    }

    @Nullable
    public String getContent()
    {
        return content;
    }

    public void setContent(@Nullable final String content)
    {
        this.content = content;
        actualContentId = content == null ? "" : content.substring(0, content.indexOf('@'));
    }

    @Nullable
    public String getActualContentId()
    {
        Timber.d("Actual content id is %s",actualContentId);
        return actualContentId;
    }

    public List<String> getExtras()
    {
        return extras;
    }

    public void setExtras(final List<String> extras)
    {
        this.extras = extras;
    }

    public long getType()
    {
        return type;
    }

    public void setType(final long type)
    {
        this.type = type;
    }

    @NonNull
    public final Map<String, Object> toMap()
    {
        final Map<String, Object> map = new HashMap<>();
        map.put("extras", extras);
        map.put("content", content);
        map.put("timestamp", ServerValue.TIMESTAMP);
        return map;
    }

    @Nullable
    public String getId()
    {
        return id;
    }

    public void setId(final String id)
    {
        this.id = id;
    }
}
