package com.cloo.android.main.util;

import com.cloo.android.BuildConfig;

public final class Assert
{
    public static void check(final boolean condition, final String message)
    {
        if (BuildConfig.DEBUG && !condition) {
            throw new AssertionError(message);
        }
    }
}
