package com.cloo.android.main.injection.module;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.service.util.network.ServerConnectionListener;

import dagger.Module;
import dagger.Provides;

@Module(includes = DatabaseInstanceModule.class)
public class ServerConnectionListenerModule {
    @NonNull
    @Provides
    public ServerConnectionListener serverConnectionListener(
            @NonNull final DatabaseAdapter databaseAdapter)
    {
        return new ServerConnectionListener(databaseAdapter);
    }
}
