package com.cloo.android.main.util;

import android.support.annotation.NonNull;

public final class ArrayUtils
{

    private ArrayUtils()
    {
    }

    public static boolean isEmpty(@NonNull final byte[] array)
    {
        return array.length <= 0;
    }

}
