package com.cloo.android.main.service.util.image;

import android.widget.ImageView;

public interface ImageLoader {
    void load(String uri, ImageView image);

    void clear(ImageView image);
}
