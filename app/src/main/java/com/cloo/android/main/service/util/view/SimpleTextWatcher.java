package com.cloo.android.main.service.util.view;

import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;

public class SimpleTextWatcher implements TextWatcher
{
    @NonNull
    private final ObservableField<String> observable;
    @NonNull
    private final AfterTextChangedListener listener;

    public SimpleTextWatcher(@NonNull final ObservableField<String> observable)
    {
        this.observable = observable;
        listener = s -> {};
    }

    public SimpleTextWatcher(@NonNull final ObservableField<String> observable, @NonNull final AfterTextChangedListener
            listener)
    {
        this.observable = observable;
        this.listener = listener;
    }

    @Override
    public void beforeTextChanged(@NonNull final CharSequence sequence, final int start, final int count, final int after)
    {
    }

    @Override
    public void onTextChanged(@NonNull final CharSequence sequence, final int start, final int before, final int count)
    {
    }

    @Override
    public final void afterTextChanged(@NonNull final Editable editable)
    {
        observable.set(editable.toString());
        listener.afterTextChanged(editable.toString());
    }

    @FunctionalInterface
    public interface AfterTextChangedListener
    {
        void afterTextChanged(String s);
    }
}
