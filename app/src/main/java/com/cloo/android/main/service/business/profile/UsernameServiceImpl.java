package com.cloo.android.main.service.business.profile;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.database.DatabaseAdapter;
import com.cloo.android.main.backend.database.DatabaseCodes;
import com.cloo.android.main.function.ExecutionCallback;
import com.cloo.android.main.function.ReceiveCallback;
import com.cloo.android.main.model.User;

import timber.log.Timber;

public class UsernameServiceImpl implements UsernameService
{

    private final DatabaseAdapter databaseAdapter;

    public UsernameServiceImpl(final DatabaseAdapter databaseAdapter)
    {
        this.databaseAdapter = databaseAdapter;
    }

    @Override
    public void changeUsername(final String userId, final String username,
                               @NonNull final ExecutionCallback successCallback,
                               @NonNull final DatabaseCodes.DatabaseOperationErrorCallback errorCallback)
    {
        databaseAdapter.getUserDetailsUsernameNode(userId)
                       .receive(String.class, (s, oldUsername) -> {
                           if (oldUsername.equals(username)) {
                               Timber.d("Username wasn't changed, success");
                               successCallback.execute();
                           }
                           else {
                               databaseAdapter.getUsernameNode(username)
                                              .receive(String.class, (uname, id) -> {
                                                  if (id == null) {
                                                      // TODO Check if the order of these operations is correct
                                                      databaseAdapter.getUsernameNode(username)
                                                                     .set(userId)
                                                                     .onSuccess(
                                                                             () -> databaseAdapter.getUserDetailsUsernameNode(
                                                                                     userId)
                                                                                                  .set(username)
                                                                                                  .onSuccess(
                                                                                                          successCallback)
                                                                                                  .onError(
                                                                                                          error -> errorCallback
                                                                                                                  .onError(
                                                                                                                          DatabaseCodes.OPERATION_UNAVAILABLE))
                                                                                                  .recycleAfterUse()
                                                                                                  .submit())
                                                                     .onError(
                                                                             error -> errorCallback.onError(
                                                                                     DatabaseCodes.OPERATION_UNAVAILABLE))
                                                                     .recycleAfterUse()
                                                                     .submit();
                                                  }
                                                  else {
                                                      databaseAdapter.getUserDetailsNode(id)
                                                                     .receive(User.class,
                                                                              (key, user) -> {
                                                                                  if (!user.getUsername()
                                                                                          .equals(uname)) {
                                                                                      databaseAdapter
                                                                                              .getUsernameNode(
                                                                                                      username)
                                                                                              .set(userId)
                                                                                              .onSuccess(
                                                                                                      () -> databaseAdapter
                                                                                                              .getUserDetailsUsernameNode(
                                                                                                                      userId)
                                                                                                              .set(username)
                                                                                                              .onSuccess(
                                                                                                                      successCallback)
                                                                                                              .onError(
                                                                                                                      error -> errorCallback
                                                                                                                              .onError(
                                                                                                                                      DatabaseCodes.OPERATION_UNAVAILABLE))
                                                                                                              .recycleAfterUse()
                                                                                                              .submit())
                                                                                              .onError(
                                                                                                      error -> errorCallback
                                                                                                              .onError(
                                                                                                                      DatabaseCodes.OPERATION_UNAVAILABLE))
                                                                                              .recycleAfterUse()
                                                                                              .submit();
                                                                                  }
                                                                                  else {
                                                                                      errorCallback.onError(
                                                                                              DatabaseCodes.OPERATION_INVALID);
                                                                                  }
                                                                              })
                                                                     .onError(errorCallback)
                                                                     .recycleAfterUse()
                                                                     .submit();
                                                  }
                                              })
                                              .onError(errorCallback)
                                              .recycleAfterUse()
                                              .submit();
                           }
                       })
                       .onError(error -> errorCallback.onError(DatabaseCodes.OPERATION_UNAVAILABLE))
                       .recycleAfterUse()
                       .submit();
    }

    @Override
    public void generateUsername(@NonNull final ReceiveCallback<String> receiveCallback,
                                 final ReceiveCallback<Exception> errorCallback)
    {
        receiveCallback.onReceive("User" + Long.toHexString(System.currentTimeMillis()));
    }
}
