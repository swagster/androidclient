package com.cloo.android.main.backend.storage;

import android.net.Uri;

@FunctionalInterface
public interface OnUploadFinishedListener {
    void onUploadFinished(String storageKey, Uri downloadUrl);
}
