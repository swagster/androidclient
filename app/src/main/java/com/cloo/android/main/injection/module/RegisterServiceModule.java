package com.cloo.android.main.injection.module;

import android.support.annotation.NonNull;

import com.cloo.android.main.backend.authentication.Authentication;
import com.cloo.android.main.service.business.profile.UserCreationService;
import com.cloo.android.main.service.business.register.RegisterService;
import com.cloo.android.main.service.business.register.RegisterServiceImpl;

import dagger.Module;
import dagger.Provides;

@Module(includes = {AuthenticationModule.class, UserCreationModule.class})
public class RegisterServiceModule {
    @NonNull
    @Provides
    public RegisterService registerService(final Authentication authentication, final UserCreationService userCreationService) {
        return new RegisterServiceImpl(authentication, userCreationService);
    }
}
