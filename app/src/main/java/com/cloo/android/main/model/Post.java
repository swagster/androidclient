package com.cloo.android.main.model;

import android.databinding.ObservableInt;
import android.support.annotation.Keep;
import android.support.annotation.NonNull;

import com.google.firebase.database.Exclude;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Keep
public class Post
{
    @Exclude public final ObservableInt selectedOption = new ObservableInt(-1);

    // Server model
    private boolean deleted;
    private int category = 0;
    private String shareComment = "";
    private String sharedFrom = "";
    @NonNull private String author = "";
    @NonNull private String title = "";
    @NonNull private String description = "";
    private long timestamp = 0L;
    @Exclude private String key = "";
    private Map<String, Boolean> restricted = new HashMap<>();
    private List<PostOption> options = new ArrayList<>();
    private List<String> hashTags = new ArrayList<>();
    private String placeId = "";
    private double lon;
    private double lat;

    public Post()
    {
        // For json builder
    }

    public Map<String, Boolean> getRestricted()
    {
        return restricted;
    }

    public void setRestricted(Map<String, Boolean> restricted)
    {
        this.restricted = restricted;
    }

    @NonNull
    public ObservableInt getSelectedOption()
    {
        return selectedOption;
    }

    public String getShareComment()
    {
        return shareComment;
    }

    public void setShareComment(String shareComment)
    {
        this.shareComment = shareComment;
    }

    public String getSharedFrom()
    {
        return sharedFrom;
    }

    public void setSharedFrom(String sharedFrom)
    {
        this.sharedFrom = sharedFrom;
    }

    public List<String> getHashTags()
    {
        return hashTags;
    }

    public void setHashTags(List<String> hashTags)
    {
        this.hashTags = hashTags;
    }

    public String getPlaceId()
    {
        return placeId;
    }

    public void setPlaceId(String placeId)
    {
        this.placeId = placeId;
    }

    public double getLon()
    {
        return lon;
    }

    public void setLon(double lon)
    {
        this.lon = lon;
    }

    public double getLat()
    {
        return lat;
    }

    public void setLat(double lat)
    {
        this.lat = lat;
    }

    public final int getCategory()
    {
        return category;
    }

    public final void setCategory(final int category)
    {
        this.category = category;
    }

    public boolean isDeleted()
    {
        return deleted;
    }

    public void setDeleted(final boolean deleted)
    {
        this.deleted = deleted;
    }

    public final List<PostOption> getOptions()
    {
        return options;
    }

    public final void setOptions(final List<PostOption> options)
    {
        this.options = options;
    }

    @Exclude
    public final String getKey()
    {
        return key;
    }

    @Exclude
    public final void setKey(final String key)
    {
        this.key = key;
    }

    @NonNull
    public final String getAuthor()
    {
        return author;
    }

    public final void setAuthor(@NonNull final String author)
    {
        this.author = author;
    }

    @NonNull
    public final String getTitle()
    {
        return title;
    }

    public final void setTitle(@NonNull final String title)
    {
        this.title = title;
    }

    @NonNull
    public final String getDescription()
    {
        return description;
    }

    public final void setDescription(@NonNull final String description)
    {
        this.description = description;
    }


    public final long getTimestamp()
    {
        return timestamp;
    }

    public final void setTimestamp(final long timestamp)
    {
        this.timestamp = timestamp;
    }

    @NonNull
    @Exclude
    @Override
    public final String toString()
    {
        return String.format(Locale.getDefault(),
                             "Post{title:%s,description:%s,author:%s,key:%s,timestamp:%d", title,
                             description, author, key, timestamp);
    }

    public final boolean equals(final Object object)
    {
        if (!(object instanceof Post)) {
            return false;
        }
        final Post other = (Post) object;
        return options.equals(
                other.options) && other.selectedOption.get() == selectedOption.get() && other.title.equals(
                title) && other.timestamp == timestamp && other.author.equals(
                author) && other.getKey().equals(key) && other.description.equals(description);
    }
}
