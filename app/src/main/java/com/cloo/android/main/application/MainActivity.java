package com.cloo.android.main.application;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.cloo.android.R;
import com.cloo.android.databinding.ActivityMainBinding;
import com.cloo.android.main.ui.base.AuthenticatedActivity;
import com.cloo.android.main.ui.common.AddByUsernameDialog;
import com.cloo.android.main.ui.compose.general.AboutActivity;
import com.cloo.android.main.ui.compose.settings.SettingsActivity;
import com.cloo.android.main.ui.compose.user.UserActivity;
import com.cloo.android.main.util.AndroidUtils;
import com.google.firebase.iid.FirebaseInstanceId;

import java.lang.reflect.Field;

import timber.log.Timber;

public class MainActivity extends AuthenticatedActivity
{
    private static final int REQUEST_POSITION = 89;

    @Nullable private ActivityMainBinding binding = null;
    @Nullable private MainSlidePagerAdapter adapter = null;

    private AlertDialog getPermissionRationale()
    {
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Need location permission");
        alertBuilder.setMessage(
                "This lets Cloo track your current position so you get content relevant to your position.");
        alertBuilder.setPositiveButton(android.R.string.yes,
                                       (dialog, which) -> ActivityCompat.requestPermissions(
                                               MainActivity.this,
                                               new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                               MainActivity.REQUEST_POSITION));
        return alertBuilder.create();
    }

    @Override
    public final void onRequestPermissionsResult(final int requestCode,
                                                 @NonNull final String[] permissions,
                                                 @NonNull final int[] grantResults)
    {
        switch (requestCode) {
            case MainActivity.REQUEST_POSITION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getClooApplication().setupLocationTracker();
                }
                break;
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }


    @Override
    protected final void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Timber.d("MainActivity onCreate:start");
        final String notificationToken = PreferenceManager.getDefaultSharedPreferences(this)
                                                          .getString("notificationToken", "");

        getDatabase().getNotificationTokensNode(getAuthentication().getSession().getUserId(),
                                                notificationToken.isEmpty() ? FirebaseInstanceId.getInstance()
                                                                                                .getToken() : notificationToken)
                     .set(true)
                     .recycleAfterUse()
                     .submit();

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        adapter = new MainSlidePagerAdapter(getSupportFragmentManager(), this,
                                            binding.listPromotedAction);

        binding.pager.setAdapter(adapter);
        binding.pager.setOffscreenPageLimit(3);
        binding.pager.setCurrentItem(1, false);
        adapter.setupFab(1);
        binding.tabs.setupWithViewPager(binding.pager);
        binding.pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(final int position, final float positionOffset,
                                       final int positionOffsetPixels)
            {
            }

            @Override
            public void onPageSelected(final int position)
            {
                adapter.setupFab(position);
            }

            @Override
            public void onPageScrollStateChanged(final int state)
            {

            }
        });
        setSupportActionBar(binding.toolbar);
        Timber.d("MainActivity onCreate:set Title onClick listener");
        Field titleField;
        try {
            titleField = Toolbar.class.getDeclaredField("mTitleTextView");
            titleField.setAccessible(true);
            TextView barTitleView = (TextView) titleField.get(binding.toolbar);
            barTitleView.setOnClickListener(v -> showToast("Stop touchy!"));
        } catch (@NonNull NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        Timber.d("MainActivity onCreate:addLoginChangeListener");
        getAuthentication().addLoginChangeListener(isLoggedIn -> {
            if (!isLoggedIn) {
                final long sessionStart = AndroidUtils.getUserSharedPreferences(this)
                                                      .getLong(getString(
                                                              R.string.storage_session_start), 0);
                AndroidUtils.getUserSharedPreferences(this)
                            .edit()
                            .putLong(getString(R.string.storage_usage_time),
                                     AndroidUtils.getUserSharedPreferences(this)
                                                 .getLong(getString(R.string.storage_usage_time),
                                                          0) + (System.currentTimeMillis() - sessionStart))
                            .apply();
                getAuthentication().signOut();
                finish();
            }
            else {
                final ClooApplication application = ((ClooApplication) getApplication());
                AndroidUtils.getApplicationSharedPreferences(this)
                            .edit()
                            .putString(getString(R.string.preference_last_user_id),
                                       getAuthenticatedUserId())
                            .apply();

                AndroidUtils.getUserSharedPreferences(this)
                            .edit()
                            .putLong(getString(R.string.storage_session_start),
                                     System.currentTimeMillis())
                            .apply();
                if (AndroidUtils.getUserSharedPreferences(this)
                                .getBoolean(
                                        getString(R.string.preference_local_switch_track_position),
                                        true) && ActivityCompat.checkSelfPermission(application,
                                                                                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat
                        .checkSelfPermission(application,
                                             Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                                                                            Manifest.permission.ACCESS_FINE_LOCATION)) {
                        getPermissionRationale().show();
                    }
                    else {
                        ActivityCompat.requestPermissions(this,
                                                          new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                                          MainActivity.REQUEST_POSITION);
                    }
                }
                getClooApplication().setupLocationTracker();
            }
        });
        Timber.d("MainActivity onCreate:finish");
    }

    @Override
    public final boolean onCreateOptionsMenu(@NonNull final Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_post_feed, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public final boolean onOptionsItemSelected(@NonNull final MenuItem item)
    {
        switch (item.getItemId()) {
            case R.id.menu_profil:
                startActivity(new Intent(this, UserActivity.class));
                return true;
            case R.id.menu_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.menu_add_friend:
                final AddByUsernameDialog dialog = new AddByUsernameDialog();
                getSupportFragmentManager().popBackStack();
                dialog.show(getSupportFragmentManager(), "add_user");
                return true;
            case R.id.menu_about:
                startActivity(new Intent(this, AboutActivity.class));
                return true;
            case R.id.menu_privacy:
                final String url = "https://theclooapp.com/privacy";
                final Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                return true;
            case R.id.menu_terms:
                final String urll = "http://www.theclooapp.com/terms";
                final Intent ii = new Intent(Intent.ACTION_VIEW);
                ii.setData(Uri.parse(urll));
                startActivity(ii);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
