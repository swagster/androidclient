package com.cloo.android.main.ui.compose.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.cloo.android.R;
import com.cloo.android.main.application.ClooApplication;
import com.cloo.android.main.util.AndroidUtils;

import java.util.Locale;

import timber.log.Timber;

public class ClooPreferenceFragment extends android.preference.PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener
{
    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null) {
            PreferenceManager.getDefaultSharedPreferences(getActivity())
                             .registerOnSharedPreferenceChangeListener(this);
        }
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference);
    }

    @Override
    public void onSharedPreferenceChanged(@NonNull final SharedPreferences sharedPreferences,
                                          @NonNull final String s)
    {
        if (getActivity() != null && s.equals(
                getActivity().getString(R.string.preference_local_list_localization))) {
            Timber.d("Changed language");
            onLocaleChanged(sharedPreferences.getString(
                    getActivity().getString(R.string.preference_local_list_localization), "en"));
        }
        if (getActivity() != null && s.equals(
                getString(R.string.preference_local_switch_track_position))) {
            onTrackPositionChanged(sharedPreferences.getBoolean(s, true));
        }
    }

    private void onLocaleChanged(@NonNull final String lang)
    {
        final Locale defaultLocale = getResources().getConfiguration().locale;
        if (!isDefaultLocaleDefined()) {
            AndroidUtils.setLanguage(getActivity(), Locale.ENGLISH);
        }
        else if (lang.equals("en") && !defaultLocale.equals(Locale.ENGLISH) || lang.equals(
                "de") && !defaultLocale.equals(Locale.GERMAN)) {
            AndroidUtils.setLanguage(getActivity(),
                                     lang.equals("en") ? Locale.ENGLISH : Locale.GERMAN);
            Toast.makeText(getActivity(), R.string.toast_changed_language, Toast.LENGTH_LONG)
                 .show();
        }
    }

    private boolean isDefaultLocaleDefined()
    {
        final Locale defaultLocale = getResources().getConfiguration().locale;
        return defaultLocale.equals(Locale.ENGLISH) || defaultLocale.equals(Locale.GERMAN);
    }

    private void onTrackPositionChanged(final boolean enabled)
    {
        if (enabled) { ((ClooApplication) getActivity().getApplication()).setupLocationTracker(); }
    }
}