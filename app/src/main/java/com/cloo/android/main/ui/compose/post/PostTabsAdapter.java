package com.cloo.android.main.ui.compose.post;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;

import com.cloo.android.R;
import com.cloo.android.main.application.ClooApplication;

class PostTabsAdapter extends FragmentStatePagerAdapter
{
    private static final int NUM_TABS = 3;
    @NonNull private final PostDetailsFragment postDetailsFragment;
    @NonNull private final CommentListFragment commentListFragment;
    @NonNull private final PostVotesListFragment postVotesListFragment;
    @NonNull private final Context context;
    private long commentCount;

    public PostTabsAdapter(@NonNull final FragmentManager fragmentManager, @NonNull final Context context,
                           @NonNull final String postId)
    {
        super(fragmentManager);
        this.context = context;
        postDetailsFragment = PostDetailsFragment.newInstance(postId);
        commentListFragment = CommentListFragment.newInstance(postId);
        postVotesListFragment = PostVotesListFragment.newInstance(postId);
        ClooApplication app = (ClooApplication) context.getApplicationContext();
        app.getDatabase().getCommentCount(postId).onUpdates(Long.class, (key, count) -> {
            commentCount = count == null ? 0 : count;
            notifyDataSetChanged();
        }).submit();
    }

    @NonNull
    @Override
    public final CharSequence getPageTitle(final int position)
    {
        switch (position) {
            case 0:
                return context.getString(R.string.title_post_details_short);
            case 1:
                String title;
                Drawable myDrawable;
                myDrawable = context.getResources().getDrawable(R.drawable.v_icon_comments);
                title = (commentCount == 0 ? "" : "(" + commentCount + ")");
                SpannableStringBuilder sb = new SpannableStringBuilder(
                        "  " + title); // space added before text for convenience
                myDrawable.setBounds(0, 0, myDrawable.getIntrinsicWidth(),
                                     myDrawable.getIntrinsicHeight());
                ImageSpan span = new ImageSpan(myDrawable, ImageSpan.ALIGN_BASELINE);
                sb.setSpan(span, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                return sb;
            case 2:
                return context.getString(R.string.title_post_voted);
            default:
                return context.getString(R.string.app_name);
        }
    }

    @NonNull
    @Override
    public final Fragment getItem(final int position)
    {
        switch (position) {
            case 0:
                return postDetailsFragment;
            case 1:
                return commentListFragment;
            case 2:
                return postVotesListFragment;
            default:
                return postVotesListFragment;
        }
    }

    @Override
    public final int getCount()
    {
        return PostTabsAdapter.NUM_TABS;
    }
}
