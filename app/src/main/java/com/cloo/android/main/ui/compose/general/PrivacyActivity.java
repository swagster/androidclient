package com.cloo.android.main.ui.compose.general;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.cloo.android.R;
import com.cloo.android.databinding.ActivityPoliciesBinding;
import com.cloo.android.main.ui.base.BaseActivity;

public class PrivacyActivity extends BaseActivity
{
    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        final ActivityPoliciesBinding binding = DataBindingUtil.setContentView(this,
                                                                               R.layout.activity_policies);
        binding.policies.loadUrl("https://theclooapp.com/privacy");
    }
}
