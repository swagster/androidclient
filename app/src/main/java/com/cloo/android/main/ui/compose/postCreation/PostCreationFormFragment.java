package com.cloo.android.main.ui.compose.postCreation;

import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.cloo.android.R;
import com.cloo.android.databinding.FragmentPostCreationFormBinding;
import com.cloo.android.main.model.bindingModels.PostCreationForm;
import com.cloo.android.main.service.util.validation.PostCreationValidator;

public class PostCreationFormFragment extends Fragment
{
    public final PostCreationForm creationForm = new PostCreationForm();
    @Nullable PostCreationValidator validator = null;
    @Nullable private FragmentPostCreationFormBinding binding = null;
    private FloatingActionButton fab;
    private final Observable.OnPropertyChangedCallback propertyChangedCallback = new Observable.OnPropertyChangedCallback()
    {
        @Override
        public void onPropertyChanged(final Observable observable, final int i)
        {
            if (isValid()) {
                fab.setImageDrawable(getActivity().getDrawable(R.drawable.v_icon_forward));
            }
            else {

                fab.setImageDrawable(getActivity().getDrawable(R.drawable.v_icon_forward_disabled));
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater,
                             @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_post_creation_form, container,
                                          false);
        binding.setModel(creationForm);
        validator = new PostCreationValidator(getActivity());
        setupCategorySelection();

        return binding.getRoot();
    }

    public void setFab(FloatingActionButton fab)
    {
        this.fab = fab;
        creationForm.title.addOnPropertyChangedCallback(propertyChangedCallback);
    }

    private void setupCategorySelection()
    {
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                                                                                   R.array.post_categories,
                                                                                   android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.postCreationCategory.setAdapter(adapter);
    }

    public int getCategory()
    {
        return binding.postCreationCategory.getSelectedItemPosition();
    }

    public String getDescription()
    {
        return creationForm.description.get();
    }

    public String getTitle()
    {
        return creationForm.title.get();
    }

    public boolean isValid()
    {
        return validate();
    }

    private boolean validate()
    {
        boolean valid = validator.isValidPostTitle(creationForm.title.get());
        if (!valid) {
            binding.title.setError("Title is invalid");
        }
        valid = valid && validator.isValidPostDescription(creationForm.description.get());
        if (!valid) {
            binding.description.setError("Description is invalid");
        }
        return valid;
    }
}
