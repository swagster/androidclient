package com.cloo.android.main.service.business.follow;

import android.support.annotation.NonNull;

import com.cloo.android.main.function.FinishCallback;
import com.cloo.android.main.function.ReceiveCallback;

public interface FollowService {
    /**
     * Starts an async operation.
     * Marks the user with the userid follower as a follower of the user with the userid followedUser, or, if follower already follows followedUser, unfollows him.
     *
     * @param follower     Userid of the user that wants to follow / unfollow followedUser.
     * @param followedUser Userid of the user that will be followed / unfollowed.
     * @param onFinish     Executed when the operation finishs. Passes true, if the operation was successful.
     */
    void follow(@NonNull final String follower, @NonNull final String followedUser, @NonNull final FinishCallback onFinish);

    void isFollower(String follower, String followedUser, ReceiveCallback<Boolean> isFollower);
}
