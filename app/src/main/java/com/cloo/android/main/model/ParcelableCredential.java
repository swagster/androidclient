package com.cloo.android.main.model;

import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.firebase.auth.AuthCredential;

public interface ParcelableCredential extends Parcelable
{
    @NonNull
    AuthCredential getCredential();
}
