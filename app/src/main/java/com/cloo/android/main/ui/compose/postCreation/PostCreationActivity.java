package com.cloo.android.main.ui.compose.postCreation;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.content.res.AppCompatResources;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.cloo.android.R;
import com.cloo.android.databinding.ActivityPostCreationBinding;
import com.cloo.android.main.injection.component.DaggerPostCreationActivityComponent;
import com.cloo.android.main.injection.component.PostCreationActivityComponent;
import com.cloo.android.main.injection.module.CameraServiceModule;
import com.cloo.android.main.injection.module.ContextModule;
import com.cloo.android.main.injection.module.DatabaseInstanceModule;
import com.cloo.android.main.model.PostOption;
import com.cloo.android.main.service.business.post.PostCreationService;
import com.cloo.android.main.service.util.network.ServerConnectionListener;
import com.cloo.android.main.ui.base.BaseActivity;
import com.cloo.android.main.util.AndroidUtils;
import com.cloo.android.main.util.AnimationUtils;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class PostCreationActivity extends BaseActivity implements ServerConnectionListener.ConnectionChangeCallback
{
    private static final String BUNDLE_TITLE = "title";
    private static final String BUNDLE_DESCRIPTION = "description";
    @Nullable PostCreationOptionSelectionFragment selectionFragment = null;
    @Nullable PostCreationFormFragment formFragment = null;
    @Nullable private PostCreationService postCreationService = null;
    @Nullable private ActivityPostCreationBinding binding = null;
    private boolean pending = false;
    @Nullable private ServerConnectionListener connectionListener = null;

    private boolean isConnected = false;
    private int index;

    @Override
    public final void onSaveInstanceState(@NonNull final Bundle savedInstanceState)
    {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString(PostCreationActivity.BUNDLE_DESCRIPTION,
                                     formFragment.creationForm.description.get());
        savedInstanceState.putString(PostCreationActivity.BUNDLE_TITLE,
                                     formFragment.creationForm.title.get());
    }

    @Override
    public final void onActivityResult(final int requestCode, final int resultCode,
                                       @Nullable final Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
            selectionFragment.set(requestCode, data.getData());
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        connectionListener.recycle();
    }

    @Override
    protected final void onCreate(@Nullable final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        final PostCreationActivityComponent dependencies = DaggerPostCreationActivityComponent.builder()
                                                                                              .contextModule(
                                                                                                      new ContextModule(
                                                                                                              this))
                                                                                              .databaseInstanceModule(
                                                                                                      new DatabaseInstanceModule(
                                                                                                              getDatabase()))
                                                                                              .cameraServiceModule(
                                                                                                      new CameraServiceModule(
                                                                                                              this))
                                                                                              .build();
        postCreationService = dependencies.getPostCreationService();
        connectionListener = dependencies.getServerConnectionListener();
        connectionListener.addCallback(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_post_creation);
        binding.postCreationContent.setInAnimation(this, android.R.anim.slide_in_left);
        binding.postCreationContent.setOutAnimation(this, android.R.anim.slide_out_right);
        selectionFragment = (PostCreationOptionSelectionFragment) getSupportFragmentManager().findFragmentById(
                R.id.selection_fragment);
        formFragment = (PostCreationFormFragment) getSupportFragmentManager().findFragmentById(
                R.id.form_fragment);
        binding.setModel(formFragment.creationForm);
        formFragment.setFab(binding.registerForward);
        setupBackButton(binding.toolbar);
    }

    private void tryCreatePost(@NonNull final View view)
    {
        if (!isConnected) {
            Toast.makeText(this, "Please connect to the internet first.", Toast.LENGTH_SHORT)
                 .show();
        }
        else if (!pending && formFragment.isValid()) {
            pending = true;
            AndroidUtils.hideKeyboard(this);
            selectionFragment.uploadOptions(success -> {
                AnimationUtils.lerp(this, binding.display, binding.loadingSpinner);
                final List<PostOption> postOptionList = new ArrayList<>();
                for (final Uri url : selectionFragment.getUrls()) {
                    postOptionList.add(new PostOption(url.toString()));
                }
                postCreationService.createPost(getAuthentication().getSession().getUserId(),
                                               formFragment.getCategory(), formFragment.getTitle(),
                                               formFragment.getDescription(), postOptionList,
                                               object -> finish(), error -> {
                            Timber.e("Error creating post " + error);
                            AnimationUtils.lerp(this, binding.loadingSpinner, binding.display);
                            pending = false;
                        });

            });
        }
    }

    @Override
    public final void onBackPressed()
    {
        if (index == 0) {
            finish();
        }
        else {
            previousDialogPage(null);
        }
    }

    @Override
    public final boolean onOptionsItemSelected(@NonNull final MenuItem item)
    {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (index == 0) {
                    finish();
                }
                else {
                    previousDialogPage(null);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void previousDialogPage(final View view)
    {
        if (index > 0) {
            index = 0;
            binding.registerSubmit.hide();
            binding.registerForward.show();
            binding.postCreationContent.showPrevious();
        }
    }

    public final void nextDialogPage(final View view)
    {
        if (index < 1 && formFragment.isValid()) {
            index = 1;
            binding.registerForward.hide();
            binding.registerSubmit.show();
            binding.postCreationContent.showNext();
        }
        else if (index == 1) {
            tryCreatePost(null);
        }
    }

    @Override
    public final void onConnectionChange(final boolean connected)
    {
        isConnected = connected;
        if (!isConnected) {
            binding.registerSubmit.setImageDrawable(
                    AppCompatResources.getDrawable(this, R.drawable.v_icon_forward_disabled));
        }
        binding.registerOfflineLabel.setVisibility(isConnected ? View.GONE : View.VISIBLE);
    }
}
