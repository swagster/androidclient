package com.cloo.android.main.service.business.camera;

import com.cloo.android.main.function.ReceiveCallback;

public interface CameraService {
    /**
     * @return True, if the device has at least on camera that could be acquired (but maybe not at the moment though)
     */
    boolean hasCamera();

    /**
     * @return The total count of camera that the device has. The number is not equivalent to the number of
     * cameras that can be acquired currently.
     */
    int getCameraCount();

    /**
     * Returns the primary front camera, if the device has an front camera.
     * If the system has no front camera the next best camera is returned.
     * Returns null if the device has no camera or can't aquire a camera.
     */
    void getFrontCamera(ReceiveCallback<ClooCamera> receiveCallback);

    /**
     * Returns the back camera. If there is no back camera it returns the next best camera or null,
     * if there is not camera.
     */
    void getBackCamera(ReceiveCallback<ClooCamera> receiveCallback);
}
