package com.cloo.android.main.service.business.register;

import com.cloo.android.main.function.ReceiveCallback;

@FunctionalInterface
public interface RegisterService {
    void register(String email, String password, ReceiveCallback<Boolean> onSuccess);
}
