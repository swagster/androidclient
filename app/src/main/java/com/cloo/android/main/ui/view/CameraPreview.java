package com.cloo.android.main.ui.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Size;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import com.cloo.android.main.service.business.camera.ClooCamera;

import static com.cloo.android.main.util.ScreenUtils.getAspectRatio;

public class CameraPreview extends SurfaceView
{
    @Nullable private final SurfaceHolder holder;
    @Nullable private ClooCamera camera = null;
    private int previewWidth = 0;
    private int previewHeight = 0;

    public CameraPreview(final Context context)
    {
        super(context);
        holder = null;
    }

    public CameraPreview(final Context context, final AttributeSet attrs)
    {
        super(context, attrs);
        holder = null;
    }

    public CameraPreview(final Context context, final AttributeSet attrs, final int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        holder = null;
    }

    public CameraPreview(final Context context, final ClooCamera camera)
    {
        super(context);
        this.camera = camera;
        holder = getHolder();
        final SurfaceHolderCallback callback = new SurfaceHolderCallback();
        holder.addCallback(callback);
    }

    @Nullable
    public final ClooCamera getCamera()
    {
        return camera;
    }

    public final void setCamera(@Nullable final ClooCamera camera)
    {
        this.camera = camera;
        if (camera == null) { return; }
        final Size size = camera.getFullscreenPreviewSize(
                getResources().getDisplayMetrics().widthPixels,
                getResources().getDisplayMetrics().heightPixels);
        previewHeight = size.getHeight();
        previewWidth = size.getWidth();
        camera.setPreviewSize(previewWidth, previewHeight);
        camera.setPreviewDisplay(holder);
        camera.startPreview();
    }

    @Override
    protected final void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec)
    {
        final int width = View.resolveSize(getResources().getDisplayMetrics().widthPixels,
                                           widthMeasureSpec);
        final int height = View.resolveSize(getResources().getDisplayMetrics().heightPixels,
                                            heightMeasureSpec);

        final Size size = camera.getFullscreenPreviewSize(width, height);
        previewHeight = size.getHeight();
        previewWidth = size.getWidth();
        final float ratio = getAspectRatio(previewWidth, previewHeight);
        setMeasuredDimension((int) (height / ratio), height);
    }

    private class SurfaceHolderCallback implements android.view.SurfaceHolder.Callback
    {

        @Override
        public final void surfaceCreated(final SurfaceHolder holder)
        {
            camera.setPreviewDisplay(holder);
            camera.startPreview();
        }

        @Override
        public void surfaceDestroyed(final SurfaceHolder holder)
        {
        }

        @Override
        public final void surfaceChanged(@NonNull final SurfaceHolder holder, final int format,
                                         final int w, final int h)
        {
            if (holder.getSurface() != null) {
                camera.stopPreview();
                camera.setPreviewSize(previewWidth, previewHeight);
                camera.setPreviewDisplay(holder);
                camera.startPreview();
            }
        }
    }
}
