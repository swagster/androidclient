package com.cloo.android;

import com.cloo.android.main.util.MathUtils;

import org.junit.Test;

import static org.junit.Assert.assertSame;

public class MathUtilsTest
{

    @Test
    public void testClamp()
    {
        // val < min < max
        assertSame(0, MathUtils.clamp(-5, 0, 5));
        assertSame(0, MathUtils.clamp(-5.f, 0.f, 5.f));

        // min < max < val
        assertSame(-3, MathUtils.clamp(-5, -8, -3));
        assertSame(-3, MathUtils.clamp(-5.f, -8.f, -3.f));

        // min < max < val
        assertSame(0, MathUtils.clamp(20, -4, 0));
        assertSame(0, MathUtils.clamp(20.f, -4.f, 0.f));

        // min < max < val
        assertSame(-5, MathUtils.clamp(20, -8, -5));
        assertSame(-5, MathUtils.clamp(20.f, -8.f, -5.f));

        // min < val < max
        assertSame(20, MathUtils.clamp(20, -8, 25));
        assertSame(20, MathUtils.clamp(20.f, -8.f, 25.f));
    }

    @Test
    public void testClampZero()
    {
        assertSame(MathUtils.clampZero(-20), 0);
        assertSame(MathUtils.clampZero(5), 5);
        assertSame(MathUtils.clampZero(-20L), 0);
        assertSame(MathUtils.clampZero(5L), 5);
    }
}
