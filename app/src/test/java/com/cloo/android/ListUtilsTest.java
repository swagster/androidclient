package com.cloo.android;

import com.cloo.android.main.util.ListUtils;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ListUtilsTest
{
    @Test
    public final void testIsValidIndex()
    {
        // Empty list have no valid indices
        assertFalse(ListUtils.isValidIndex(0, new ArrayList(0)));
        // Negativ indices are out of bound
        assertFalse(ListUtils.isValidIndex(-1, new ArrayList(0)));

        final Collection<Object> list = new ArrayList<>(16);
        list.add(new Object());

        // Indices greater than size of the list are out of bound
        assertFalse(ListUtils.isValidIndex(1, list));
        assertTrue(ListUtils.isValidIndex(0, list));
    }
}
