package com.cloo.android;

import com.cloo.android.main.service.util.validation.CredentialValidation;

import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class LoginValidationTest
{
    @Test
    public final void testEmailValidation()
    {
        // Empty strings are not valid emails
        assertFalse(CredentialValidation.isEmailValid(""));
        // Random mail is allowed
        assertTrue(CredentialValidation.isEmailValid("te4351st@test.test"));
        // At symbol is not allowed alone
        assertFalse(CredentialValidation.isEmailValid("@"));
        // Email need prefix
        assertFalse(CredentialValidation.isEmailValid("@tets.com"));
        // Email need sufix with provider
        assertFalse(CredentialValidation.isEmailValid("test@test"));
        // Email needs prefix and suffix
        assertFalse(CredentialValidation.isEmailValid("@test"));
        // It can only contain one @ symbol.
        assertFalse(CredentialValidation.isEmailValid("test@t@st.test"));
    }

    @Test
    public final void testPasswordValidation()
    {
        assertTrue(CredentialValidation.isPasswordValid("1jjf9sugi03jksef"));
        assertTrue(!CredentialValidation.isPasswordValid("a"));
    }
}
