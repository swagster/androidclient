package com.cloo.android;

import android.widget.TextView;

import com.cloo.android.main.util.AndroidUtils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AndroidUtilsTest {
    private final String test = "TEST";

    @Test
    public final void testGetStringOrEmptyOnNull() {
        assertTrue(AndroidUtils.getStringOrEmptyOnNull(null).isEmpty());
        assertTrue(AndroidUtils.getStringOrEmptyOnNull(test).equals(test));
    }

    @Test
    public final void testEmptyTextView() {
        final TextView textView = Mockito.mock(TextView.class);
        when(textView.getText()).thenReturn(test);
        assertTrue(!AndroidUtils.isEmpty(textView));
        when(textView.getText()).thenReturn("");
        assertTrue(AndroidUtils.isEmpty(textView));
        when(textView.getText()).thenReturn(null);
        assertTrue(AndroidUtils.isEmpty(textView));
    }

    @Test
    public final void testGetStringFromTextView() {
        final TextView textView = Mockito.mock(TextView.class);
        when(textView.getText()).thenReturn(test);
        assertTrue(test.equals(AndroidUtils.getString(textView)));
        when(textView.getText()).thenReturn(null);
        assertTrue(AndroidUtils.getString(textView).isEmpty());
    }

}
