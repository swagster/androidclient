package com.cloo.android;

import android.support.annotation.NonNull;

import com.cloo.android.main.util.Assert;

import org.junit.Rule;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertTrue;

public class AssertTest
{
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    public final void testAssertionSuccess() {
        AssertionError e = null;
        try {
            Assert.check(true, "");
        } catch (@NonNull final AssertionError error) {
            e = error;
        }
        assertTrue(e == null);
    }

    public final void testAssertionFail() {
        exception.expect(AssertionError.class);
        Assert.check(false, "This should trigger");
    }
}
