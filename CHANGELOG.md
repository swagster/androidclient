# CHANGELOG #

Log of the changes between in each Version of the Cloo Android Client App.

# Version Codes and Names
## major.minor.patch
### major
Major release. 1 is the first appstore release after the beta ends. Update will be required.

### minor
Scheduled release to add more features. (Planed about 1-2 times per month). No required updates.

### patch
Frequent release that improve performance and fix bugs. Update can be required.

# Log
## 0.0.25, released 07.03.17
- Added missing user profile placeholder images (UserDetails, PostDetails, UserActivities)
- Fixed camera crash issue
- Display post creation timestamp and author profile image correctly

## 0.0.24, released 05.03.17
- Fixed multiple bugs in activity list
- Clicking on an activity in the list opens the corresponding post
- Added locale selection settings
- Added german localization
- Fixed crash issues in Password Recovery and registration

## 0.0.23, released 04.03.17
- Items that are added to a DatabaseList can be auto-added or added on request by the user
- The vote count directs to the vote tab of the PostDetails
- Fixed the funky reloading and blinking behaviour of profile images in post lists
- Percent of post options add up to 100
- Deletion of a comment removes the comment from the comment list immediately
- Reduced shuffle-look when new items are loaded in a post list

## 0.0.22, released 28.02.17
- Added missing toolbars
- Fixed some glitches on the profile edit activity
- Users can only be found by their current username
- The timeline is now filled when you follow a user
- Comments can be removed
- Facebook Login works again
- Reporting comments and posts is possible
- The vote count is displayed on post details
- Activities are created if you have a new follower, got a comment or a vote on a post
- You're not displayed as your own follower
- Added a settings activity through which the logout is possible
- Fixed a issue regarding registration, that crashed the appstore
- Improved the light adjustment of the embedded camera
- Post options can be reselected
- You are directed to the user details if you click on the profile image of the author in the post details
- Time label handle plurals

## 0.0.21, released 24.02.17
- Fixed crash, caused by clicking on a zoomed image
- Fixed calculation of percent for vote options
- Changed design of UserDetails
- Fixed menu for user profiles
- Fixed ActionBar of AboutActivity
- Added placeholder for empty lists
- Fixed remaining registration bug

## 0.0.20, released 17.02.17 
- Fixed profile image change
- List auto update with new elements
- ProfileEditActivity is closable through the ActionBar
- Post option images can be zoomed on through a click and saved to disk
- The creation timestamp of posts is visualized
- Unused options that are more than the 2 needed options don't block post creation and are getting removed, when the post is created
- The email can now be edited in the login form after it was confirmed
- The created post activity is functioning
- The profile image of the author is now displayed in post details

## 0.0.19, released 10.02.17
- Added current version to About 
- Fixed many major crashing issues and bugs, including:
- Fixed post creation
- Fixed registration

## 0.0.18, released 03.02.17
- Fixed alignment and layout of post-options.
- Edited gestures: double-click to vote, single click to open post details on post options.
- Improved DatabaseList performance. While scrolling down in a DatabaseList only new items are queried.
- Long comments are shortened to limit the height of a comment.
- Clicking on a comment opens a detail dialog with the option to report or remove the comment.
- Users can get back to the Main Activity through the menu of a Post Details Activity.
- Fixed the displayed height of comments. Characters aren't cut off anymore.
- Fixed an issue that caused crashs while trying to open the crash report dialog.
- EditText fields were changed. The remaining character count is now displayed and the design is Material Design compliant.
- Posts can be deleted and a list of all deleted posts can be opened through the user Activity.
- Reset-Password and Login Activity got an Material Design Overhaul. Login Activity has still ways to go.
- The displayed percent on all options of a post should now add up to exact 100 always.

## 0.0.17, released 26.01.17
- began changelog 