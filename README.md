# README #

This project contains the Cloo android client.

* [Changelog](CHANGELOG.md)

## Branches ##

* Master
* Dev

### Master ###

* contains current app store version
* compiled, tested, linted

### Dev ###

* latest commit
* maybe broken

## Contribution Guidelines ##

* Standard Java-Code-Conventions
* Android Performance Documentation is first Reference for Performance Questions
* Material Design Style-Guidelines
* Obey the linting rules that can be found in the project